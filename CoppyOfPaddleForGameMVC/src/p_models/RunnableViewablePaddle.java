package p_models;


import p_abstractions.PaddleViewSpecification;

/**
 * <b> Implements</b>  {@link Runnable}<br><br>
 * <b> Extends</b>  {@link Paddle} with link back to the view and a run method<br><br>
 * 
 * 
 * For teaching MVC design pattern.
 * 
 * @version 1.0.0
 * @author J Paul Gibson
 */
public class RunnableViewablePaddle extends Paddle implements Runnable{
	
	/**
	 * A 1/10th second delay between movements of Paddle
	 */
	public static final int DELAY = 100;
	
	/**
	 * The view to be informed that the state of the Paddle has changed
	 */
	private PaddleViewSpecification paddleView;
	

	/**
	 * 
	 * @param paddleView2 the current view which responds to state changes
	 */
	public void setView(PaddleViewSpecification paddleView2){
		paddleView = paddleView2;
	}
	
	/**
	 * Every 10th of second:
	 * <ul>
	 * <li> update the paddle position</li>
	 * <li> inform the view (if it has been initialised) of the update </li>
	 * </ul>
	 */
	public void run(){
		
		do{
	try {
		Thread.sleep(DELAY);
	} catch (InterruptedException e) {e.printStackTrace();}
	
	updatePosition();
	if (paddleView !=null) paddleView.updateView();
	}
	while (true);
	}
}

