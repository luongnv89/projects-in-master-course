package p_tests;

import p_models.Paddle;
import p_views.PaddleView;

/**
 * <b> Tests</b>  {@link PaddleView}<br>
 * 
 * Should print out paddle in default position at bottom left of screen.
 * 
 * @version 1.0.0
 * @author J Paul Gibson
 */
public class Test_PaddleView {
	
	public static void main(String args[]) {
		
		Paddle paddleModel = new Paddle(0);
		PaddleView paddleView = new PaddleView(paddleModel);
		paddleView.updateView();
		
	}
	
}
