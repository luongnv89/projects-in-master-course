package p_views;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;

import p_abstractions.PaddleSpecification;
import p_abstractions.PaddleViewSpecification;
import p_models.Paddle;

/**
 * 
 * <b> Implements </b> {@link PaddleViewSpecification}<br><br>
 * 
 * For teaching MVC design pattern.
 * 
 * @version 1.0.0
 * @author J Paul Gibson
 */
public class PaddleView implements PaddleViewSpecification{
	
	/**
	 * The frame which contains the canvas in which the {@link Paddle} view is to be painted and which generates the
	 * events that need to be handled by the {@link p_controllers.PaddleController}
	 */
	private  JFrame frame;
	
	/**
	 * The canvas in which the view is painted
	 */
	private PaddleViewCanvas canvas;
	
	/**
	 * @param rvPaddle is the {@link Paddle} model which is to be associated with the view
	 */
	public PaddleView(PaddleSpecification rvPaddle){
		
		frame = new JFrame("Paddle MVC");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(VIEW_WIDTH+(BORDER/2),VIEW_HEIGHT+(BORDER/2));
		frame.setVisible(true);
		frame.setResizable(false);
		canvas = new PaddleViewCanvas(rvPaddle, VIEW_WIDTH, VIEW_HEIGHT);
		
		frame.getContentPane().add(canvas, BorderLayout.CENTER );
	}
	
	public JFrame getFrame(){return frame;}
	
	public void updateView(){canvas.repaint();}
	
}

/**
 * Local canvas class for inside of {@link PaddleView}
 * frame responsible for graphical representation of the {@link Paddle} model
 * 
 * @version 1.0.0
 * @author J Paul Gibson
 *
 */
 class PaddleViewCanvas extends Canvas{

	private final Color BACKGROUND_COLOUR = Color.blue;
	private final Color PADDLE_COLOUR = Color.yellow;
	private final int PADDLE_HEIGHT = 8;
	private final int PADDLE_WIDTH;
	private final int CANVAS_WIDTH;
	private final int CANVAS_HEIGHT;
	private final int PADDLE_INCREMENT;
	
	
	private static final long serialVersionUID = 1L;
	
	private PaddleSpecification paddleModel;
	
	public PaddleViewCanvas (PaddleSpecification rvPaddle, int width, int height){
		
		super();
		paddleModel = rvPaddle;
		CANVAS_WIDTH = width;
		CANVAS_HEIGHT = height;
		setSize(CANVAS_WIDTH, CANVAS_HEIGHT);
		setBackground(BACKGROUND_COLOUR);
		PADDLE_INCREMENT = CANVAS_WIDTH/(Paddle.MAXIMUM_position - Paddle.MINIMUM_position);
		PADDLE_WIDTH= PADDLE_INCREMENT;
	}
	


	public void update(Graphics g){paint(g);}
	
	public void paint(Graphics g){
		
		g.setColor(BACKGROUND_COLOUR);
		g.fillRect((paddleModel.get_position()-1)*PADDLE_INCREMENT,
				    CANVAS_HEIGHT-36,
				    PADDLE_WIDTH*3,
				    PADDLE_HEIGHT);
		
		g.setColor(PADDLE_COLOUR);
		g.fillRect(paddleModel.get_position()*PADDLE_INCREMENT,CANVAS_HEIGHT-36,PADDLE_WIDTH,PADDLE_HEIGHT); 
 	}
	
	
}// ENDCLASS PaddleViewCanvas


