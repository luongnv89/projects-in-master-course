package p_controllers;

import java.awt.event.KeyEvent;

import p_abstractions.PaddleControllerAbstraction;
import p_abstractions.PaddleSpecification;

/**
 * <b> Extends </b> {@link PaddleControllerAbstraction}<br><br>
 * A simple paddle controller for use in a video game.<br>
 * Typing a character on the keyboard changes direction of movement of the Paddle. <br>
 * For teaching MVC design pattern.
 * 
 * @version 1.0.0
 * @author J Paul Gibson
 */
public class PaddleController extends PaddleControllerAbstraction{
	
	/**
	 * The model being controlled by the controller
	 */
	PaddleSpecification paddle;
	
	/**
	 * @param rvPaddle is the model to be controlled by the controller
	 */
	public PaddleController(PaddleSpecification rvPaddle){
		
		this.paddle = rvPaddle;
	}

   /**
    * Change direction when a key is typed
    */
   public void keyTyped(KeyEvent e){
	   
	  paddle.changeDirection();
    }
   
}
