package p_applications;


import p_models.PaddleMVC;

/**
 * Instantiates {@link PaddleMVC} and starts its execution <br>
 * For teaching MVC design pattern.
 * 
 * @version 1.0.0
 * @author J Paul Gibson
 */
public class PaddleMVC_Application {
	
	public static void main(String[] args){
		
		PaddleMVC application = new PaddleMVC();
		application.startgame();
		
	}

}
