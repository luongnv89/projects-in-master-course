package p_abstractions;

import javax.swing.JFrame;

/**
 * Specification of a simple paddle view for use in a video game:
 * <ul>
 * <li> View is 640*640 square with a 20 pixel border around it </li>
 * </ul>
 * 
 * For teaching MVC design pattern.
 * 
 * @version 1.0.0
 * @author J Paul Gibson
 */
public interface PaddleViewSpecification {
	
	 final int VIEW_WIDTH = 640;
	 final int VIEW_HEIGHT = 640;
	 final int BORDER = 20;
	
	/**
	 * @return the frame which contains the canvas in which the view is to be painted and which generates the
	 * events that need to be handled by the controller
	 */
	public JFrame getFrame();
	
	/**
	 * update the canvas on which the view is painted
	 */
	public void updateView();

}
