package specifications;

import java.util.ArrayList;

import models.Move;
import tools.HasInvariant;

/**
 * 
 * @author J Paul Gibson
 * @version 1
 */
public interface JugsSpecification extends HasInvariant {

	public int numberOfJugs();

	public void initialise(int jugToFill) throws IllegalArgumentException;

	public int amountInJug(int jugToMeasure) throws IllegalArgumentException;

	public int sizeOfJug(int jugToMeasure) throws IllegalArgumentException;

	public void moveBetweenJugs(int from, int to)
			throws IllegalArgumentException;

	public boolean measures(int quantity) throws IllegalArgumentException;

	// Add more
	public boolean solve(int q) throws IllegalArgumentException;

	public String findSolutionA(int q) throws IllegalArgumentException;

	public ArrayList<Move> findSolutionB(int q) throws IllegalArgumentException;

	public String solveA(int q) throws IllegalArgumentException;

	public ArrayList<Move> solveB(int q) throws IllegalArgumentException;

	public void doSequences(ArrayList<Move> sequences)
			throws IllegalArgumentException;

	public boolean thereIsASolution(int q) throws IllegalArgumentException;

}
