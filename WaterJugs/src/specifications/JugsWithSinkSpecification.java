package specifications;

public interface JugsWithSinkSpecification extends JugsSpecification {
	
	public void emptyToSink(int jug) throws IllegalArgumentException;

}
