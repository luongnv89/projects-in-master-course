package tests;

import org.junit.Assert;
import org.junit.Test;

import specifications.JugsWithHoseSpecification;

public abstract class JUnit_JugsWithHoseSpecification extends JUnit_JugsSpecification{

	
	@Test
	public  void testfillfromHose(){
		
		JugsWithHoseSpecification jugsWithHose = (JugsWithHoseSpecification) jugsUnderTest;
		
		jugsWithHose.fillfromHose(0);
		Assert.assertEquals(jugsWithHose.sizeOfJug(0), jugsWithHose.amountInJug(0));
		Assert.assertEquals(0, jugsWithHose.amountInJug(1));
		Assert.assertEquals(0, jugsWithHose.amountInJug(2));
		Assert.assertTrue(jugsWithHose.invariant());
		
		jugsWithHose.fillfromHose(1);
		Assert.assertEquals(jugsWithHose.sizeOfJug(0), jugsWithHose.amountInJug(0));
		Assert.assertEquals(jugsWithHose.sizeOfJug(1), jugsWithHose.amountInJug(1));
		Assert.assertEquals(0, jugsWithHose.amountInJug(2));
		Assert.assertTrue(jugsWithHose.invariant());
		
		jugsWithHose.fillfromHose(2);
		Assert.assertEquals(jugsWithHose.sizeOfJug(0), jugsWithHose.amountInJug(0));
		Assert.assertEquals(jugsWithHose.sizeOfJug(1), jugsWithHose.amountInJug(1));
		Assert.assertEquals(jugsWithHose.sizeOfJug(2), jugsWithHose.amountInJug(2));
		Assert.assertTrue(jugsWithHose.invariant());
		
		
		
	}
}
