package models;

import java.util.ArrayList;

import specifications.JugsWithSinkSpecification;
import utils.MessageConsole;

public class JugsWithSink extends Jugs implements JugsWithSinkSpecification {

	public JugsWithSink(int[] jugSizes) {
		super(jugSizes);
		MessageConsole.setDEBUG(false);
		MessageConsole.setINFOR(false);
	}

	public void emptyToSink(int jugToEmpty) throws IllegalArgumentException {

		if (jugToEmpty < 0 || jugToEmpty >= NUMBER_OF_JUGS)
			throw (new IllegalArgumentException("The jug indice is not valid"));

		jugs[jugToEmpty] = 0;
	}

	public String toString() {

		String str = super.toString();
		str = str + "\n With Sink";
		return str;
	}

	// Add more
	@Override
	public boolean solve(int target) throws IllegalArgumentException {

		if (super.solve(target))
			return true;
		else {
			for (int i = 0; i < numberOfJugs(); i++) {
				if (amountInJug(i) > 0) {
					int[] initialState = arrayCopy(jugs);
					emptyToSink(i);
					if (!listStates.contains(stateToString())) {
						listStates.add(stateToString());
						Move move = new Move(i, -1);
						if (solve(target)) {
							listMoves.add(move);
							return true;
						}
					}
					jugs = arrayCopy(initialState);

				}
			}
		}
		return false;
	}

	@Override
	public void doSequences(ArrayList<Move> sequences)
			throws IllegalArgumentException {
		MessageConsole.setINFOR(true);
		MessageConsole.setINFOR(true);
		MessageConsole.inforMessage(problemInfro());
		for (int i = 0; i < sequences.size(); i++) {
			Move move = sequences.get(i);
			MessageConsole.inforMessage(move.toString());
			if (move.to == -1) {
				emptyToSink(move.from);
			} else {
				moveBetweenJugs(move.from, move.to);
			}
			MessageConsole.inforMessage(stateToString());
		}
		MessageConsole.setINFOR(false);
	}

	@Override
	public boolean thereIsASolution(int q) throws IllegalArgumentException {
		if (getTotalOfJugs() < q) {
			MessageConsole
					.debugMessage("The total water is less than the value of target");
		}
		return super.invariant();
	}

	/**
	 * Get the total water of all jars in solver
	 * @return
	 */
	protected int getTotalOfJugs() {
		int total = 0;
		for (int i = 0; i < numberOfJugs(); i++) {
			total += amountInJug(i);
		}
		return total;
	}
}
