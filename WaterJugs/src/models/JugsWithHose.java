package models;

import java.util.ArrayList;

import specifications.JugsWithHoseSpecification;
import utils.MessageConsole;

public class JugsWithHose extends Jugs implements JugsWithHoseSpecification {

	public JugsWithHose(int[] jugSizes) {
		super(jugSizes);
	}

	public void fillfromHose(int jugToFill) throws IllegalArgumentException {

		if (jugToFill < 0 || jugToFill >= NUMBER_OF_JUGS)
			throw (new IllegalArgumentException("The jug indice is not valid"));

		jugs[jugToFill] = jugSizes[jugToFill];
	}

	public String toString() {

		String str = super.toString();
		str = str + "\n With Hose";
		return str;
	}

	// Add more
	public boolean solve(int target) throws IllegalArgumentException {

		if (super.solve(target))
			return true;
		else {
			for (int i = 0; i < numberOfJugs(); i++) {
				if (amountInJug(i) < sizeOfJug(i)) {
					int[] initialState = arrayCopy(jugs);
					fillfromHose(i);
					if (!listStates.contains(stateToString())) {
						listStates.add(stateToString());
						Move move = new Move(-1, i);
						if (solve(target)) {
							listMoves.add(move);
							return true;
						}
					}
					jugs = arrayCopy(initialState);

				}
			}
		}
		return false;
	}

	public void doSequences(ArrayList<Move> sequences)
			throws IllegalArgumentException {
		MessageConsole.setINFOR(true);
		MessageConsole.inforMessage(problemInfro());
		MessageConsole.inforMessage(stateToString());
		for (int i = 0; i < sequences.size(); i++) {
			Move move = sequences.get(i);
			MessageConsole.inforMessage(move.toString());
			if (move.from == -1) {
				fillfromHose(move.to);
			} else {
				moveBetweenJugs(move.from, move.to);
			}
			MessageConsole.inforMessage(stateToString());
		}
		MessageConsole.setINFOR(false);

	}

}
