package applications;

import models.JugsWithHose;

public class JugsWithHoseSimulation {

final static int NUMBER_OF_MOVES = 10;
	
	public static void main(String[] args) {
		
		int randomTo;
		int randomFrom;
		
		int [] sizes = new int [] {8, 5, 3};
		JugsWithHose jugs = new JugsWithHose (sizes);
		System.out.println(jugs);
		
		randomTo = (int) (Math.random()*jugs.numberOfJugs());
		jugs.fillfromHose(randomTo);
		System.out.println("jugs.fillfromHose("+randomTo+")");
		System.out.println(jugs);
		
		
		for (int move =0; move < NUMBER_OF_MOVES; move++){
			
		randomTo = (int) (Math.random()*jugs.numberOfJugs());
		randomFrom = (int) (Math.random()*jugs.numberOfJugs());
		
		jugs.moveBetweenJugs(randomFrom, randomTo);
		System.out.println("jugs.moveBetweenJugs("+randomFrom+", "+randomTo+")");
		System.out.println(jugs);

		}
	}

}
