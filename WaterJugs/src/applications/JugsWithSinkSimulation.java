package applications;

import models.JugsWithSink;

public class JugsWithSinkSimulation {

	final static int NUMBER_OF_MOVES = 10;
		
		public static void main(String[] args) {
			
			int randomToEmpty;
			int randomTo;
			int randomFrom;
			
			int [] sizes = new int [] {8, 5, 3};
			JugsWithSink jugs = new JugsWithSink (sizes);
			
			
			randomToEmpty = (int) (Math.random()*jugs.numberOfJugs());
			jugs.initialise(randomToEmpty);
			System.out.println(jugs);
			
			System.out.println("jugs.emptyToSink("+randomToEmpty+")");
			jugs.emptyToSink(randomToEmpty);
			System.out.println(jugs);
			
			
			randomTo = (int) (Math.random()*jugs.numberOfJugs());
            jugs.initialise(randomTo);
			System.out.println("jugs.initialise("+randomTo+")");
			System.out.println(jugs);
			
			
			for (int move =0; move < NUMBER_OF_MOVES; move++){
				
			randomTo = (int) (Math.random()*jugs.numberOfJugs());
			randomFrom = (int) (Math.random()*jugs.numberOfJugs());
			
			jugs.moveBetweenJugs(randomFrom, randomTo);
			System.out.println("jugs.moveBetweenJugs("+randomFrom+", "+randomTo+")");
			System.out.println(jugs);

			}
		}

	}

