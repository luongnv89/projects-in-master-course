package tools;

/**
 * <TT>InvariantBroken</TT> is raised when the invariant of a class is not true in
 * the current state of the instance. This means that the instance is in an <em> unsafe </em> state
 * 
 * @author J Paul Gibson
 * @version  1
 */
public class InvariantBroken extends RuntimeException {

private static final long serialVersionUID = 1L;

/**
   * Constructor of the exception InvariantBroken.
   *   @param message Error message explaining the cause of the broken invariant
   */
  public InvariantBroken(String message) {
    super(message);
  }
}

