package tools;

/**
 * Interface for any class whose safe states can be specified by an {@link InvariantBroken#invariant} predicate
 * 
 * @author J Paul Gibson
 * @version  1
 */
public interface HasInvariant {

	/**
	 * 
	 * @return whether the object/instance is in a safe state
	 */
	public boolean invariant();
}
