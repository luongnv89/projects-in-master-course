package tests;

import java.util.LinkedList;
import java.util.List;

import tspga.models.Graph;
//import models.Dice;

import models.ListReflector;

/**
 * 
 * @author J Paul Gibson
 * 
 * Template test code for reflection problem (OOD)<br>
 * 
 * @todo <em> The students </em> are to improve the test to show that the method
 * {@link ListReflector#lowestCommonSuperclass} is currently not working correctly<br>
 * 
 * They are then to fix the method {@link ListReflector#lowestCommonSuperclass} and show that
 * their fix is correct (by executing the updated test)
 *
 */
public class TestListReflector {
	
	public static void main(String[] args) {
		
	List <Object> listOfObjects = new LinkedList <Object>();
	
	listOfObjects.add(1);
	listOfObjects.add(new Character ('a'));
	listOfObjects.add("Hello");
	listOfObjects.add(new Graph(10));
	
	ListReflector.reflect(listOfObjects);
	
	Class<? extends Object> lowest = ListReflector.lowestCommonSuperclass(listOfObjects);
	
	System.out.println("ListReflector.lowestCommonSuperclass(listOfObjects) = " + lowest );

}
}
