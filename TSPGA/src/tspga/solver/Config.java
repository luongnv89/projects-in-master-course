package tspga.solver;

import java.util.Random;

/**
 * Configuration of the GA
 * 
 * @author luongnv89
 * 
 */
public class Config {

	public static Random ran = new Random();

	public static int POPULATION_SIZE = 300;

	public static int MAX_DISTANCE = 100;// The max distance when generate the graph randomly

	public static int UNLIMITED_DISTANCE = 1000000;// The weight between two node, that means no edge between two nodes

	public static int NUMBER_UNLIMITED_EDGE = 2;

	public static int N_MUTATION = 20;// (20% of population_size) chromosomes will be generate 

	public static int N_CROSSOVER = 50;// (50% of population_size) chromosomes will be generate

	public static int MAX_GENERATION = 600; // The max generation 

	public static boolean SHOW_EACH_GENERATION = false;

	public static int MAX_INTERATION = 300;// The max generation without the better chromosome is generated

	public static boolean STOP_MAXGENERATION = true;// Stop condition by max generation
	public static boolean STOP_MAXINTERATION = true;// Stop condition by max interation
	public static boolean STOP_EXPECTVALUE = false; // Stop condition by expect value

	public static int EXPECTED_VALUE = 2085;
	public static int RANGE_EXPECTED_VALUE = 10;//10%

}
