package tspga.solver;

import java.util.ArrayList;

import tspga.models.Chromosomes;
import tspga.models.Graph;
import tspga.models.Population;

/**
 * The solver class
 * 
 * @author luongnv89
 * 
 */
public class GASolver extends Config {
	/**
	 * The population
	 */
	Population p;

	/**
	 * Initial a GASolver from the size of graph
	 * 
	 * @param size
	 */
	public GASolver(int size) {
		Graph graph = new Graph(size);
		p = new Population(graph);
		p.Init();
	}

	/**
	 * initial a GASolver from a graph
	 * 
	 * @param graph
	 */
	public GASolver(Graph graph) {
		p = new Population(graph);
		p.Init();
	}

	/**
	 * Solve the problem
	 */
	public void run() {
		int nGeneration = 0;
		int numberMinCost = 0;

		boolean stopMaxGeneration = false;
		boolean stopMaxInteration = false;
		boolean stopExpectValue = false;
		boolean stopCondition = false;
		while (stopCondition == false) {
			nGeneration++;
			if (STOP_MAXGENERATION == true) {
				if (nGeneration >= MAX_GENERATION)
					stopMaxGeneration = true;
			}
			int preBest = p.getTheBest().getFitness();
			ArrayList<Chromosomes> listExtends = new ArrayList<Chromosomes>();
			listExtends.addAll(p.Mutation());
			ArrayList<Chromosomes> listCross = new ArrayList<Chromosomes>();
			listCross.addAll(p.CrossOver());
			listExtends.removeAll(listCross);
			listExtends.addAll(listCross);
			p = p.Next(listExtends);
			if (STOP_MAXINTERATION == true) {
				if (p.getTheBest().getFitness() == preBest) {
					numberMinCost++;
					if (numberMinCost >= MAX_INTERATION)
						stopMaxInteration = true;
				}
			}

			if (STOP_EXPECTVALUE == true) {
				if (p.getTheBest().getFitness() - EXPECTED_VALUE < (EXPECTED_VALUE * RANGE_EXPECTED_VALUE) / 100)
					stopExpectValue = true;
			}

			stopCondition = stopExpectValue || stopMaxGeneration
					|| stopMaxInteration;
			if (SHOW_EACH_GENERATION == true) {
				p.showChromosome();
			}

		}

		System.out.println("The result:");
		Chromosomes best = p.getTheBest();
		System.out.println("Path: ");
		best.Decode();
		System.out.println("\nCost: " + best.getFitness());
		if (!stopExpectValue || !stopMaxInteration)
			System.out.println("The number of interation: " + nGeneration);

	}
}
