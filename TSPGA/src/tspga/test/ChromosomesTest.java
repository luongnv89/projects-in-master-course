package tspga.test;

import org.junit.Test;

import tspga.models.Chromosomes;
import tspga.models.Graph;
import tspga.specific.ChromosomesSpecific;

public class ChromosomesTest{

	Chromosomes chromosomes;
	Graph graph = new Graph(15);

	/**
	 * Generate randomly a solution
	 */
	@Test
	public void test() {
		chromosomes = new Chromosomes(15);
		chromosomes.Decode();
		chromosomes.Eval(graph);
		System.out.println("\n The fitness: " + chromosomes.getFitness());
	}

	/**
	 * Mutation a chromosome
	 */
	@Test
	public void testMutation() {
		ChromosomesSpecific newChromosomes = test3();
		System.out.println("\n The fitness: " + newChromosomes.getFitness());

	}

	/**
	 * Crossover test
	 */
	@Test
	public void testCrossover() {
		ChromosomesSpecific newChromosomes = test3();
		System.out.println("\n The fitness: " + newChromosomes.getFitness());

		System.out.println("\nThe new: \n");
		ChromosomesSpecific newChromosomes2 = chromosomes
				.CrossOver(newChromosomes);
		newChromosomes2.Decode();
		newChromosomes2.Eval(graph);
		System.out.println("\n The fitness: " + newChromosomes2.getFitness());
	}

	private ChromosomesSpecific test3() {
		chromosomes = new Chromosomes(15);
		chromosomes.Decode();
		chromosomes.Eval(graph);
		System.out.println("\nThe new: \n");
		chromosomes.Eval(graph);
		System.out.println("\n The fitness: " + chromosomes.getFitness());

		ChromosomesSpecific newChromosomes = chromosomes.Mutation();
		newChromosomes.Decode();
		newChromosomes.Eval(graph);
		return newChromosomes;
	}

	/**
	 * mution operator number 2 test
	 */
	@Test
	public void testMutation2() {
		graph.show();
		chromosomes = new Chromosomes(15);
		chromosomes.Decode();
		chromosomes.Eval(graph);
		System.out.println("\nThe new: \n");
		chromosomes.Eval(graph);
		System.out.println("\n The fitness: " + chromosomes.getFitness());

		ChromosomesSpecific newChromosomes = chromosomes.Mutation2(graph);
		newChromosomes.Decode();
		newChromosomes.Eval(graph);
		System.out.println("\n The fitness: " + newChromosomes.getFitness());

	}

}
