package tspga.specific;

import java.util.ArrayList;
import java.util.Random;

import tspga.models.Graph;
import tspga.solver.Config;

/**
 * The Chromosome specific class, represents a solution of problem
 * @author luongnv89
 *
 */
public abstract class ChromosomesSpecific {

	/**
	 * The list ordered node represent the sequence of the cities will be traveled 
	 */
	public ArrayList<Integer> listInteger = new ArrayList<Integer>();
	/**
	 * The fitness value of this solution
	 */
	int fitness = Config.UNLIMITED_DISTANCE;
	
	/**
	 * Mutation a chromosome
	 * @return a new chromosome
	 */
	public abstract ChromosomesSpecific Mutation();
	/**Crossover this chromosome with other chromosome
	 * @param other The other chromosome
	 * @return a new chromosome
	 */
	public abstract ChromosomesSpecific CrossOver(ChromosomesSpecific other);
	
	/**
	 * Initial a new chromosome randomly with the size
	 * @param size
	 *            the size of ChromosomesSpecific
	 */
	public ChromosomesSpecific(int size) {
		ArrayList<Integer> listOrder = new ArrayList<Integer>();
		for (int i = 0; i < size; i++) {
			listOrder.add(i);
		}

		while (!listOrder.isEmpty()) {
			Random ran = new Random();
			int index = ran.nextInt(listOrder.size());
			listInteger.add(listOrder.get(index));
			listOrder.remove(index);
		}
	}

	/**
	 * Initial a new Chromosome with the sequence of cities will be traveled
	 * @param list List of cities
	 */
	public ChromosomesSpecific(ArrayList<Integer> list) {
		listInteger.addAll(list);
	}

	public ChromosomesSpecific(ChromosomesSpecific other) {
		this.listInteger.addAll(other.listInteger);
		this.fitness = other.getFitness();
	}
	
	/**
	 * Show the list ordered node will be traveled
	 */
	public void Decode() {
		for (int i = 0; i < listInteger.size(); i++) {
			System.out.print(" " + listInteger.get(i));
		}
		System.out.println(" " + listInteger.get(0));
	}

	/**
	 * Get the fitness of this solution
	 * @return The fitness value
	 */
	public int getFitness() {
		return fitness;
	}

	/**Set the fitness value for this solution
	 * @param fitness the fitness value
	 */
	public void setFitness(int fitness) {
		this.fitness = fitness;
	}

	/**
	 * Calculate the fitness value of this solution
	 * @param graph The graph of the cities
	 */
	public void Eval(Graph graph) {
		int fit = 0;
		for (int i = 0; i < this.listInteger.size() - 1; i++) {
			fit += graph.getweight(this.listInteger.get(i),
					this.listInteger.get(i + 1));
		}
		fit += graph.getweight(
				this.listInteger.get(this.listInteger.size() - 1),
				this.listInteger.get(0));
		this.setFitness(fit);
	}

	/**
	 * Compare two solutions
	 * @param other The other solution
	 * @return True if the two solutions are the same
	 */
	public boolean Equal(ChromosomesSpecific other) {
		for (int i = 0; i < listInteger.size(); i++) {
			if (listInteger.get(i) != other.listInteger.get(i))
				return false;
		}
		return true;
	}

}
