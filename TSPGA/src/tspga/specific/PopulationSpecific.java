package tspga.specific;

import java.util.ArrayList;

import tspga.models.Chromosomes;
import tspga.models.Graph;
import tspga.solver.Config;

/**
 * The population store the population of solutions of the problem
 * @author luongnv89
 *
 */
public abstract class PopulationSpecific {

	/**
	 * List the solution in population
	 */
	protected ArrayList<Chromosomes> listChromosomes = new ArrayList<Chromosomes>();
	/**
	 * Graph represent cities
	 */
	protected Graph graph;

	/**
	 * Initial a new population
	 * @param grap {@link Graph}
	 */
	public PopulationSpecific(Graph grap) {
		this.graph = new Graph(grap);
	}

	/**
	 * Initial a new population with the list solution 
	 * @param graph {@link Graph}
	 * @param lisChromosomes List solution
	 */
	public PopulationSpecific(Graph graph, ArrayList<Chromosomes> lisChromosomes) {
		this.graph = new Graph(graph);
		this.listChromosomes.addAll(lisChromosomes);
	}

	/**
	 * Initial a new population
	 */
	public abstract void Init();

	/**
	 * Mutation on population
	 * @return A list chromosomes are generated from mution 
	 */
	public abstract ArrayList<Chromosomes> Mutation();

	/**
	 * Crossover on population
	 * @return A list chromosomes are generate from crossover
	 */
	public abstract ArrayList<Chromosomes> CrossOver();

	/**
	 * The next population
	 * @param listExtends The new solutions
	 * @return A new population
	 */
	public abstract PopulationSpecific Next(ArrayList<Chromosomes> listExtends);

	/**
	 * Get the best solution of the population
	 * @return The best chromosome
	 */
	public Chromosomes getTheBest() {
		int index = 0;
		int fitMin = listChromosomes.get(0).getFitness();
		for (int i = 1; i < Config.POPULATION_SIZE; i++) {
			if (fitMin > listChromosomes.get(i).getFitness()) {
				index = i;
				fitMin = listChromosomes.get(i).getFitness();
			}
		}
		return listChromosomes.get(index);
	}

	/**
	 * Show the list solutions of population
	 */
	public void showChromosome() {
		for (int i = 0; i < listChromosomes.size(); i++) {
			listChromosomes.get(i).Decode();
			System.out.println("\nCost: " + listChromosomes.get(i).getFitness()
					+ "\n");
		}
	}
}
