package tspga.specific;

/**
 * The graph specific
 * @author luongnv89
 *
 */
public abstract class GraphSpecific {
	/**
	 * The size of graph is size*size
	 */
	protected int size;
	/**
	 * The array store the states of graph
	 */
	protected int[][] weight;

	/**
	 * Check a graph is valid or not
	 * @return True if the graph is valid
	 */
	public abstract boolean valid();

	/**
	 * Set randomly the states of the board 
	 */
	public abstract void setRandom();

	/**
	 * Get the size of the graph
	 * @return The size of graph
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Initial a Graph without any weight
	 * 
	 * @param size
	 *            The size of Graph
	 */
	public GraphSpecific(int size) {
		this.size = size;
		weight = new int[size][size];
		this.setRandom();
	}

	/**
	 * Initial a graph from a array
	 * @param s the state array
	 * @param size the size of graph
	 */
	public GraphSpecific(int[][] s, int size) {
		this.size = size;
		weight = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				weight[i][j] = s[i][j];
			}
		}
		this.valid();
	}

	/**
	 * Create a Graph from another Graph
	 * 
	 * @param otherGraph
	 */
	public GraphSpecific(GraphSpecific otherGraph) {
		this.size = otherGraph.size;
		this.weight = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = i; j < size; j++) {
				this.weight[i][j] = otherGraph.getweight(i, j);
				this.weight[j][i] = otherGraph.getweight(j, i);
			}
		}
	}

	/**
	 * Create a Graph with the state of Graph is given by user
	 * 
	 * @param s
	 *            The state of Graph
	 * @param size
	 *            The size of Graph
	 */
	public GraphSpecific(int[] s, int size) {
		this.size = size;
		weight = new int[size][size];
		if (size != Math.sqrt(s.length)) {
			System.out.println("Invalid size!");
		} else {
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					weight[i][j] = s[i * size + j];
				}
			}
		}
		this.valid();
	}

	/**
	 * Show the current state of graph
	 */
	public void show() {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				System.out.print(" " + weight[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Get the value of cell (i,j) of graph
	 * @param i the number of row
	 * @param j the number of column
	 * @return The value of cell (i,j)
	 */
	public int getweight(int i, int j) {
		return weight[i][j];
	}

	/**
	 * Set the value of cell(i,j) of graph
	 * @param i the number of row
	 * @param j the number of column
	 * @param value the value set for cell(i,j)
	 */
	public void setweight(int i, int j, int value) {
		weight[i][j] = value;
	}

}
