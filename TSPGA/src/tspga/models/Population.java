package tspga.models;

import java.util.ArrayList;

import tspga.solver.Config;
import tspga.specific.ChromosomesSpecific;
import tspga.specific.PopulationSpecific;

public class Population extends PopulationSpecific {

	public Population(Graph grap) {
		super(grap);
	}

	public Population(Graph graph, ArrayList<Chromosomes> lisChromosomes) {
		super(graph, lisChromosomes);
	}

	public void Init() {
		for (int i = 0; i < Config.POPULATION_SIZE; i++) {
			Chromosomes newChromosomes = new Chromosomes(graph.getSize());
			while (exist(newChromosomes, listChromosomes)) {
				newChromosomes = new Chromosomes(graph.getSize());
			}
			newChromosomes.Eval(graph);
			listChromosomes.add(newChromosomes);
		}
	}

	public ArrayList<Chromosomes> Mutation() {
		ArrayList<Chromosomes> listMutation = new ArrayList<Chromosomes>();

		for (int i = 0; i < Config.POPULATION_SIZE * Config.N_MUTATION / 100; i++) {
			int index = Config.ran.nextInt(listChromosomes.size());
			Chromosomes mChromosomes = new Chromosomes(listChromosomes.get(
					index).Mutation());
			int stop = 0;
			while (exist(mChromosomes, listChromosomes) && stop < 10) {
				mChromosomes = new Chromosomes(listChromosomes.get(index)
						.Mutation2(graph));
				stop++;
			}
			mChromosomes.Eval(graph);
			listMutation.add(mChromosomes);
		}
		
		for (int i = 0; i < Config.POPULATION_SIZE * Config.N_MUTATION / 100; i++) {
			int index = Config.ran.nextInt(listChromosomes.size());
			Chromosomes mChromosomes = new Chromosomes(listChromosomes.get(
					index).Mutation());
			int stop = 0;
			while (exist(mChromosomes, listChromosomes) && stop < 10) {
				mChromosomes = new Chromosomes(listChromosomes.get(index)
						.Mutation());
				stop++;
			}
			mChromosomes.Eval(graph);
			listMutation.add(mChromosomes);
		}
		
		Chromosomes mBest = new Chromosomes(getTheBest().Mutation2(graph));
		listMutation.add(mBest);
		return listMutation;
	}

	public ArrayList<Chromosomes> CrossOver() {
		ArrayList<Chromosomes> listCross = new ArrayList<Chromosomes>();

		for (int i = 0; i < Config.POPULATION_SIZE * Config.N_CROSSOVER / 100; i++) {
			int node1 = Config.ran.nextInt(listChromosomes.size() / 2);
			int node2 = listChromosomes.size() / 2
					+ Config.ran.nextInt(listChromosomes.size() / 2);

			Chromosomes cChromosomes = new Chromosomes(listChromosomes.get(
					node1).CrossOver(listChromosomes.get(node2)));
			int stop = 0;
			while (exist(cChromosomes, listChromosomes) && stop < 5) {
				stop++;
				// System.out.println("Exist!"+stop);
				cChromosomes = new Chromosomes(listChromosomes.get(node1)
						.CrossOver(listChromosomes.get(node2)));
			}
			cChromosomes.Eval(graph);
			listCross.add(cChromosomes);
		}
		return listCross;
	}

	public Population Next(ArrayList<Chromosomes> listExtends) {
		ArrayList<Chromosomes> listTotal = new ArrayList<Chromosomes>();
		listTotal.addAll(listChromosomes);

		for (int i = 0; i < listExtends.size(); i++) {
			for (int j = 0; j < listTotal.size(); j++) {
				if (listExtends.get(i).getFitness() < listTotal.get(j)
						.getFitness()) {
					{
						listTotal.set(j, listExtends.get(i));
						break;
					}
				}
			}
		}
		return new Population(graph, listTotal);
	}

	/**
	 * Check does a chromosome exist in a list chromosome 
	 * @param mChromosomes
	 * @param list
	 * @return True if exist
	 */
	public boolean exist(ChromosomesSpecific mChromosomes,
			ArrayList<Chromosomes> list) {
		for (int i = 0; i < list.size(); i++) {
			if (mChromosomes.Equal(list.get(i)))
				return true;
		}
		return false;
	}

}
