package tspga.models;

import tspga.solver.Config;
import tspga.specific.GraphSpecific;

public class Graph extends GraphSpecific {

	public Graph(int size) {
		super(size);
		this.addUnlimited();
	}

	public void setRandom() {
		for (int i = 0; i < size; i++) {
			for (int j = i; j < size; j++) {
				if (i == j)
					weight[i][j] = 0;
				else {
					weight[i][j] = Config.ran.nextInt(Config.MAX_DISTANCE);
					while (weight[i][j] == 0) {
						weight[i][j] = Config.ran.nextInt(Config.MAX_DISTANCE);
					}
				}

				weight[j][i] = weight[i][j];
			}
		}
	}

	/**
	 * Create a Graph with the state of Graph is given by user
	 * 
	 * @param s
	 *            The state of Graph
	 * @param size
	 *            The size of Graph
	 */
	public Graph(int[][] s, int size) {
		super(s, size);
	}

	/**
	 * Create a Graph from another Graph
	 * 
	 * @param otherGraph
	 */
	public Graph(GraphSpecific otherGraph) {
		super(otherGraph);
		this.addUnlimited();
	}

	/**
	 * Create a Graph with the state of Graph is given by user
	 * 
	 * @param s
	 *            The state of Graph
	 * @param size
	 *            The size of Graph
	 */
	public Graph(int[] s, int size) {
		super(s, size);
	}

	public boolean valid() {
		for (int i = 0; i < this.size; i++) {
			for (int j = i; j < this.size; j++) {
				if (i == j) {
					if (weight[i][j] != 0) {
						System.out.println("Invalid graph!");
						return false;
					}
				} else {
					if (weight[i][j] != weight[j][i]) {
						System.out.println("Invalid graph!");
						return false;
					} else {
						if (weight[i][j] < 0) {
							System.out.println("Invalid graph!");
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Add some edge with the very big value, that means no edge between two nodes
	 * The number of edge with the very big value is less than half of the number of node
	 */
	private void addUnlimited() {
		int nUnlimitedEdge = Config.ran.nextInt(this.size
				/ Config.NUMBER_UNLIMITED_EDGE);
		if (nUnlimitedEdge == 0)
			nUnlimitedEdge = 1;
		for (int i = 0; i < nUnlimitedEdge; i++) {
			int node1 = Config.ran.nextInt(this.size);
			int node2 = Config.ran.nextInt(this.size);
			while (node1 == node2) {
				node2 = Config.ran.nextInt(this.size);
			}
			this.setweight(node1, node2, Config.UNLIMITED_DISTANCE);
			this.setweight(node2, node1, Config.UNLIMITED_DISTANCE);
		}
	}
}
