package tspga.models;

import java.util.ArrayList;
import java.util.Random;

import tspga.solver.Config;
import tspga.specific.ChromosomesSpecific;

/**
 * Represent a solution of problems
 * 
 * @author luongnv89
 * 
 */
public class Chromosomes extends ChromosomesSpecific {

	public Chromosomes(int size) {
		super(size);
	}

	/**
	 * @param list
	 *            the
	 */
	public Chromosomes(ArrayList<Integer> list) {
		super(list);
	}

	public Chromosomes(ChromosomesSpecific other) {
		super(other);
	}

	public ChromosomesSpecific Mutation() {
		Random ran = new Random();
		int index1 = ran.nextInt(listInteger.size());
		int index2 = ran.nextInt(listInteger.size());
		while (index1 == index2) {
			index2 = ran.nextInt(listInteger.size());
		}

		if (index1 > index2) {
			int temp = index1;
			index1 = index2;
			index2 = temp;
		}

		ArrayList<Integer> listOrder = new ArrayList<Integer>();
		for (int i = 0; i < index1; i++) {
			listOrder.add(listInteger.get(i));
		}

		for (int i = index2; i >= index1; i--) {
			listOrder.add(listInteger.get(i));
		}

		for (int i = index2 + 1; i < listInteger.size(); i++) {
			listOrder.add(listInteger.get(i));
		}

		ChromosomesSpecific newChromosomes = new Chromosomes(listOrder);
		return newChromosomes;
	}

	public ChromosomesSpecific CrossOver(ChromosomesSpecific other) {

		ArrayList<Integer> listOrder = new ArrayList<Integer>();
		int crossPoint = Config.ran.nextInt(listInteger.size() / 2);
		if (crossPoint == 0) {
			crossPoint = listInteger.size() / 2;
		}
		listOrder.addAll(listInteger.subList(0, crossPoint));

		for (int i = 0; i < other.listInteger.size(); i++) {
			if (!listOrder.contains(other.listInteger.get(i))) {
				listOrder.add(other.listInteger.get(i));
			}
		}

		ChromosomesSpecific newChromosomes = new Chromosomes(listOrder);
		return newChromosomes;
	}

	/**
	 * Another mutation operator
	 * 
	 * @param graph
	 * @return
	 */
	public ChromosomesSpecific Mutation2(Graph graph) {
		int crossPoint = Config.ran.nextInt(listInteger.size() / 2);
		if (crossPoint == 0) {
			crossPoint = listInteger.size() / 2;
		}
		ArrayList<Integer> listNode = new ArrayList<Integer>();
		listNode.addAll(listInteger.subList(0, crossPoint));
		for (int i = 0; i < listInteger.size() - crossPoint; i++) {
			int next = nextNode(listNode, graph);
			listNode.add(next);
		}

		return new Chromosomes(listNode);
	}

	/**
	 * Find the nearest node
	 * @param listNode List node remain of graph
	 * @param graph {@link Graph}
	 * @return The id of next node
	 */
	private int nextNode(ArrayList<Integer> listNode, Graph graph) {
		ArrayList<Integer> listRemain = new ArrayList<Integer>();
		for (int i = 0; i < graph.getSize(); i++) {
			listRemain.add(i);
		}
		listRemain.removeAll(listNode);
		int next = listRemain.get(0);
		int min = graph.getweight(listNode.get(listNode.size() - 1),
				listRemain.get(0));
		for (int i = 1; i < listRemain.size(); i++) {
			if (graph.getweight(listNode.get(listNode.size() - 1),
					listRemain.get(i)) < min) {
				min = graph.getweight(listNode.get(listNode.size() - 1),
						listRemain.get(i));
				next = listRemain.get(i);
			}
		}
		return next;
	}

}
