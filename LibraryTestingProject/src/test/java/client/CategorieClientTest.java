/**
 * 
 */
package client;

import static org.junit.Assert.*;

import mediatheque.client.CategorieClient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *Test for {@link CategorieClient}
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class CategorieClientTest {

	CategorieClient defaultOK1;
	CategorieClient defaultOK2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		defaultOK1 = new CategorieClient("Category1", 20, 1.5f, 2.5f, 3.5f,
				true);
		defaultOK2 = new CategorieClient("Category2");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		defaultOK1 = null;
		defaultOK2 = null;
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#CategorieClient(java.lang.String, int, double, double, double, boolean)}.
	 */
	@Test
	public void testCategorieClientDefaultConstructor1() {
		CategorieClient cat1 = new CategorieClient("Category", 10, 1.5, 2.5,
				3.5, true);
		assertTrue(cat1.getNom().equals("Category"));
		assertTrue(cat1.getNbEmpruntMax() == 10);
		assertTrue(cat1.getCotisation() == 1.5);
		assertTrue(cat1.getCoefDuree() == 2.5);
		assertTrue(cat1.getCoefTarif() == 3.5);
		assertTrue(cat1.getCodeReducUtilise());
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#CategorieClient(java.lang.String)}.
	 */
	@Test
	public void testCategorieClientDefaultConstructor2() {
		CategorieClient cat2 = new CategorieClient("Category2");
		assertTrue(cat2.getNom().equals("Category2"));
		assertTrue(cat2.getNbEmpruntMax() == 0);
		assertTrue(cat2.getCotisation() == 0.0);
		assertTrue(cat2.getCoefDuree() == 0.0);
		assertTrue(cat2.getCoefTarif() == 0.0);
		assertFalse(cat2.getCodeReducUtilise());
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#modifierNom(java.lang.String)}.
	 */
	@Test
	public void testModifierNom() {
		assertTrue(defaultOK1.getNom().equals("Category1"));
		defaultOK1.modifierNom("CategoryModifier");
		assertTrue(defaultOK1.getNom().equals("CategoryModifier"));
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#modifierMax(int)}.
	 */
	@Test
	public void testModifierMax() {
		assertTrue(defaultOK2.getNbEmpruntMax() == 0);
		defaultOK2.modifierMax(15);
		assertTrue(defaultOK2.getNbEmpruntMax() == 15);
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#modifierCotisation(double)}.
	 */
	@Test
	public void testModifierCotisation() {
		assertTrue(defaultOK2.getCotisation() == 0.0);
		defaultOK2.modifierCotisation(1.5f);
		assertTrue(defaultOK2.getCotisation() == 1.5);
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#modifierCoefDuree(double)}.
	 */
	@Test
	public void testModifierCoefDuree() {
		assertTrue(defaultOK2.getCoefDuree() == 0.0);
		defaultOK2.modifierCoefDuree(0.5);
		assertTrue(defaultOK2.getCoefDuree() == 0.5);
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#modifierCoefTarif(double)}.
	 */
	@Test
	public void testModifierCoefTarif() {
		assertTrue(defaultOK2.getCoefTarif() == 0.0);
		defaultOK2.modifierCoefTarif(2.5);
		assertTrue(defaultOK2.getCoefTarif() == 2.5);
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#modifierCodeReducActif(boolean)}.
	 */
	@Test
	public void testModifierCodeReducActif() {
		assertFalse(defaultOK2.getCodeReducUtilise());
		defaultOK2.modifierCodeReducActif(true);
		assertTrue(defaultOK2.getCodeReducUtilise());
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#getNbEmpruntMax()}.
	 */
	@Test
	public void testGetNbEmpruntMax() {
		assertTrue(defaultOK1.getNbEmpruntMax() == 20);
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#getCotisation()}.
	 */
	@Test
	public void testGetCotisation() {
		assertTrue(defaultOK1.getCotisation() == 1.5);
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#getCoefDuree()}.
	 */
	@Test
	public void testGetCoefDuree() {
		assertTrue(defaultOK1.getCoefDuree() == 2.5);
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#getCoefTarif()}.
	 */
	@Test
	public void testGetCoefTarif() {
		assertTrue(defaultOK1.getCoefTarif() == 3.5);
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#toString()}.
	 */
	@Test
	public void testToString() {
		assertTrue(defaultOK1.toString().equals(defaultOK1.getNom()));
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#getNom()}.
	 */
	@Test
	public void testGetNom() {
		assertTrue(defaultOK1.getNom().equals("Category1"));
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#getCodeReducUtilise()}.
	 */
	@Test
	public void testGetCodeReducUtilise() {
		assertTrue(defaultOK1.getCodeReducUtilise());
	}

	/**
	 * Test method for {@link mediatheque.client.CategorieClient#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(defaultOK1.equals(defaultOK1));
		assertFalse(defaultOK1.equals(defaultOK2));
	}

}
