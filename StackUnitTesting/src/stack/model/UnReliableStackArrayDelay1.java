package stack.model;

import java.util.Random;

/**
 * The elements of stack is stored in an array
 * 
 * @author luongnv89
 * 
 */
public class UnReliableStackArrayDelay1 extends StackLinkList {
	/**
	 * How often, on average, the {@link UnreliableBoundedQueue1#push} method
	 * has a problematic delay.<br>
	 * Every time push is called, there is a probability of
	 * <code>  1/PUSH_PROBLEM_FREQ </code> that the execution is delayed by
	 * {@link UnreliableBoundedQueue1#DELAY} milliseconds.
	 */
	private static int PUSH_PROBLEM_FREQ = 10000;

	/**
	 * The number of milliseconds that occur when there is a delay problem in
	 * the method {@link UnreliableBoundedQueue1#push}
	 */
	private static long DELAY = 10;

	/**
	 * Default constructor, the size of store is default, the store is empty
	 */
	public UnReliableStackArrayDelay1() {
		super();
	}

	/**
	 * Constructor a store with the size is input by user
	 * 
	 * @param size2
	 *            The size of store
	 */
	public UnReliableStackArrayDelay1(int size2) {
		super(size2);
	}

	/**
	 * Constructor a store with the size and the elements is input by user <li>
	 * if the input size is unbound -> default size <li>if the length of array
	 * of elements is bigger than the size of store -> store is empty
	 * 
	 * @param elems
	 *            The array of elements
	 * @param size2
	 *            the size of store
	 */
	public UnReliableStackArrayDelay1(int[] elems, int size2) {
		super(elems, size2);
	}

	/**
	 * Copy a store
	 * 
	 * @param stackCoppy
	 *            store to copy
	 */
	public UnReliableStackArrayDelay1(UnReliableStackArrayDelay1 stackCoppy) {
		super(stackCoppy);
	}

	@Override
	public void push(int x) {
		Random RNG = new Random();
		if (RNG.nextInt(PUSH_PROBLEM_FREQ) == 0)
			try {
				// System.out.println("Push: Waiting....");
				Thread.sleep(DELAY);
				// System.out.println("Push: Wake up....");
			} catch (InterruptedException ie) {
				System.out.println(Thread.currentThread().getId()
						+ " - Interupted!");
			}
		;
		super.push(x);

	}
}
