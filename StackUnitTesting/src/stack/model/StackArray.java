package stack.model;

import java.util.Arrays;

import stack.specification.StoreAbstract;

/**
 * The elements of stack is stored in an array
 * 
 * @author luongnv89
 * 
 */
public class StackArray extends StoreAbstract {
	/**
	 * The array store elements of stack
	 */
	int elements[];

	/**
	 * Default constructor, the size of store is default, the store is empty
	 */
	public StackArray() {
		size = DEFAULT_SIZE;
		numberOfElement = 0;
		elements = new int[size];
	}

	/**
	 * Constructor a store with the size is input by user
	 * 
	 * @param size2
	 *            The size of store
	 */
	public StackArray(int size2) {
		if (size2 < MIN_SIZE || size2 > MAX_SIZE)
			size = DEFAULT_SIZE;
		else
			size = size2;
		numberOfElement = 0;
		elements = new int[size];
	}

	/**
	 * Constructor a store with the size and the elements is input by user <li>
	 * if the input size is unbound -> default size <li>if the length of array
	 * of elements is bigger than the size of store -> store is empty
	 * 
	 * @param elems
	 *            The array of elements
	 * @param size2
	 *            the size of store
	 */
	public StackArray(int[] elems, int size2) {
		if (size2 < MIN_SIZE || size2 > MAX_SIZE)
			size = DEFAULT_SIZE;
		else
			size = size2;
		if (elems.length > size) {
			System.out.println("Invalid input! Default construct!");
			numberOfElement = 0;
			elements = new int[size];
		} else {
			numberOfElement = elems.length;
			elements = elems;
		}
	}

	/**
	 * Copy a store
	 * 
	 * @param stackCoppy
	 *            store to copy
	 */
	public StackArray(StackArray stackCoppy) {
		size = stackCoppy.size;
		numberOfElement = stackCoppy.numberOfElement;
		elements = stackCoppy.elements;
	}

	@Override
	public void push(int x) {
		if (this.isFull())
			System.out.println("Store is full!");
		else {
			elements[numberOfElement] = x;
			numberOfElement++;
		}

	}

	@Override
	public int head() {
		if (this.isEmpty()) {
			System.out.println("Store is empty!");
			return 0;
		} else {
			return elements[numberOfElement - 1];
		}
	}

	@Override
	public void pop() {
		if (this.isEmpty()) {
			System.out.println("Store is empty!");
		} else {
			elements[numberOfElement - 1] = 0;
			numberOfElement--;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StackArray other = (StackArray) obj;
		if (!Arrays.equals(elements, other.elements))
			return false;
		return true;
	}

	@Override
	public void show() {
		System.out.println();
		for (int i = 0; i < this.getNumberOfElement(); i++) {
			System.out.print(" " + elements[i]);
		}

	}

}
