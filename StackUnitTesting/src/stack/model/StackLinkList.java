package stack.model;

import java.util.ArrayList;

import stack.specification.StoreAbstract;

/**
 * Stack with the elements is stored in an array list.
 * 
 * @author luongnv89
 * 
 */
public class StackLinkList extends StoreAbstract {

	/**
	 * Arraylist store the element
	 */
	ArrayList<Integer> listElement = new ArrayList<Integer>();

	/**
	 * Default constructor
	 */
	public StackLinkList() {
		size = DEFAULT_SIZE;
		numberOfElement = 0;
	}

	/**
	 * Constructor a store with input size
	 * 
	 * @param size2
	 */
	public StackLinkList(int size2) {
		if (size2 < MIN_SIZE || size2 > MAX_SIZE)
			size = DEFAULT_SIZE;
		else
			size = size2;
		numberOfElement = 0;
	}

	/**
	 * Constructor a store with the size and the elements is input by user <li>
	 * if the input size is unbound -> default size <li>if the length of array
	 * of elements is bigger than the size of store -> store is empty
	 * 
	 * @param elems
	 *            The array of elements
	 * @param size2
	 *            the size of store
	 */
	public StackLinkList(int[] elems, int size2) {
		if (size2 < MIN_SIZE || size2 > MAX_SIZE)
			size = DEFAULT_SIZE;
		else
			size = size2;
		if (elems.length > size) {
			System.out.println("Invalid input! Default construct!");
			numberOfElement = 0;
		} else {
			numberOfElement = elems.length;
			for (int i = 0; i < elems.length; i++) {
				listElement.add(elems[i]);
			}
		}
	}

	/**
	 * Copy a store
	 * 
	 * @param stackCoppy
	 *            store to copy
	 */
	public StackLinkList(StackLinkList stackCoppy) {
		size = stackCoppy.getSize();
		numberOfElement = stackCoppy.getNumberOfElement();
		listElement.addAll(stackCoppy.listElement);
	}

	@Override
	public void push(int x) {
		if (this.isFull())
			System.out.println("Store is full!");
		else {
			numberOfElement++;
			listElement.add(x);
		}

	}

	@Override
	public int head() {
		if (this.isEmpty()) {
			System.out.println("Store is empty!");
			return 0;
		} else {
			return listElement.get(numberOfElement - 1);
		}
	}

	@Override
	public void pop() {
		if (this.isEmpty()) {
			System.out.println("Store is empty!");
		} else {
			listElement.remove(numberOfElement - 1);
			numberOfElement--;
		}
	}

	@Override
	public void show() {
		for (int i = 0; i < this.getNumberOfElement(); i++) {
			System.out.print(" " + listElement.get(i));
		}
		System.out.println();

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StackLinkList other = (StackLinkList) obj;
		if (listElement == null) {
			if (other.listElement != null)
				return false;
		} else if (!listElement.equals(other.listElement))
			return false;
		return true;
	}

}
