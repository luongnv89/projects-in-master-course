package stack.model;

import stack.specification.StoreAbstract;
import tools.InvariantBroken;

/**
 * Queue is implemented from two stack
 * 
 * @author luongnv89
 * 
 */
public class Queue extends StoreAbstract {
	StackArray stackin;
	StackArray stackout;

	/**
	 * Default constructor, the queue is empty
	 */
	public Queue() {
		stackin = new StackArray();
		stackout = new StackArray();
		this.size = stackin.getSize();
		this.numberOfElement = stackin.getNumberOfElement()
				+ stackout.getNumberOfElement();
	}

	/**
	 * Constructor a store with input size
	 * 
	 * @param size
	 */
	public Queue(int size) {
		stackin = new StackArray(size);
		stackout = new StackArray(size);
		this.size = stackin.getSize();
		this.numberOfElement = stackin.getNumberOfElement()
				+ stackout.getNumberOfElement();
	}

	/**
	 * Constructor a store with the size and the elements is input by user <li>
	 * if the input size is unbound -> default size <li>if the length of array
	 * of elements is bigger than the size of store -> store is empty
	 * 
	 * @param elems
	 *            The array of elements
	 * @param size2
	 *            the size of store
	 */
	public Queue(int[] elems, int size2) {
		stackin = new StackArray(elems, size2);
		stackout = new StackArray(size2);
		this.size = stackin.getSize();
		this.numberOfElement = stackin.getNumberOfElement()
				+ stackout.getNumberOfElement();
	}

	/**
	 * Copy a store
	 * 
	 * @param other
	 *            store to copy
	 */
	public Queue(Queue other) {
		stackin = new StackArray(other.stackin);
		stackout = new StackArray(other.stackout);
		this.size = stackin.getSize();
		this.numberOfElement = stackin.getNumberOfElement()
				+ stackout.getNumberOfElement();
	}

	@Override
	public void push(int x) {
		if (this.isFull()) {
			System.out.println("Queue is full!");
		} else {
			movetoStackin();
			stackin.push(x);
			this.numberOfElement = stackin.getNumberOfElement()
					+ stackout.getNumberOfElement();
		}
	}

	/**
	 * Move every elements to stackin
	 */
	private void movetoStackin() {
		while (!stackout.isEmpty()) {
			stackin.push(stackout.head());
			stackout.pop();
		}

	}

	@Override
	public int head() throws InvariantBroken {
		int head = 0;
		if (this.isEmpty()) {
			System.out.println("Stack is empty!");
		} else {
			movetoStackout();
			head = stackout.head();
			movetoStackin();
		}
		return head;
	}

	/**
	 * Move every elements to stackout
	 */
	private void movetoStackout() {
		while (!stackin.isEmpty()) {
			stackout.push(stackin.head());
			stackin.pop();
		}
	}

	@Override
	public void pop() throws InvariantBroken {
		if (this.isEmpty()) {
			System.out.println("Stack is empty!");
		} else {
			movetoStackout();
			stackout.pop();
			movetoStackin();
			this.numberOfElement = stackin.getNumberOfElement()
					+ stackout.getNumberOfElement();
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Queue other = (Queue) obj;
		if (stackin == null) {
			if (other.stackin != null)
				return false;
		} else if (!stackin.equals(other.stackin))
			return false;
		if (stackout == null) {
			if (other.stackout != null)
				return false;
		} else if (!stackout.equals(other.stackout))
			return false;
		return true;
	}

	@Override
	public boolean invariant() {
		super.invariant();
		if (numberOfElement != (stackin.getNumberOfElement() + stackout
				.getNumberOfElement()))
			return false;
		return true;
	}

	@Override
	public void show() {
		stackin.show();

	}

}
