package stack.model;

import stack.specification.StoreInterface;

public class HanoiTower2 {
	private int numberOfDisk;
	private StoreInterface storeSource;
	private StoreInterface storeTemp;
	private StoreInterface storeDes;

	public HanoiTower2(StoreInterface source) {
		storeSource = source;
		this.numberOfDisk = source.getNumberOfElement();
	}

	public void setStoreTemp(StoreInterface temp) {
		storeTemp = temp;
	}

	public void setStoreDes(StoreInterface des) {
		storeDes = des;
	}

	/**
	 * Move numberDisk of Disk from source to des
	 * 
	 * @param numberDisk
	 *            number of disk will be move
	 * @param source
	 *            source stack
	 * @param des
	 *            destination stack
	 * @param temp
	 *            temp stack
	 */
	public void move(int numberDisk, StoreInterface source, StoreInterface des,
			StoreInterface temp) {
		if ((numberDisk > 0) && (this.invariant())) {
			move(numberDisk - 1, source, temp, des);
			des.push(source.head());
			source.pop();
			// this.showAll();
			move(numberDisk - 1, temp, des, source);
		}
	}

	public void run() {
		System.out.println("************At the beginning:*********");
		showAll();
		move(numberOfDisk, storeSource, storeDes, storeTemp);
		System.out.println("*********At the end:*********");
		showAll();
	}

	private boolean invariant() {
		return storeDes.invariant() && storeTemp.invariant()
				&& storeSource.invariant();

	}

	private void showAll() {
		System.out.println("\n**** SHOW THE TOWER ****");
		System.out.println("\nSource Stack: ");
		storeSource.show();
		System.out.println("\nTemp Stack: ");
		storeTemp.show();
		System.out.println("\nDestination Stack: ");
		storeDes.show();
		System.out.println();
	}

}
