package stack.model;

import stack.specification.StoreAbstract;
import tools.InvariantBroken;

public class Queue2 extends StoreAbstract {
	StackArray stackin;
	StackArray stackout;

	public Queue2() {
		stackin = new StackArray();
		stackout = new StackArray();
		this.size = stackin.getSize();
		this.numberOfElement = stackin.getNumberOfElement()
				+ stackout.getNumberOfElement();
	}

	public Queue2(int size) {
		stackin = new StackArray(size);
		stackout = new StackArray(size);
		this.size = stackin.getSize();
		this.numberOfElement = stackin.getNumberOfElement()
				+ stackout.getNumberOfElement();
	}

	public Queue2(int[] elems, int size2) {
		stackin = new StackArray(elems, size2);
		stackout = new StackArray(size2);
		this.size = stackin.getSize();
		this.numberOfElement = stackin.getNumberOfElement()
				+ stackout.getNumberOfElement();
	}

	public Queue2(Queue2 other) {
		stackin = new StackArray(other.stackin);
		stackout = new StackArray(other.stackout);
		this.size = stackin.getSize();
		this.numberOfElement = stackin.getNumberOfElement()
				+ stackout.getNumberOfElement();
	}

	@Override
	public void push(int x) {
		if (this.isFull()) {
			System.out.println("Queue is full!");
		} else {
			movetoStackin();
			stackin.push(x);
			this.numberOfElement = stackin.getNumberOfElement()
					+ stackout.getNumberOfElement();
		}
	}

	private void movetoStackin() {
		while (!stackout.isEmpty()) {
			stackin.push(stackout.head());
			stackout.pop();
		}

	}

	@Override
	public int head() throws InvariantBroken {
		int head = 0;
		if (this.isEmpty()) {
			System.out.println("Stack is empty!");
		} else {
			movetoStackout();
			head = stackout.head();
			movetoStackin();
		}
		return head;
	}

	private void movetoStackout() {
		while (!stackin.isEmpty()) {
			stackout.push(stackin.head());
			stackin.pop();
		}
	}

	@Override
	public void pop() throws InvariantBroken {
		if (this.isEmpty()) {
			System.out.println("Stack is empty!");
		} else {
			movetoStackout();
			stackout.pop();
			movetoStackin();
			this.numberOfElement = stackin.getNumberOfElement()
					+ stackout.getNumberOfElement();
		}

	}

	public boolean equalElement(Object pop) {

		return this.stackin.equals(((Queue2) pop).stackin);
	}

	@Override
	public boolean invariant() {
		super.invariant();
		if (numberOfElement != (stackin.getNumberOfElement() + stackout
				.getNumberOfElement()))
			return false;
		return true;
	}

	@Override
	public void show() {
		stackin.show();

	}

}
