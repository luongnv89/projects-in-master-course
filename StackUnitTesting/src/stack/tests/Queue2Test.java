package stack.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import stack.model.Queue2;
import stack.specification.StoreInterface;

public class Queue2Test extends StoreAbstractTest {

	@Before
	public void setUp() throws Exception {
		defaultStore = new Queue2();
		nonDefaultStoreOK = new Queue2(
				(StoreInterface.MIN_SIZE + StoreInterface.MAX_SIZE) / 2);
		nonDefaultStoreKO = new Queue2(StoreInterface.MAX_SIZE + 1);

		nonDefaultStoreKO2 = new Queue2(inputFull(), inputFull().length - 1);
		nonDefaultStoreOK2 = new Queue2(inputFull(), inputFull().length + 1);

		fullStore = new Queue2(inputFull(), inputFull().length);
		pushStore = new Queue2(new int[StoreInterface.DEFAULT_SIZE],
				StoreInterface.DEFAULT_SIZE);
	}

	@Test
	public void testHead() {
		// when the store is empty
		Assert.assertEquals(nonDefaultStoreKO.head(), 0);

		// when the store is non empty
		Assert.assertEquals(fullStore.head(), inputFull()[0]);
	}
}
