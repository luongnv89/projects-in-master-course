package stack.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import stack.model.StackLinkList;
import stack.specification.StoreInterface;

public class StackLinkListTest extends StackAbstractTest {

	@Before
	public void setUp() throws Exception {
		defaultStore = new StackLinkList();
		nonDefaultStoreOK = new StackLinkList(
				(StoreInterface.MIN_SIZE + StoreInterface.MAX_SIZE) / 2);
		nonDefaultStoreKO = new StackLinkList(StoreInterface.MAX_SIZE + 1);

		nonDefaultStoreKO2 = new StackLinkList(inputFull(),
				inputFull().length - 1);
		nonDefaultStoreOK2 = new StackLinkList(inputFull(),
				inputFull().length + 1);

		fullStore = new StackLinkList(inputFull(), inputFull().length);
		pushStore = new StackLinkList();
		coppyStore = new StackLinkList((StackLinkList) fullStore);
	}

	@Test
	public void testHead() {
		// when the store is empty
		Assert.assertEquals(nonDefaultStoreKO.head(), 0);

		// when the store is non empty
		Assert.assertEquals(fullStore.head(),
				inputFull()[inputFull().length - 1]);
	}
}
