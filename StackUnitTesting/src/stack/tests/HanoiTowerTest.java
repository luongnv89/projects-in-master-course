package stack.tests;

import org.junit.Test;

import stack.model.HanoiTower;
import stack.model.StackArray;
import stack.model.StackLinkList;

public class HanoiTowerTest {

	@Test
	public void test() {
		int[] input = { 7, 6, 5, 4, 3, 2, 1 };
		int[] input2 = { 7, 6, 5, 4, 3, 2, 1 };

		// With store is stack array
		StackArray stack = new StackArray(input, 10);
		HanoiTower test = new HanoiTower(stack);
		test.setStoreDes(new StackArray(stack.getSize()));
		test.setStoreTemp(new StackArray(stack.getSize()));
		test.run();

		// With store is stack link list
		StackLinkList stack2 = new StackLinkList(input2, 10);
		HanoiTower test2 = new HanoiTower(stack2);
		test2.setStoreDes(new StackArray(stack2.getSize()));
		test2.setStoreTemp(new StackArray(stack2.getSize()));
		test2.run();
	}

}
