package stack.tests;

import org.junit.Assert;
import org.junit.Test;

public class StackAbstractTest extends StoreAbstractTest{

	@Test
	public void testPush() {
		int i = 1;
		while (!pushStore.isFull()) {
			pushStore.push(i);
			Assert.assertEquals(pushStore.head(), i);
			i++;
		}
	}

	@Test
	public void testPop() {
		int i = 0;
		int k = inputFull()[inputFull().length - 1 - i];
		while (fullStore.getNumberOfElement() > 1) {
			Assert.assertEquals(fullStore.head(), k);
			i++;
			k = inputFull()[inputFull().length - 1 - i];
			fullStore.pop();
		}
	}

}
