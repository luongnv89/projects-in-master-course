package stack.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import stack.model.Queue;
import stack.specification.StoreInterface;

public class QueueTest extends StoreAbstractTest {

	@Before
	public void setUp() throws Exception {
		defaultStore = new Queue();
		nonDefaultStoreOK = new Queue(
				(StoreInterface.MIN_SIZE + StoreInterface.MAX_SIZE) / 2);
		nonDefaultStoreKO = new Queue(StoreInterface.MAX_SIZE + 1);

		nonDefaultStoreKO2 = new Queue(inputFull(), inputFull().length - 1);
		nonDefaultStoreOK2 = new Queue(inputFull(), inputFull().length + 1);

		fullStore = new Queue(inputFull(), inputFull().length);
		pushStore = new Queue(StoreInterface.DEFAULT_SIZE);
		coppyStore = new Queue((Queue) fullStore);
	}

	@Test
	public void testPush() {
		int i = 1;
		while (!pushStore.isFull()) {
			pushStore.push(i);
			Assert.assertEquals(pushStore.head(), 1);
			Assert.assertEquals(pushStore.getNumberOfElement(), i);
			i++;
		}
	}

	@Test
	public void testPop() {
		int i = 0;
		int k = inputFull()[i];
		while (fullStore.getNumberOfElement() > 1) {
			Assert.assertEquals(fullStore.head(), k);
			i++;
			k = inputFull()[i];
			fullStore.pop();
		}
	}

	@Test
	public void testHead() {
		// when the store is empty
		Assert.assertEquals(nonDefaultStoreKO.head(), 0);

		// when the store is non empty
		Assert.assertEquals(fullStore.head(), inputFull()[0]);
	}
}
