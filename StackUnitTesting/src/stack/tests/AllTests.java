package stack.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ HanoiTowerTest.class, QueueTest.class, StackArrayTest.class,
		StackLinkListTest.class })
public class AllTests {

}
