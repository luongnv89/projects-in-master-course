package stack.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import stack.specification.StoreAbstract;
import stack.specification.StoreInterface;

public abstract class StoreAbstractTest {

	protected static StoreAbstract defaultStore;

	protected static StoreAbstract nonDefaultStoreOK;

	protected static StoreAbstract nonDefaultStoreKO;

	protected static StoreAbstract nonDefaultStoreKO2;

	protected static StoreAbstract nonDefaultStoreOK2;

	protected static StoreAbstract fullStore;

	protected static StoreAbstract pushStore;

	protected static StoreAbstract coppyStore;

	@Test
	public void testClassInvariant() {

		Assert.assertTrue(nonDefaultStoreOK.invariant());
		Assert.assertTrue(nonDefaultStoreOK2.invariant());
		Assert.assertTrue(nonDefaultStoreKO.invariant());
		Assert.assertTrue(nonDefaultStoreKO2.invariant());
		Assert.assertTrue(fullStore.invariant());
		Assert.assertTrue(pushStore.invariant());

	}

	@Test
	public void testDefaultConstructor() {
		Assert.assertTrue(defaultStore.invariant());
		Assert.assertEquals(defaultStore.getSize(), StoreInterface.DEFAULT_SIZE);
		Assert.assertEquals(defaultStore.getNumberOfElement(), 0);
	}

	@Test
	public void testNonDefaultConstructor() {
		// When number of parameter is invalid
		Assert.assertTrue(nonDefaultStoreKO.invariant());
		Assert.assertEquals(nonDefaultStoreKO.getSize(),
				StoreInterface.DEFAULT_SIZE);
		Assert.assertEquals(nonDefaultStoreKO.getNumberOfElement(), 0);
		// When the number of element of list is more than the size of store
		Assert.assertTrue(nonDefaultStoreKO2.invariant());
		Assert.assertEquals(nonDefaultStoreKO2.getSize(),
				inputFull().length - 1);
		Assert.assertEquals(nonDefaultStoreKO2.getNumberOfElement(), 0);

		// When number of parameter is valid
		Assert.assertTrue(nonDefaultStoreOK.invariant());
		Assert.assertEquals(nonDefaultStoreOK.getSize(),
				(StoreInterface.MIN_SIZE + StoreInterface.MAX_SIZE) / 2);
		Assert.assertEquals(nonDefaultStoreOK.getNumberOfElement(), 0);
		// When number element of list is less than the size of store
		Assert.assertTrue(nonDefaultStoreOK2.invariant());
		Assert.assertEquals(nonDefaultStoreOK2.getSize(),
				inputFull().length + 1);
		Assert.assertEquals(nonDefaultStoreOK2.getNumberOfElement(),
				inputFull().length);

	}

	@Test
	public void testFull() {
		Assert.assertTrue(fullStore.invariant());
		Assert.assertTrue(fullStore.isFull());

	}

	@Test
	public void testEquals() {
		Assert.assertTrue(fullStore.equals(coppyStore));
		Assert.assertTrue(fullStore.equals(fullStore));
		Assert.assertFalse(fullStore.equals(defaultStore));
		Assert.assertFalse(fullStore.equals(null));

	}

	protected int[] inputFull() {
		int[] array = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19 };
		return array;
	}

	@After
	public void tearDown() throws Exception {
		defaultStore = null;
		nonDefaultStoreOK = null;
		nonDefaultStoreKO = null;
		fullStore = null;
		pushStore = null;
	}

}
