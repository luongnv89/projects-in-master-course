package stack.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import stack.model.StackArray;
import stack.specification.StoreInterface;

public class StackArrayTest extends StackAbstractTest {

	@Before
	public void setUp() throws Exception {
		defaultStore = new StackArray();
		nonDefaultStoreOK = new StackArray(
				(StoreInterface.MIN_SIZE + StoreInterface.MAX_SIZE) / 2);
		nonDefaultStoreKO = new StackArray(StoreInterface.MAX_SIZE + 1);

		nonDefaultStoreKO2 = new StackArray(inputFull(), inputFull().length - 1);
		nonDefaultStoreOK2 = new StackArray(inputFull(), inputFull().length + 1);

		fullStore = new StackArray(inputFull(), inputFull().length);
		pushStore = new StackArray();
		coppyStore = new StackArray((StackArray) fullStore);
	}

	@Test
	public void testHead() {
		// when the store is empty
		Assert.assertEquals(nonDefaultStoreKO.head(), 0);

		// when the store is non empty
		Assert.assertEquals(fullStore.head(),
				inputFull()[inputFull().length - 1]);
	}
}
