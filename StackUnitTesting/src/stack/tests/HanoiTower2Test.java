package stack.tests;

import org.junit.Test;

import stack.model.HanoiTower2;
import stack.model.StackArray;
import stack.model.StackLinkList;

public class HanoiTower2Test {

	@Test
	public void test() {
		int[] input = { 1, 2, 3, 4, 5, 6, 7 };
		int[] input2 = { 1, 2, 3, 4, 5, 6, 7 };
		StackArray stack = new StackArray(input, 10);
		HanoiTower2 test = new HanoiTower2(stack);
		test.setStoreDes(new StackArray(stack.getSize()));
		test.setStoreTemp(new StackArray(stack.getSize()));
		test.run();

		StackLinkList stack2 = new StackLinkList(input2, 10);
		HanoiTower2 test2 = new HanoiTower2(stack2);
		test2.setStoreDes(new StackArray(stack2.getSize()));
		test2.setStoreTemp(new StackArray(stack2.getSize()));
		test2.run();
	}

}
