package stack.specification;

import tools.InvariantBroken;

/**
 * Store interface
 * 
 * @author konal89
 * 
 */
public interface StoreInterface {
	/**
	 * The maximum size of store
	 */
	final static int MAX_SIZE = 100;
	/**
	 * The minimum size of store
	 */
	final static int MIN_SIZE = 1;
	/**
	 * The default size of store
	 */
	final static int DEFAULT_SIZE = 10;

	/**
	 * Push an element into store
	 * 
	 * @param x
	 *            element
	 */
	public void push(int x);

	/**
	 * Get the head element of store
	 * 
	 * @return value of the head element <li>0: if the store is empty
	 * @throws InvariantBroken
	 */
	public int head() throws InvariantBroken;

	/**
	 * Remove the head element of store If the store is empty, this method will
	 * do nothing
	 * 
	 * @throws InvariantBroken
	 */
	public void pop() throws InvariantBroken;

	/**
	 * Get the size of store
	 * 
	 * @return The size of store
	 * 
	 */
	public int getSize();

	/**
	 * Get the number of elements of store
	 * 
	 * @return the number of elements of store <li>0 if store is empty
	 */
	public int getNumberOfElement();

	/**
	 * Check a store is empty or not
	 * 
	 * @return true if store empty <li>false otherwise
	 */
	public boolean isEmpty();

	/**
	 * Check a store is full or not
	 * 
	 * @return true if store is full <li>false if store isn't full
	 */
	public boolean isFull();

	/**
	 * Check a store is invariant or not
	 * 
	 * @return true if store is invariant
	 */
	public boolean invariant();

	/**
	 * Show the content of store, <li>for example: {1,2,3,4,5} <li>if store is a
	 * stack -> head = 5 <li>if store is a queue -> head = 1
	 */
	public void show();

}
