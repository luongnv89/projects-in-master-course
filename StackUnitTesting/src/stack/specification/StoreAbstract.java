package stack.specification;

/**
 * Store abstract
 * @author konal89
 *
 */
public abstract class StoreAbstract implements StoreInterface {
	/**
	 * The size of store
	 */
	protected int size;

	/**
	 * The number of elements of store
	 */
	protected int numberOfElement;

	public int getSize() {
		return size;
	}

	public int getNumberOfElement() {
		return numberOfElement;
	}

	public boolean isEmpty() {
		return getNumberOfElement() == 0;
	}

	public boolean isFull() {
		return getNumberOfElement() == getSize();
	}

	public boolean invariant() {
		if (size < MIN_SIZE || size > MAX_SIZE || numberOfElement > size)
			return false;
		return true;
	}

}
