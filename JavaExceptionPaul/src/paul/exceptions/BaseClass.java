package paul.exceptions;

public class BaseClass {
	
	
	public void m1 (){
		System.out.println("In m1 of BaseClass");
	}
	
	public void m2 () throws Exception{
		System.out.println("In m2 of BaseClass");
		throw new Exception ("Exception in m2 of BaseClass");
	}
	
	public void m3 () throws RuntimeException{
		System.out.println("In m2 of BaseClass");
		throw new RuntimeException ("RuntimeException in m3 of BaseClass");
	}

}
