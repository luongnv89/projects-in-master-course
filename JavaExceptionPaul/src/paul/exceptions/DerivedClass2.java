package paul.exceptions;

public class DerivedClass2 extends BaseClass{
	


		public void m2 () throws CheckedException, UncheckedException{
			System.out.println("In m2 of DerivedClass2");
			throw new CheckedException ("CheckedException in m2 of DerivedClass2");
		}
		
		/*Will Not compile
		 * 
			public void m3 () throws UncheckedException, CheckedException{
			System.out.println("In m3 of DerivedClass2");
		}
		*/

		public void m3 () throws UncheckedException{
			System.out.println("In m3 of DerivedClass2");
			throw new UncheckedException ("UncheckedException in m3 of DerivedClass2");
		}
}
