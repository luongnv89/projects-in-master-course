package paul.exceptions;

public class DerivedClass1 extends BaseClass{
	

		/*
		 * Will not compile
		 * 
		 	public void m1 ()throws CheckedException {
			System.out.println("In m1 of DerivedClass1");
		}
		*/
				
	   public void m1 () throws UncheckedException {
		System.out.println("In m1 of DerivedClass1");
		throw new UncheckedException ("UncheckedException in m1 of DerivedClass1");
	   }
	
		
		public void m2 () {
			System.out.println("In m2 of DerivedClass1");
		}
		
		public void m3 () {
			System.out.println("In m3 of DerivedClass1");
		}

}
