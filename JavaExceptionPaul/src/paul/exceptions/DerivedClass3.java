package paul.exceptions;


public class DerivedClass3 extends DerivedClass1{
	

				
	   public void m1 () throws RuntimeException {
		System.out.println("In m3 of DerivedClass3");
		throw new RuntimeException ("RuntimeException in m1 of DerivedClass3");
	   }
	
		
		public void m2 () throws RuntimeException {
			System.out.println("In m2 of DerivedClass3");
			throw new RuntimeException ("RuntimeException in m2 of DerivedClass3");
		}
		
		/*
		 * Will not compile
		public void m3 () throws Exception{
			System.out.println("In m3 of DerivedClass3");
		}
		*/

}
