package paul.exceptions;

public class TestExceptions {

	/**
	 * To test the following semantics for overriding and exceptions:
	 * <ul>
	 *  Implementing an interface or overriding a method has to do more than just match the signature. <br>
	 *  It also has to avoid declaring that it will throw any more checked exceptions than those declared
	 * </ul>
	 * @param args is not used
	 */
	public static void main(String[] args) {
		
		BaseClass derived1 = new DerivedClass1();
		try {derived1.m1();}
		    catch (UncheckedException e){System.out.println(e);}

	    // derived1.m2();  - will not compile if you do not handle exception
		try {derived1.m2();}
		    catch (Exception e){System.out.println(e);}
		         
		derived1.m3();
		
		BaseClass derived2 = new DerivedClass2();
		
		derived2.m1();
		
	    // derived2.m2();  - will not compile if you do not handle exception
		try {derived2.m2();}
	         catch (Exception e){System.out.println(e);}
	         
	    try {derived2.m3();}
	         catch (Exception e){System.out.println(e);}
		    
	    BaseClass derived3 = new DerivedClass3();
	 		
	    try {derived3.m1();}
        catch (Exception e){System.out.println(e);}
	    
	    try {derived3.m2();}
        catch (Exception e){System.out.println(e);}
	 		
	 	// derived3.m2();  - will not compile if you do not handle exception
	 	try {derived3.m2();}
	         catch (Exception e){System.out.println(e);}
	         
	 	derived3.m3();
	}

}
