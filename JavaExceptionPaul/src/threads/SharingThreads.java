package threads;

class SharingThread extends Thread {
	
	SharedString stringofchars;
	char increment;
	
    public SharingThread(String str, SharedString s, char inc) {
        super(str);
        stringofchars = s;
        increment = inc;
    }
    
    public void run() {
        for (int i = 0; i < 5; i++) {
        	
        	
            try {
                sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {}
            stringofchars.add(increment);
            System.out.println("Shared String extended by " + getName()+ " to "+ stringofchars );
        }
        System.out.println("No more increments left " + getName());
    }
}

 class SharedString {
	
	public SharedString(){str ="";}
	 
	public String str;
	
	public  void add (char c){str = str + c;}
	
	public String toString () {return str;}
	}


public class SharingThreads {

		    public static void main (String[] args) {  	
	    	
	    	SharedString soc = new SharedString();        
	        new SharingThread("SharingAdda", soc, 'a').start();
	        new SharingThread("SharingAddb", soc, 'b').start();
	    }
	
}

