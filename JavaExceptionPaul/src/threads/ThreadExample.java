package threads;


class SimpleThread extends Thread {
	
	String stringofchars;
	char increment;
	
    public SimpleThread(String str,  char inc) {
        super(str);
        stringofchars = "";
        increment = inc;
    }
    public void run() {
        for (int i = 0; i < 5; i++) {     	

            try {
                sleep((int)(Math.random() * 3000));
            } catch (InterruptedException e) {}
        	stringofchars = stringofchars + increment;
            System.out.println("String " + getName()+ " extended to "+ stringofchars );
        }
        System.out.println("No more increments left for thread" + getName());
    }
}


public class ThreadExample {

		    public static void main (String[] args) { 	
		    System.out.println("Starting Thread main");
	        new SimpleThread("Add1", '1').start();
	        new SimpleThread("Add2", '2').start();
	        System.out.println("Finishing Thread main");
	    }
}
