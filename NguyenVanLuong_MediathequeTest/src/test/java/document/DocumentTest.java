/**
 * 
 */
package document;

import static org.junit.Assert.*;

import mediatheque.Genre;
import mediatheque.Localisation;
import mediatheque.OperationImpossible;
import mediatheque.document.Document;

import org.junit.After;
import org.junit.Test;

import util.InvariantBroken;

/**
 *
 *Test for {@link Document}
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public abstract class DocumentTest {

	protected Document defaultOK1;
	protected Document defaultOK2;

	protected Document empruntableDoc;
	protected Document pruntableDoc1;
	protected Document pruntableDoc2;

	protected Document emprunteDoc;
	protected Document prunteDoc1;
	protected Document prunteDoc2;

	// Default attributes value
	protected String CODE_DF = "CODE_DEFAULT";
	protected String TITRE_DF = "Default title";
	protected String AUTEUR_DF = "Default author";
	protected String ANNEE_DF = "2013";
	protected Genre GENRE_DF = new Genre("Default genre");
	protected boolean EMPRUNTABLE_DF = false;
	protected boolean EMPRUNTE_DF = false;
	protected int NBEMPRUNTS_DF = 0;
	protected Localisation LOCAL_DF = new Localisation("Default salle",
			"Default rayon");

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		defaultOK1 = null;
		defaultOK2 = null;
		empruntableDoc = null;
		pruntableDoc1 = null;
		pruntableDoc2 = null;
		emprunteDoc = null;
		prunteDoc1 = null;
		prunteDoc2 = null;
	}

	/**
	 * Test method for {@link mediatheque.document.Document#getCode()}.
	 */
	@Test
	public void testGetCode() {
		assertTrue(defaultOK1.getCode().equals("CODE_DEFAULT"));
	}

	/**
	 * Test method for {@link mediatheque.document.Document#getTitre()}.
	 */
	@Test
	public void testGetTitre() {
		assertTrue(defaultOK1.getTitre().equals("Default title"));
	}

	/**
	 * Test method for {@link mediatheque.document.Document#getAuteur()}.
	 */
	@Test
	public void testGetAuteur() {
		assertTrue(defaultOK1.getAuteur().equals("Default author"));
	}

	/**
	 * Test method for {@link mediatheque.document.Document#getLocalisation()}.
	 */
	@Test
	public void testGetLocalisation() {
		assertTrue(defaultOK1.getLocalisation().equals(
				new Localisation("Default salle", "Default rayon")));
	}

	/**
	 * Test method for {@link mediatheque.document.Document#getAnnee()}.
	 */
	@Test
	public void testGetAnnee() {
		assertTrue(defaultOK1.getAnnee().equals("2013"));
	}

	/**
	 * Test method for {@link mediatheque.document.Document#getGenre()}.
	 */
	@Test
	public void testGetGenre() {
		System.out.println(defaultOK1.getGenre().toString());
	}

	/**
	 * Test method for {@link mediatheque.document.Document#getNbEmprunts()}.
	 */
	@Test
	public void testGetNbEmprunts() {
		assertTrue(defaultOK1.getNbEmprunts() == 0);
	}

	/**
	 * Test method for {@link mediatheque.document.Document#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(defaultOK1.equals(defaultOK1));
		assertFalse(defaultOK1.equals(defaultOK2));
	}

	/**
	 * Test method for {@link mediatheque.document.Document#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(defaultOK1.toString());
		System.out.println(defaultOK2.toString());
	}

	/**
	 * Test method for {@link mediatheque.document.Document#metEmpruntable()}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testMetEmpruntableOperationImpossible()
			throws OperationImpossible, InvariantBroken {
		empruntableDoc.metEmpruntable();
	}

	/**
	 * Test method for {@link mediatheque.document.Document#metEmpruntable()}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testMetEmpruntableOK() throws OperationImpossible,
			InvariantBroken {
		assertFalse(pruntableDoc1.estEmpruntable());
		pruntableDoc1.metEmpruntable();
		assertTrue(pruntableDoc1.estEmpruntable());
	}

	/**
	 * Test method for {@link mediatheque.document.Document#metConsultable()}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testMetConsultableOperationImpossibleEmprunte()
			throws OperationImpossible, InvariantBroken {
		pruntableDoc1.metConsultable();

	}

	/**
	 * Test method for {@link mediatheque.document.Document#metConsultable()}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testMetConsultableOperationImpossibleEmpruntable()
			throws OperationImpossible, InvariantBroken {
		emprunteDoc.metConsultable();

	}

	/**
	 * Test method for {@link mediatheque.document.Document#metConsultable()}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testMetConsultableOK() throws OperationImpossible,
			InvariantBroken {
		prunteDoc1.metConsultable();
		assertFalse(prunteDoc1.estEmpruntable());
	}

	/**
	 * Test method for {@link mediatheque.document.Document#estEmpruntable()}.
	 */
	@Test
	public void testEstEmpruntable() {
		assertTrue(empruntableDoc.estEmpruntable());
		assertFalse(pruntableDoc1.estEmpruntable());
	}

	/**
	 * Test method for {@link mediatheque.document.Document#emprunter()}.
	 * @throws OperationImpossible 
	 * @throws InvariantBroken 
	 */
	@Test(expected = OperationImpossible.class)
	public void testEmprunterOperationImpossibleEmpruntableFalse()
			throws InvariantBroken, OperationImpossible {
		pruntableDoc1.emprunter();
	}

	/**
	 * Test method for {@link mediatheque.document.Document#emprunter()}.
	 * @throws OperationImpossible 
	 * @throws InvariantBroken 
	 */
	@Test(expected = OperationImpossible.class)
	public void testEmprunterOperationImpossibleEmprunteTrue()
			throws InvariantBroken, OperationImpossible {
		emprunteDoc.emprunter();
	}

	/**
	 * Test method for {@link mediatheque.document.Document#emprunter()}.
	 * @throws OperationImpossible 
	 * @throws InvariantBroken 
	 */
	@Test
	public void testEmprunter() throws InvariantBroken, OperationImpossible {
		int nbEmprunts = prunteDoc1.getNbEmprunts();
		prunteDoc1.emprunter();
		assertTrue(prunteDoc1.estEmprunte());
		assertTrue(prunteDoc1.getNbEmprunts() == nbEmprunts + 1);
	}

	/**
	 * Test method for {@link mediatheque.document.Document#estEmprunte()}.
	 */
	@Test
	public void testEstEmprunte() {
		assertTrue(emprunteDoc.estEmprunte());
		assertFalse(prunteDoc1.estEmprunte());
	}

	/**
	 * Test method for {@link mediatheque.document.Document#restituer()}.
	 * @throws OperationImpossible 
	 * @throws InvariantBroken 
	 */
	@Test(expected = OperationImpossible.class)
	public void testRestituerOperationImpossibleEmpruntable()
			throws InvariantBroken, OperationImpossible {
		pruntableDoc1.restituer();
	}

	/**
	 * Test method for {@link mediatheque.document.Document#restituer()}.
	 * @throws OperationImpossible 
	 * @throws InvariantBroken 
	 */
	@Test
	public void testRestituer() throws InvariantBroken, OperationImpossible {
		prunteDoc2.restituer();
		assertFalse(prunteDoc2.estEmprunte());
	}

	/**
	 * Test method for {@link mediatheque.document.Document#afficherStatDocument()}.
	 */
	@Test
	public void testAfficherStatDocument() {
		defaultOK1.afficherStatDocument();
	}

	/**
	 * Test method for {@link mediatheque.document.Document#afficherStatistiques()}.
	 */
	@Test
	public void testAfficherStatistiques() {
		Document.afficherStatistiques();
	}

	/**
	 * Test method for {@link mediatheque.document.Document#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(emprunteDoc.invariant());
		assertTrue(prunteDoc1.invariant());
		assertTrue(empruntableDoc.invariant());
	}

}
