/**
 * 
 */
package document;

import static org.junit.Assert.assertTrue;
import mediatheque.OperationImpossible;
import mediatheque.document.Document;
import mediatheque.document.Livre;

import org.junit.Before;
import org.junit.Test;

import util.InvariantBroken;

/**
 *Test for {@link Livre}
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class LivreTest extends DocumentTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		defaultOK1 = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 2011);
		defaultOK2 = new Livre("CODE_DEFAULT_2", LOCAL_DF, TITRE_DF + "_2",
				AUTEUR_DF + "_2", ANNEE_DF, GENRE_DF, 2012);
		empruntableDoc = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 2014);
		empruntableDoc.metEmpruntable();
		pruntableDoc1 = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 2014);

		pruntableDoc2 = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 2015);
		pruntableDoc2.metEmpruntable();
		pruntableDoc2.emprunter();

		emprunteDoc = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 2016);
		emprunteDoc.metEmpruntable();
		emprunteDoc.emprunter();
		prunteDoc1 = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 2017);
		prunteDoc1.metEmpruntable();
		prunteDoc2 = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 2018);
		prunteDoc2.metEmpruntable();
		prunteDoc2.emprunter();
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#Livre(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testLivreOperationImpossibleCodeNull()
			throws OperationImpossible, InvariantBroken {
		Document doc = new Livre(null, LOCAL_DF, TITRE_DF, AUTEUR_DF, ANNEE_DF,
				GENRE_DF, 10);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#Livre(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testLivreOperationImpossibleLocalNull()
			throws OperationImpossible, InvariantBroken {
		Document doc = new Livre(CODE_DF, null, TITRE_DF, AUTEUR_DF, ANNEE_DF,
				GENRE_DF, 10);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#Livre(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testLivreOperationImpossibleTitreNull()
			throws OperationImpossible, InvariantBroken {
		Document doc = new Livre(CODE_DF, LOCAL_DF, null, AUTEUR_DF, ANNEE_DF,
				GENRE_DF, 10);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#Livre(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testLivreOperationImpossibleAuthorNull()
			throws OperationImpossible, InvariantBroken {
		Document doc = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, null, ANNEE_DF,
				GENRE_DF, 10);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#Livre(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testLivreOperationImpossibleAnneeNull()
			throws OperationImpossible, InvariantBroken {
		Document doc = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF, null,
				GENRE_DF, 10);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#Livre(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testLivreOperationImpossibleGenreNull()
			throws OperationImpossible, InvariantBroken {
		Document doc = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, null, 10);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#Livre(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testLivreOperationImpossiblePages0()
			throws OperationImpossible, InvariantBroken {
		Document doc = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, -10);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#Livre(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testLivreOK() throws OperationImpossible, InvariantBroken {
		Document doc = new Livre(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 10);
		System.out.println(doc.toString());
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#getStat()}.
	 */
	@Test
	public void testGetStat() {
		System.out.println("Livre: " + Livre.getStat());
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#dureeEmprunt()}.
	 */
	@Test
	public void testDureeEmprunt() {
		assertTrue(((Livre) defaultOK1).dureeEmprunt() == 42);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#tarifEmprunt()}.
	 */
	@Test
	public void testTarifEmprunt() {
		assertTrue(((Livre) defaultOK1).tarifEmprunt() == 0.5);
	}

	/**
	 * Test method for {@link mediatheque.document.Livre#invariantLivre()}.
	 */
	@Test
	public void testInvariantLivre() {
		assertTrue(((Livre) defaultOK1).invariant());
	}

}
