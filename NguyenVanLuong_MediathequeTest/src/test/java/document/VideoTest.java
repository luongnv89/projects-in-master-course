/**
 * 
 */
package document;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import mediatheque.OperationImpossible;
import mediatheque.document.Video;

import org.junit.Before;
import org.junit.Test;

import util.InvariantBroken;

/**
 *
 *Test for {@link Video}
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class VideoTest extends DocumentTest {
	int DUREEFILE_DF = 100;
	String MENTIONLEGALE_DF = "DRAMA";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		defaultOK1 = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, MENTIONLEGALE_DF);
		defaultOK2 = new Video("CODE_DEFAULT_2", LOCAL_DF, TITRE_DF + "_2",
				AUTEUR_DF + "_2", ANNEE_DF, GENRE_DF, DUREEFILE_DF,
				MENTIONLEGALE_DF);
		empruntableDoc = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, MENTIONLEGALE_DF);
		empruntableDoc.metEmpruntable();
		pruntableDoc1 = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, MENTIONLEGALE_DF);

		pruntableDoc2 = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, MENTIONLEGALE_DF);
		pruntableDoc2.metEmpruntable();
		pruntableDoc2.emprunter();

		emprunteDoc = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, MENTIONLEGALE_DF);
		emprunteDoc.metEmpruntable();
		emprunteDoc.emprunter();
		prunteDoc1 = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, MENTIONLEGALE_DF);
		prunteDoc1.metEmpruntable();
		prunteDoc2 = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, MENTIONLEGALE_DF);
		prunteDoc2.metEmpruntable();
		prunteDoc2.emprunter();
	}

	/**
	 * Test method for {@link mediatheque.document.Video#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(((Video) defaultOK1).toString());
	}

	/**
	 * Test method for {@link mediatheque.document.Video#Video(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int, java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testVideoOK() throws OperationImpossible, InvariantBroken {
		Video video = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, MENTIONLEGALE_DF);
		System.out.println(video.toString());
	}

	/**
	 * Test method for {@link mediatheque.document.Video#Video(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int, java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testVideoDureeFilm0() throws OperationImpossible,
			InvariantBroken {
		Video video = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, 0, MENTIONLEGALE_DF);
	}

	/**
	 * Test method for {@link mediatheque.document.Video#Video(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, int, java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testVideoMentionLegaleNull() throws OperationImpossible,
			InvariantBroken {
		Video video = new Video(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, DUREEFILE_DF, null);
	}

	/**
	 * Test method for {@link mediatheque.document.Video#getStat()}.
	 */
	@Test
	public void testGetStat() {
		System.out.println("Video: " + Video.getStat());
	}

	/**
	 * Test method for {@link mediatheque.document.Video#dureeEmprunt()}.
	 */
	@Test
	public void testDureeEmprunt() {
		assertTrue(defaultOK1.dureeEmprunt() == 14);
	}

	/**
	 * Test method for {@link mediatheque.document.Video#tarifEmprunt()}.
	 */
	@Test
	public void testTarifEmprunt() {
		assertTrue(defaultOK1.tarifEmprunt() == 1.5);
	}

	/**
	 * Test method for {@link mediatheque.document.Video#getMentionLegale()}.
	 */
	@Test
	public void testGetMentionLegale() {
		assertTrue(((Video) defaultOK1).getMentionLegale().equals(
				MENTIONLEGALE_DF));
	}

	/**
	 * Test method for {@link mediatheque.document.Video#getDureeFilm()}.
	 */
	@Test
	public void testGetDureeFilm() {
		assertTrue(((Video) defaultOK1).getDureeFilm() == DUREEFILE_DF);
	}
}
