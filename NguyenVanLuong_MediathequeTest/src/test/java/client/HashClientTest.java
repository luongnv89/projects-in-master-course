/**
 * 
 */
package client;

import static org.junit.Assert.*;

import mediatheque.client.HashClient;

import org.junit.Before;
import org.junit.Test;

/**
 *
 *Test for {@link HashClient}
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class HashClientTest {

	HashClient defaultOK1;
	HashClient defaultOK2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		defaultOK1 = new HashClient("Nom", "Prenom");
		defaultOK2 = new HashClient("Nom2", "Prenom2");
	}

	/**
	 * Test method for {@link mediatheque.client.HashClient#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		assertFalse(defaultOK1.hashCode() == defaultOK2.hashCode());
	}

	/**
	 * Test method for {@link mediatheque.client.HashClient#HashClient(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testHashClientDefaultConstructor() {
		HashClient newHashClient = new HashClient("Nom", "Prenom");
		assertTrue(newHashClient.getNom().equals("Nom"));
		assertTrue(newHashClient.getPrenom().equals("Prenom"));
	}

	/**
	 * Test method for {@link mediatheque.client.HashClient#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(defaultOK1.equals(defaultOK1));
		assertFalse(defaultOK1.equals(defaultOK2));
	}

	/**
	 * Test method for {@link mediatheque.client.HashClient#getNom()}.
	 */
	@Test
	public void testGetNom() {
		assertTrue(defaultOK1.getNom().equals("Nom"));
	}

	/**
	 * Test method for {@link mediatheque.client.HashClient#getPrenom()}.
	 */
	@Test
	public void testGetPrenom() {
		assertTrue(defaultOK1.getPrenom().equals("Prenom"));
	}

}
