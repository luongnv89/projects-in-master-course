/**
 * 
 */
package mediatheque;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *Test for {@link Genre}
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class GenreTest {
	Genre genreDefaultOK;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		genreDefaultOK = new Genre("DEFAULT_GENRE");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		genreDefaultOK = null;
	}

	/**
	 * Test method for {@link mediatheque.Genre#Genre(java.lang.String)}.
	 */
	@Test
	public void testGenreDefaultConstruction() {
		Genre mGenre = new Genre("New Genre");
		assertTrue(mGenre.getNbEmprunts() == 0);
		assertTrue(mGenre.getNom().equals("New Genre"));
	}

	/**
	 * Test method for {@link mediatheque.Genre#emprunter()}.
	 */
	@Test
	public void testEmprunter() {
		int nbEmprunts_old = genreDefaultOK.getNbEmprunts();
		genreDefaultOK.emprunter();
		assertTrue(genreDefaultOK.getNbEmprunts() == nbEmprunts_old + 1);
	}

	/**
	 * Test method for {@link mediatheque.Genre#getNom()}.
	 */
	@Test
	public void testGetNom() {
		assertTrue(genreDefaultOK.getNom().equals("DEFAULT_GENRE"));
	}

	/**
	 * Test method for {@link mediatheque.Genre#toString()}.
	 */
	@Test
	public void testToString() {
		assertTrue(genreDefaultOK.toString().equals(
				"Genre: DEFAULT_GENRE, nbemprunts:0"));
	}

	/**
	 * Test method for {@link mediatheque.Genre#modifier(java.lang.String)}.
	 */
	@Test
	public void testModifier() {
		genreDefaultOK.modifier("NewName");
		assertTrue(genreDefaultOK.getNom().equals("NewName"));
	}

	/**
	 * Test method for {@link mediatheque.Genre#getNbEmprunts()}.
	 */
	@Test
	public void testGetNbEmprunts() {
		assertTrue(genreDefaultOK.getNbEmprunts() == 0);
	}

	/**
	 * Test method for {@link mediatheque.Genre#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(genreDefaultOK.equals(genreDefaultOK));
		Genre mGenre = new Genre("New Genre");
		assertFalse(genreDefaultOK.equals(mGenre));

	}

	/**
	 * Test method for {@link mediatheque.Genre#afficherStatistiques()}.
	 */
	@Test
	public void testAfficherStatistiques() {
		genreDefaultOK.afficherStatistiques();
	}

}
