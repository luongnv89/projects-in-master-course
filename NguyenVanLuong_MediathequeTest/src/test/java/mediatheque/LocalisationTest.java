/**
 * 
 */
package mediatheque;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *Test for {@link Localisation}
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class LocalisationTest {
	Localisation localisationDefault;
	String SALLE_DF = "SALLE_DEFAULT";
	String RAYON_DF = "RAYON_DEFAULT";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		localisationDefault = new Localisation(SALLE_DF, RAYON_DF);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		localisationDefault = null;
	}

	/**
	 * Test method for {@link mediatheque.Localisation#Localisation(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testLocalisation() {
		Localisation mLocalisation = new Localisation("mSalle", "mRayon");
		assertTrue(mLocalisation.getSalle().equals("mSalle"));
		assertTrue(mLocalisation.getRayon().equals("mRayon"));
	}

	/**
	 * Test method for {@link mediatheque.Localisation#getSalle()}.
	 */
	@Test
	public void testGetSalle() {
		assertTrue(localisationDefault.getSalle().equals(SALLE_DF));
	}

	/**
	 * Test method for {@link mediatheque.Localisation#setSalle(java.lang.String)}.
	 */
	@Test
	public void testSetSalle() {
		localisationDefault.setSalle("NewSalle");
		assertTrue(localisationDefault.getSalle().equals("NewSalle"));
	}

	/**
	 * Test method for {@link mediatheque.Localisation#getRayon()}.
	 */
	@Test
	public void testGetRayon() {
		assertTrue(localisationDefault.getRayon().equals(RAYON_DF));
	}

	/**
	 * Test method for {@link mediatheque.Localisation#setRayon(java.lang.String)}.
	 */
	@Test
	public void testSetRayon() {
		localisationDefault.setRayon("NewRayon");
		assertTrue(localisationDefault.getRayon().equals("NewRayon"));
	}

	/**
	 * Test method for {@link mediatheque.Localisation#toString()}.
	 */
	@Test
	public void testToString() {
		assertTrue(localisationDefault.toString().equals(
				"Salle/Rayon : SALLE_DEFAULT/RAYON_DEFAULT"));
	}

	/**
	 * Test method for {@link mediatheque.Localisation#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(localisationDefault.equals(localisationDefault));
		Localisation mLocalisation = new Localisation("mSalle", "mRayon");
		assertFalse(localisationDefault.equals(mLocalisation));
	}

}
