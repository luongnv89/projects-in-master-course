/**
 * 
 */
package mediatheque;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import mediatheque.client.CategorieClient;
import mediatheque.client.Client;
import mediatheque.document.Audio;
import mediatheque.document.Document;
import mediatheque.document.Livre;
import mediatheque.document.Video;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.InvariantBroken;

/**
 *Test for {@link Mediatheque}
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class MediathequeTest {

	Mediatheque mediatheque;
	Mediatheque mediatheque2;

	Genre genre1;
	Genre genre2;
	Genre genre3;

	Localisation local1;
	Localisation local2;
	Localisation local3;

	CategorieClient cat1;
	CategorieClient cat2;
	CategorieClient cat3;

	Document doc1;
	Document doc2;
	Document doc3;

	Client client1;
	Client client2;
	Client client3;

	FicheEmprunt ficheEmprunt1;
	FicheEmprunt ficheEmprunt2;
	FicheEmprunt ficheEmprunt3;
	private Localisation local4;
	private Localisation local5;
	private Localisation local6;
	private Livre doc4;
	private Genre genre4;
	private Livre doc5;
	private Localisation local7;
	private Livre doc6;
	private CategorieClient cat4;
	private CategorieClient cat5;
	private Client client4;
	private Genre genre5;
	private Livre doc7;
	private Livre doc8;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		// Create mediatheque
		mediatheque = new Mediatheque("mediatheque");
		mediatheque2 = new Mediatheque("tspmedia");
		mediatheque2.empty();

		// Create list genres
		genre1 = new Genre("Genre1");
		genre2 = new Genre("Genre2");
		genre3 = new Genre("Genre3");
		genre4 = new Genre("Genre4");
		genre5 = new Genre("Genre5");

		mediatheque2.ajouterGenre("Genre1");
		mediatheque2.ajouterGenre("Genre2");
		mediatheque2.ajouterGenre("Genre3");
		mediatheque2.ajouterGenre("Genre5");

		// Create list localisations
		local1 = new Localisation("Salle1", "Rayon1");
		local2 = new Localisation("Salle2", "Rayon2");
		local3 = new Localisation("Salle3", "Rayon3");
		local4 = new Localisation("Salle4", "Rayon4");
		local5 = new Localisation("Salle5", "Rayon5");
		local6 = new Localisation("Salle6", "Rayon6");
		local7 = new Localisation("Salle10", "Rayon10");

		mediatheque2.ajouterLocalisation("Salle1", "Rayon1");
		mediatheque2.ajouterLocalisation("Salle2", "Rayon2");
		mediatheque2.ajouterLocalisation("Salle3", "Rayon3");
		mediatheque2.ajouterLocalisation("Salle10", "Rayon10");

		// Create list CategorieClient
		cat1 = new CategorieClient("Cate1", 10, 0.5, 1.0, 1.5, false);
		cat2 = new CategorieClient("Cate2", 15, 1.5, 2.0, 3.5, false);
		cat3 = new CategorieClient("Cate3", 30, 3.5, 4.0, 5.5, false);
		cat4 = new CategorieClient("Cate4", 30, 3.5, 1.0, 5.5, false);
		cat5 = new CategorieClient("Cate5", 20, 3.5, 1.0, 5.5, false);

		mediatheque2.ajouterCatClient("Cate1", 10, 0.5, 1.0, 1.5, false);
		mediatheque2.ajouterCatClient("Cate2", 15, 1.5, 2.0, 3.5, false);
		mediatheque2.ajouterCatClient("Cate3", 30, 3.5, 4.0, 5.5, false);
		mediatheque2.ajouterCatClient("Cate4", 30, 3.5, 4.0, 5.5, false);
		mediatheque2.ajouterCatClient("Cate5", 20, 3.5, 1.0, 5.5, false);

		// Create list document
		doc1 = new Video("VIDEO_CODE2", local1, "VIDEO_TITRE2", "VIDEO_AUTHO2",
				"2013", genre1, 90, "ROMANTIC2");
		doc2 = new Audio("AUDIO_CODE2", local2, "AUDIO_TITRE3", "AUDIO_AUTHO3",
				"2014", genre2, "AUDIO_ROCK3");
		doc3 = new Livre("LIVRE_CODE3", local3, "LIVRE_TITRE3", "LIVRE_AUTHO3",
				"2015", genre3, 100);

		doc4 = new Livre("LIVRE_CODE4", local3, "LIVRE_TITRE4", "LIVRE_AUTHO4",
				"2015", genre4, 100);
		doc5 = new Livre("LIVRE_CODE5", local5, "LIVRE_TITRE4", "LIVRE_AUTHO4",
				"2015", genre3, 100);

		doc6 = new Livre("LIVRE_CODE6", local7, "LIVRE_TITRE4", "LIVRE_AUTHO4",
				"2015", genre5, 100);

		doc7 = new Livre("LIVRE_CODE7", local3, "LIVRE_TITRE3", "LIVRE_AUTHO3",
				"2015", genre3, 100);
		doc8 = new Livre("LIVRE_CODE8", local3, "LIVRE_TITRE3", "LIVRE_AUTHO3",
				"2015", genre3, 100);

		doc1.metEmpruntable();
		doc2.metEmpruntable();
		doc3.metEmpruntable();
		doc7.metEmpruntable();

		mediatheque2.ajouterDocument(doc1);
		mediatheque2.ajouterDocument(doc2);
		mediatheque2.ajouterDocument(doc3);
		mediatheque2.ajouterDocument(doc7);
		mediatheque2.ajouterDocument(doc8);

		// Create list Clients
		client1 = new Client("Client1Nom", "Client1Prenom", "Client1Address",
				cat1);
		client2 = new Client("Client2Nom", "Client2Prenom", "Client2Address",
				cat2);
		client2.emprunter();
		client2.emprunter();
		client2.restituer(false);
		client2.restituer(false);
		client3 = new Client("Client3Nom", "Client3Prenom", "Client3Address",
				cat3);
		client4 = new Client("Client4Nom", "Client4Prenom", "Client4Address",
				cat5);

		mediatheque2.inscrire("Client1Nom", "Client1Prenom", "Client1Address",
				"Cate1");
		mediatheque2.inscrire("Client2Nom", "Client2Prenom", "Client2Address",
				"Cate2");
		mediatheque2.inscrire("Client3Nom", "Client3Prenom", "Client3Address",
				"Cate3");
		// Create list ficheEmprunt
		// ficheEmprunt1 = new FicheEmprunt(mediatheque, client1, doc1);
		// ficheEmprunt2 = new FicheEmprunt(mediatheque, client2, doc2);
		// ficheEmprunt3 = new FicheEmprunt(mediatheque, client3, doc3);
		mediatheque2.emprunter("Client1Nom", "Client1Prenom", "VIDEO_CODE2");
		mediatheque2.emprunter("Client2Nom", "Client2Prenom", "AUDIO_CODE2");
		mediatheque2.emprunter("Client3Nom", "Client3Prenom", "LIVRE_CODE3");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		mediatheque = null;

		// clear list genres
		genre1 = null;
		genre2 = null;
		genre3 = null;

		// clear list localisations
		local1 = null;
		local2 = null;
		local3 = null;

		// clear list CategorieClient
		cat1 = null;
		cat2 = null;
		cat3 = null;

		// clear list document
		doc1 = null;
		doc2 = null;
		doc3 = null;

		// clear list Clients
		client1 = null;
		client2 = null;
		client3 = null;

		// clear list ficheEmprunt
		ficheEmprunt1 = null;
		ficheEmprunt2 = null;
		ficheEmprunt3 = null;
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#empty()}.
	 */
	@Test
	public void testEmpty() {
		mediatheque.empty();
		assertTrue(mediatheque.getGenresSize() == 0);
		assertTrue(mediatheque.getLocalisationsSize() == 0);
		assertTrue(mediatheque.getDocumentsSize() == 0);
		assertTrue(mediatheque.getClientsSize() == 0);
		assertTrue(mediatheque.getFicheEmpruntsSize() == 0);
		assertTrue(mediatheque.getCategoriesSize() == 0);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#chercherGenre(java.lang.String)}.
	 */
	@Test
	public void testChercherGenre() {
		System.out.println(mediatheque2.chercherGenre("Genre1").toString());
		assertNull(mediatheque2.chercherGenre("Genre4"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#supprimerGenre(java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testSupprimerGenreOperationImpossible()
			throws OperationImpossible {
		mediatheque2.supprimerGenre("Genre4");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#supprimerGenre(java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testSupprimerGenre() throws OperationImpossible {
		mediatheque2.supprimerGenre("Genre1");
		assertNull(mediatheque2.chercherGenre("Genre1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#ajouterGenre(java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testAjouterGenreOperationImpossible()
			throws OperationImpossible {
		mediatheque2.ajouterGenre("Genre1");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#ajouterGenre(java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testAjouterGenre() throws OperationImpossible {
		mediatheque2.ajouterGenre("Genre4");
		assertNotNull(mediatheque2.chercherGenre("Genre4"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierGenre(java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testModifierGenreOperationImpossible()
			throws OperationImpossible {
		mediatheque2.modifierGenre("Genre7", "Genre6");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierGenre(java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierGenre() throws OperationImpossible {
		mediatheque2.modifierGenre("Genre1", "Genre6");
		assertNotNull(mediatheque2.chercherGenre("Genre6"));
		assertNull(mediatheque2.chercherGenre("Genre1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#listerGenres()}.
	 */
	@Test
	public void testListerGenres() {
		mediatheque2.listerGenres();
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getGenreAt(int)}.
	 */
	@Test
	public void testGetGenreAt() {
		System.out.println(mediatheque2.getGenreAt(0));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getGenresSize()}.
	 */
	@Test
	public void testGetGenresSize() {
		System.out.println(mediatheque2.getGenresSize());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#supprimerLocalisation(java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testSupprimerLocalisationOperationImpossibleNotExist()
			throws OperationImpossible {
		mediatheque2.supprimerLocalisation("Salle5", "Rayon5");
	}

	/**
		 * Test method for {@link mediatheque.Mediatheque#supprimerLocalisation(java.lang.String, java.lang.String)}.
		 * @throws OperationImpossible 
		 */
	@Test(expected = OperationImpossible.class)
	public void testSupprimerLocalisationOperationImpossibleExistDoc()
			throws OperationImpossible {
		mediatheque2.supprimerLocalisation("Salle1", "Rayon1");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#supprimerLocalisation(java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testSupprimerLocalisation() throws OperationImpossible {
		mediatheque2.supprimerLocalisation("Salle10", "Rayon10");
		assertNull(mediatheque2.chercherLocalisation("Salle10", "Rayon10"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#chercherLocalisation(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testChercherLocalisation() {
		assertNull(mediatheque2.chercherLocalisation("Salle11", "Rayon11"));
		System.out.println(mediatheque2
				.chercherLocalisation("Salle1", "Rayon1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#ajouterLocalisation(java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testAjouterLocalisationOperationImpossible()
			throws OperationImpossible {
		mediatheque2.ajouterLocalisation("Salle1", "Rayon1");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#ajouterLocalisation(java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testAjouterLocalisation() throws OperationImpossible {
		mediatheque2.ajouterLocalisation("Salle15", "Rayon15");
		assertNotNull(mediatheque2.chercherLocalisation("Salle15", "Rayon15"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierLocalisation(mediatheque.Localisation, java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierLocalisationSalle() throws OperationImpossible {
		mediatheque2.modifierLocalisation(local1, "Salle7", "Rayon1");
		assertNull(mediatheque2.chercherLocalisation("Salle1", "Rayon1"));
		assertNotNull(mediatheque2.chercherLocalisation("Salle7", "Rayon1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierLocalisation(mediatheque.Localisation, java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierLocalisationRayon() throws OperationImpossible {
		mediatheque2.modifierLocalisation(local1, "Salle1", "Rayon7");
		assertNull(mediatheque2.chercherLocalisation("Salle1", "Rayon1"));
		assertNotNull(mediatheque2.chercherLocalisation("Salle1", "Rayon7"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierLocalisation(mediatheque.Localisation, java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierLocalisation() throws OperationImpossible {
		mediatheque2.modifierLocalisation(local1, "Salle7", "Rayon7");
		assertNull(mediatheque2.chercherLocalisation("Salle1", "Rayon1"));
		assertNotNull(mediatheque2.chercherLocalisation("Salle7", "Rayon7"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierLocalisation(mediatheque.Localisation, java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierLocalisationNoChange() throws OperationImpossible {
		mediatheque2.modifierLocalisation(local1, "Salle1", "Rayon1");
		assertNotNull(mediatheque2.chercherLocalisation("Salle1", "Rayon1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierLocalisation(mediatheque.Localisation, java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testModifierLocalisationOperationImpossible()
			throws OperationImpossible {
		Localisation local7 = new Localisation("Salle4", "Rayon2");
		mediatheque2.modifierLocalisation(local7, "Salle7", "Rayon7");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#listerLocalisations()}.
	 */
	@Test
	public void testListerLocalisations() {
		mediatheque2.listerLocalisations();
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getLocalisationAt(int)}.
	 */
	@Test
	public void testGetLocalisationAt() {
		System.out.println(mediatheque2.getLocalisationAt(0));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getLocalisationsSize()}.
	 */
	@Test
	public void testGetLocalisationsSize() {
		System.out.println(mediatheque2.getLocalisationsSize());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#chercherCatClient(java.lang.String)}.
	 */
	@Test
	public void testChercherCatClient() {
		assertNotNull(mediatheque2.chercherCatClient("Cate1"));
		assertNull(mediatheque2.chercherCatClient("Cate7"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#supprimerCatClient(java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testSupprimerCatClient() throws OperationImpossible {
		mediatheque2.supprimerCatClient("Cate4");
		assertNull(mediatheque2.chercherCatClient("Cate4"));
	}

	/**
		 * Test method for {@link mediatheque.Mediatheque#supprimerCatClient(java.lang.String)}.
		 * @throws OperationImpossible 
		 */
	@Test(expected = OperationImpossible.class)
	public void testSupprimerCatClientOperationImpossibleNotExist()
			throws OperationImpossible {
		mediatheque2.supprimerCatClient("Cate6");
	}

	/**
		 * Test method for {@link mediatheque.Mediatheque#supprimerCatClient(java.lang.String)}.
		 * @throws OperationImpossible 
		 */
	@Test(expected = OperationImpossible.class)
	public void testSupprimerCatClientOperationImpossibleCannot()
			throws OperationImpossible {
		mediatheque2.supprimerCatClient("Cate1");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#ajouterCatClient(java.lang.String, int, double, double, double, boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testAjouterCatClient() throws OperationImpossible {
		mediatheque2.ajouterCatClient("Cate6", 10, 1.5, 1.6, 1.7, false);
	}

	/**
	* Test method for {@link mediatheque.Mediatheque#ajouterCatClient(java.lang.String, int, double, double, double, boolean)}.
	* @throws OperationImpossible 
	*/
	@Test(expected = OperationImpossible.class)
	public void testAjouterCatClientOperationImpossible()
			throws OperationImpossible {
		mediatheque2.ajouterCatClient("Cate1", 10, 1.5, 1.6, 1.7, false);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierCatClient(mediatheque.client.CategorieClient, java.lang.String, int, double, double, double, boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierCatClientAll() throws OperationImpossible {
		mediatheque2.modifierCatClient(cat1, "Cate10", 17, 1.8, 1.9, 2.1, true);
		System.out.println(mediatheque2.chercherCatClient("Cate10"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierCatClient(mediatheque.client.CategorieClient, java.lang.String, int, double, double, double, boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierCatClientName() throws OperationImpossible {
		mediatheque2
				.modifierCatClient(cat1, "Cate10", 10, 0.5, 1.0, 1.5, false);
		System.out.println(mediatheque2.chercherCatClient("Cate10"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierCatClient(mediatheque.client.CategorieClient, java.lang.String, int, double, double, double, boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierCatClientMax() throws OperationImpossible {
		mediatheque2.modifierCatClient(cat1, "Cate1", 15, 0.5, 1.0, 1.5, false);
		System.out.println(mediatheque2.chercherCatClient("Cate11"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierCatClient(mediatheque.client.CategorieClient, java.lang.String, int, double, double, double, boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierCatCot() throws OperationImpossible {
		mediatheque2.modifierCatClient(cat1, "Cate1", 10, 1.5, 1.0, 1.5, false);
		System.out.println(mediatheque2.chercherCatClient("Cate1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierCatClient(mediatheque.client.CategorieClient, java.lang.String, int, double, double, double, boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierCatClientCoef() throws OperationImpossible {
		mediatheque2.modifierCatClient(cat1, "Cate1", 10, 0.5, 1.5, 1.5, false);
		System.out.println(mediatheque2.chercherCatClient("Cate1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierCatClient(mediatheque.client.CategorieClient, java.lang.String, int, double, double, double, boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierCatClientCodeReduc() throws OperationImpossible {
		mediatheque2.modifierCatClient(cat1, "Cate1", 10, 0.5, 1.0, 2.5, true);
		System.out.println(mediatheque2.chercherCatClient("Cate1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierCatClient(mediatheque.client.CategorieClient, java.lang.String, int, double, double, double, boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierCatClientNoChange() throws OperationImpossible {
		mediatheque2.modifierCatClient(cat1, "Cate1", 10, 0.5, 1.0, 2.5, false);
		System.out.println(mediatheque2.chercherCatClient("Cate1"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#listerCatsClient()}.
	 */
	@Test
	public void testListerCatsClient() {
		mediatheque2.listerCatsClient();
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getCategorieAt(int)}.
	 */
	@Test
	public void testGetCategorieAt() {
		System.out.println(mediatheque2.getCategorieAt(0));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getCategoriesSize()}.
	 */
	@Test
	public void testGetCategoriesSize() {
		System.out.println(mediatheque2.getCategoriesSize());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#chercherDocument(java.lang.String)}.
	 */
	@Test
	public void testChercherDocument() {
		assertNotNull(mediatheque2.chercherDocument("VIDEO_CODE2"));
		assertNull(mediatheque2.chercherDocument("VIDEO_CODE6"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#ajouterDocument(mediatheque.document.Document)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testAjouterDocumentOperationImpossibleExist()
			throws OperationImpossible {
		mediatheque2.ajouterDocument(doc1);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#ajouterDocument(mediatheque.document.Document)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testAjouterDocumentOperationImpossibleGenreNotExist()
			throws OperationImpossible {
		mediatheque2.ajouterDocument(doc4);
	}

	/**
	* Test method for {@link mediatheque.Mediatheque#ajouterDocument(mediatheque.document.Document)}.
	* @throws OperationImpossible 
	*/
	@Test(expected = OperationImpossible.class)
	public void testAjouterDocumentOperationImpossibleLocalisationNotExist()
			throws OperationImpossible {
		mediatheque2.ajouterDocument(doc5);
	}

	/**
	* Test method for {@link mediatheque.Mediatheque#ajouterDocument(mediatheque.document.Document)}.
	* @throws OperationImpossible 
	*/
	@Test
	public void testAjouterDocumentOK() throws OperationImpossible {
		mediatheque2.ajouterDocument(doc6);
		assertNotNull(mediatheque2.chercherDocument("LIVRE_CODE6"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#retirerDocument(java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testRetirerDocumentOperationImpossibleNotExist()
			throws OperationImpossible {
		mediatheque2.retirerDocument("VIDEO_CODE7");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#retirerDocument(java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testRetirerDocumentOperationImpossibleNotEmprunte()
			throws OperationImpossible {
		mediatheque2.retirerDocument("LIVRE_CODE3");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#retirerDocument(java.lang.String)}.
	 * @throws OperationImpossible 
	 * @throws InvariantBroken 
	 */
	@Test
	public void testRetirerDocumentOK() throws OperationImpossible,
			InvariantBroken {
		mediatheque2.retirerDocument("LIVRE_CODE7");
		assertNull(mediatheque2.chercherDocument("LIVRE_CODE7"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#metEmpruntable(java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testMetEmpruntableOK() throws OperationImpossible,
			InvariantBroken {
		mediatheque2.metEmpruntable("LIVRE_CODE8");
		System.out.println(mediatheque2.chercherDocument("LIVRE_CODE8"));
	}

	/**
		 * Test method for {@link mediatheque.Mediatheque#metEmpruntable(java.lang.String)}.
		 * @throws InvariantBroken 
		 * @throws OperationImpossible 
		 */
	@Test(expected = OperationImpossible.class)
	public void testMetEmpruntableOperationImpossible()
			throws OperationImpossible, InvariantBroken {
		mediatheque2.metEmpruntable("VIDEO_CODE7");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#metConsultable(java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testMetConsultableOK() throws OperationImpossible,
			InvariantBroken {
		mediatheque2.metConsultable("LIVRE_CODE7");
		System.out.println(mediatheque2.chercherDocument("LIVRE_CODE7"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#metConsultable(java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testMetConsultableOperationImpossible()
			throws OperationImpossible, InvariantBroken {
		mediatheque2.metConsultable("VIDEO_CODE8");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#listerDocuments()}.
	 */
	@Test
	public void testListerDocuments() {
		mediatheque2.listerDocuments();
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getDocumentAt(int)}.
	 */
	@Test
	public void testGetDocumentAt() {
		System.out.println(mediatheque2.getDocumentAt(0));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getDocumentsSize()}.
	 */
	@Test
	public void testGetDocumentsSize() {
		System.out.println(mediatheque2.getDocumentsSize());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#emprunter(java.lang.String, java.lang.String, java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testEmprunter() throws OperationImpossible, InvariantBroken {
		mediatheque2.emprunter("Client1Nom", "Client1Prenom", "LIVRE_CODE7");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#listerFicheEmprunts()}.
	 */
	@Test
	public void testListerFicheEmprunts() {
		mediatheque2.listerFicheEmprunts();
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getFicheEmpruntAt(int)}.
	 */
	@Test
	public void testGetFicheEmpruntAt() {
		System.out.println(mediatheque2.getFicheEmpruntAt(0).toString());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getFicheEmpruntsSize()}.
	 */
	@Test
	public void testGetFicheEmpruntsSize() {
		System.out.println(mediatheque2.getFicheEmpruntsSize());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#inscrire(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testInscrireStringStringStringString()
			throws OperationImpossible {
		mediatheque2.inscrire("NewClient", "NewClientPrenom",
				"NewClientAddress", "Cate1");
		System.out.println(mediatheque2.chercherClient("NewClient",
				"NewClientPrenom").toString());
		assertTrue(mediatheque2.chercherClient("NewClient", "NewClientPrenom")
				.getReduc() == 0);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#inscrire(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testInscrireStringStringStringStringCatNotExist()
			throws OperationImpossible {
		mediatheque2.inscrire("NewClient", "NewClientPrenom",
				"NewClientAddress", "Cate15");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#inscrire(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testInscrireStringStringStringStringInt()
			throws OperationImpossible {
		mediatheque2.inscrire("NewClientNom", "NewClientPrenom",
				"NewClientAddress", "Cate1", 15);
		System.out.println(mediatheque2.chercherClient("NewClientNom",
				"NewClientPrenom").toString());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#inscrire(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testInscrireStringStringStringStringIntCateNotExist()
			throws OperationImpossible {
		mediatheque2.inscrire("NewClientNom", "NewClientPrenom",
				"NewClientAddress", "Cate1", 15);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#resilier(java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testResilierOperationImpossibleNotExist()
			throws OperationImpossible {
		mediatheque2.resilier("Client9Nom", "Client9Prenom");
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#resilier(java.lang.String, java.lang.String)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testResilierOperationImpossibleEncourse()
			throws OperationImpossible {
		mediatheque2.resilier("Client2Nom", "Client2Prenom");
	}

	// /**
	// * Test method for {@link
	// mediatheque.Mediatheque#resilier(java.lang.String, java.lang.String)}.
	// * @throws OperationImpossible
	// */
	// @Test
	// public void testResilierOK() throws OperationImpossible {
	// mediatheque2.resilier("Client2Nom", "Client2Prenom");
	// }

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierClient(mediatheque.client.Client, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testModifierClientOperationImpossible()
			throws OperationImpossible {
		Client client = new Client("Hello", "Iam find");
		mediatheque2.modifierClient(client, "NewClient1", "NewClient1Prenom",
				"NewClient1Address", "Cate2", 14);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierClient(mediatheque.client.Client, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierClientAllOK() throws OperationImpossible {
		mediatheque2.modifierClient(client1, "NewClient1", "NewClient1Prenom",
				"NewClient1Address", "Cate2", 14);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierClient(mediatheque.client.Client, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierClientNom() throws OperationImpossible {
		mediatheque2.modifierClient(client1, "NewClient1Nom", "Client1Prenom",
				"Client1Address", "Cate1", 10);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierClient(mediatheque.client.Client, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierClientPreNom() throws OperationImpossible {
		mediatheque2.modifierClient(client1, "Client1Nom", "NewClient1Prenom",
				"Client1Address", "Cate1", 10);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierClient(mediatheque.client.Client, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierClientAddress() throws OperationImpossible {
		mediatheque2.modifierClient(client1, "Client1Nom", "Client1Prenom",
				"NewClient1Address", "Cate1", 10);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierClient(mediatheque.client.Client, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierClientCate() throws OperationImpossible {
		mediatheque2.modifierClient(client1, "Client1Nom", "Client1Prenom",
				"Client1Address", "Cate2", 10);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierClient(mediatheque.client.Client, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierClientCodeReduc() throws OperationImpossible {
		mediatheque2.modifierClient(client1, "Client1Nom", "Client1Prenom",
				"Client1Address", "Cate1", 15);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#modifierClient(mediatheque.client.Client, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testModifierClientNothingChange() throws OperationImpossible {
		mediatheque2.modifierClient(client1, "Client1Nom", "Client1Prenom",
				"Client1Address", "Cate1", 10);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#changerCategorie(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testChangerCategorieOperationImpossibleNoClient()
			throws OperationImpossible {
		mediatheque2.changerCategorie("Client10Nom", "Client10Prenom", "Cate2",
				15);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#changerCategorie(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testChangerCategorieOperationImpossibleNoCate()
			throws OperationImpossible {
		mediatheque2.changerCategorie("Client1Nom", "Client1Prenom", "Cate9",
				15);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#changerCategorie(java.lang.String, java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testChangerCategorie() throws OperationImpossible {
		mediatheque2.changerCategorie("Client1Nom", "Client1Prenom", "Cate2",
				15);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#changerCodeReduction(java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testChangerCodeReductionOperationImpossibleNoClient()
			throws OperationImpossible {
		mediatheque2.changerCodeReduction("Client10Nom", "Client10Prenom", 16);
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#changerCodeReduction(java.lang.String, java.lang.String, int)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testChangerCodeReductionOperationImpossibleNoCodeReduce()
			throws OperationImpossible {
		mediatheque2.changerCodeReduction("Client4Nom", "Client4Prenom", 16);
	}

	// /**
	// * Test method for {@link
	// mediatheque.Mediatheque#changerCodeReduction(java.lang.String,
	// java.lang.String, int)}.
	// * @throws OperationImpossible
	// */
	// @Test
	// public void testChangerCodeReduction() throws OperationImpossible {
	// mediatheque2.changerCodeReduction("Client2Nom", "Client2Prenom", 16);
	// System.out.println(mediatheque2.chercherClient("Client1Nom",
	// "Client1Prenom"));
	// }

	/**
	 * Test method for {@link mediatheque.Mediatheque#chercherClient(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testChercherClient() {
		assertNotNull(mediatheque2
				.chercherClient("Client1Nom", "Client1Prenom"));
		assertNull(mediatheque2.chercherClient("Client10Nom", "Client10Prenom"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#listerClients()}.
	 */
	@Test
	public void testListerClients() {
		mediatheque2.listerClients();
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#existeClient(mediatheque.client.CategorieClient)}.
	 */
	@Test
	public void testExisteClient() {
		assertTrue(mediatheque2.existeClient(cat1));
		assertFalse(mediatheque2.existeClient(cat4));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getClientAt(int)}.
	 */
	@Test
	public void testGetClientAt() {
		System.out.println(mediatheque2.getClientAt(0).toString());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getClientsSize()}.
	 */
	@Test
	public void testGetClientsSize() {
		System.out.println(mediatheque2.getClientsSize());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#findClient(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testFindClient() {
		assertNotNull(mediatheque2.findClient("Client1Nom", "Client1Prenom"));
		assertNull(mediatheque2.findClient("Client10Nom", "Client10Prenom"));
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#afficherStatistiques()}.
	 */
	@Test
	public void testAfficherStatistiques() {
		mediatheque2.afficherStatistiques();
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#getNom()}.
	 */
	@Test
	public void testGetNom() {
		System.out.println(mediatheque2.getNom());
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#initFromFile()}.
	 */
	@Test
	public void testInitFromFile() {
		mediatheque2.initFromFile();
	}

	/**
	 * Test method for {@link mediatheque.Mediatheque#saveToFile()}.
	 */
	@Test
	public void testSaveToFile() {
		mediatheque2.saveToFile();
	}

}
