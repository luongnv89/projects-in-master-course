import util.InvariantBroken;
import mediatheque.*;
import mediatheque.document.*;


public class InitMedia {
        static Mediatheque media = null;

        /**
         * Fonction main.
         * @throws InvariantBroken 
         */
        public static void main(String[] args) throws InvariantBroken {
                if (args.length == 0) {
                        throw new IllegalArgumentException("Mediatheque needs at least its name as an argument");
                }
                media = new Mediatheque(args[0]);
                media.empty();
                boolean res = initMedia(media);
                if(!res)System.out.println("No media data");
                res = media.saveToFile();
                if(!res)System.out.println("Unable to save media data");
        }
        /**
         * Initialisation de la mediatheque avec quelques objets au depart.
         * 
         * @param media
         *            la mediatheque.
         * @throws InvariantBroken 
         * @throws OperationImpossible
         *             exception levee.
         */
        private static boolean initMedia(Mediatheque media) throws InvariantBroken{
                /* Ajout des genres et localisations */
                try {
                        media.ajouterLocalisation("Musset", "1");
                        media.ajouterLocalisation("Musset", "2");
                        media.ajouterLocalisation("Lumieres", "1");
                        media.ajouterLocalisation("Lumieres", "2");
                        media.ajouterLocalisation("Chopin", "1");
                        media.ajouterLocalisation("Chopin", "2");

                        media.ajouterGenre("Informatique");
                        media.ajouterGenre("Thriller");
                        media.ajouterGenre("Rock");
                } catch (OperationImpossible o) {
                        System.err.println("Mediatheque: operation impossible car \""
                                        + o.getMessage() + "\"");
                        return false;
                }
                /* On cree les documents */
                Livre java;
                Video tor;
                Audio wyatt;
                Genre g ;
                Localisation l;
                try {
                        g = media.chercherGenre("Informatique");
                        l = media.chercherLocalisation("Musset", "2");
                        java = new Livre("001", l, "Java facile", "Arsene Lupin", "1998",
                                        g, 512);
                        media.ajouterDocument(java);
                        
            media.metEmpruntable(java.getCode());
                } catch (OperationImpossible o) {
                        System.err.println("Mediatheque: operation impossible car \""
                                        + o.getMessage() + "\"");
                }
                try {
                        g = media.chercherGenre("Thriller");
                        l = media.chercherLocalisation("Lumieres", "1");
                        tor = new Video("002", l, "Terminator", "Droopy", "1998", g,
                                        120, "Interdit aux moins de 13 ans");
                        media.ajouterDocument(tor);
            media.metEmpruntable(tor.getCode());
                } catch (OperationImpossible o) {
                        System.err.println("Mediatheque: operation impossible car \""
                                        + o.getMessage() + "\"");
                }    
                try {
                        g = media.chercherGenre("Rock");
                        l = media.chercherLocalisation("Chopin", "1");
                        wyatt = new Audio("003", l, "Rock bottom", "Rober Wyatt", "1973",
                                        g, "Progressif");
                        media.ajouterDocument(wyatt);
                } catch (OperationImpossible o) {
                        System.err.println("Mediatheque: operation impossible car \""
                                        + o.getMessage() + "\"");
                }
                /* Ajout des categories de clients 
                 * 	public CategorieClient(String name, int max, double cot, double coefDuree, double coefTarif, boolean codeReducUsed ){
                 */
        try {
            media.ajouterCatClient("TarifNormal", 5, 0.0, 1.0, 1.0, false);
            media.ajouterCatClient("TarifReduit", 3, 0.0, .5, .5, true);
            media.ajouterCatClient("Abonne", 7, 25.0, 2., 2., false);
            media.inscrire("Dupond", "Francois", "10, rue de Picardie","Abonne");
            media.inscrire("Durand", "Christian", "25, rue des Saussaies","TarifNormal");
            media.inscrire("Martin", "Daniel","2, rue de la plaine", "TarifReduit",1);
         } catch (OperationImpossible o) {
            System.err.println("Mediatheque: operation impossible car \""
                    + o.getMessage() + "\"");
        }  
                return true;
        }
}