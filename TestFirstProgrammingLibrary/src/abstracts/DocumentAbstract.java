package abstracts;

import models.DocumentGenre;

public abstract class DocumentAbstract {

	protected static int TOTAL_DOCUMENTS = 0;
	protected String ID;
	protected int location;
	protected String title;
	protected String author;
	protected int year;

	public DocumentAbstract(String ID, int location, String title,
			String author, int year) {
		TOTAL_DOCUMENTS++;
		this.ID = ID;
		this.location = location;
		this.title = title;
		this.author = author;
		this.year = year;
	}

	public String getID() {
		return ID;
	}

	public int getLocation() {
		return location;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public int getYear() {
		return year;
	}

	public static int getTotal() {
		return TOTAL_DOCUMENTS;
	}

	public abstract DocumentGenre getGenre();

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentAbstract other = (DocumentAbstract) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (location != other.location)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (year != other.year)
			return false;
		return true;
	}

}
