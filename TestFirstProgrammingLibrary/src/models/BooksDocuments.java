package models;

import abstracts.DocumentAbstract;

public class BooksDocuments extends DocumentAbstract {

	private DocumentGenre genre;
	private int pages;

	public BooksDocuments(String string, int i, String string2, String string3,
			int j, DocumentGenre novel, int k) {
		super(string, i, string2, string3, j);
		genre = novel;
		pages = k;
	}

	public DocumentGenre getGenre() {
		return genre;
	}

	public int getPages() {
		return pages;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return super.equals(obj);
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BooksDocuments other = (BooksDocuments) obj;
		if (genre != other.genre)
			return false;
		if (pages != other.pages)
			return false;
		return super.equals(obj);
	}

}
