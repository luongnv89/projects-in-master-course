package models;

public class Customer {

	String name;
	String surname;
	String address;
	/**
	 * The category of customer: <li>1: Normal customer <li>2: Discount customer
	 * <li>3: Subcriber customer
	 */
	int category;
	private String username;

	public Customer(String string, String string2, String string3,
			String string4, int i) {
		username = string;
		name = string2;
		surname = string3;
		address = string4;
		category = 1;
	}

	public String getName() {
		return name;
	}

	public String getSurName() {
		return surname;
	}

	public int getCategory() {
		return category;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (category != other.category)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

	public void updateName(String string) {
		this.name = string;
	}

	public void updateSurName(String string) {
		this.surname = string;
	}

	public void updateAddress(String string) {
		this.address = string;
	}

	public void updateCategory(int i) {
		this.category = i;
	}

	public String getID() {
		return username;
	}

	public String getAddress() {
		// TODO Auto-generated method stub
		return address;
	}

}
