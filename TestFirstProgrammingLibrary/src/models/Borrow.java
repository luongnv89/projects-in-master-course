package models;

import abstracts.DocumentAbstract;

public class Borrow {

	private String ID;
	private Customer customer;
	private DocumentAbstract listDocuments;
	private String date;

	public Borrow(String string, Customer customer2, DocumentAbstract document,
			String date2) {
		ID = string;
		customer = customer2;
		listDocuments = document;
		date = date2;
	}

	public String getID() {
		// TODO Auto-generated method stub
		return ID;
	}

	public Customer getCustomer() {
		return customer;
	}

	public DocumentAbstract getListDocuments() {
		return listDocuments;
	}

	public String getDate() {
		return date;
	}

}
