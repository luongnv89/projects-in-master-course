package models;

import abstracts.DocumentAbstract;

public class DVDsDocuments extends DocumentAbstract {

	private DocumentGenre genre;
	private int duration;
	private int broadcast;

	public DVDsDocuments(String string, int i, String string2, String string3,
			int j, DocumentGenre comedy, int k, int l) {
		super(string, i, string2, string3, j);
		genre = comedy;
		duration = k;
		broadcast = l;
	}

	@Override
	public DocumentGenre getGenre() {
		return genre;
	}

	public int getDuration() {
		return duration;
	}

	public int getBroadCast() {
		return broadcast;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return super.equals(obj);
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DVDsDocuments other = (DVDsDocuments) obj;
		if (broadcast != other.broadcast)
			return false;
		if (duration != other.duration)
			return false;
		if (genre != other.genre)
			return false;
		return super.equals(obj);
	}

}
