package models;

public enum DocumentGenre {
	NOVEL, CLASSIC, COMEDY, OPERA
}
