package tests;

import static org.junit.Assert.assertEquals;
import models.CDsDocuments;
import models.DocumentGenre;

import org.junit.BeforeClass;
import org.junit.Test;

public class CDsDocumentsTest extends DocumentAbstractTest {

	@BeforeClass
	public static void setUp() throws Exception {
		document1 = new CDsDocuments("CDs1", 5, "CD Happy", "NGUYEN VIET HA",
				1999, DocumentGenre.CLASSIC, DocumentGenre.OPERA);
		document2 = new CDsDocuments("CDs1", 5, "CD Happy", "NGUYEN VIET HA",
				1999, DocumentGenre.CLASSIC, DocumentGenre.OPERA);
	}

	@Test
	public void testGetMethod() {
		assertEquals(document1.getID(), "CDs1");
		assertEquals(document1.getLocation(), 5);
		assertEquals(document1.getTitle(), "CD Happy");
		assertEquals(document1.getAuthor(), "NGUYEN VIET HA");
		assertEquals(document1.getYear(), 1999);
		assertEquals(((CDsDocuments) document1).getGenre(),
				DocumentGenre.CLASSIC);
		assertEquals(((CDsDocuments) document1).getClassification(),
				DocumentGenre.OPERA);
	}

}
