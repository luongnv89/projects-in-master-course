package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BooksDocumentsTest.class, CDsDocumentsTest.class,
		CustomerTest.class, DVDsDocumentsTest.class })
public class AllTests {

}
