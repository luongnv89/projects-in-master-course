package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import abstracts.DocumentAbstract;

public class DocumentAbstractTest {

	static DocumentAbstract document1;
	static DocumentAbstract document2;

	@Test
	public void testEquals() {
		assertTrue(document1.equals(document2));
		assertTrue(document1.equals(document1));
		assertFalse(document1.equals(null));
		assertFalse(document1.equals(78));
	}

}
