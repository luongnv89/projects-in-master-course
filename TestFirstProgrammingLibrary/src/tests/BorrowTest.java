package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import models.BOOKGenre;
import models.BooksDocuments;
import models.Borrow;
import models.Customer;

import org.junit.BeforeClass;
import org.junit.Test;

import abstracts.DocumentAbstract;

public class BorrowTest {

	static Borrow borrow;
	static Customer customer = new Customer("user1", "NGUYEN", "VAN A",
			"Telecom Sudparis1", 1);
	static DocumentAbstract document = new BooksDocuments("BOOK1", 7,
			"Robinson Cruiso", "Jack London", 2001, BOOKGenre.NOVEL, 130);
	static String date = "17/04/2013";

	@BeforeClass
	public static void setUp() throws Exception {

		borrow = new Borrow("br1", customer, document, date);
	}

	@Test
	public void testGetMethod() {
		assertEquals(borrow.getID(), "br1");
		assertTrue(borrow.getCustomer().equals(customer));
		assertEquals(borrow.getListDocuments(), document);
		assertEquals(borrow.getDate(), date);
	}

}
