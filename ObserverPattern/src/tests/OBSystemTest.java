/**
 * 
 */
package tests;

import static org.junit.Assert.fail;

import java.util.Random;

import models.OBClient;
import models.OBServer;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 *
 */
public class OBSystemTest extends OBSpecificationTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Random ran = new Random();

		int numberObserver = 5 + ran.nextInt(10);
		for (int i = 0; i < numberObserver; i++) {
			int isClient = ran.nextInt(2);
			int takeAction = ran.nextInt(2);
			if (isClient == 0) {
				OBClient newClient = new OBClient();
				if (takeAction == 1)
					newClient.takeTicketNumber();
			} else {
				OBServer newServer = new OBServer();
				if (takeAction == 1)
					newServer.leaveOutSystem();
			}
		}
	}

	@Test
	public void test() {
		fail("Not yet implemented"); // TODO
	}

}
