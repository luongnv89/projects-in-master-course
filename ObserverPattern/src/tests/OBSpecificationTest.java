/**
 * 
 */
package tests;

import models.OBSystem;
import models.TicketMachine;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import specifications.MyObserver;
import utils.MessageConsole;

/**
 * 
 *
 */
public class OBSpecificationTest {
	static MyObserver MACHINE;
	static MyObserver SYSTEM;
	protected MyObserver ob;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpClass() throws Exception {
		MACHINE = new TicketMachine();
		SYSTEM = new OBSystem();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		ob = null;
	}

	@Test
	public void testObNotify() {
		MessageConsole.inforMessage("SYSTEM INFORMATION");
		SYSTEM.obNotify();
	}

}
