/**
 * 
 */
package tests;

import models.OBServer;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 *
 */
public class OBServerTest extends OBSpecificationTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ob = new OBServer();
	}

	@Test
	public void testLeaveOutSystem() {
		((OBServer) ob).leaveOutSystem();
	}

}
