/**
 * 
 */
package tests;

import models.OBClient;

import org.junit.Before;
import org.junit.Test;

import utils.MessageConsole;

/**
 * 
 *
 */
public class OBClientTest extends OBSpecificationTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ob = new OBClient();
	}

	/**
	 * Test method for {@link models.OBClient#takeTiketNumber()}.
	 */
	@Test
	public void testTakeTiketNumber() {
		MessageConsole.inforMessage(((OBClient) ob).getId()
				+ "Client take tiket");
		((OBClient) ob).takeTicketNumber();
	}
}
