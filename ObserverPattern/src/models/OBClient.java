/**
 * 
 */
package models;

import specifications.MyObserver;
import utils.MessageConsole;

/**
 * 
 *
 */
public class OBClient implements MyObserver {

	String id;
	int tiketNumber;

	/**
	 * @param id
	 */
	public OBClient() {
		this.id = "Client" + String.valueOf(OBSystem.IDCLIENT);
		MessageConsole.inforMessage(getId() + " arrives!");
		OBSystem.IDCLIENT++;
		OBSystem.registerObserver(this);
		tiketNumber = -1;
	}

	public void takeTicketNumber() {
		tiketNumber = TicketMachine.NEXT_NUMBER;
		TicketMachine.NEXT_NUMBER++;
	}

	@Override
	public void obNotify() {
		MessageConsole.inforMessage(toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OBClient [id=" + id + ", tiketNumber=" + tiketNumber + "]";
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the tiketNumber
	 */
	public int getTiketNumber() {
		return tiketNumber;
	}

	/**
	 * @param tiketNumber the tiketNumber to set
	 */
	public void setTiketNumber(int tiketNumber) {
		this.tiketNumber = tiketNumber;
	}

}
