/**
 * 
 */
package models;

import specifications.MyObserver;
import utils.MessageConsole;

/**
 * 
 *
 */
public class TicketMachine implements MyObserver {

	public static int NEXT_NUMBER;

	/**
	 * 
	 */
	public TicketMachine() {
		NEXT_NUMBER = 1;
		OBSystem.registerObserver(this);
	}

	@Override
	public void obNotify() {
		MessageConsole.inforMessage(toString());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TicketMachine #: " + NEXT_NUMBER;
	}

}
