/**
 * 
 */
package models;

import specifications.MyObserver;
import utils.MessageConsole;

/**
 * 
 *
 */
public class OBServer implements MyObserver {
	String id;
	boolean status;

	/**
	 * @param id
	 */
	public OBServer() {
		this.id = "Server_" + String.valueOf(OBSystem.IDSERVER);
		MessageConsole.inforMessage(getId() + " arrives and free");
		OBSystem.IDSERVER++;
		OBSystem.registerObserver(this);
		status = true;
	}

	@Override
	public void obNotify() {
		MessageConsole.inforMessage(toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OBServer [id=" + id + ", status=" + status + "]";
	}

	public void leaveOutSystem() {
		if (status)
			OBSystem.unregisterObserver(this);
		else {
			MessageConsole.inforMessage(this.id
					+ " is busy. Cannot leave out of system");
		}
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

}
