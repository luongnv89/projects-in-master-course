/**
 * 
 */
package models;

import java.util.ArrayList;

import specifications.MyObserver;
import utils.MessageConsole;

/**
 * 
 *
 */
public class OBSystem implements MyObserver {
	public static int IDCLIENT;
	public static int IDSERVER;
	static ArrayList<MyObserver> observerCollection = new ArrayList<MyObserver>();

	/**
	 * 
	 */
	public OBSystem() {
		IDCLIENT = 1;
		IDSERVER = 1;
	}

	@Override
	public void obNotify() {
		MessageConsole.inforMessage("System");
		for (int i = 0; i < observerCollection.size(); i++) {
			observerCollection.get(i).obNotify();
		}

	}

	public static void registerObserver(MyObserver ob) {
		observerCollection.add(ob);
	}

	public static void unregisterObserver(MyObserver ob) {
		observerCollection.remove(ob);
	}

}
