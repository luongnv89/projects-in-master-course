/**
 * 
 */
package louis.abstractions;

import louis.specifications.EncryptSpecifications;

/**
 * @author luongnv89
 *
 */
public abstract class EncryptAbstraction implements EncryptSpecifications{
	String encryptAlgorithmName;

	/**
	 * @param encryptAlgorithmName
	 */
	public EncryptAbstraction(String encryptAlgorithmName) {
		super();
		this.encryptAlgorithmName = encryptAlgorithmName;
	}

	/**
	 * @return the encryptAlgorithmName
	 */
	public String getEncryptAlgorithmName() {
		return encryptAlgorithmName;
	}
	
	
}
