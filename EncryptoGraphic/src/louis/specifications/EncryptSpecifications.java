/**
 * 
 */
package louis.specifications;

/**
 * @author luongnv89
 *
 */
public interface EncryptSpecifications {
	/**
	 * Encrypt a message by the provide key
	 * @param msg message
	 * @param key key to encypt
	 * @return the encrypt message
	 */
	public String encrypt(String msg, String key);
}
