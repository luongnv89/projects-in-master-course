package util;

public class InvariantBroken extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8534543357597239807L;
	String message;
	
	public InvariantBroken(String message){
		super(message);
	}

}
