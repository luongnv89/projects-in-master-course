/**
 * 
 */
package document;

import static org.junit.Assert.assertTrue;
import mediatheque.OperationImpossible;
import mediatheque.document.Audio;
import mediatheque.document.Document;

import org.junit.Before;
import org.junit.Test;

import util.InvariantBroken;

/**
 * Test for {@link Audio}
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class AudioTest extends DocumentTest {

	String CLASS_DF = "Default Classification";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		defaultOK1 = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, CLASS_DF);
		defaultOK2 = new Audio("CODE_DEFAULT_2", LOCAL_DF, TITRE_DF + "_2",
				AUTEUR_DF + "_2", ANNEE_DF, GENRE_DF, CLASS_DF);
		empruntableDoc = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, CLASS_DF);
		empruntableDoc.metEmpruntable();
		pruntableDoc1 = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, CLASS_DF);

		pruntableDoc2 = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, CLASS_DF);
		pruntableDoc2.metEmpruntable();
		pruntableDoc2.emprunter();

		emprunteDoc = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, CLASS_DF);
		emprunteDoc.metEmpruntable();
		emprunteDoc.emprunter();
		prunteDoc1 = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, CLASS_DF);
		prunteDoc1.metEmpruntable();
		prunteDoc2 = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, CLASS_DF);
		prunteDoc2.metEmpruntable();
		prunteDoc2.emprunter();
	}

	/**
	 * Test method for {@link mediatheque.document.Audio#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(((Audio) defaultOK1).toString());
	}

	/**
	 * Test method for {@link mediatheque.document.Audio#Audio(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testAudioOperationImpossibleClassificationNull()
			throws OperationImpossible, InvariantBroken {
		Document doc = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, null);
	}

	/**
	 * Test method for {@link mediatheque.document.Audio#Audio(java.lang.String, mediatheque.Localisation, java.lang.String, java.lang.String, java.lang.String, mediatheque.Genre, java.lang.String)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testAudioOK() throws OperationImpossible, InvariantBroken {
		Document doc = new Audio(CODE_DF, LOCAL_DF, TITRE_DF, AUTEUR_DF,
				ANNEE_DF, GENRE_DF, CLASS_DF);
		System.out.println(doc.toString());
	}

	/**
	 * Test method for {@link mediatheque.document.Audio#getStat()}.
	 */
	@Test
	public void testGetStat() {
		System.out.println("Audio: " + Audio.getStat());
	}

	/**
	 * Test method for {@link mediatheque.document.Audio#getClassification()}.
	 */
	@Test
	public void testGetClassification() {
		assertTrue(((Audio) defaultOK1).getClassification().equals(CLASS_DF));
	}

	/**
	 * Test method for {@link mediatheque.document.Audio#dureeEmprunt()}.
	 */
	@Test
	public void testDureeEmprunt() {
		assertTrue(((Audio) defaultOK1).dureeEmprunt() == 28);
	}

	/**
	 * Test method for {@link mediatheque.document.Audio#tarifEmprunt()}.
	 */
	@Test
	public void testTarifEmprunt() {
		assertTrue(((Audio) defaultOK1).tarifEmprunt() == 1.0);
	}

}
