/**
 * 
 */
package mediatheque;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import client.CategorieClientTest;
import client.ClientTest;
import client.HashClientTest;
import document.AudioTest;
import document.LivreTest;
import document.VideoTest;

/**
 *
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
@RunWith(Suite.class)
@SuiteClasses({ FicheEmpruntTest.class, GenreTest.class,
		LettreRappelTest.class, LocalisationTest.class, MediathequeTest.class,
		CategorieClientTest.class, ClientTest.class, HashClientTest.class,
		AudioTest.class, LivreTest.class, VideoTest.class })
public class AllTests {

}
