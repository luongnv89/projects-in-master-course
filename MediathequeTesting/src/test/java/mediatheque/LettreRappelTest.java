/**
 * 
 */
package mediatheque;

import mediatheque.client.CategorieClient;
import mediatheque.client.Client;
import mediatheque.document.Document;
import mediatheque.document.Video;

import org.junit.Before;
import org.junit.Test;

/**
 *Test for {@link LettreRappel}
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class LettreRappelTest {
	LettreRappel defaultOK;
	FicheEmprunt mFicheEmprunt;
	Mediatheque mMediatheque;
	Client mClient;
	Document mVideo;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mMediatheque = new Mediatheque("mediatheque");
		mClient = new Client(
				"Client1Nom",
				"Client1Prenom",
				"Client1Address",
				new CategorieClient("Client1Category", 10, 1.5, 2.5, 3.5, false));
		mVideo = new Video("VIDEO_CODE", new Localisation("VIDEO_SALLE",
				"VIDEO_RAYON"), "VIDEO_TITRE", "VIDEO_AUTHO", "2013",
				new Genre("VIDEO_GENRE"), 90, "ROMANTIC");
		mVideo.metEmpruntable();
		mFicheEmprunt = new FicheEmprunt(mMediatheque, mClient, mVideo);
		defaultOK = new LettreRappel("NOM_MEDIA", mFicheEmprunt);
	}

	/**
	 * Test method for {@link mediatheque.LettreRappel#relancer()}.
	 */
	@Test
	public void testRelancer() {
		defaultOK.relancer();
	}

	/**
	 * Test method for {@link mediatheque.LettreRappel#getDateRappel()}.
	 */
	@Test
	public void testGetDateRappel() {
		System.out.println(defaultOK.getDateRappel());
	}
}
