/**
 * 
 */
package mediatheque;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mediatheque.client.CategorieClient;
import mediatheque.client.Client;
import mediatheque.document.Audio;
import mediatheque.document.Document;
import mediatheque.document.Livre;
import mediatheque.document.Video;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.InvariantBroken;

/**
 *
 *
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class FicheEmpruntTest {

	FicheEmprunt mFicheEmprunt;
	Mediatheque mMediatheque;
	Client mClient;
	Document mVideo;
	Document mAudio;
	Document mLivre;

	FicheEmprunt depassOK;
	Client clientMarOK;
	FicheEmprunt ficheClientMarquerOK;
	Client clientMarKO;
	FicheEmprunt ficheClientMarquerKO;

	CategorieClient codeReducUtiliseFalse;
	Client clientRestOK;
	Document docRetulisOK;
	FicheEmprunt ficheClientRestitureOK;

	Client clientRestKO1;
	FicheEmprunt clientRestitureKO1;
	Client clientRestKO2;
	FicheEmprunt clientRestitureKO2;
	FicheEmprunt docRestKO;
	private Audio mAudio2;
	private Audio mAudio3;
	private Audio mAudio4;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mMediatheque = new Mediatheque("mediatheque");
		mClient = new Client(
				"Client1Nom",
				"Client1Prenom",
				"Client1Address",
				new CategorieClient("Client1Category", 10, 1.5, 2.5, 3.5, false));
		mVideo = new Video("VIDEO_CODE", new Localisation("VIDEO_SALLE",
				"VIDEO_RAYON"), "VIDEO_TITRE", "VIDEO_AUTHO", "2013",
				new Genre("VIDEO_GENRE"), 90, "ROMANTIC");
		mVideo.metEmpruntable();
		mAudio = new Audio("AUDIO_CODE", new Localisation("AUDIO_SALLE",
				"AUDIO_RAYON"), "AUDIO_TITRE", "AUDIO_AUTHO", "2014",
				new Genre("AUDIO_GENRE"), "AUDIO_ROCK");
		mAudio2 = new Audio("AUDIO_CODE2", new Localisation("AUDIO_SALLE",
				"AUDIO_RAYON"), "AUDIO_TITRE", "AUDIO_AUTHO", "2015",
				new Genre("AUDIO_GENRE"), "AUDIO_ROCK");
		mAudio.metEmpruntable();
		mAudio2.metEmpruntable();

		mAudio3 = new Audio("AUDIO_CODE3", new Localisation("AUDIO_SALLE",
				"AUDIO_RAYON"), "AUDIO_TITRE", "AUDIO_AUTHO", "2015",
				new Genre("AUDIO_GENRE"), "AUDIO_ROCK");
		mAudio3.metEmpruntable();

		mAudio4 = new Audio("AUDIO_CODE4", new Localisation("AUDIO_SALLE",
				"AUDIO_RAYON"), "AUDIO_TITRE", "AUDIO_AUTHO", "2015",
				new Genre("AUDIO_GENRE"), "AUDIO_ROCK");
		mAudio4.metEmpruntable();

		mLivre = new Livre("LIVRE_CODE", new Localisation("LIVRE_SALLE",
				"LIVRE_RAYON"), "LIVRE_TITRE", "LIVRE_AUTHO", "2015",
				new Genre("LIVRE_GENRE"), 100);

		clientMarOK = new Client("Client2Nom", "Client2Prenom",
				"Client2Address", new CategorieClient("Client2Category", 10,
						1.5, 2.5, 3.5, false));
		mFicheEmprunt = new FicheEmprunt(mMediatheque, mClient, mVideo);

		clientMarOK.emprunter();
		clientMarOK.emprunter();
		clientMarOK.marquer();
		ficheClientMarquerOK = new FicheEmprunt(mMediatheque, clientMarOK,
				mAudio);

		depassOK = new FicheEmprunt(mMediatheque, clientMarOK, mAudio2);
		depassOK.verifier();

		clientMarKO = new Client("Client3Nom", "Client3Prenom",
				"Client3Address", new CategorieClient("DefaultCategory"));
		ficheClientMarquerKO = new FicheEmprunt(mMediatheque, clientMarKO,
				mAudio3);

		codeReducUtiliseFalse = new CategorieClient("codeReducUtiliseFalse",
				20, 2.5, 3.5, 4.5, false);
		clientRestOK = new Client("Client4Nom", "Client4Prenom",
				"Client4Address", codeReducUtiliseFalse);
		docRetulisOK = new Video("VIDEO_CODE", new Localisation("VIDEO_SALLE",
				"VIDEO_RAYON"), "VIDEO_TITRE", "VIDEO_AUTHO", "2013",
				new Genre("VIDEO_GENRE"), 90, "ROMANTIC");
		docRetulisOK.metEmpruntable();
		ficheClientRestitureOK = new FicheEmprunt(mMediatheque, clientRestOK,
				docRetulisOK);

		// clientRestKO1;
		// clientRestitureKO1;
		// clientRestKO2 = ;
		// clientRestitureKO2;
		docRestKO = new FicheEmprunt(mMediatheque, mClient, mAudio4);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		mMediatheque = null;
		mClient = null;
		mVideo = null;
		mAudio = null;
		mLivre = null;
		mFicheEmprunt = null;
		depassOK = null;
		clientMarOK = null;
		ficheClientMarquerOK = null;
		clientMarKO = null;
		ficheClientMarquerKO = null;
		clientRestOK = null;
		ficheClientRestitureOK = null;
		clientRestKO1 = null;
		clientRestitureKO1 = null;
		clientRestKO2 = null;
		clientRestitureKO2 = null;
		docRestKO = null;
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#FicheEmprunt(mediatheque.Mediatheque, mediatheque.client.Client, mediatheque.document.Document)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testFicheEmpruntOperationImpossible()
			throws OperationImpossible, InvariantBroken {
		FicheEmprunt mFicheEmprunt2 = new FicheEmprunt(mMediatheque, mClient,
				mLivre);
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#FicheEmprunt(mediatheque.Mediatheque, mediatheque.client.Client, mediatheque.document.Document)}.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testFicheEmpruntOK() throws OperationImpossible,
			InvariantBroken {
		Document doc = new Audio("AUDIO_CODE2", new Localisation("AUDIO_SALLE",
				"AUDIO_RAYON"), "AUDIO_TITRE", "AUDIO_AUTHO", "2015",
				new Genre("AUDIO_GENRE"), "AUDIO_ROCK");
		doc.metEmpruntable();
		FicheEmprunt mFicheEmprunt3 = new FicheEmprunt(mMediatheque, mClient,
				doc);
		assertFalse(mFicheEmprunt3.getDepasse());
	}

//	/**
//	 * Test method for {@link mediatheque.FicheEmprunt#verifier()}.
//	 * @throws OperationImpossible 
//	 */
//	@Test(expected = OperationImpossible.class)
//	public void testVerifierOperationImpossible() throws OperationImpossible {
//		ficheClientMarquerKO.verifier();
//	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#verifier()}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testVerifierClientMarquerOK() throws OperationImpossible {
		ficheClientMarquerOK.verifier();
		assertFalse(ficheClientMarquerOK.getDepasse());
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#modifierClient(mediatheque.client.Client)}.
	 */
	@Test
	public void testModifierClient() {
		Client newClient = new Client("NewClient", "NewClient");
		mFicheEmprunt.modifierClient(newClient);
		assertTrue(mFicheEmprunt.getClient().equals(newClient));
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#correspond(mediatheque.client.Client, mediatheque.document.Document)}.
	 */
	@Test
	public void testCorrespond() {
		assertTrue(mFicheEmprunt.correspond(mClient, mVideo));
		assertFalse(mFicheEmprunt.correspond(clientMarOK, mVideo));
		assertFalse(mFicheEmprunt.correspond(mClient, mAudio));
		assertFalse(mFicheEmprunt.correspond(clientMarOK, mAudio));
	}

//	/**
//	 * Test method for {@link mediatheque.FicheEmprunt#restituer()}.
//	 * @throws OperationImpossible 
//	 * @throws InvariantBroken 
//	 */
//	@Test(expected = OperationImpossible.class)
//	public void testRestituerOperationImpossibleClient()
//			throws InvariantBroken, OperationImpossible {
//		docRestKO.restituer();
//	}
//
//	/**
//	 * Test method for {@link mediatheque.FicheEmprunt#restituer()}.
//	 * @throws OperationImpossible 
//	 * @throws InvariantBroken 
//	 */
//	@Test(expected = OperationImpossible.class)
//	public void testRestituerOperationImpossibleDoc() throws InvariantBroken,
//			OperationImpossible {
//		ficheClientMarquerOK.restituer();
//	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#restituer()}.
	 * @throws OperationImpossible 
	 * @throws InvariantBroken 
	 */
	@Test
	public void testRestituerOK() throws InvariantBroken, OperationImpossible {
		ficheClientRestitureOK.restituer();
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#getClient()}.
	 */
	@Test
	public void testGetClient() {
		assertTrue(mFicheEmprunt.getClient().equals(mClient));
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#getDocument()}.
	 */
	@Test
	public void testGetDocument() {
		assertTrue(mFicheEmprunt.getDocument().equals(mVideo));
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#getDateEmprunt()}.
	 */
	@Test
	public void testGetDateEmprunt() {
		System.out.println(mFicheEmprunt.getDateEmprunt());
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#getDateLimite()}.
	 */
	@Test
	public void testGetDateLimite() {
		System.out.println(mFicheEmprunt.getDateLimite());
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#getDepasse()}.
	 */
	@Test
	public void testGetDepasse() {
		assertFalse(mFicheEmprunt.getDepasse());
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#getDureeEmprunt()}.
	 */
	@Test
	public void testGetDureeEmprunt() {
		int duree = (int) ((ficheClientMarquerOK.getDateLimite().getTime() - ficheClientMarquerOK
				.getDateEmprunt().getTime()) / (1000 * 60 * 60 * 24));
		assertTrue(ficheClientMarquerOK.getDureeEmprunt() == duree);
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#getTarifEmprunt()}.
	 */
	@Test
	public void testGetTarifEmprunt() {
		double tarifNominal = ficheClientMarquerOK.getDocument().tarifEmprunt();
		double tarif = ficheClientMarquerOK.getClient().sommeDue(tarifNominal);
		assertTrue(ficheClientMarquerOK.getTarifEmprunt() == tarif);
	}

//	/**
//	 * Test method for {@link mediatheque.FicheEmprunt#changementCategorie()}.
//	 * @throws OperationImpossible 
//	 */
//	@Test(expected = OperationImpossible.class)
//	public void testChangementCategorieOperationImpossible()
//			throws OperationImpossible {
//		ficheClientMarquerKO.changementCategorie();
//	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#changementCategorie()}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testChangementCategorieDepassFalse() throws OperationImpossible {
		ficheClientMarquerOK.changementCategorie();
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#changementCategorie()}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testChangementCategorieDepassTrue() throws OperationImpossible {
		depassOK.changementCategorie();
		assertFalse(depassOK.getDepasse());
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(depassOK.toString());
		System.out.println(ficheClientMarquerKO.toString());
	}

	/**
	 * Test method for {@link mediatheque.FicheEmprunt#afficherStatistiques()}.
	 */
	@Test
	public void testAfficherStatistiques() {
		mFicheEmprunt.afficherStatistiques();
	}

}
