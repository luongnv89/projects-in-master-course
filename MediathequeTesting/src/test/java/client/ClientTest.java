/**
 * 
 */
package client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import mediatheque.FicheEmprunt;
import mediatheque.OperationImpossible;
import mediatheque.client.CategorieClient;
import mediatheque.client.Client;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Datutil;

/**
 *
 *Test for {@link Client}
 * @author NGUYEN VAN LUONG
 * @version 1.0
 */
public class ClientTest {

	Client defaultOK1;
	Client codeRedue0;
	Client codeReducUtiliseOK;
	Client codeReducUtiliseKO;
	Client encoursOK;
	Client depass0;
	Client encoursDepass0;
	Client lesEmpruntOK;
	Client lesEmpruntKO;

	CategorieClient codeRedueBigger0;
	CategorieClient codeReducUtiliseTrue;
	CategorieClient codeReducUtiliseFalse;
	CategorieClient defaultCategory;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		codeRedueBigger0 = new CategorieClient("codeRedueBigger0", 20, 3.5,
				4.5, 0.5, false);
		codeReducUtiliseTrue = new CategorieClient("codeReducUtiliseTrue", 3,
				1.5, 2.5, 3.5, true);
		codeReducUtiliseFalse = new CategorieClient("codeReducUtiliseFalse",
				20, 2.5, 3.5, 4.5, false);
		defaultCategory = new CategorieClient("DefaultCategory");

		defaultOK1 = new Client("Client1Nom", "Client1Prenom",
				"Client1Address", defaultCategory);
		codeRedue0 = new Client("Client2Nom", "Client2Prenom",
				"Client2Address", codeRedueBigger0);
		codeReducUtiliseOK = new Client("Client3Nom", "Client3Prenom",
				"Client3Address", codeReducUtiliseFalse);
		codeReducUtiliseKO = new Client("Client4Nom", "Client4Prenom",
				"Client4Address", codeReducUtiliseFalse);
		// encoursOK with nbEmpruntsEnCours>catClient.getNbEmpruntMax()>0
		// and nbEmpruntsDepasses = 0
		encoursOK = new Client("Client5Nom", "Client5Prenom", "Client5Address",
				codeReducUtiliseFalse);
		encoursOK.emprunter();// nbEmpruntsEnCours = 1
		encoursOK.emprunter();// nbEmpruntsEnCours = 2
		encoursOK.emprunter();// nbEmpruntsEnCours = 3
		encoursOK.emprunter();// nbEmpruntsEnCours = 4 > 3

		// depass0 with nbEmpruntsDepasses > 0
		depass0 = new Client("Client6Nom", "Client6Prenom", "Client6Address",
				codeReducUtiliseFalse);
		depass0.emprunter();// nbEmpruntsEnCours = 1
		depass0.emprunter();// nbEmpruntsEnCours = 2
		depass0.emprunter();// nbEmpruntsEnCours = 3
		depass0.marquer();// nbEmpruntsDepasses = 1 >0;

		// encoursDepass0 with nbEmpruntsDepasses > 0 and nbEmpruntsEnCours >0
		encoursDepass0 = new Client("Client7Nom", "Client7Prenom",
				"Client7Address", codeReducUtiliseFalse);
		encoursDepass0.emprunter();
		encoursDepass0.emprunter();
		encoursDepass0.marquer();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		defaultOK1 = null;
		codeRedue0 = null;
		codeReducUtiliseOK = null;
		codeReducUtiliseKO = null;
		codeRedueBigger0 = null;
		codeReducUtiliseTrue = null;
		codeReducUtiliseFalse = null;
		defaultCategory = null;
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testClientConstructor1() {
		Client client1 = new Client("Client5Nom", "Client5Prenom");
		assertTrue(client1.getNom().equals("Client5Nom"));
		assertTrue(client1.getPrenom().equals("Client5Prenom"));
		assertTrue(client1.getNbEmpruntsEffectues() == 0);
		assertTrue(client1.getNbEmpruntsEnCours() == 0);
		assertTrue(client1.getNbEmpruntsEnRetard() == 0);
		assertTrue(client1.getCategorie() == null);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String, java.lang.String, mediatheque.client.CategorieClient)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testClientConstructor2() throws OperationImpossible {
		Client client2 = new Client("Client6Nom", "Client6Prenom",
				"Client6Address", defaultCategory);
		assertTrue(client2.getNom().equals("Client6Nom"));
		assertTrue(client2.getPrenom().equals("Client6Prenom"));
		assertTrue(client2.getAdresse().equals("Client6Address"));
		assertTrue(client2.getCategorie().equals(defaultCategory));
		assertTrue(client2.getNbEmpruntsEffectues() == 0);
		assertTrue(client2.getNbEmpruntsEnCours() == 0);
		assertTrue(client2.getNbEmpruntsEnRetard() == 0);
		assertTrue(client2.getCategorie() == defaultCategory);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String, java.lang.String, mediatheque.client.CategorieClient)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testClientConstructorThrowExceptionNom()
			throws OperationImpossible {
		Client client3 = new Client(null, "Client6Prenom", "Client6Address",
				defaultCategory);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String, java.lang.String, mediatheque.client.CategorieClient)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testClientConstructorThrowExceptionPrenom()
			throws OperationImpossible {
		Client client4 = new Client("Client6Nom", null, "Client6Address",
				defaultCategory);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String, java.lang.String, mediatheque.client.CategorieClient)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testClientConstructorThrowExceptionCodeReduUtiliseTrue()
			throws OperationImpossible {
		Client client4 = new Client("Client6Nom", "Client6Prenom",
				"Client6Address", codeReducUtiliseTrue);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String, java.lang.String, mediatheque.client.CategorieClient,int)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testClientConstructorThrowExceptionCodeReduUtiliseFalse()
			throws OperationImpossible {
		Client client4 = new Client("Client6Nom", "Client6Prenom",
				"Client6Address", codeReducUtiliseFalse, 15);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String, java.lang.String, mediatheque.client.CategorieClient)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testClientConstructorThrowExceptionAddress()
			throws OperationImpossible {
		Client client5 = new Client("Client6Nom", "Client6Prenom", null,
				defaultCategory);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String, java.lang.String, mediatheque.client.CategorieClient)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testClientConstructorThrowExceptionCategory()
			throws OperationImpossible {
		Client client6 = new Client("Client6Nom", "Client6Prenom",
				"Client6Address", null);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#Client(java.lang.String, java.lang.String, java.lang.String, mediatheque.client.CategorieClient, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testClientStringString() throws OperationImpossible {
		Client client7 = new Client("Client7Nom", "Client7Prenom",
				"Client7Address", codeReducUtiliseTrue, 15);
		assertTrue(client7.getReduc() == 15);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getNom()}.
	 */
	@Test
	public void testGetNom() {
		assertTrue(defaultOK1.getNom().equals("Client1Nom"));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getPrenom()}.
	 */
	@Test
	public void testGetPrenom() {
		assertTrue(defaultOK1.getPrenom().equals("Client1Prenom"));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getAdresse()}.
	 */
	@Test
	public void testGetAdresse() {
		assertTrue(defaultOK1.getAdresse().equals("Client1Address"));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getNbEmpruntsEnCours()}.
	 */
	@Test
	public void testGetNbEmpruntsEnCours() {
		assertTrue(defaultOK1.getNbEmpruntsEnCours() == 0);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getNbEmpruntsEffectues()}.
	 */
	@Test
	public void testGetNbEmpruntsEffectues() {
		assertTrue(defaultOK1.getNbEmpruntsEffectues() == 0);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getNbEmpruntsEnRetard()}.
	 */
	@Test
	public void testGetNbEmpruntsEnRetard() {
		assertTrue(defaultOK1.getNbEmpruntsEnRetard() == 0);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getCoefTarif()}.
	 */
	@Test
	public void testGetCoefTarif() {
		assertTrue(defaultOK1.getCoefTarif() == 0);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getCoefDuree()}.
	 */
	@Test
	public void testGetCoefDuree() {
		assertTrue(defaultOK1.getCoefDuree() == 0);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertTrue(defaultOK1.equals(defaultOK1));
		assertFalse(defaultOK1.equals(codeRedue0));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#aDesEmpruntsEnCours()}.
	 */
	@Test
	public void testADesEmpruntsEnCours() {
		assertFalse(defaultOK1.aDesEmpruntsEnCours());
		assertTrue(encoursOK.aDesEmpruntsEnCours());
	}

	/**
	 * Test method for {@link mediatheque.client.Client#peutEmprunter()}.
	 */
	@Test
	public void testPeutEmprunter() {
		assertFalse(depass0.peutEmprunter());
		assertTrue(encoursOK.peutEmprunter());
		assertFalse(defaultOK1.peutEmprunter());
	}


	/**
	 * Test method for {@link mediatheque.client.Client#emprunter()}.
	 */
	@Test
	public void testEmprunter() {
		int nbEmpruntsEffectues_Old = defaultOK1.getNbEmpruntsEffectues();
		int nbEmpruntsEnCours_Old = defaultOK1.getNbEmpruntsEnCours();
		defaultOK1.emprunter();
		assertTrue(defaultOK1.getNbEmpruntsEffectues() == nbEmpruntsEffectues_Old + 1);
		assertTrue(defaultOK1.getNbEmpruntsEnCours() == nbEmpruntsEnCours_Old + 1);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#marquer()}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testMarquerOperationImpossible() throws OperationImpossible {
		defaultOK1.marquer();
	}

	/**
	 * Test method for {@link mediatheque.client.Client#marquer()}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testMarquer() throws OperationImpossible {
		int nbEmpruntsDepasses_old = depass0.getNbEmpruntsEnRetard();
		depass0.marquer();
		assertTrue(depass0.getNbEmpruntsEnRetard() == nbEmpruntsDepasses_old + 1);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#restituer(boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testRestituerBooleanOperationImpossibleEnretardTrue()
			throws OperationImpossible {
		defaultOK1.restituer(true);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#restituer(boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testRestituerBooleanOperationImpossibleEnretardTrue2()
			throws OperationImpossible {
		encoursOK.restituer(true);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#restituer(boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testRestituerBooleanEnretardFalse() throws OperationImpossible {
		int nbEmpruntsEnCours = encoursOK.getNbEmpruntsEnCours();
		encoursOK.restituer(false);
		assertTrue(encoursOK.getNbEmpruntsEnCours() == (nbEmpruntsEnCours - 1));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#restituer(boolean)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testRestituerBoolean() throws OperationImpossible {
		int nbEmpruntsEnCours = encoursDepass0.getNbEmpruntsEnCours();
		int nbEmpruntsDepasses = encoursDepass0.getNbEmpruntsEnRetard();
		encoursDepass0.restituer(true);
		assertTrue(encoursDepass0.getNbEmpruntsEnCours() == nbEmpruntsEnCours - 1);
		assertTrue(encoursDepass0.getNbEmpruntsEnRetard() == nbEmpruntsDepasses - 1);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#afficherStatistiques()}.
	 */
	@Test
	public void testAfficherStatistiques() {
		defaultOK1.afficherStatistiques();
	}

	/**
	 * Test method for {@link mediatheque.client.Client#afficherStatCli()}.
	 */
	@Test
	public void testAfficherStatCli() {
		defaultOK1.afficherStatCli();
	}

	/**
	 * Test method for {@link mediatheque.client.Client#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println("CodeReduction==0: \n" + defaultOK1.toString());
		defaultOK1.setReduc(15);
		System.out.println("CodeReduction!=0: \n" + defaultOK1.toString());
	}

	/**
	 * Test method for {@link mediatheque.client.Client#dateRetour(java.util.Date, int)}.
	 */
	@Test
	public void testDateRetour() {
		Date jour = new Date(2013, 5, 31);
		int duree = 7;
		duree = (int) ((double) duree * defaultOK1.getCategorie()
				.getCoefDuree());
		assertTrue(defaultOK1.dateRetour(jour, duree).equals(
				Datutil.addDate(jour, duree)));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#sommeDue(double)}.
	 */
	@Test
	public void testSommeDue() {
		double tarif = 0.5;
		assertTrue(defaultOK1.sommeDue(tarif) == tarif
				* defaultOK1.getCategorie().getCoefTarif());
	}

	/**
	 * Test method for {@link mediatheque.client.Client#nbMaxEmprunt()}.
	 */
	@Test
	public void testNbMaxEmprunt() {
		assertTrue(defaultOK1.nbMaxEmprunt() == defaultCategory
				.getNbEmpruntMax());
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getDateCotisation()}.
	 */
	@Test
	public void testGetDateCotisation() {
		Date dateInscription = Datutil.dateDuJour();
		assertTrue(defaultOK1.getDateCotisation().equals(
				Datutil.addDate(dateInscription, 365)));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getDateInscription()}.
	 */
	@Test
	public void testGetDateInscription() {
		assertTrue(defaultOK1.getDateInscription().equals(Datutil.dateDuJour()));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getCategorie()}.
	 */
	@Test
	public void testGetCategorie() {
		assertTrue(defaultOK1.getCategorie().equals(defaultCategory));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#setCategorie(mediatheque.client.CategorieClient)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testSetCategorieCategorieClientOperationImpossible()
			throws OperationImpossible {
		codeReducUtiliseOK.setCategorie(codeReducUtiliseTrue);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#setCategorie(mediatheque.client.CategorieClient)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testSetCategorieCategorieClientOK() throws OperationImpossible {
		CategorieClient newCate = new CategorieClient("NewCate2");
		codeReducUtiliseKO.setCategorie(newCate);
		assertTrue(codeReducUtiliseKO.getCategorie().equals(newCate));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#setCategorie(mediatheque.client.CategorieClient, int)}.
	 * @throws OperationImpossible 
	 */
	@Test(expected = OperationImpossible.class)
	public void testSetCategorieCategorieClientIntOperationImpossible()
			throws OperationImpossible {
		CategorieClient newCate = new CategorieClient("NewCate");
		int reduc = 5;
		codeReducUtiliseKO.setCategorie(newCate, reduc);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#setCategorie(mediatheque.client.CategorieClient, int)}.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testSetCategorieCategorieClientIntOK()
			throws OperationImpossible {
		int reduc = 5;
		codeReducUtiliseOK.setCategorie(codeReducUtiliseTrue, reduc);
		assertTrue(codeReducUtiliseOK.getReduc() == reduc);
		assertTrue(codeReducUtiliseOK.getCategorie().equals(
				codeReducUtiliseTrue));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#setReduc(int)}.
	 */
	@Test
	public void testSetReduc() {
		defaultOK1.setReduc(15);
		assertTrue(defaultOK1.getReduc() == 15);
	}

	/**
	 * Test method for {@link mediatheque.client.Client#setNom(java.lang.String)}.
	 */
	@Test
	public void testSetNom() {
		defaultOK1.setNom("NewNameClient");
		assertTrue(defaultOK1.getNom().equals("NewNameClient"));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#setPrenom(java.lang.String)}.
	 */
	@Test
	public void testSetPrenom() {
		defaultOK1.setPrenom("NewPrenom");
		assertTrue(defaultOK1.getPrenom().equals("NewPrenom"));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#setAddresse(java.lang.String)}.
	 */
	@Test
	public void testSetAddresse() {
		defaultOK1.setAddresse("NewAddress");
		assertTrue(defaultOK1.getAdresse().equals("NewAddress"));
	}

	/**
	 * Test method for {@link mediatheque.client.Client#getReduc()}.
	 */
	@Test
	public void testGetReduc() {
		assertTrue(defaultOK1.getReduc() == 0);
	}
	//
	// /**
	// * Test method for {@link mediatheque.client.Client#getnbEmpruntsTotal()}.
	// */
	// @Test
	// public void testGetnbEmpruntsTotal() {
	// defaultOK1.
	// }
	//
	// /**
	// * Test method for {@link mediatheque.client.Client#getStat()}.
	// */
	// @Test
	// public void testGetStat() {
	// fail("Not yet implemented");
	// }

}
