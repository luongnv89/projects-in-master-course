package louis.objects;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 
 * {@link GetCityInformations} is a simple service. <br>
 * Input: A City name. <br>
 * Output: <li>Short information about city : get from wikipedia
 * {@link GetCityInformations#wikiURL} <li>Detail weather information of city:
 * get from a weather website {@link GetCityInformations#weatherURL}
 * 
 * @author luongnv89
 * 
 */
public class GetCityInformations {
	private static final String wikiURL = "http://en.wikipedia.org/wiki/";
	private static final String weatherURL = "http://www.timeanddate.com";

	/**
	 * Timeout for request data
	 */
	private static int TIME_OUT = 1000;

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("Give a city name:");
		String cityName = getCityName();
		System.out.println("***** " + cityName.toUpperCase() + " ******");
		System.out.println("INFORMATION");
		getShortInformation(cityName);
		System.out.println("WEATHER");
		getWeatherInformation(cityName);

	}

	/**
	 * Find the detail weather website of city
	 * @param cityName
	 */
	private static void getWeatherInformation(String cityName) {
		try {
			Document doc = Jsoup.parse(new URL(weatherURL + "/weather"),
					TIME_OUT);
			Elements elements = doc
					.select("body > div.main-content-div > table.border2 > tbody > tr > td > a");
			boolean found = false;
			for (Element element : elements) {
				if (element.text().equalsIgnoreCase(cityName)) {
					String url = element.attr("href");
					getWeatherDetail(weatherURL + url);
					found = true;
					break;
				}
			}
			if (!found) {
				System.out.println("Cannot find weather information of city: "
						+ cityName + "\nThere is not the city "
						+ cityName.toUpperCase() + " on weather website: "
						+ weatherURL);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get detail weather information of city
	 * @param detailUrl
	 */
	private static void getWeatherDetail(String detailUrl) {
		try {
			Document doc = Jsoup.parse(new URL(detailUrl), TIME_OUT);
			Elements elements = doc
					.select("body > div.main-content-div > div.fc > table.tbbox > tbody > tr.d1 > td#we1 > table.rpad > tbody > tr");
			for (Element ele : elements) {
				Elements details = ele.select("td");
				System.out.println(details.get(0).text()
						+ details.get(1).text());
			}
		} catch (IOException e) {
			System.out.println("Cannot get detail weather information!");
			e.printStackTrace();
		}
	}

	/**
	 * Get short information of city
	 * @param cityName
	 */
	private static void getShortInformation(String cityName) {
		try {
			Document doc = Jsoup.parse(new URL(wikiURL + cityName), TIME_OUT);
			Elements elements = doc
					.select("body.mediawiki > div#content.mw-body > div#bodyContent > div#mw-content-text.mw-content-ltr > p");
			System.out.println(elements.get(0).text());
		} catch (IOException e) {
			System.out.println("Cannot get short information of " + cityName);
			e.printStackTrace();
		}

	}

	private static String getCityName() {
		Scanner in = new Scanner(System.in);
		return in.next();
	}

}
