package p_tests;

import p_models.RunnableViewablePaddle;
import p_views.PaddleView;

/**
 * <b> Tests</b>  {@link RunnableViewablePaddle}<br>
 * 
 * Should initially display the paddle in the default initial position at bottom left of screen.<br>
 * Then the  paddle should move from left to right (then right to left) as it traverses the screen horizontally
 * 
 * @version 1.0.0
 * @author J Paul Gibson
 */
public class Test_RunnableViewablePaddle {

	public static void main(String args[]) {
		
			
		RunnableViewablePaddle rvPaddle = new RunnableViewablePaddle();
		PaddleView paddleView = new PaddleView(rvPaddle);
		rvPaddle.setView(paddleView);
		
		Thread paddleThread = new Thread(rvPaddle);
		paddleThread.run();
		
			
	}
}
