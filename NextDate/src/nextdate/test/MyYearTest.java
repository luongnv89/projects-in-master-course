package nextdate.test;

import nextdate.model.MyYear;

import org.junit.Assert;
import org.junit.Test;

public class MyYearTest {

	@Test
	public void test() {
		MyYear year = new MyYear(2000);
		Assert.assertTrue(year.isLeapYear());
	}

}
