package nextdate.test;

import java.util.ArrayList;

import nextdate.model.MyDate;

import org.junit.Test;

public class TestNextDate_AutoGenerate {

	@Test
	public void test() {
		// List of special day for each day class
		ArrayList<Integer> listDay = new ArrayList<Integer>();
		listDay.add(25); // day class: 1..27
		listDay.add(28); // day class : 28
		listDay.add(29);// day class : 29
		listDay.add(30);// day class: 30
		listDay.add(31);// day class 31

		// List of special month for each month class
		ArrayList<Integer> listMonth = new ArrayList<Integer>();
		listMonth.add(2);// Feb
		listMonth.add(3);// 31 days month class
		listMonth.add(4);// 30 days month class
		listMonth.add(12);// Dec

		// List of special year for each year class
		ArrayList<Integer> listYear = new ArrayList<Integer>();
		listYear.add(2000);// not a leap year
		listYear.add(2004);// a leap year

		int countTestcase = 0;
		for (int i = 0; i < listYear.size(); i++) {
			for (int j = 0; j < listMonth.size(); j++) {
				for (int k = 0; k < listDay.size(); k++) {
					countTestcase++;
					System.out.println("\n" + countTestcase);
					runTest(listDay.get(k), listMonth.get(j), listYear.get(i));
				}
			}
		}
	}

	/**
	 * @param d
	 * @param m
	 * @param y
	 */
	public void runTest(int d, int m, int y) {
		MyDate newDate = new MyDate(d, m, y);
		System.out.println("Input date: " + newDate.toString());
		MyDate nextDate = new MyDate(newDate.nextDate());
		System.out.println("-->Output date: " + nextDate.toString());
	}
}
