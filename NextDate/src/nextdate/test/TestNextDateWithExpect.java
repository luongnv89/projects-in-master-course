package nextdate.test;

import nextdate.model.MyDate;

import org.junit.Test;

public class TestNextDateWithExpect {

	@Test
	public void test() {
		// *****Not a leap year
		// Feb
		runTest(new MyDate(25, 2, 2000), new MyDate(26, 2, 2000), true);
		runTest(new MyDate(28, 2, 2000), new MyDate(1, 3, 2000), true);
		runTest(new MyDate(29, 2, 2000), new MyDate(0, 0, 0), true);// Invalid
																	// input
		runTest(new MyDate(30, 2, 2000), new MyDate(0, 0, 0), true);// Invalid
																	// input
		runTest(new MyDate(31, 2, 2000), new MyDate(0, 0, 0), true);

		// Mar - belong the 31 days month class
		runTest(new MyDate(25, 3, 2000), new MyDate(26, 3, 2000), true);
		runTest(new MyDate(28, 3, 2000), new MyDate(29, 3, 2000), true);
		runTest(new MyDate(29, 3, 2000), new MyDate(30, 3, 2000), true);
		runTest(new MyDate(30, 3, 2000), new MyDate(31, 3, 2000), true);
		runTest(new MyDate(31, 3, 2000), new MyDate(1, 4, 2000), true);

		// April - belong the 30 days month class
		runTest(new MyDate(25, 4, 2000), new MyDate(26, 2, 2000), true);
		runTest(new MyDate(28, 4, 2000), new MyDate(29, 4, 2000), true);
		runTest(new MyDate(29, 4, 2000), new MyDate(30, 4, 2000), true);
		runTest(new MyDate(30, 4, 2000), new MyDate(1, 5, 2000), true);
		runTest(new MyDate(31, 4, 2000), new MyDate(0, 0, 0), true);// Invalid
																	// input

		// December
		runTest(new MyDate(25, 12, 2000), new MyDate(26, 12, 2000), true);
		runTest(new MyDate(28, 12, 2000), new MyDate(29, 12, 2000), true);
		runTest(new MyDate(29, 12, 2000), new MyDate(30, 12, 2000), true);
		runTest(new MyDate(30, 12, 2000), new MyDate(31, 12, 2000), true);
		runTest(new MyDate(31, 12, 2000), new MyDate(1, 1, 2001), true);

		// *** A leap year
		// Feb
		runTest(new MyDate(25, 2, 2004), new MyDate(26, 2, 2004), true);
		runTest(new MyDate(28, 2, 2004), new MyDate(29, 2, 2004), true);
		runTest(new MyDate(29, 2, 2004), new MyDate(1, 3, 2004), true);
		runTest(new MyDate(30, 2, 2004), new MyDate(0, 0, 0), true);// Invalid
																	// input
		runTest(new MyDate(31, 2, 2004), new MyDate(0, 0, 0), true);

		// Mar - belong the 31 days month class
		runTest(new MyDate(25, 3, 2004), new MyDate(26, 3, 2004), true);
		runTest(new MyDate(28, 3, 2004), new MyDate(29, 3, 2004), true);
		runTest(new MyDate(29, 3, 2004), new MyDate(30, 3, 2004), true);
		runTest(new MyDate(30, 3, 2004), new MyDate(31, 3, 2004), true);
		runTest(new MyDate(31, 3, 2004), new MyDate(1, 4, 2004), true);

		// April - belong the 30 days month class
		runTest(new MyDate(25, 4, 2004), new MyDate(26, 2, 2004), true);
		runTest(new MyDate(28, 4, 2004), new MyDate(29, 4, 2004), true);
		runTest(new MyDate(29, 4, 2004), new MyDate(30, 4, 2004), true);
		runTest(new MyDate(30, 4, 2004), new MyDate(1, 5, 2004), true);
		runTest(new MyDate(31, 4, 2004), new MyDate(0, 0, 0), true);// Invalid
																	// input

		// December
		runTest(new MyDate(25, 12, 2004), new MyDate(26, 12, 2004), true);
		runTest(new MyDate(28, 12, 2004), new MyDate(29, 12, 2004), true);
		runTest(new MyDate(29, 12, 2004), new MyDate(30, 12, 2004), true);
		runTest(new MyDate(30, 12, 2004), new MyDate(31, 12, 2004), true);
		runTest(new MyDate(31, 12, 2004), new MyDate(1, 1, 2005), true);

	}

	/**
	 * Test the nextDate() method
	 * 
	 * @param myDate
	 *            The input Date
	 * @param myDate2
	 *            The next Date expected
	 * @param b
	 *            The expected value. If b is true, that means the expect Date
	 *            equal the result of NextDate() method.
	 */
	private void runTest(MyDate myDate, MyDate myDate2, boolean b) {
		System.out.println("\nInput Date: " + myDate.toString());
		MyDate newDate = new MyDate(myDate.nextDate());
		System.out.println("---> Next Date: " + newDate);
		System.out.println(">>>>Expect Date: " + myDate2.toString());
		if (b == newDate.equal(myDate2))
			System.out.println("***Test is true");
		else
			System.out.println("---Test is false");
	}
}
