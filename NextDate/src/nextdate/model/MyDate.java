package nextdate.model;

import nextdate.invariant.Invariant;

/**
 * The Date class
 * 
 * @author luongnv89
 * 
 */
public class MyDate implements Invariant {
	/**
	 * The day of date
	 */
	MyDay day;
	/**
	 * The month of date
	 */
	MyMonth month;
	/**
	 * The year of date
	 */
	MyYear year;

	/**
	 * Initial a date with the value of day, month, year are input
	 * 
	 * @param d
	 *            day
	 * @param m
	 *            month
	 * @param y
	 *            year
	 */
	public MyDate(int d, int m, int y) {
		day = new MyDay(d);
		month = new MyMonth(m);
		year = new MyYear(y);
	}

	@Override
	public boolean invariant() {
		if (month.classify() == 2)
			if (year.isLeapYear()) {
				if (day.d > 29)
					return false;
				else
					return true;
			} else {
				if (day.d > 28)
					return false;
				else
					return true;
			}
		else if (month.classify() == 3) {
			if (day.d > 30)
				return false;
			else
				return true;
		}
		return month.invariant();

	}

	/**
	 * Get the next of this date
	 * 
	 * @return next date if the input date is valid
	 */
	public MyDate nextDate() {
		if (this.invariant()) {
			switch (month.classify()) {
			case 1:
				if (day.d < 31) {
					return new MyDate(this.day.d + 1, this.month.m, this.year.y);
				} else {
					return new MyDate(1, this.month.m + 1, this.year.y);
				}
			case 3:
				if (day.d < 30) {
					return new MyDate(this.day.d + 1, this.month.m, this.year.y);
				} else {
					return new MyDate(1, this.month.m + 1, this.year.y);
				}
			case 4:
				if (day.d < 31) {
					return new MyDate(this.day.d + 1, this.month.m, this.year.y);
				} else {
					return new MyDate(1, 1, this.year.y + 1);
				}
			case 2:
				if (day.d < 28 || (day.d == 28 && year.isLeapYear())) {
					return new MyDate(this.day.d + 1, this.month.m, this.year.y);
				} else {
					return new MyDate(1, this.month.m + 1, this.year.y);
				}
			default:
				System.out.println("***ERROR IN SWITCH CASE!");
				break;
			}
		} else {
			System.out.println("***ERROR ! THE INPUT DATE IS INVALID!");
			return new MyDate(0, 0, 0);
		}
		return new MyDate(0, 0, 0);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.day.d + "/" + this.month.m + "/" + this.year.y;
	}

	/**
	 * Copy a date
	 * 
	 * @param date
	 */
	public MyDate(MyDate date) {
		this.day = new MyDay(date.day.d);
		this.month = new MyMonth(date.month.m);
		this.year = new MyYear(date.year.y);
	}

	public int getDay() {
		return this.day.d;
	}

	public int getMonth() {
		return this.month.m;
	}

	public int getYear() {
		return this.year.y;
	}

	public boolean equal(MyDate myDate2) {
		return (this.getDay() == myDate2.getDay()
				&& this.getMonth() == myDate2.getMonth() && this.getYear() == myDate2
				.getYear());
	}
}
