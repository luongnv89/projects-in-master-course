package nextdate.model;

import nextdate.invariant.Invariant;

/**
 * The day of date
 * @author luongnv89
 *
 */
public class MyDay implements Invariant {
	/**
	 * The value of day
	 */
	int d;

	/**
	 * initial a day with value is input
	 * @param day input value
	 */
	public MyDay(int day) {
		// TODO Auto-generated constructor stub
		d = day;
	}

	/* (non-Javadoc)
	 * @see nextdate.invariant.Invariant#invariant()
	 */
	@Override
	public boolean invariant() {
		if ((d < 0) || (d > 31))
			return false;
		else
			return true;
	}
}
