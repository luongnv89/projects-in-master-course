package nextdate.model;

import nextdate.invariant.Invariant;

/**
 * The year of date
 * 
 * @author luongnv89
 * 
 */
public class MyYear implements Invariant {
	/**
	 * The value of year
	 */
	int y;

	/**
	 * Initial a year with the value is input
	 * 
	 * @param year
	 *            input value
	 */
	public MyYear(int year) {
		// TODO Auto-generated constructor stub
		y = year;
	}

	@Override
	public boolean invariant() {
		return y >= 0;
	}

	/**
	 * Check a year is a leap year
	 * 
	 * @return true if the year is a leap year and otherwise
	 */
	public boolean isLeapYear() {
		if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
			return true;
		return false;
	}

}
