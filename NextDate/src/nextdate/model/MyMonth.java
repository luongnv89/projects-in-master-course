package nextdate.model;

import nextdate.invariant.Invariant;

/**
 * The month of Date
 * 
 * @author luongnv89
 * 
 */
public class MyMonth implements Invariant {
	/**
	 * The value of month
	 */
	int m;

	// int c;

	/**
	 * Initial a month with the value is input
	 * 
	 * @param month
	 *            input value
	 */
	public MyMonth(int month) {
		// TODO Auto-generated constructor stub
		m = month;
	}

	@Override
	public boolean invariant() {
		return (m > 0 && m < 13);
	}

	/**
	 * Classify a month according to the value of month.
	 * 
	 * @return 1: the month belong to the 31 days months class , except the
	 *         December <br>
	 *         2: the Feb month <br>
	 *         3: The month belong to the 30 days months class. <br>
	 *         4: The December month
	 */
	public int classify() {
		switch (m) {
		case 4:
		case 6:
		case 9:
		case 11:
			// c=1;
			return 3;
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
			return 1;
		case 2:
			return 2;
		case 12:
			return 4;
		default:
			return 0;
		}
	}

}
