package nextdate.invariant;

/**
 * The interface to check validation of Objects
 * 
 * @author luongnv89
 * 
 */
public interface Invariant {
	/**
	 * @return true if object is valid
	 */
	boolean invariant();
}
