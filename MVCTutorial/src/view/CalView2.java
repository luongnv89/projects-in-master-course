package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.CalModel;
import control.CalController2;

public class CalView2 extends JFrame {

	CalModel model;
	CalController2 controller;

	JTextField number1 = new JTextField(10);
	JTextField number2 = new JTextField(10);
	// JLabel operator = new JLabel("+");
	JButton add = new JButton("Add");
	JButton sub = new JButton("Sub");
	JButton mul = new JButton("Mul");
	JButton div = new JButton("Div");
	JTextField result = new JTextField(10);

	public CalView2(CalModel model, CalController2 controller) {
		this.model = model;
		this.controller = controller;

		JPanel jpane = new JPanel();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("My Calculator!");
		this.setSize(500, 300);

		jpane.add(number1);
		jpane.add(number2);
		// jpane.add(operator);
		jpane.add(add);
		jpane.add(sub);
		jpane.add(mul);
		jpane.add(div);
		jpane.add(result);

		this.add(jpane);
	}

	public int getNumber1() {
		return Integer.parseInt(number1.getText());
	}

	public int getNumber2() {
		return Integer.parseInt(number2.getText());
	}

	public int getResult() {
		return Integer.parseInt(result.getText());
	}

	public void setResult(int result) {
		this.result.setText(Integer.toString(result));
	}

	public void addListener() {
		controller.update(this.getNumber1(), this.getNumber2());
		this.add.addActionListener(controller);
	}

	public void subListener(ActionListener pressButton) {
		this.sub.addActionListener(pressButton);
	}

	public void mulListener(ActionListener pressButton) {
		this.mul.addActionListener(pressButton);
	}

	public void divListener(ActionListener pressButton) {
		this.div.addActionListener(pressButton);
	}

	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}
}
