package tests;

import control.CalControl;
import control.CalController2;
import view.CalView;
import view.CalView2;
import models.CalModel;

public class CopyOfMVCTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CalModel model = new CalModel();
		CalController2 controller = new CalController2(model);
		CalView2 view = new CalView2(model, controller);
		view.setVisible(true);

	}

}
