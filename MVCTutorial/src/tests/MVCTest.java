package tests;

import control.CalControl;
import view.CalView;
import models.CalModel;

public class MVCTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CalModel model = new CalModel();
		CalView view = new CalView();
		CalControl controller = new CalControl(model, view);
		view.setVisible(true);

	}

}
