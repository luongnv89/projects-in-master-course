package models;

public class CalModel {
	int result;

	public void addCal(int n1, int n2) {
		result = n1 + n2;
	}

	public void subCal(int n1, int n2) {
		result = n1 - n2;
	}

	public void mulCal(int n1, int n2) {
		result = n1 * n2;
	}

	public void divCal(int n1, int n2) {
		result = n1 / n2;
	}

	public int getResult() {
		return result;
	}

}
