package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import models.CalModel;

public class CalController2 implements ActionListener {
	int n1;
	int n2;
	CalModel model;

	public CalController2(CalModel model) {
		this.model = model;
	}

	public void update(int n1, int n2) {
		this.n1 = n1;
		this.n2 = n2;
	};

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Add":
			model.addCal(n1, n2);
			break;
		case "Sub":
			model.subCal(n1, n2);
			break;
		case "Mul":
			model.mulCal(n1, n2);
			break;
		case "Div":
			model.divCal(n1, n2);
			break;

		default:
			break;
		}

	}

}
