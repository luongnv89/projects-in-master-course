package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.CalView;
import models.CalModel;

public class CalControl {
	CalModel model;
	CalView view;

	public CalControl(CalModel model, CalView view) {
		this.model = model;
		this.view = view;

		this.view.addListener(new CalListener());
		this.view.subListener(new CalListener());
		this.view.mulListener(new CalListener());
		this.view.divListener(new CalListener());

	}

	class CalListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int n1 = 0, n2 = 0;
			try {
				n1 = view.getNumber1();
				n2 = view.getNumber2();
				switch (e.getActionCommand()) {
				case "Add":
					model.addCal(n1, n2);
					break;
				case "Sub":
					model.subCal(n1, n2);
					break;
				case "Mul":
					model.mulCal(n1, n2);
					break;
				case "Div":
					model.divCal(n1, n2);
					break;

				default:
					break;
				}
				view.setResult(model.getResult());
			} catch (NumberFormatException e2) {
				view.displayErrorMessage("You have to input two integer numbers");
			}

		}

	}
}
