package abstractions;

public interface ObservableDiceSpecification {
	String vertical();

	String horizontal();
}
