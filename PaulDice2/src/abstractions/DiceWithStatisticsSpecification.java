package abstractions;

/**
 * 
 * @author J Paul Gibson
 * @version 1.0 Extend the Dice behaviour with statistics which store the roll
 *          frequencies
 */
public interface DiceWithStatisticsSpecification extends DiceSpecification {

	/**
	 * @return the number of times the dice has rolled the specified side
	 * @param side
	 *            specifies the side of the dice for which we wish to know the
	 *            number of rolls
	 * @throws IllegalArgumentException
	 *             when the number of sides is not valid
	 */
	public abstract int frequencyOfRoll(int side)
			throws IllegalArgumentException;

	/**
	 * Check that the DiceWithStatistics1 is in a safe (meaningful) state
	 * 
	 * @return the number of sides of dice is within allowed bounds and the
	 *         lastRoll is within the range 1.. number of sides, and none of the
	 *         frequencies are negative
	 */
	public abstract boolean invariant();

	/**
	 * rolls the dice, updates the lastRoll value and updates the frequency
	 * history
	 */
	public abstract void roll();

	/**
	 * @return The current state of the Dice as a String<br>
	 *         The format to be followed is:<br>
	 * 
	 *         <pre>
	 * 6-sided Dice - lastRoll = 6. (Total number of rolls = 6)
	 *   Frequencies: 
	 * (1, 1)(2, 1)(3, 0)(4, 0)(5, 1)(6, 3)
	 * </pre>
	 */
	public String toString();
}
