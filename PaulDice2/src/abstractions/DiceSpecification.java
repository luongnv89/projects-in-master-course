package abstractions;

import tools.HasInvariant;
import tools.InvariantBroken;

/**
 * The original natural language specification - <br>
 * <em>
 * <ul>
 *  <li> We wish to be able to construct a virtual/electronic dice.</li>
 *  <li> The dice will have by default six sides (numbered 1..6) </li>
 *  <li> We can, if we wish, also construct a dice with any number of sides between a minimum 
 *       of 3 and a maximum of 36. We can also construct a new dice as a copy of an existing dice.</li>
 *  <li> The dice provides a main service/function/method � you can roll it and see the result of this last
 *       roll. The last roll value is initially set to 0 before the dice is rolled.</li>
 *  <li> Each dice also provides a secondary service � it counts the number of times it has been rolled 
 *       since it was created, and allows this value to be read.</li>
 *  <li> We must be able to make many instances of the Dice. Our system will count 
 *       the number of instances that have been constructed. </li>
 *  <li> Finally, our Dice must implement standard methods: 
 *         <code> invariant</code>, <code> toString</code>, <code> equals </code> and <code> hashCode </code></li>
 *  </ul>
 * </em>
 * 
 * We choose to make the following interpretation of the requirements specification:
 * <ul>
 * <li> separate the roll from the reading of the last roll value </li>                    
 * <li> equality between die means  having the same number of sides and the same last roll value</li>
 * </ul>
 * 
 * @author J Paul Gibson
 * @version 1.0.0
 *
 */
public interface DiceSpecification extends HasInvariant{

	/**
	 * The minimum number of sides for a Dice as specified in the requirements<br>
	 */
	 final static int   MINIMUM_numberOfSides = 3;
	
	/**
	 * The maximum number of sides for a Dice as specified in the requirements
	 */
	 final static int   MAXIMIM_numberOfSides = 36;
	
	/**
	 * The default number of sides for a Dice as specified in the requirements
	 */
	 final static int   DEFAULT_numberOfSides = 6;
	 
     /**
	  * True if   the <code> MAXIMIM_numberOfSides </code>, <code>MINIMUM_numberOfSides</code> and 
	  *     <code> DEFAULT_numberOfSides </code> values for the number of sides of the Dice are coherent/valid, 
	  * otherwise false
	  * @see DiceSpecification#invariant()
	  */
	 boolean INVARIANT_OF_CLASS = ( MAXIMIM_numberOfSides >= MINIMUM_numberOfSides &&
			 MINIMUM_numberOfSides >0 &&
			 DEFAULT_numberOfSides >= MINIMUM_numberOfSides &&
			 DEFAULT_numberOfSides <= MAXIMIM_numberOfSides);
	
	 /**
	  * Check that the Dice object is in a safe (meaningful) state
	  * 
	  * @return true if
	  * <ul>
      * <li> <ul><li> once the Dice is rolled, the last roll is within range  1 ... <code> NUMBEROFSIDES </code></li>, or 
	  *      <li> if the Dice is yet to be rolled then the last roll value is zero, </li>  </ul>  and</li>
	  * <li> the <code> numberOfRolls</code> is non-negative, and </li><li> the number of sides of dice is within range 
	  *      <code> MINIMUM_numberOfSides </code> ... <code> MAXIMUM_numberOfSides</code>,  </li>
	  * </ul>
	  * otherwise false
	  * 
	  * @see DiceSpecification#INVARIANT_OF_CLASS
	  */
	 boolean invariant();
		
	/**
	 * Getter method for <code> NUMBEROFSIDES</code>
	 * 
	 * @return the number of sides of the Dice
	 */
	  int numberOfSides();
	
	/**
	 * Getter method for <code> numberOfRolls</code>
	 * 
	 * @return the number of times the Dice has been rolled since it was constructed
	 */
	  int numberOfRolls();
	
	
	/**
	 * Getter method for reading the value of the <code> lastRoll </code><br>
	 * 
	 * @return the lastRoll value
	 */
	 int lastRoll ();
	
	
	/**
	 * Updates the last roll to a random  value between 1 and the number of sides,
	 * and increments the roll count.
	 * 
	 * @throws InvariantBroken if Dice is no longer in a safe state
	 */
	 void roll() throws InvariantBroken;
	
	
	
	/**
	 * @return The current state of the Dice as a String<br>
	 * The format to be followed is:<br>
	 * 
	 <pre>
6-sided Dice - lastRoll = 6. (Total number of rolls = 6)
	 </pre>
	 */
	 String toString();
	
	/**
	 * @param thing is the Object to test for equality
	 * @return true if 
	 * <ul>
	 * <li> the two objects reference the same address, or
	 * <li> the thing is a Dice and there is equality on <code> lastRoll</code> and <code> NUMBEROFSIDES</code> fields
	 * </ul>
	 * otherwise false
	 */
	 boolean equals(Object thing);
	
}
