package models;

import java.util.Random;

import tests.JUnit_Dice;
import abstractions.DiceSpecification;

/**
 * 
 * This class extends the behaviour of the {@link Dice} with statistics for
 * frequency of rolls<br>
 * These statistics are readable through the toString method
 * 
 * @author J Paul Gibson
 * @version 1 <br>
 *          <ul>
 *          <li>Design decision - use composition to re-use already existing
 *          Dice behaviour</li>
 *          <li>Implementation decision - store the roll frequencies in an array
 *          of integers</li>
 *          </ul>
 */
public class DiceWithStatistics2 implements DiceSpecification {

	protected Dice dice;

	/**
	 * The number of dieWithStatistics objects that are currently instantiated<br>
	 * We decrement this value when a DiceWithStatistics1 destructor is called -
	 */
	protected static int numberOfDieWithStatistics = 0;

	/**
	 * Stores the number of times each side of the dice has been rolled
	 */
	protected int rollsFrequency[];

	public DiceWithStatistics2() {
		dice = new Dice();
		rollsFrequency = new int[dice.numberOfSides()];
		resetFrequencies();
		numberOfDieWithStatistics++;
	}

	/**
	 * Create a dice and reset the roll frequencies for each side to be 0
	 * 
	 * @param sides
	 *            can specify the number of sides (within range of 3..36)
	 */
	public DiceWithStatistics2(int sides) {
		dice = new Dice(sides);
	}

	/**
	 * Create a new DiceWithStatistics1 as a copy of a Dice<br>
	 * <br>
	 * Reset all frequencies to 0 except frequency of last roll which should be
	 * set to 1.
	 */
	public DiceWithStatistics2(Dice diceToCopy) {

		dice = new Dice(diceToCopy);
		rollsFrequency = new int[dice.numberOfSides()];
		resetFrequencies();
		if (dice.lastRoll() != 0)
			rollsFrequency[lastRoll() - 1] = 1;
		numberOfDieWithStatistics++;
	}

	/**
	 * Getter method for <code> lastRoll </code>
	 * 
	 * @return the value of the last roll of the Dice
	 */
	public int lastRoll() {

		return dice.lastRoll();
	}

	/**
	 * Getter method for <code> NUMBEROFSIDES </code>
	 * 
	 * @return the number of sides of the Dice
	 */
	public int numberOfSides() {
		return dice.numberOfSides();
	}

	/**
	 * Getter method for <code> numberOfRolls </code>
	 * 
	 * @return the number of times the Dice has been rolled (i.e. lastRoll value
	 *         updated updated)
	 */
	public int numberOfRolls() {
		return dice.numberOfRolls();
	}

	protected void finalize() throws Throwable {
		numberOfDieWithStatistics--;
	}

	/**
	 * Check that the DiceWithStatistics1 is in a safe (meaningful) state
	 * 
	 * @return the number of sides of dice is within allowed bounds and the
	 *         lastRoll is within the range 1.. number of sides, and none of the
	 *         frequencies are negative
	 */
	public boolean invariant() {

		if (rollsFrequency == null)
			dice.invariant();
		else
			for (int i = 0; i < dice.numberOfSides(); i++)
				if (rollsFrequency[i] < 0)
					return false;

		return true;
	}

	/**
	 * resets the history of dice rolls to 0 rolls for all sides
	 */
	private void resetFrequencies() {

		if (rollsFrequency == null)
			rollsFrequency = new int[dice.numberOfSides()];
		for (int i = 0; i < dice.numberOfSides(); i++)
			rollsFrequency[i] = 0;
	}

	/**
	 * rolls the dice, updates the lastRoll value and updates the frequency
	 * history
	 */
	public void roll() {
		dice.roll();
		rollsFrequency[lastRoll() - 1] = rollsFrequency[lastRoll() - 1] + 1;

	}

	/**
	 * @return the number of times the dice has rolled the specified side
	 * @param side
	 *            specifies the side of the dice for which we wish to know the
	 *            number of rolls
	 */
	public int frequencyOfRoll(int side) throws IllegalArgumentException {
		if (side <= 0 || side > MAXIMIM_numberOfSides)
			throw (new IllegalArgumentException(
					"The number of sides specified is not valid"));
		return rollsFrequency[side - 1];
	}

	/**
	 * @return the number of DieWithStatistics that are currently instantiated
	 */
	public static int numberOfDieWithStatistics() {
		return numberOfDieWithStatistics;
	}

	/**
	 * @return a string representation of the current dice state, including
	 *         statistics
	 */
	public String toString() {
		String str = dice.toString();
		str = str + "\n Frequencies: \n";
		for (int i = 0; i < dice.numberOfSides(); i++)
			str = str + "(" + (i + 1) + ", " + rollsFrequency[i] + ")";
		str = str + "\n";
		return str;
	}

	/**
	 * Setter method for the random number generator local to the dice<br>
	 * 
	 * Tested by {@link JUnit_Dice#testSetRNGException}, which shows that we do
	 * not need to check invariant as any invalid change to rng is caught by
	 * exception
	 * 
	 * @param rng
	 *            is the new random number generator to be used when rolling the
	 *            dice
	 * @throws IllegalArgumentException
	 *             if argument <code> rng </code> is <code>null</code>
	 */
	public void setRNG(Random rng) throws IllegalArgumentException {

		if (rng == null)
			throw (new IllegalArgumentException("Cannot set rng to null"));
		dice.rng = rng;

		// if (!invariant()) throw (new
		// InvariantBroken("Dice is no longer in a safe state"));
	}

	public Dice getDice() {
		Dice newDice = new Dice(dice);
		return newDice;
	}

	public static int numberOfDie() {
		// TODO Auto-generated method stub
		return Dice.numberOfDie();
	}
}
