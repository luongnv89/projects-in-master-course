package models;

import abstractions.DiceWithStatisticsSpecification;
import abstractions.ObservableDiceSpecification;

public class ObservableDice implements ObservableDiceSpecification {
	DiceWithStatisticsSpecification dice;

	public ObservableDice(DiceWithStatisticsSpecification dice) {
		this.dice = dice;
	};

	@Override
	public String vertical() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String horizontal() {
		// TODO Auto-generated method stub
		String str = "";
		for (int i = 0; i < dice.numberOfSides(); i++) {

			str = str + (i + 1) + " |";
			for (int j = 0; j < dice.frequencyOfRoll(i); j++) {
				str = str + "*";
			}
			str = str + "\n";
		}
		str = str + "\n";
		return str;
	}

}
