package tests;

import java.util.Random;

import models.DiceWithStatistics2;
import tools.SeedRNGCommandLine;

public class Random_DiceWithStatistics2Test extends Random_Abstract {

	public static void main(String[] args) {
		randomDice = new DiceWithStatistics2();
		Random rng = SeedRNGCommandLine.getRandom(args);
		((DiceWithStatistics2) randomDice).setRNG(rng);
		randomTest(args);
	}

}
