package tests;

import java.util.Random;

import models.Dice;
import tools.DateHeader;
import tools.SeedRNGCommandLine;

/**
 * Test class for {@link Dice} that uses a {@link Random} RNG for simulation
 * purposes.<br>
 * The RNG can be seeded at the command line, or a default value of 0 can be
 * used. <br>
 * <br>
 * 
 * We use the {@link DateHeader} class to document the date/time of the test
 * execution<br>
 * <br>
 * 
 * Expected Output (using default RNG seed = 0) and
 * <code> NUMBER_OF_TEST_ROLLS </code> = 6:
 * 
 * <pre>
 * The seed used for the random number generator in the test is 0.
 * You can override this value by passing an integer value as a main argument parameter, if you so wish.
 * 
 * *******************************************************************
 * Execution Date/Time 2011/01/31 16:38:43
 * *******************************************************************
 * 
 * 6-sided Dice - lastRoll = 0. (Total number of rolls = 0)
 * Rolling 6 times
 * 1. Roll  = 1
 * 2. Roll  = 5
 * 3. Roll  = 2
 * 4. Roll  = 6
 * 5. Roll  = 6
 * 6. Roll  = 6
 * 6-sided Dice - lastRoll = 6. (Total number of rolls = 6)
 * </pre>
 * 
 * @author J Paul Gibson
 * @version 1
 * @see JUnit_Dice
 */

public class Random_DiceTest extends Random_Abstract {

	public static void main(String[] args) {

		randomDice = new Dice();
		Random rng = SeedRNGCommandLine.getRandom(args);
		((Dice) randomDice).setRNG(rng);
		randomTest(args);
	}

}
