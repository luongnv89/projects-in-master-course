package tests;

import java.util.Random;

import abstractions.DiceWithStatisticsSpecification;

import models.DiceWithStatistics1;
import models.ObservableDice;
import tools.SeedRNGCommandLine;

public class ObserverDiceTest extends Random_Abstract {
	public static void main(String args[]) {

		randomDice = new DiceWithStatistics1();
		Random rng = SeedRNGCommandLine.getRandom(args);
		((DiceWithStatistics1) randomDice).setRNG(rng);
		randomTest(args);
		ObservableDice observableDice = new ObservableDice(
				(DiceWithStatisticsSpecification) randomDice);
		System.out.println(observableDice.horizontal());
	}
}
