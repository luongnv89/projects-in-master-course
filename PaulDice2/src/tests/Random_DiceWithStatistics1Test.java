package tests;

import java.util.Random;

import models.DiceWithStatistics1;
import tools.SeedRNGCommandLine;

public class Random_DiceWithStatistics1Test extends Random_Abstract {

	public static void main(String[] args) {
		randomDice = new DiceWithStatistics1();
		Random rng = SeedRNGCommandLine.getRandom(args);
		((DiceWithStatistics1) randomDice).setRNG(rng);
		randomTest(args);
	}
}
