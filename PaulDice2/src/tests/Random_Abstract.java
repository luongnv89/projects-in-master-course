package tests;

import tools.DateHeader;
import abstractions.DiceSpecification;

public abstract class Random_Abstract {

	protected static DiceSpecification randomDice;
	/**
	 * The number of rolls in our simulation
	 */
	final static int NUMBER_OF_TEST_ROLLS = 15;

	public static void randomTest(String args[]) {
		System.out.println(DateHeader.dateString());
		System.out.println(randomDice);
		System.out.println("Rolling " + NUMBER_OF_TEST_ROLLS + " times");
		for (int i = 1; i <= NUMBER_OF_TEST_ROLLS; i++) {
			randomDice.roll();
			System.out.println(i + ". Roll  = " + randomDice.lastRoll());
		}
		System.out.println(randomDice);

	}

}
