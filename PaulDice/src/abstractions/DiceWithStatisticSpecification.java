package abstractions;

public interface DiceWithStatisticSpecification {
	public String toString();

	public int getFrequencies(int side);

	public int[] getFrequency();
}
