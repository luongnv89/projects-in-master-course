package abstractions;



/**
 * 
 * @author J Paul Gibson
 * @version 1
 * Provides implementations of the methods of the DiceSpecification that can be coded independently
 * of the concrete implementation of the required behaviour:
 * <ul>
 * <li> {@link DiceAbstraction#equals} </li>
 * <li> {@link DiceAbstraction#hashCode} </li>
 * <li> {@link DiceAbstraction#toString} </li>
 * <li> {@link DiceAbstraction#invariant} </li>
 * </ul>
 */
public abstract class DiceAbstraction   implements DiceSpecification{
	
	
	public boolean equals( Object thing){
		
		if (thing ==null) return false;
		if ( this == thing) return true;
		if (! (thing instanceof DiceAbstraction)) return false;
		
		DiceAbstraction that = (DiceAbstraction) thing;
		return ( (this.lastRoll() == that.lastRoll()) && (this.numberOfSides() == that.numberOfSides()) );
	}
	
	/**
	 * Algorithm taken from Josh Bloch's "Effective Java", using primes 37 and 41
	 * <ul>
	 * <li>	 initialise with prime = 37</li>
	 * <li>  for each field tested in {@link DiceAbstraction#equals} increase iteratively with
	 *       another prime multiplier:  hash = 41 * hash + field_integer_value </li>
	 * </ul>
	 * 
	 */
	public int hashCode(){
		
		int hash = 37;   // start with a prime
		hash = 41 * hash + lastRoll(); // iteratively increase with another prime multiplier
		hash = 41 * hash + numberOfSides();
		return hash;
	}
	
	

	public String toString(){
		
		String str ="";
		str = str+ numberOfSides()+"-sided Dice - lastRoll = "+lastRoll()+ ". (Total number of rolls = "+numberOfRolls()+")";
		return str;
	}
	

	public boolean invariant(){
		
				return (( (numberOfRolls() == 0  && lastRoll() == 0 ) || 
		                  (numberOfRolls() > 0 && lastRoll()>0 && lastRoll()<=numberOfSides() ) )&&
				        (numberOfSides()>=MINIMUM_numberOfSides) && 
				        (numberOfSides() <= MAXIMIM_numberOfSides) );     
	}
    
	


}
