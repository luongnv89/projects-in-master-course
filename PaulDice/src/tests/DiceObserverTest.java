
package tests;

import java.util.Random;

import models.DiceObserver;
import models.ObservableDice;
import tools.DateHeader;
import tools.SeedRNGCommandLine;

public class DiceObserverTest {
	
public static void main(String[] args) {
		
	    ObservableDice observableDice;
	    DiceObserver diceObserver;
	    
		/**
		 * The number of rolls in our simulation
		 */
		final int NUMBER_OF_TEST_ROLLS = 6;
		
		observableDice = new ObservableDice();
		diceObserver = new DiceObserver();
		diceObserver.diceToObserver(observableDice);
		
		Random rng = SeedRNGCommandLine.getRandom(args);
		System.out.println(DateHeader.dateString());
		
		observableDice.setRNG(rng);
		
        System.out.println("Rolling "+NUMBER_OF_TEST_ROLLS+ " times");
		for (int i =1; i<=NUMBER_OF_TEST_ROLLS;i++){ 
			observableDice.roll(); 
			System.out.println(i+". Roll  = " +observableDice.lastRoll() );
		}
		System.out.println(observableDice);

		} 


}
