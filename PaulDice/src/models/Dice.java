package models;
import java.util.Random;

import tests.JUnit_DiceAbstractionTest;
import tests.JUnit_DiceTest;
import tools.HasInvariant;
import tools.InvariantBroken;
import abstractions.DiceAbstraction;
import abstractions.DiceSpecification;

/**
 * <b> Implements</b>  {@link DiceSpecification}<br>
 * <b> Implements</b>  {@link HasInvariant}
 * 
 * @author J Paul Gibson
 * @version 1.0.0
 * <ul>
 * <li> Design decision - choose to make a random number generator local to each dice and
 *                        to provide a method to change the rng at runtime
 *                        (throwing an exception if we try to reset the rng value to null)</li>
 * <li> Implementation decision - choose to make the number of sides constant (once set by the constructor)</li>
 * <li> Implementation decision - choose to implement a default dice (rather than throwing an exception) if we try to 
 *                                construct a dice with state that does not respect the invariant </li>
 * <li> Implementation decision - choose to implement the copy constructor to copy the number of sides and the same roll
 *                                value (for consistency with  the decision for equals). As the copy now has a rolled value we 
 *                                choose to initialise the number of rolls in the copy to be 1.</li>                      
 * <li> Implementation decision - The format of the <code> toString </code> result will follow the template 
 *                                as given for an initial default dice:
 *  <pre>
  6-sided Dice - lastRoll = 0. (Total number of rolls = 0) 
                                                           
 * </pre>
 * </ul>
 */
public class Dice extends DiceAbstraction implements  DiceSpecification{
	

	/**
	 * The number of sides of the Dice. <br>
	 * This can be made public because the value is final, and so we don't need a getter method, but will provide one<br>
	 * A consequence is that once this value is initialised it cannot be changed.
	 */
public final int NUMBEROFSIDES;

/**
 * The random number generator local to each dice<br>
 * This design decision was taken to guarantee independence of die behaviours<br>
 * We do not make this <code> final</code> as we wish to be able to reset the rng to a new value using {@link Dice#setRNG}
 */
protected  Random rng = new Random();

/**
 * The  number of die that have been constructed in the lifetime of the system<br>
 * We do <em> not </em>  decrement this value when a Dice object is finalized -
 */
protected static  int   numberOfDie = 0;

/**
 * The  number of times the dice has been rolled since construction
 */
protected int   numberOfRolls = 0;

/**
 * The value of the last Dice roll 
 */
protected int lastRoll = 0;
	
/**
 * Check that the Dice object is in a safe (meaningful) state
 * 
 * @return true if
 * <ul>
 * <li> <ul><li> once the Dice is rolled, the last roll is within range  1 ... <code> NUMBEROFSIDES </code></li>, or 
 *      <li> if the Dice is yet to be rolled then the last roll value is zero, </li> </ul> and </li>
 * <li> the <code> numberOfRolls</code> is non-negative, and </li><li> the number of sides of dice is within range 
 *      <code> MINIMUM_numberOfSides </code> ... <code> MAXIMUM_numberOfSides</code>, and </li>
 * <li> the <code> rng </code>  is not <code>null </code></li>
 * </ul>
 * otherwise false
 * 
 * @see Dice#invariantOfClass
 */
public boolean invariant(){
	
			return super.invariant() &&
			       rng !=null;
}

/**
 * Dice constructor by default will make a dice with <code> DEFAULT_numberOfSides </code> sides.<br>
 * Increments the <code> numberOfDie</code> counter<br><br>
 * 
 * Tested by  {@link JUnit_DiceAbstractionTest#testDefaultConstructor()}, which guarantees that the Dice is
 * constructed in a safe state as specified by {@link Dice#invariant}.
 */
public Dice () throws InvariantBroken {
	
	NUMBEROFSIDES = DEFAULT_numberOfSides;
	numberOfDie++;
	numberOfRolls =0;
	
	//if (!invariant()) throw (new InvariantBroken("Dice was not constructed in a safe state"));
}

/**
 * Dice constructor with the specified number of sides<br>
 * Increments the <code> numberOfDie</code><br>
 * If the specified number <code> sides </code> is bigger than <code>MAXIMUM_numberOfSides</code>
 * or less than <code> MINIMUM_numberOfSides</code> then the default of <code> DEFAULT_numberOfSides </code> is used.<br><br>
 * 
 * Tested by  {@link JUnit_DiceAbstractionTest#testNonDefaultConstructor()}, which guarantees that the Dice is
 * constructed in a safe state as specified by {@link Dice#invariant}.
 * 
 * @param sides defines the number of sides for the new dice being constructed.
 */
public Dice (int sides) throws InvariantBroken{
	
	if (sides<MINIMUM_numberOfSides || sides > MAXIMIM_numberOfSides)
		sides = DEFAULT_numberOfSides;
	NUMBEROFSIDES =sides;
	numberOfDie++;
	numberOfRolls =0;
	
	//if (!invariant()) throw (new InvariantBroken("Dice was not constructed in a safe state"));
} 


/**
 * Dice copy constructor - which copies the <code> NUMBEROFSIDES </code> and the <code> lastRoll </code> values,
 * it does not copy the <code> numberOfRolls</code> value as this is intialised to 1 in the new Dice.<br>
 * Increments the <code> numberOfDie </code>.<br><br>
 * 
 * Tested by {@link JUnit_DiceAbstractionTest#testCopyConstructor()}, which guarantees that the Dice is
 * constructed in a safe state as specified by {@link Dice#invariant}.
 * 
 * @param diceToCopy provides the values to be copied in the new Dice.
 * @throws IllegalArgumentException if argument <code> diceToCopy </code> is <code>null</code>
 */
public Dice (Dice diceToCopy) throws IllegalArgumentException, InvariantBroken{

	if (diceToCopy == null) throw (new IllegalArgumentException("Can't copy a null dice"));
	NUMBEROFSIDES = diceToCopy.NUMBEROFSIDES;
	if (diceToCopy.numberOfRolls()==0) lastRoll = 0; else lastRoll = diceToCopy.lastRoll();
	numberOfDie++;
	numberOfRolls =1;
	
	// if (!invariant()) throw (new InvariantBroken("Dice was not constructed in a safe state"));
} 



public int lastRoll (){
	
	   return lastRoll;
	}

public  int numberOfSides(){
	return NUMBEROFSIDES;
}

public  int numberOfRolls(){
	return numberOfRolls;
}


/**
 * Getter method for the static <code> numberOfDie </code>
 * @return the number of Die that have been constructed
 */
public static int numberOfDie(){
	return numberOfDie;
}

/**
 * Setter method for the random number generator local to the dice<br>
 *
 * Tested by {@link JUnit_DiceTest#testSetRNGException}, which shows that
 * we do not need to check invariant as any invalid change to rng is caught by exception
 * 
 * @param rng is the new random number generator to be used when rolling the dice
 * @throws IllegalArgumentException if argument <code> rng </code> is <code>null</code>
 */
public void setRNG( Random rng) throws IllegalArgumentException{
	
	if (rng==null) throw (new IllegalArgumentException("Cannot set rng to null"));
	this.rng = rng;

	// if (!invariant()) throw (new InvariantBroken("Dice is no longer in a safe state"));
}

/**
 * Tested by {@link JUnit_DiceAbstractionTest#testRoll}, which guarantees that the Dice is
 * constructed in a safe state as specified by {@link Dice#invariant}.
 * 
 */
public void roll() throws InvariantBroken{
	
	lastRoll = rng.nextInt(NUMBEROFSIDES)+1; numberOfRolls++;
	
	// if (!invariant()) throw (new InvariantBroken("Dice is no longer in a safe state"));
}

}