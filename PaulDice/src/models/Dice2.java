package models;

import tools.InvariantBroken;
import abstractions.DiceWithStatisticSpecification;

/**
 * 
 * @author luongnv89
 *
 */
public class Dice2 extends Dice implements DiceWithStatisticSpecification {
	/**
	 * 
	 */
	int[] frequencies = new int[numberOfSides()];

	@Override
	public void roll() throws InvariantBroken {

		lastRoll = rng.nextInt(NUMBEROFSIDES) + 1;
		numberOfRolls++;
		frequencies[lastRoll - 1] = frequencies[lastRoll - 1] + 1;
		// if (!invariant()) throw (new
		// InvariantBroken("Dice is no longer in a safe state"));
	}

	/* (non-Javadoc)
	 * @see abstractions.DiceAbstraction#toString()
	 */
	@Override
	public String toString() {
		String str = "";
		str = str + numberOfSides() + "-sided Dice - lastRoll = " + lastRoll()
				+ ". (Total number of rolls = " + numberOfRolls() + ")" + "\n"
				+ "Frequencies\n";
		for (int i = 0; i < numberOfSides(); i++) {
			str = str + "( " + (i + 1) + "," + frequencies[i] + " ) ";
		}
		return str;
	}
	/* (non-Javadoc)
	 * @see abstractions.DiceWithStatisticSpecification#getFrequencies(int)
	 */
	@Override
	public int getFrequencies(int side) {
		return frequencies[side];
	}

	/* (non-Javadoc)
	 * @see abstractions.DiceWithStatisticSpecification#getFrequency()
	 */
	@Override
	public int[] getFrequency() {
		return frequencies;
	}
}
