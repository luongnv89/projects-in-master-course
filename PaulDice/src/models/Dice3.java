package models;

import java.util.Random;

import tools.InvariantBroken;
import abstractions.DiceWithStatisticSpecification;

public class Dice3 implements DiceWithStatisticSpecification {

	protected Dice dice;
	public Dice getDice() {
		return dice;
	}

	int[] frequencies;

	public Dice3() {
		dice = new Dice();
		frequencies = new int[dice.numberOfSides()];
	}

	public Dice3(Dice3 diceToCopy) throws IllegalArgumentException,
			InvariantBroken {
		if (diceToCopy == null) throw (new IllegalArgumentException("Can't copy a null dice"));
		dice = new Dice(diceToCopy.dice);

		// if (!invariant()) throw (new
		// InvariantBroken("Dice was not constructed in a safe state"));
	}

	public Dice3(int side) {
		dice = new Dice(side);
	}

	public boolean invariant() {
		// TODO Auto-generated method stub
		return dice.invariant();
	}

	public int numberOfSides() {
		// TODO Auto-generated method stub
		return dice.numberOfSides();
	}

	public int numberOfRolls() {
		// TODO Auto-generated method stub
		return dice.numberOfRolls();
	}

	public int lastRoll() {
		// TODO Auto-generated method stub
		return dice.lastRoll();
	}

	public void roll() throws InvariantBroken {

		dice.lastRoll = dice.rng.nextInt(dice.NUMBEROFSIDES) + 1;
		dice.numberOfRolls++;
		frequencies[dice.lastRoll - 1] = frequencies[dice.lastRoll - 1] + 1;
	}

	public int getFrequencies(int side) {
		return frequencies[side];
	}

	@Override
	public String toString() {
		String str = "";
		str = str + numberOfSides() + "-sided Dice - lastRoll = " + lastRoll()
				+ ". (Total number of rolls = " + numberOfRolls() + ")" + "\n"
				+ "Frequencies\n";
		for (int i = 0; i < numberOfSides(); i++) {
			str = str + "( " + (i + 1) + "," + frequencies[i] + " ) ";
		}
		return str;
	}

	public void setRNG(Random rng) throws IllegalArgumentException {
		this.dice.setRNG(rng);
	}

	public static int numberOfDie() {
		// TODO Auto-generated method stub
		return Dice.numberOfDie();
	}

	@Override
	public int[] getFrequency() {
		// TODO Auto-generated method stub
		return null;
	}
}
