package controller;

import views.ViewsInterface;

public interface ControllerInterface {
	public void updateView(ViewsInterface views);

}
