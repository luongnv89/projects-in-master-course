/**
 * 
 */
package louis.aspects;

import louis.specifications.FirgureInterfaces;
import louis.objects.*;

/**
 * @author luongnv89
 * 
 */
public aspect PointCutMethodCall {
	pointcut stateChange() : call(* FirgureInterfaces.moveBy(int,int))||
	call(* MyPoint.set*(*))||call(* MyLine.set*(*));
		
	after() returning : stateChange() {
		System.out.println("State is change!");
		Frigures.display();
	}
	
	pointcut stateChangeWithParameter(FirgureInterfaces fg):target(fg) && (call(* FirgureInterfaces.moveBy(int,int))||
	call(* MyPoint.set*(*))||call(* MyLine.set*(*)));
	
	after(FirgureInterfaces fg) returning: stateChangeWithParameter(fg){
		System.out.println("State is change (with paramter)!");
		Frigures.display();
	}

	pointcut diaHelp(int dx, int dy):call(void FirgureInterfaces.moveBy(int,int))&&args(dx,dy)&& if(dx==dy);
	pointcut diagMove(int dxy): diaHelp(dxy,int);
	
	
}
