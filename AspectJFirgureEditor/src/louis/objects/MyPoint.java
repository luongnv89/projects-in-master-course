/**
 * 
 */
package louis.objects;

import louis.specifications.FirgureInterfaces;

/**
 * @author luongnv89
 * 
 */
public class MyPoint implements FirgureInterfaces {

	int x;
	int y;

	/**
	 * @param x
	 * @param y
	 */
	public MyPoint(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public void moveBy(int x, int y) {
		this.x += x;
		this.y += y;

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Point ("+this.x+"; "+this.y+")";
	}

	
}
