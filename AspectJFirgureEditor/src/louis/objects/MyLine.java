/**
 * 
 */
package louis.objects;

import louis.specifications.FirgureInterfaces;

/**
 * @author luongnv89
 * 
 */
public class MyLine implements FirgureInterfaces {
	MyPoint p1;
	MyPoint p2;

	/**
	 * @param p1
	 * @param p2
	 */
	public MyLine(MyPoint p1, MyPoint p2) {
		super();
		this.p1 = p1;
		this.p2 = p2;
	}

	/**
	 * @return the p1
	 */
	public MyPoint getP1() {
		return p1;
	}

	/**
	 * @param p1
	 *            the p1 to set
	 */
	public void setP1(MyPoint p1) {
		this.p1 = p1;
	}

	/**
	 * @return the p2
	 */
	public MyPoint getP2() {
		return p2;
	}

	/**
	 * @param p2
	 *            the p2 to set
	 */
	public void setP2(MyPoint p2) {
		this.p2 = p2;
	}

	@Override
	public void moveBy(int x, int y) {
		p1.moveBy(x, y);
		p2.moveBy(x, y);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Line: "+p1.toString()+" --- " + p2.toString();
	}

	
}
