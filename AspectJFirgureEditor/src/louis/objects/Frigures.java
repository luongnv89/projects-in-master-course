/**
 * 
 */
package louis.objects;

import java.util.ArrayList;

import louis.specifications.FirgureInterfaces;

/**
 * @author luongnv89
 *
 */
public class Frigures {
	static ArrayList<FirgureInterfaces> listFirgues;

	/**
	 * 
	 */
	public Frigures() {
		super();
		listFirgues = new ArrayList<>();
	}
	
	public void makePoint(int x, int y){
		listFirgues.add(new MyPoint(x, y));
	}
	
	public void makeLine(int x1, int y1, int x2, int y2){
		listFirgues.add(new MyLine(new MyPoint(x1, y1), new MyPoint(x2, y2)));
	}
	
	public static void display(){
		for(int i=0;i<listFirgues.size();i++){
			System.out.println(listFirgues.get(i).toString()+"\n");
		}
	}
}
