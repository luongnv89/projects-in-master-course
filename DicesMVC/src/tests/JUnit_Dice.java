package tests;

import models.Dice;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import abstractions.DiceSpecification;


/**
 * Test {@link Dice} Using JUnit<br>
 * 
 * Note that we cannot guarantee the order of the test execution:
 * 
 * <ul>
 * <li> <b> new </b> test exception in {@link Dice#setRNG(java.util.Random)}</li>
 * <li> <b> new </b> test the static method {@link Dice#numberOfDie()}</li>
 * <li> <b> inherited </b> test the class Invariant {@link DiceSpecification#INVARIANT_OF_CLASS}</li>
 * <li> <b> inherited </b> test the default constructor {@link Dice#Dice}</li>
 * <li> <b> inherited </b> test the non default constructor {@link Dice#Dice(int)}</li>
 * <li> <b> inherited </b> test the copy constructor {@link Dice#Dice(Dice)}</li>
 * <li> <b> inherited </b> test exception in copy constructor {@link Dice#Dice(Dice)}</li>
 * <li> <b> inherited </b> test {@link Dice#roll} </li>
 * <li> <b> inherited </b> test {@link Dice#numberOfRolls()} </li>
 * <li> <b> inherited </b> test {@link Dice#equals} </li>
 * <li> <b> inherited </b> test {@link Dice#numberOfDie()} </li>
 * <li> <b> inherited </b> test {@link Dice#toString} </li>
 * </ul>
 * 
 * @author J Paul Gibson
 * @version 1.0.0.1
 * @see Random_DiceTest
 */
public class JUnit_Dice extends JUnit_DiceSpecification {
	

/**
 * Initialise our die to be used during testing:
 * <ul> 
 * <li> The valid non-default number of sides is set to the average of the maximum and minimum </li>
 * <li> The invalid non-default number of sides is set to 1 above the maximum </li>
 * @throws Exception
 */
@Before
public void setUp() throws Exception {
	
		diceDefault = new Dice();
		diceNonDefaultOK = new Dice((Dice.MAXIMIM_numberOfSides + Dice.MINIMUM_numberOfSides)/2);
		diceNonDefaultKO = new Dice(Dice.MAXIMIM_numberOfSides + 1);
		diceRolledTwice = new Dice(); diceRolledTwice.roll(); diceRolledTwice.roll();
		diceCopyRolledTwice = new Dice ( (Dice) diceRolledTwice);
}
	

/**
 * Resets our test die to null
 * @throws Exception
 */
@After
public void tearDown() throws Exception {
	
		diceDefault = null;
		diceNonDefaultOK = null;
		diceNonDefaultKO = null;
		diceRolledTwice = null;
		diceCopyRolledTwice = null;
}
	

	
/**
 *  test exception in {@link Dice#setRNG(java.util.Random)}
 */
@Test(expected = IllegalArgumentException.class)
public void testSetRNGException() {
	
	((Dice) diceDefault).setRNG(null);
}

/**
 *  test {@link Dice#numberOfDie()} by creating 10 new die and checking count
 */
@Test
public void testNumberOfDie() {
	
		int countNumberOfDie = Dice.numberOfDie();
		final int NUMBER_OF_DIE_TO_CONSTRUCT = 10;
		
		for (int i=0; i<NUMBER_OF_DIE_TO_CONSTRUCT; i++) new Dice(); 
		
		Assert.assertEquals(countNumberOfDie+NUMBER_OF_DIE_TO_CONSTRUCT, Dice.numberOfDie());
}
 
}
