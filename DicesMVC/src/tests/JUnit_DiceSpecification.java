package tests;

import models.Dice;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import abstractions.DiceSpecification;


/**
 * Tests for {@link DiceSpecification} using JUnit, these are inherited by the concrete test subclass(es)<br>
 * 
 * Note that we cannot guarantee the order of the test execution:
 * 
 * <ul>
 * <li> test the class Invariant {@link DiceSpecification#INVARIANT_OF_CLASS}</li>
 * <li> test the default constructor </li>
 * <li> test the non default constructor </li>
 * <li> test the copy constructor </li>
 * <li> test exception in copy constructor </li>
 * <li> test {@link DiceSpecification#roll()} </li>
 * <li> test {@link DiceSpecification#numberOfRolls()} </li>
 * <li> test {@link DiceSpecification#equals(Object)} </li>
 * <li> test {@link DiceSpecification#invariant()} </li>
 * <li> test {@link DiceSpecification#toString()} </li>
 * </ul>
 * 
 * @author J Paul Gibson
 * @version 1
 * @see Random_DiceTest
 */
public abstract class JUnit_DiceSpecification {
	
	/**
	 * default Dice 
	 */
	protected static DiceSpecification diceDefault; 
	
	/**
	 * non-default Dice 
	 */
	protected static DiceSpecification diceNonDefaultOK;
	
	/**
	 * non-default Dice (whose construction reverted to default) 
	 */
	protected static DiceSpecification diceNonDefaultKO;
	
	/**
	 * default Dice rolled twice 
	 */
	protected static DiceSpecification diceRolledTwice;
	
	/**
	 * a copy of the default Dice that had been rolled twice
	 */
	protected static DiceSpecification diceCopyRolledTwice;

/**
 * Initialise our die to be used during testing:
 * <ul> 
 * <li> The valid non-default number of sides is set to the average of the maximum and minimum </li>
 * <li> The invalid non-default number of sides is set to 1 above the maximum </li>
 * </ul>
 * @throws Exception
 */
@Before
public abstract void setUp() throws Exception;
	

/**
 * Resets our test die to null
 * @throws Exception
 */
@After
public abstract void tearDown() throws Exception;
	


/**
 * tests the the class Invariant {@link DiceSpecification#INVARIANT_OF_CLASS}
 */
@Test
public void testClassInvariant() {

	    Assert.assertTrue(DiceSpecification.INVARIANT_OF_CLASS);
}


/**
 * tests the default constructor {@link Dice#Dice()}
 */
@Test
public void testDefaultConstructor() {

		Assert.assertTrue(diceDefault.invariant());
		Assert.assertEquals(diceDefault.numberOfRolls(), 0);
		Assert.assertEquals(diceDefault.numberOfSides(), Dice.DEFAULT_numberOfSides);	    
}
	
/**
 * tests the non default constructor {@link Dice#Dice(int)}:
 * <ul>
 * <li> When number of sides parameter is valid</li>
 * <li> When number of sides parameter is not valid</li>
 * </ul>
 */
@Test
public void testNonDefaultConstructor() {

	    // When number of sides parameter is valid
		Assert.assertTrue(diceNonDefaultOK.invariant());
		Assert.assertEquals(diceNonDefaultOK.numberOfRolls(), 0);
	    Assert.assertEquals(diceNonDefaultOK.numberOfSides(), (Dice.MAXIMIM_numberOfSides + Dice.MINIMUM_numberOfSides)/2);
		
	    //When number of sides parameter is not valid
		Assert.assertTrue(diceNonDefaultKO.invariant());
		Assert.assertEquals(diceNonDefaultKO.numberOfRolls(), 0);
		Assert.assertEquals(diceNonDefaultKO.numberOfSides(), Dice.DEFAULT_numberOfSides);
}
	
/**
 * tests the copy constructor {@link Dice#Dice(Dice)} 
 *<ul>
 *<li> the copy respects the invariant </li>
 *<li> the new dice copy has its number of rolls correctly initialised to 0 </li>
 *<li> the last roll and the number of sides values have been correctly copied </li>
 *</ul>
 */
	@Test
public void testCopyConstructor() {
		
		Assert.assertTrue(diceCopyRolledTwice.invariant());
		Assert.assertEquals(diceCopyRolledTwice.numberOfRolls(), 1);
		Assert.assertEquals(diceCopyRolledTwice.lastRoll(), diceRolledTwice.lastRoll());
		Assert.assertEquals(diceCopyRolledTwice.numberOfSides(), diceRolledTwice.numberOfSides());
}
	
/**
 *  tests exception in copy constructor {@link Dice#Dice(Dice)} due to null Dice to be copied
 */
@Test(expected = IllegalArgumentException.class)
public void testCopyConstructorException() {
	
		new Dice(null);	
}
	
/**
 *  test {@link Dice#roll()} by rolling the default dice once and checking that:
 *  <ul>
 *  <li> the invariant has not been broken </li>
 *  <li> the roll count has been incremented </li>
 *  </ul>
 */
@Test
public void testRoll() {

	    int countRolls = diceDefault.numberOfRolls();
		diceDefault.roll();
		Assert.assertTrue(diceDefault.invariant());
		Assert.assertEquals(diceDefault.numberOfRolls(), countRolls+1);
}
	
/**
 *  test {@link Dice#numberOfRolls()}, roll the dice 1000 times checking that:
 *  <ul>
 *  <li> invariant is respected after every roll</li>
 *  <li> the roll count is correct after every roll </li>
 *  </ul>
 */
@Test
public void testNumberOfRolls() {

		int countRolls = diceDefault.numberOfRolls();
		final int TIMES_TO_ROLL = 1000;
		
		for (int rollCount=1; rollCount<=TIMES_TO_ROLL; rollCount++){
			 diceDefault.roll();
		     Assert.assertTrue(diceDefault.invariant());
		     Assert.assertEquals(diceDefault.numberOfRolls(), countRolls+rollCount);
		}		
}
	
/**
 *  test {@link Dice#equals()}, by checking:
 *  <ul>
 *  <li> equality between the same default dice </li>
 *  <li> equality between a Dice and its copy, even with different number of rolls</li>
 *  <li> inequality between 2 die with a different number of sides (if we can create 2 such die)</li>
 *  <li> inequality between 2 die with the same number of sides but different last roll values</li>
 *  </ul> 
 */
@Test
public void testEquals() {

		Assert.assertEquals(diceDefault, diceDefault);
		Assert.assertTrue(diceRolledTwice.equals(diceCopyRolledTwice));

        if ( (( Dice.MAXIMIM_numberOfSides + Dice.MINIMUM_numberOfSides)/2 )!= Dice.DEFAULT_numberOfSides)
         Assert.assertFalse(diceDefault.equals(diceNonDefaultOK));
        
        while (diceCopyRolledTwice.lastRoll() == diceRolledTwice.lastRoll())    	
        	diceCopyRolledTwice.roll();
       
         Assert.assertFalse(diceCopyRolledTwice.equals(diceRolledTwice));
}
	
	

/**
 *  test {@link Dice#toString()} checks the format returned by the default dice:
 *  <pre>
  6-sided Dice - lastRoll = 0. (Total number of rolls = 0) 
 * </pre>
 */
 @Test
 public void testToString() {
	 
		Assert.assertEquals(diceDefault.toString(), "6-sided Dice - lastRoll = 0. (Total number of rolls = 0)");
}
 
}

