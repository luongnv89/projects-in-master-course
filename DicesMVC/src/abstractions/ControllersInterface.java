package abstractions;

import java.awt.event.ActionListener;

public interface ControllersInterface extends ActionListener{
	public void updateView();
}
