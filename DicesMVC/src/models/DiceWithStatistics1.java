package models;

import abstractions.DiceSpecification;
import abstractions.DiceWithStatisticsSpecification;


/**
 * 
 * This class extends the behaviour of the {@link Dice} with statistics for frequency of rolls<br>
 * These statistics are viewable through an updated toString method
 * 
 * @author J Paul Gibson
 * @version 1 <br>
 * <ul>
 * <li> Design decision - use inheritance to extend already existing Dice behaviour </li>
 * <li> Implementation decision - store the roll frequencies in an array of integers </li>
 * </ul>
 */
public class DiceWithStatistics1 extends Dice implements DiceWithStatisticsSpecification {
	
	/**
	 * The  number of dieWithStatistics objects that are currently instantiated<br>
	 * We  decrement this value when a DiceWithStatistics1 <code> finalized </code> is called -
	 */
	protected static  int   numberOfDieWithStatistics = 0;
	
	/**
	 * Stores the number of times each side of the dice has been rolled
	 */
	protected int rollsFrequency [];

/**
 *  Create a default dice and reset the roll frequencies for each side to be 0<br>
 *  Increment the count for the <code> numberOfDieWithStatistics</code>
 */
public DiceWithStatistics1 (){
	super();
	rollsFrequency = new int [NUMBEROFSIDES];
	resetFrequencies();
	numberOfDieWithStatistics++;
}

/**
 *  Create a dice and reset the roll frequencies for each side to be 0<br>
 *  Increment the count for the <code> numberOfDieWithStatistics</code>
 *  @param sides can specify the number of sides 
 *  (within range of {@link DiceSpecification#MINIMUM_numberOfSides} ..
 *                   {@link DiceSpecification#MAXIMIM_numberOfSides})
 */
public DiceWithStatistics1 (int sides){
	super(sides);
	rollsFrequency = new int [NUMBEROFSIDES];
	resetFrequencies();
	if (lastRoll!=0) rollsFrequency[lastRoll()-1] = 1;
	numberOfDieWithStatistics++;
}

/**
 *  Create a new DiceWithStatistics1 as a copy of a Dice<br><br>
 *  Reset all frequencies to 0 except frequency of last roll which should be set to 1.
 */
public DiceWithStatistics1 (Dice diceToCopy){
	
	super(diceToCopy);
	rollsFrequency = new int [NUMBEROFSIDES];
	resetFrequencies();
	if (lastRoll!=0) rollsFrequency[lastRoll()-1] = 1;
	numberOfDieWithStatistics++;
}



protected void finalize() throws Throwable{
	
	numberOfDieWithStatistics--;
}

/**
 * Check that the DiceWithStatistics1 is in a safe (meaningful) state
 * @return the number of sides of dice is within allowed bounds and the lastRoll
 * is within the range 1.. number of sides, and none of the frequencies are negative
 */
public boolean invariant(){

  if (rollsFrequency==null ) super.invariant();
  else
	 for (int i =0; i<NUMBEROFSIDES; i++) 
       if (rollsFrequency[i] <0) return false;

   return true;
} 

/**
 * resets the history of dice rolls to 0 rolls for all sides
 */
private void resetFrequencies(){

	if (rollsFrequency==null ) rollsFrequency = new int [NUMBEROFSIDES]; 
	for (int i =0; i<NUMBEROFSIDES; i++) rollsFrequency[i] =0;
}

/**
 * rolls the dice, updates the lastRoll value and updates the frequency history
 */
public void roll(){
	super.roll();
	rollsFrequency[lastRoll()-1] = rollsFrequency[lastRoll()-1]+1;

}


public int frequencyOfRoll(int side) throws IllegalArgumentException{
	if (side <=0 || side > MAXIMIM_numberOfSides)
		throw (new IllegalArgumentException("The number of sides specified is not valid"));
 return rollsFrequency[side-1];
}

/**
 * @return the number of DieWithStatistics that are currently instantiated
 */
public static int numberOfDieWithStatistics(){
	return numberOfDieWithStatistics;
}

/**
 * @return a string representation of the current dice state, including roll history frequency statistics<br>
 * Typical output of the format:
 <pre>
6-sided Dice - lastRoll = 6. (Total number of rolls = 6)
Frequencies: 
(1, 1)(2, 1)(3, 0)(4, 0)(5, 1)(6, 3)
</pre>
 */
@Override
public String toString(){
	String str = super.toString();
	str = str+"\n Frequencies: \n";
	for (int i =0; i<NUMBEROFSIDES; i++) 
		str=str+"("+(i+1)+", "+rollsFrequency[i]+")";
	str=str+"\n";
	return str;
}

}


