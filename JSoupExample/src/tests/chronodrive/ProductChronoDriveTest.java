package tests.chronodrive;

import org.junit.Before;

import tests.asbtract.ProductAbstractTest;
import chronodrive.ProductChronoDrive;

public class ProductChronoDriveTest extends ProductAbstractTest {

	@Before
	public void setUp() throws Exception {
		product = new ProductChronoDrive("Brand", "Name", "Link", "Image",
				"Price", "Unit Price");
	}

}
