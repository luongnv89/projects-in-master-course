package tests.chronodrive;

import org.junit.Before;

import tests.asbtract.RetailerAbstractTest;

import chronodrive.RetailerChronodrive;

public class RetailerChronoDriveTest extends RetailerAbstractTest {

	@Before
	public void setUp() throws Exception {
		retailer = new RetailerChronodrive("http://www.chronodrive.com");
		retailer.setListStore();
	}
}
