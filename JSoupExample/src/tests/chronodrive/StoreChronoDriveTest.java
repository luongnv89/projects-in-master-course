package tests.chronodrive;

import org.junit.Before;

import tests.asbtract.StoreAbstractTest;
import chronodrive.StoreChronodrive;

public class StoreChronoDriveTest extends StoreAbstractTest {

	@Before
	public void setUp() throws Exception {
		store = new StoreChronodrive(
				"Cannes - La Bocca",
				"Ouvert du lundi au samedi de 8h30 à 20h30",
				"Avenue Francis Tonner 06150 Cannes - La Bocca",
				"Tél. 04 92 97 97 16",
				"http://www.chronodrive.com/img/fr/portail/jpeg/gf/visu.1.1031.jpg",
				"http://www.chronodrive.com/img/fr/portail/jpeg/gf/map.1.1031.jpg",
				"http://www.chronodrive.com/refonte/init/?shopId=1031");
		store.setListShelfs();
	}
}
