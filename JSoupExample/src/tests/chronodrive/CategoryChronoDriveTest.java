package tests.chronodrive;

import org.junit.Before;

import tests.asbtract.CategoryAbstractTest;
import chronodrive.CategoryChronoDrive;

public class CategoryChronoDriveTest extends CategoryAbstractTest {

	@Before
	public void setUp() throws Exception {
		category = new CategoryChronoDrive(
				"Tondeuses",
				"http://www.chronodrive.com/refonte/store/articles/1829365/1829366/1829367/1829376");
		category.setListProduct();
	}
}
