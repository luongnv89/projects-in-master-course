package tests.asbtract;

import org.junit.AfterClass;
import org.junit.Test;

import abstracts.MainShelfAbstract;

public class MainShelfAbstractTest {

	protected static MainShelfAbstract shelf;

	@Test
	public void testGetMethod() {
		System.out.println(shelf.toString());
	}

	@Test
	public void testGetListMainShelfLevel2() {
		System.out.println("Total MainShelf: "
				+ shelf.getListMainShelfLevel2().size());
		for (int i = 0; i < shelf.getListMainShelfLevel2().size(); i++) {
			System.out
					.println(shelf.getListMainShelfLevel2().get(i).toString());
		}
	}

	@AfterClass
	public static void tearDown() throws Exception {
		shelf = null;
	}

}
