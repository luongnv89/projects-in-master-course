package tests.asbtract;

import java.util.ArrayList;


import org.junit.AfterClass;
import org.junit.Test;

import abstracts.RetailerAbstract;
import abstracts.StoreAbstract;

public class RetailerAbstractTest {

	protected static RetailerAbstract retailer;

	@Test
	public void testGetMethod() {
		System.out.println(retailer.toString());
	}

	@Test
	public void testGetStore() {
		ArrayList<StoreAbstract> listStore = new ArrayList<>();
		listStore.addAll(retailer.getListStore());
		System.out.println("Total store:" + listStore.size());
		for (int i = 0; i < listStore.size(); i++) {
			System.out.println(listStore.get(i).toString());
		}
	}

	@AfterClass
	public static void tearDown() throws Exception {
		retailer = null;
	}

}
