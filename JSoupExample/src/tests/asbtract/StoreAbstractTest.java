package tests.asbtract;

import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.Test;

import abstracts.MainShelfAbstract;
import abstracts.StoreAbstract;

public class StoreAbstractTest {

	protected static StoreAbstract store;

	@Test
	public void testGetMethod() {
		System.out.println(store.toString());
	}

	@Test
	public void testGetListShelfs() {
		ArrayList<MainShelfAbstract> listShelft = new ArrayList<MainShelfAbstract>();
		listShelft.addAll(store.getListShelfs());
		System.out.println("\n\n\n\nTotal Shelf: " + listShelft.size());
		for (int i = 0; i < listShelft.size(); i++) {
			System.out.println(listShelft.get(i).toString());
		}
	}

	@AfterClass
	public static void tearDown() throws Exception {
		store = null;
	}

}
