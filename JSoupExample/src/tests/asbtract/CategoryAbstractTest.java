package tests.asbtract;

import org.junit.AfterClass;
import org.junit.Test;

import abstracts.CategoryAbstract;

public class CategoryAbstractTest {

	protected static CategoryAbstract category;

	@Test
	public void testGetMethod() {
		System.out.println(category.toString());
	}

	@Test
	public void testGetListCategories() {
		for (int i = 0; i < category.getListProduct().size(); i++) {
			System.out.println(category.getListProduct().get(i).toString());
		}
	}

	@AfterClass
	public static void tearDown() throws Exception {
		category = null;
	}
}
