package tests.asbtract;

import org.junit.AfterClass;
import org.junit.Test;

import abstracts.ProductAbstract;

public class ProductAbstractTest {
	protected static ProductAbstract product;

	@Test
	public void testGetMethod() {
		System.out.println(product.toString());
	}

	@AfterClass
	public static void tearDown() throws Exception {
		product = null;
	}
}
