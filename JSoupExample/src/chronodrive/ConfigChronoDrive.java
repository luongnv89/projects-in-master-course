package chronodrive;

import abstracts.ConfigAbstract;

public class ConfigChronoDrive extends ConfigAbstract {

	public static final String RETAILER_LINK = "http://www.chronodrive.com";
	
	public static final String QUERY_GET_LIST_STORE = "div#middle2 > ul > li > div > span > a";
	
	public static final String QUERY_GET_LIST_MAIN_SHELF="div#template > div#global > div#super_top > ul#top4_2 > li.category > a.category";
	
	public static final String QUERY_GET_LIST_MAIN_SHELF_LV2="div#template > div#global > div#super_top > ul#top4_2 > li.category > div.rayons > ul > li.rayon";
	
	public static final String QUERY_GET_LIST_CATEGORIES="div#template > div#global > div#super_top > ul#top4_2 > li.category > div.rayons > ul > li.segment";
	
	public static final String QUERY_GET_LIST_PRODUCT="div#template > div#global > div#middle > div#middle_main > div > div#middle_left > div#content.contenu > div#liste_articles.block_float > div#MOD_Liste_article > ul > li";
	
}
