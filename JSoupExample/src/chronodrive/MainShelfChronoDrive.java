package chronodrive;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import abstracts.ConfigAbstract;
import abstracts.MainShelfAbstract;

public class MainShelfChronoDrive extends MainShelfAbstract {

	public MainShelfChronoDrive(String shelfName, String linkMainShelf) {
		super(shelfName, linkMainShelf);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setListCategories() throws MalformedURLException,
			IOException {
		Document categoriesDoc = Jsoup.parse(new URL(linkMainShelf),
				ConfigAbstract.TIME_OUT);
		Elements listCategories = categoriesDoc
				.select(ConfigChronoDrive.QUERY_GET_LIST_CATEGORIES);
		for (Element category : listCategories) {
			Elements categoryElement = category.getElementsByTag("a");
			// Name
			String categoryName = categoryElement.text();

			// Link
			String categoryLink = ConfigChronoDrive.RETAILER_LINK
					+ categoryElement.attr("href");

			// Create new
			CategoryChronoDrive newCategory = new CategoryChronoDrive(
					categoryName, categoryLink);
			this.listMainShelfLv2.add(newCategory);
		}

	}

}
