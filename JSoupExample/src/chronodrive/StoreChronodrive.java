package chronodrive;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import abstracts.ConfigAbstract;
import abstracts.StoreAbstract;

public class StoreChronodrive extends StoreAbstract {

	public StoreChronodrive(String name, String openingHour, String address,
			String telephone, String linkImage, String linkMap, String linkStore) {
		super(name, openingHour, address, telephone, linkImage, linkMap,
				linkStore);
	}

	@Override
	public void setListShelfs() throws MalformedURLException, IOException {
		Document shelfDoc = Jsoup.parse(new URL(this.getLinkStore()),
				ConfigAbstract.TIME_OUT);
		Elements listMainShelf = shelfDoc
				.select(ConfigChronoDrive.QUERY_GET_LIST_MAIN_SHELF);
		for (Element mainShelf : listMainShelf) {
			String storeName = mainShelf.text();
			this.listShelfs.add(new MainShelfChronoDrive(storeName, this
					.getLinkStore()));
		}

	}

}
