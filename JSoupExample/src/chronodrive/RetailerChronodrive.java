package chronodrive;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import abstracts.ConfigAbstract;
import abstracts.RetailerAbstract;

public class RetailerChronodrive extends RetailerAbstract {

	public RetailerChronodrive(String retailerLink)
			throws MalformedURLException, IOException {
		super(retailerLink);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setListStore() throws MalformedURLException, IOException {
		Document doc = Jsoup.parse(new URL(getRetailerLink()), ConfigAbstract.TIME_OUT);
		Elements linkListRetailer = doc.getElementsByClass("search_bot");
		Elements linkListRetailer2 = linkListRetailer.get(0).getElementsByTag(
				"a");
		String linkListRetailers = linkListRetailer2.attr("href");
		Document docStore = Jsoup.parse(new URL(linkListRetailers), ConfigAbstract.TIME_OUT);
		Elements listRetailer = docStore
				.select("div#middle2 > ul > li > div > span > a");
		for (Element retailerList : listRetailer) {
			// Store Name
			String storeName = retailerList.text();
			String storeLinkIntro = getRetailerLink()
					+ retailerList.attr("href");
			Document storeDoc = Jsoup.parse(new URL(storeLinkIntro), ConfigAbstract.TIME_OUT);
			// Store Link
			Elements storeElement = storeDoc.getElementsByClass("e_mag");
			String storeLink = getRetailerLink() + storeElement.attr("href");
			// opening Hour
			Elements openingHourElement = storeDoc.getElementsByTag("time");
			String openingHour = openingHourElement.text();
			// Address
			Elements addressElement = storeDoc.select("div.adr_mag");
			String address = addressElement.text();
			// Tel
			Elements tel = storeDoc.select("div.tel_mag");
			String telephone = tel.text();
			// Link Image
			Elements linkImageElement = storeDoc
					.select("div#global.png > div.ct_global > div#middle.auto_width > div.bloc_mag > img");
			String linkImage = getRetailerLink() + linkImageElement.attr("src");
			// Link map
			String linkMap = linkImage.replace("visu", "map");
			// Show

			listStore.add(new StoreChronodrive(storeName, openingHour, address,
					telephone, linkImage, linkMap, storeLink));
		}
	}

	@Override
	public String setRetailerName() throws MalformedURLException, IOException {
		Document doc = Jsoup.parse(new URL(getRetailerLink()), ConfigAbstract.TIME_OUT);
		return doc.title();

	}

	@Override
	public String setRetailerLogoLink() throws MalformedURLException,
			IOException {
		Document doc = Jsoup.parse(new URL(getRetailerLink()), ConfigAbstract.TIME_OUT);
		Elements logoLink = doc.select("img#logo_chronodrive");
		return logoLink.attr("src");
	}

}
