package abstracts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

public abstract class CategoryAbstract {
	static int NUMBER_CATEGORY = 0;

	int categoryId;
	String categoryName;
	protected String categoryLink;

	ArrayList<ProductAbstract> listProduct = new ArrayList<>();

	public CategoryAbstract(String categoryName,
			String categoryLink) {
		this.categoryId = NUMBER_CATEGORY;
		this.categoryName = categoryName;
		this.categoryLink = categoryLink;
		NUMBER_CATEGORY++;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryLink() {
		return categoryLink;
	}

	public void setCategoryLink(String categoryLink) {
		this.categoryLink = categoryLink;
	}

	public ArrayList<ProductAbstract> getListProduct() {
		return listProduct;
	}

	public abstract void setListProduct() throws MalformedURLException, IOException;

	@Override
	public String toString() {
		String str = "";
		str = str + "\nID: " + this.getCategoryId();
		str = str + "\nName: " + this.getCategoryName();
		str = str + "\nLink: " + this.getCategoryLink();
		return str;
	}
}
