package abstracts;

public abstract class ProductAbstract {

	static int NUMBER_PRODUCT = 0;

	int productID;
	String productBrand;
	String productName;
	String productLink;
	String productImage;
	String productPrice;
	String productUnitPrice;

	public ProductAbstract(String productBrand, String productName,
			String productLink, String productImage, String productPrice,
			String productUnitPrice) {
		super();
		this.productID = NUMBER_PRODUCT;
		this.productBrand = productBrand;
		this.productName = productName;
		this.productLink = productLink;
		this.productImage = productImage;
		this.productPrice = productPrice;
		this.productUnitPrice = productUnitPrice;
		NUMBER_PRODUCT++;
	}

	public int getProductID() {
		return productID;
	}

	public String getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductLink() {
		return productLink;
	}

	public void setProductLink(String productLink) {
		this.productLink = productLink;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductUnitPrice() {
		return productUnitPrice;
	}

	public void setProductUnitPrice(String productUnitPrice) {
		this.productUnitPrice = productUnitPrice;
	}

	@Override
	public String toString() {
		String str = "";
		str = str + "\nId: " + this.getProductID();
		str = str + "\nName: " + this.getProductName();
		str = str + "\nBrand: " + this.getProductBrand();
		str = str + "\nPrice: " + this.getProductPrice();
		str = str + "\nUnit Price: " + this.getProductUnitPrice();
		str = str + "\nLink : " + this.getProductLink();
		str = str + "\nLink Image: " + this.getProductImage();
		return str;
	}
}
