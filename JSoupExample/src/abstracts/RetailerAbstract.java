package abstracts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * The retailer abstract.
 * 
 * @author crocode
 * 
 */
/**
 * @author crocode
 * 
 */
public abstract class RetailerAbstract {
	public static int NUMBER_RETAILER = 0;
	/**
	 * The id of retailer
	 */
	int retailerId;
	/**
	 * The link of retailer
	 */
	String retailerLink;
	/**
	 * The name of retailer
	 */
	String retailerName;

	/**
	 * Link to get the logo of retailer
	 */
	String retailerLogoLink;

	/**
	 * List stores of retailer
	 */
	protected ArrayList<StoreAbstract> listStore = new ArrayList<>();

	/**
	 * Get the list stores of retailer
	 * 
	 * @return the list stores of retailer
	 */
	public ArrayList<StoreAbstract> getListStore() {
		return listStore;
	}

	/**
	 * Set the list stores of retailer. <br>
	 * Crawler
	 * 
	 * @param listStore
	 *            List store of retailer
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public abstract void setListStore() throws MalformedURLException,
			IOException;

	/**
	 * Constructor retailer with the id and the name is automatically setting
	 * 
	 * @param retailerLink
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public RetailerAbstract(String retailerLink) throws MalformedURLException,
			IOException {
		super();
		setRetailerId(NUMBER_RETAILER);
		this.retailerLink = retailerLink;
		this.retailerName = setRetailerName();
		this.retailerLogoLink = setRetailerLogoLink();
		NUMBER_RETAILER++;
	}

	public int getRetailerId() {
		return retailerId;
	}

	public void setRetailerId(int retailerId) {
		this.retailerId = retailerId;
	}

	public String getRetailerLink() {
		return retailerLink;
	}

	public void setRetailerLink(String retailerLink) {
		this.retailerLink = retailerLink;
	}

	public String getRetailerName() {
		return retailerName;
	}

	public String getRetailerLogoLink() {
		return retailerLogoLink;
	}

	public abstract String setRetailerLogoLink() throws MalformedURLException,
			IOException;

	public abstract String setRetailerName() throws MalformedURLException,
			IOException;

	@Override
	public String toString() {
		String str = "";
		str = str + "\nID: " + this.getRetailerId();
		str = str + "\nName: " + this.getRetailerName();
		str = str + "\nLink: " + this.getRetailerLink();
		str = str + "\nLink logo: " + this.getRetailerLogoLink();
		return str;
	}
}
