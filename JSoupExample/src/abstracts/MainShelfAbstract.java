package abstracts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * @author crocode
 *
 */
public abstract class MainShelfAbstract {
	static int NUMBER_MAINSHELF = 0;

	int shelfID;
	String shelfName;
	protected String linkMainShelf;

	protected ArrayList<CategoryAbstract> listMainShelfLv2 = new ArrayList<CategoryAbstract>();

	public MainShelfAbstract(String shelfName, String linkMainShelf) {
		super();
		this.shelfID = NUMBER_MAINSHELF;
		this.shelfName = shelfName;
		this.linkMainShelf = linkMainShelf;
		NUMBER_MAINSHELF++;
	}

	public int getShelfID() {
		return shelfID;
	}

	public String getShelfName() {
		return shelfName;
	}

	public void setShelfName(String shelfName) {
		this.shelfName = shelfName;
	}

	public ArrayList<CategoryAbstract> getListMainShelfLevel2() {
		return listMainShelfLv2;
	}

	public abstract void setListCategories() throws MalformedURLException, IOException;

	@Override
	public String toString() {
		String str = "";
		str = str + "\nShelf Id: " + this.getShelfID();
		str = str + "\nShelf Name: " + this.getShelfName();
		str = str + "\nShelf Link: " + this.getLinkMainShelf();
		return str;
	}

	public String getLinkMainShelf() {
		return linkMainShelf;
	}

	public void setLinkMainShelf(String linkMainShelf) {
		this.linkMainShelf = linkMainShelf;
	}
	
}
