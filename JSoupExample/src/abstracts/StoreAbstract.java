package abstracts;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

public abstract class StoreAbstract {

	public static int NUMBER_STORE = 0;

	int idStore;
	String name;
	String openingHour;
	String address;
	String telephone;
	String linkImage;
	String linkMap;
	String linkStore;

	protected ArrayList<MainShelfAbstract> listShelfs = new ArrayList<MainShelfAbstract>();

	public StoreAbstract(String name, String openingHour, String address,
			String telephone, String linkImage, String linkMap, String linkStore) {
		super();
		this.idStore = NUMBER_STORE;
		this.name = name;
		this.openingHour = openingHour;
		this.address = address;
		this.telephone = telephone;
		this.linkImage = linkImage;
		this.linkMap = linkMap;
		this.linkStore = linkStore;
		NUMBER_STORE++;
	}

	public ArrayList<MainShelfAbstract> getListShelfs() {
		return listShelfs;
	}

	public abstract void setListShelfs() throws MalformedURLException,
			IOException;

	public int getIdStore() {
		return idStore;
	}

	public String getLinkMap() {
		return linkMap;
	}

	public void setLinkMap(String linkMap) {
		this.linkMap = linkMap;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOpeningHour() {
		return openingHour;
	}

	public void setOpeningHour(String openingHour) {
		this.openingHour = openingHour;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getLinkImage() {
		return linkImage;
	}

	public void setLinkImage(String linkImage) {
		this.linkImage = linkImage;
	}

	public String getLinkStore() {
		return linkStore;
	}

	public void setLinkStore(String linkStore) {
		this.linkStore = linkStore;
	}

	@Override
	public String toString() {
		String str = "";
		str = str + "\nStore ID: " + this.getIdStore();
		str = str + "\nName: " + this.getName();
		str = str + "\nAddress: " + this.getAddress();
		str = str + "\nTelephone: " + this.getAddress();
		str = str + "\nOpening Hour: " + this.getOpeningHour();
		str = str + "\nLink: " + this.getLinkStore();
		str = str + "\nLink Image: " + this.getLinkImage();
		str = str + "\nLink Map: " + this.getLinkMap();
		return str;
	}

}
