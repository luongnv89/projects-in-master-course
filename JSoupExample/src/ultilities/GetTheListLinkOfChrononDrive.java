package ultilities;

import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetTheListLinkOfChrononDrive {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String mainPage = "http://www.chronodrive.com";
		String str = "http://www.chronodrive.com/magasins-chronodrive";
		URL url = new URL(str);
		Document doc = Jsoup.parse(url, 1000);
		Element retailerList = doc.getElementById("middle2");
		Elements getDetail = retailerList.getElementsByClass("c_mag");
		int column = 0;
		for (Element item : getDetail) {
			column++;
			System.out.println("\n****************COLUME NUMBER " + column
					+ "***************");
			Elements listLink = item.getElementsByTag("li");
			int i = 0;
			for (Element link : listLink) {
				// Process each items
				i++;
				System.out
						.println("\n ------- ITEM " + i + " ----------------");
				Elements getLinkBigTitle = link.getElementsByClass("t_mag");

				Elements getLinkSecond = link.getElementsByClass("st_mag");

				Elements getLinkLocations2 = link.getElementsByTag("a");

				System.out.println("Title < " + getLinkBigTitle.text() + "> "
						+ getLinkLocations2.get(0).attr("href"));
				System.out.println("Small title < " + getLinkSecond.text()
						+ "> " + getLinkLocations2.get(1).attr("href"));
				for (int k = 2; k < getLinkLocations2.size(); k++) {
					String linklocal = "http://www.chronodrive.com"
							+ getLinkLocations2.get(k).attr("href");
					Document linkToStore = Jsoup
							.parse(new URL(linklocal), 1000);
					Elements linkget = linkToStore.getElementsByClass("e_mag");
					// Elements linkget2 = linkget.getElementsByTag("a");
					// System.out.println(linkget.attr("href"));
					System.out.println("Local < "
							+ getLinkLocations2.get(k).text() + ">" + mainPage
							+ linkget.attr("href"));
				}

			}
		}
	}

}
