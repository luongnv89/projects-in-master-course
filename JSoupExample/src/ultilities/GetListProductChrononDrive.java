package ultilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import abstracts.ConfigAbstract;
import chronodrive.CategoryChronoDrive;
import chronodrive.ConfigChronoDrive;

public class GetListProductChrononDrive {

	/**
	 * @param args
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException,
			IOException {

		String linkLStore = getLinkCategory();
		Document productDoc = Jsoup.parse(new URL(linkLStore),
				ConfigAbstract.TIME_OUT);
		Elements listProduct = productDoc
				.select(ConfigChronoDrive.QUERY_GET_LIST_PRODUCT);
		for (Element category : listProduct) {
			// URL Image
			String linkImage = ConfigChronoDrive.RETAILER_LINK
					+ category.getElementsByClass("urlZoom").attr("src");
			System.out.println(linkImage.toString());
		}
	}

	private static String getLinkCategory() {
		// TODO Auto-generated method stub
		return "http://www.chronodrive.com/refonte/store/articles/188461/188517/3014111/3014118";
	}

	private static String getLinkStore() {
		return "http://www.chronodrive.com/refonte/init/?shopId=1044";
	}

	private static String getRetailerLink() {
		// TODO Auto-generated method stub
		return "http://www.chronodrive.com";
	}
}
