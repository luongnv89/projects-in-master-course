package ultilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CoraDriveListLink {

	/**
	 * @param args
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException,
			IOException {
		Document content = Jsoup.parse(new URL("http://www.coradrive.fr/"),
				1000);
		Elements listRetailer = content
				.select("div#sidebar > nav > ul > li > ul > li > a");
		for (Element retailerLink : listRetailer) {
			System.out.println(retailerLink.text() + ": "
					+ retailerLink.attr("href"));
		}
	}
}
