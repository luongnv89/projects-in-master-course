package ultilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import abstracts.ConfigAbstract;
import chronodrive.CategoryChronoDrive;
import chronodrive.ConfigChronoDrive;

public class GetCategoriesChrononDrive {

	/**
	 * @param args
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException,
			IOException {

		String linkLStore = getLinkStore();
		Document categoriesDoc = Jsoup.parse(new URL(linkLStore),
				ConfigAbstract.TIME_OUT);
		Elements listCategories = categoriesDoc
				.select(ConfigChronoDrive.QUERY_GET_LIST_CATEGORIES);
		for (Element category : listCategories) {
			Elements categoryElement = category.getElementsByTag("a");
			// Name
			String categoryName = categoryElement.text();

			// Link
			String categoryLink = getRetailerLink()
					+ categoryElement.attr("href");

			// Create new
			CategoryChronoDrive newCategory = new CategoryChronoDrive(categoryName,
					categoryLink);
			System.out.println(newCategory.toString());
		}
	}

	private static String getLinkStore() {
		return "http://www.chronodrive.com/refonte/init/?shopId=1044";
	}

	private static String getRetailerLink() {
		// TODO Auto-generated method stub
		return "http://www.chronodrive.com";
	}
}
