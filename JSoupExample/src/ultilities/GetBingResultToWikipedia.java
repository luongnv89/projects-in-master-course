package ultilities;

import java.io.IOException;
import java.net.URL;

import javax.print.Doc;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetBingResultToWikipedia {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String str = "http://www.bing.com/search?q=My+heart+will+go+on&qs=n&form=QBRE&filt=all&pq=my+heart+will+go+on&sc=8-19&sp=-1&sk=";
		URL url = new URL(str);
		Document doc = Jsoup.parse(url, 10000);
		// Get list Items
		Elements listItems = doc.getElementsByTag("cite");
		// Get link of each item
		for (Element item : listItems) {
			if (item.text().contains("wikipedia")) {
				String wikiLink = "http://" + item.text();
				URL wiki = new URL(wikiLink);
				Document doc2 = Jsoup.parse(wiki, 1000);
				Element content = doc2.getElementById("content");
				Elements listInfor = content.getElementsByTag("p");
				String infor = "";
				for (Element item2 : listInfor) {
					infor = infor + " "+item2.text();
				}
				System.out.println(infor);

			}
		}

	}

}
