package ultilities;

import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ChrononDriveGetListCategories {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String str = "http://www.chronodrive.com";
		Document doc = Jsoup.parse(new URL(
				"http://www.chronodrive.com/refonte/init/?shopId=1031"), 1000);
		Elements listShelf = doc.select("li.category > a");
		int countCategory = 0;
		for (Element shelf : listShelf) {
			String shelfName = shelf.text();
			Elements listCategories = doc
					.select("li.category > div.rayons > ul > li.segment > a.btlink");
			for (Element category : listCategories) {
				countCategory++;
				String pathCategory = str + category.attr("href");
				String nameCategory = category.text();
				System.out.println(shelfName + "-->" + nameCategory + "-->"
						+ pathCategory);
			}
		}
		System.out.println("Total: " + countCategory + " categories");
	}

}
