package ultilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CoraDriveListShelfs {

	/**
	 * @param args
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException,
			IOException {
		Document content = Jsoup.parse(new URL("http://www.coradrive.fr/colmar/"),
				1000);
		Elements listRetailer = content
				.select("div#nav > div.wrapinner > nav#nav1 > ul > li.nav-niv1 > div.panel > div > ul > li > span.rub > a");
		System.out.println("LIST TOP");
		for (Element retailerLink : listRetailer) {
			System.out.println(retailerLink.text() + ": "
					+ retailerLink.attr("href"));
		}
		
		Elements listRetailer2 = content
				.select("div#nav > div.wrapinner > nav#nav1 > ul > li.nav-niv1 > div.panel > div > ul > li > a");
		System.out.println("LIST SECOND");
		for (Element retailerLink : listRetailer2) {
			System.out.println(retailerLink.text() + ": "
					+ retailerLink.attr("href"));
		}
	}
}
