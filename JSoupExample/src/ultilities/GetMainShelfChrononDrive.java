package ultilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import chronodrive.ConfigChronoDrive;

import abstracts.ConfigAbstract;

public class GetMainShelfChrononDrive {

	/**
	 * @param args
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException,
			IOException {

		String linkLStore = getLinkStore();
		Document shelfDoc = Jsoup.parse(new URL(linkLStore),
				ConfigAbstract.TIME_OUT);
		Elements listMainShelf = shelfDoc
				.select(ConfigChronoDrive.QUERY_GET_LIST_MAIN_SHELF);
		for (Element mainShelf : listMainShelf) {
			String storeName = mainShelf.text();
			System.out.println(storeName);

		}
	}

	private static String getLinkStore() {
		return "http://www.chronodrive.com/refonte/init/?shopId=1044";
	}

	private static String getRetailerLink() {
		// TODO Auto-generated method stub
		return "http://www.chronodrive.com";
	}
}
