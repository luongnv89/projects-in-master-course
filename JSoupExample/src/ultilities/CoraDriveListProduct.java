package ultilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CoraDriveListProduct {

	/**
	 * @param args
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException,
			IOException {
		Document content = Jsoup
				.parse(new URL(
						"http://www.coradrive.fr/colmar/tous-les-rayons/boutiques/bio/produits-frais.html"),
						1000);
		Elements listRetailer = content.select("div.col1");
		for (Element retailerLink : listRetailer) {
			// System.out.println(retailerLink.text() + ": "
			// + retailerLink.attr("href"));
			Elements productLink = retailerLink.getElementsByClass("desc");
			Elements productLink2 = productLink.get(0).getElementsByTag("a");
			Elements productUnitPrice = retailerLink
					.select("div.price > span.info");
			Elements productPrice = retailerLink
					.getElementsByClass("prix-produit");
//			Elements productAdvan = retailerLink
//					.select("div.avantages > ul > li");

			System.out.println("Describe: " + productLink.text());
			System.out.println("UnitPrice: " + productUnitPrice.text());
			System.out.println("Price: " + productPrice.text());
//			System.out.println("Advan: " + productAdvan.text());
			System.out.println("Link: " + productLink2.attr("href"));
		}
	}
}
