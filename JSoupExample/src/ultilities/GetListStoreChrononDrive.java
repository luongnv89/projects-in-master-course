package ultilities;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GetListStoreChrononDrive {

	/**
	 * @param args
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException,
			IOException {

		String linkListRetailers = "http://www.chronodrive.com/magasins-chronodrive";
		Document docStore = Jsoup.parse(new URL(linkListRetailers), 1000);
		Elements listRetailer = docStore
				.select("div#middle2 > ul > li > div > span > a");
		for (Element retailerList : listRetailer) {
			// Store Name
			String storeName = retailerList.text();
			String storeLinkIntro = getRetailerLink()
					+ retailerList.attr("href");
			Document storeDoc = Jsoup.parse(new URL(storeLinkIntro), 1000);
			// Store Link
			Elements storeElement = storeDoc.getElementsByClass("e_mag");
			String storeLink = getRetailerLink() + storeElement.attr("href");
			// opening Hour
			Elements openingHourElement = storeDoc.getElementsByTag("time");
			String openingHour = openingHourElement.text();
			// Address
			Elements addressElement = storeDoc.select("div.adr_mag");
			String address = addressElement.text();
			// Tel
			Elements tel = storeDoc.select("div.tel_mag");
			String telephone = tel.text();
			// Link Image
			Elements linkImageElement = storeDoc
					.select("div#global.png > div.ct_global > div#middle.auto_width > div.bloc_mag > img");
			String linkImage = getRetailerLink() + linkImageElement.attr("src");
			// Link map
			String linkMap = linkImage.replace("visu", "map");
			// Show

			System.out.println("\n\nName: " + storeName);
			System.out.println("Link: " + storeLink);
			System.out.println("Opening Hour: " + openingHour);
			System.out.println("Address: " + address);
			System.out.println("Telephone: " + telephone);
			System.out.println("LinkImage: " + linkImage);
			System.out.println("linkMap: " + linkMap);

		}
	}

	private static String getRetailerLink() {
		// TODO Auto-generated method stub
		return "http://www.chronodrive.com";
	}
}
