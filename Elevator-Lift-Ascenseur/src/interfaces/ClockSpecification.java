package interfaces;

import tools.HasInvariant;


/**
 * A clock used to count time increments <br>
 * Tested by {@link tests.JUnit_ClockSpecification}
 * @author J Paul Gibson
 * @version 1
 */
public interface ClockSpecification extends HasInvariant{
	
	/**
	 * Increment the clock by 1 unit <br>
	 * Tested by {@link tests.JUnit_ClockSpecification#test_tick()}
	 */
	public void tick();
	
	/**
	 * Increment the clock <br>
	 * Tested by {@link tests.JUnit_ClockSpecification#test_tick_int}
	 * @param x is the number of time units by which the clock should be incremented
	 * @throws IllegalArgumentException when x is negative
	 */
	public void tick (int x) throws IllegalArgumentException;
	
	/**
	 * Set the clock timer to 0 <br>
	 * Tested by {@link tests.JUnit_ClockSpecification#test_resetClock}
	 */
	public void resetClock();
	
	/**
	 * Set the clock to a specific value <br>
	 * Tested by {@link tests.JUnit_ClockSpecification#test_setClock(int)}
	 * @param x
	 * @throws IllegalArgumentException when x is negative
	 */
	public void setClock(int x) throws IllegalArgumentException;
	
	/**
	 * Tested by {@link tests.JUnit_ClockSpecification#test_getTime()}
	 * @return the time value representing a number of ticks  
	 */
	public int getTime ();
	
	/**
	 * Tested by {@link tests.JUnit_ClockSpecification#test_invariant()}
	 * @return true iff the number of ticks is non-negative
	 */
	public boolean invariant();

}
