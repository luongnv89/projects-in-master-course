package interfaces;

import models.Direction;

/**
 * The interface provided by the Elevator to the Controller allowing it to read the current
 * state of the elevator system
 * (see {@link interfaces.ElevatorSpecification})
 * @author J Paul Gibson
 * @version 1
 */
public interface ElevatorForController {
	
	/**
	 * Permits elevator controller to see if a request to go down has been made at a specified floor
	 * @param floor specifies the floor at which the request is being checked
	 * @throws IllegalArgumentException if the floor is not valid (from 1.. NUMBER_OF_FLOORS-1)
	 */
	public boolean downPressedAtFloor (int floor)throws IllegalArgumentException;
	
	/**
	 * Permits elevator controller to see if a request to go up has been made at a specified floor
	 * @param floor specifies the floor at which the request is being checked
	 * @throws IllegalArgumentException if the floor is not valid (from 0.. NUMBER_OF_FLOORS-2)
	 */
	public boolean upPressedAtFloor (int floor)throws IllegalArgumentException;
	
	/**
	 * Permits elevator controller to see if there is a request to a particular floor
	 * @return Whether there is currently a request to go to the specified floor (inside the elevator)
	 * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public boolean floorPressedInElevator (int floor) throws IllegalArgumentException;
	
	/**
   	 * @param floor
	 * @return whether there is a request for a floor (inside the elevator) above the specified floor
     * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public boolean noMoreRequestsBelow(int floor)throws IllegalArgumentException;
	
	/**
	 * @param floor
	 * @return whether there is a request for a floor (inside the elevator) above the specified floor
     * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public boolean noMoreRequestsAbove(int floor)throws IllegalArgumentException;
	
	/**
	 * @return the current direction in which the elevator is moving
	 */
    public Direction currentDirection ();
    
    /**
     * @return the current floor of the elevator
     */
    public int currentFloor ();
    
    /**
     * @return the number of the top floor
     */
    public int topFloor ();
    
}
