package interfaces;


/**
 * The Elevator code was developed as a sample solution to a 
 * <a href=" http://www-public.telecom-sudparis.eu/~gibson/Teaching/CSC7003/L4-Requirements-SampleSolution.pdf">
 * lift problem </a> given to the MSc SAI students in module
 * <a href="http://www-public.telecom-sudparis.eu/~gibson/Teaching/CSC7003/">
 * CSC7003 - Basics of Software Engineering</a>
 * <br>
 * The elevator must offer 2 interfaces: 
 * <ul>
 * <li> To the controller (specified by {@link ElevatorForController})</li>
 * <li> To the users (specified by {@link ElevatorForUsers})</li>
 * </ul>
 * It also provides a means of changing the logic controller
 * @author J Paul Gibson
 * @version 1
 */
public interface ElevatorSpecification extends ElevatorForController, ElevatorForUsers{
	

	/**
     * Update the lift controller component - intended to be done by an engineer
	 * @param controller is the new controller module that will decide the logic of the elevator movement
	 * @throws IllegalArgumentException if the controller is null
	 */
	public void installController(ControllerForElevator controller) throws IllegalArgumentException;
	
	/**
	 * move to next floor (as decided by the controller)
	 * @return if the lift should stop at the next floor (as decided by the controller)
	 */
	public  boolean updateFloor ();
}
