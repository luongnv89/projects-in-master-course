package interfaces;

import models.Direction;

/**
 * The interface defining the methods offered by the controller to the elevator
 * (see {@link interfaces.ElevatorSpecification})
 * @author J Paul Gibson
 * @version 1
 */
public interface ControllerForElevator {
	
	/**
	 * @return the direction in which the lift should next move
	 */
	public Direction calculateDirection();
	
	/**
	 * @return whether the lift should stop at the next floor after it has completed its move
	 */
	public boolean stopAtNextFloor();
	
	/**
	 * Ensure that the controller is controlling only a single elevator
	 * @param elevator is the unique elevator to which the controller is now connected
	 * @throws IllegalArgumentException if the elevator being connected is null
	 */
	public void bindToElevator (ElevatorForController elevator) throws IllegalArgumentException;

}
