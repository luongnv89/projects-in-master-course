package interfaces;

/**
 * The interface offered by the elevator to the users
 * (see {@link interfaces.ElevatorSpecification})
 * @author J Paul Gibson
 * @version 1
 */
public interface ElevatorForUsers {
	
	/**
	 * Permits elevator users to request elevator to take them up
	 * @param floor specifies the floor at which the up request is made
	 * @throws IllegalArgumentException if the floor is not valid (from 0.. NUMBER_OF_FLOORS-2)
	 */
	public void pressUp(int floor)throws IllegalArgumentException;
	
	/**
	 * Permits elevator users to see if a request to go up has been made at a specified floor
	 * @param floor specifies the floor at which the request is being checked
	 * @throws IllegalArgumentException if the floor is not valid (from 0.. NUMBER_OF_FLOORS-2)
	 */
	public boolean upPressedAtFloor (int floor)throws IllegalArgumentException;
	
	/**
	 * Permits elevator users to request elevator to take them down
	 * @param floor specifies the floor at which the down request is made

	 * @throws IllegalArgumentException if the floor is not valid (from 1.. NUMBER_OF_FLOORS-1)
	 */
	public void pressDown(int floor)throws IllegalArgumentException;
	
	/**
	 * Permits elevator users to see if a request to go down has been made at a specified floor
	 * @param floor specifies the floor at which the request is being checked
	 * @throws IllegalArgumentException if the floor is not valid (from 1.. NUMBER_OF_FLOORS-1)
	 */
	public boolean downPressedAtFloor (int floor)throws IllegalArgumentException;
	
	
	/**
	 * Permits elevator users inside the elevator to request elevator to take them to a particular floor 
	 * @param floor
     * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public void pressFloorInElevator (int floor)throws IllegalArgumentException;
	
	/**
	 * Permits elevator users inside the elevator to see if there is a request to a particular floor
	 * @return Whether there is currently a request to go to the specified floor (inside the elevator)
	 * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public boolean floorPressedInElevator (int floor) throws IllegalArgumentException;

}
