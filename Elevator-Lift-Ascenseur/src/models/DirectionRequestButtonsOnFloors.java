package models;

import tests.JUnit_DirectionRequestButtonsOnFloors;
import tools.HasInvariant;
import tools.InvariantBroken;

/**
 * Tested by {@link tests.JUnit_DirectionRequestButtonsOnFloors}
 *        and {@link tests.Validation_DirectionRequestButtonsOnFloors}<br>
 * For managing the state of the direction requests on all floors of an elevator
 * (see {@link interfaces.ElevatorSpecification})
 * @author J Paul Gibson
 * @version 1
 */
public class DirectionRequestButtonsOnFloors implements HasInvariant{
	
	/**
	 * The number of floors is in range <code> 0 .. NUMBER_OF_FLOORS</code><br>
	 * This must be >=2
	 */
	private final int NUMBER_OF_FLOORS;
	
	/**
	 * The up requests on each floor<br>
	 * There must not be an up request on the top floor<br>
	 */
	private boolean [] up;
	
	/**
	 * The down requests on each floor<br>
	 * There must not be a down request on the bottom floor<br>
	 */
	private boolean [] down;
	
	/**
	 * Initialises all direction requests (up and down, on each floor) to be false
	 * @param numberOfFloors is the number of floors in the lift system
	 * @throws IllegalArgumentException if the number of floors is <=1
	 * @throws InvariantBroken if not initialised in a safe state
	 */
	public DirectionRequestButtonsOnFloors (int numberOfFloors) 
	       throws IllegalArgumentException, InvariantBroken{
			
		if (numberOfFloors <2) 
			throw new IllegalArgumentException("The number of floors must be greater than 1");
		NUMBER_OF_FLOORS = numberOfFloors;	
		
		up = new boolean [NUMBER_OF_FLOORS];
		down = new boolean [NUMBER_OF_FLOORS];
		
		for (int floor =0; floor<NUMBER_OF_FLOORS;floor++){
			up [floor] = false;
			down [floor] = false;
		}
		
		if (!invariant()) 
			throw new InvariantBroken("The direction request buttons have not been initialised in a safe state");
	}
	
	/**
	 * The following safety conditions are required:
	 * <ul>
	 * <li> number of floors must be at least 2 </li>
	 * <li> the number of up/down buttons is the same as the number of floors</li>
	 * <li> the down button on the bottom floor is disabled </li>
	 * <li> the up button on the top floor is disabled </li>
	 * </ul>
	 * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_invariant()}
	 * @return true if all these safety conditions are met, and false otherwise. 
	 */
	public boolean invariant(){
		
		if (NUMBER_OF_FLOORS<2) return false;
		if (up.length != NUMBER_OF_FLOORS) return false;
		if (down.length != NUMBER_OF_FLOORS) return false;
		if (down[0]) return false;
		if (up[NUMBER_OF_FLOORS-1]) return false;
		return true;
	}
	
	/**
	 * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_Up_presses_cancels()}
	 * @param floor specifies the floor at which the up request is being checked
	 * @throws IllegalArgumentException if the floor specified is not valid
	 * @return if a request to go up has been made at the specified floor
	 */
	public boolean upPressedAtFloor (int floor) throws IllegalArgumentException{
			
		if (floor<0 || floor > NUMBER_OF_FLOORS-1) 
			throw new IllegalArgumentException("The floor specified is not valid");
		
		return up[floor];
		}
	
	/**
	 * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_Up_presses_cancels()}
	 * @param floor specifies the floor at which the down request is being checked
	 * @throws IllegalArgumentException if the floor specified is not valid
	 * @return if a request to go down has been made at the specified floor
	 */
	public boolean downPressedAtFloor (int floor)throws IllegalArgumentException{
			
		if (floor<0 || floor > NUMBER_OF_FLOORS-1) 
			throw new IllegalArgumentException("The floor specified is not valid");
		
		return down[floor];
		}
	
	/**
	 * Update the up button request except if it is made on top floor<br>
	 * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_Up_presses_cancels()}
	 * @param floor specifies the floor at which the up request is being made
	 * @throws IllegalArgumentException if the floor specified is not valid
	 */
	public void pressUp(int floor)throws IllegalArgumentException, InvariantBroken{
			
		if (floor<0 || floor > NUMBER_OF_FLOORS-1) 
			throw new IllegalArgumentException("The floor specified is not valid");
		
		if (floor<NUMBER_OF_FLOORS-1) up[floor] = true;
		
		if (!invariant()) 
			throw new InvariantBroken("The direction request buttons are in an unsafe state");}
	
	/**
	 * Update the down button request except if it is made on bottom floor<br>
	 * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_Down_presses_cancels()}
	 * @param floor specifies the floor at which the down request is being made
	 * @throws IllegalArgumentException if the floor specified is not valid
	 */
	public void pressDown(int floor)throws IllegalArgumentException, InvariantBroken{
			
		if (floor<0 || floor > NUMBER_OF_FLOORS-1) 
			throw new IllegalArgumentException("The floor specified is not valid");
		
		if (floor>0) down[floor] = true;
		
		if (!invariant()) 
			throw new InvariantBroken("The direction request buttons are in an unsafe state");
	}
	
	/**
	 * Cancel the up request at the specified floor <br>
	 * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_Up_presses_cancels()}
	 * @param floor
	 * @throws IllegalArgumentException if the floor specified is not valid
	 */
	public void cancelUpRequest(int floor)throws IllegalArgumentException,InvariantBroken{
			
		if (floor<0 || floor > NUMBER_OF_FLOORS-1) 
			throw new IllegalArgumentException("The floor specified is not valid");
		
		up[floor] = false;
		
		if (!invariant()) 
			throw new InvariantBroken("The direction request buttons are in an unsafe state");
	}
	
	
	/**
	 * Cancel the down request at the specified floor <br>
	 * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_Down_presses_cancels()}
	 * @param floor
	 * @throws IllegalArgumentException if the floor specified is not valid
	 */
	public void cancelDownRequest(int floor)throws IllegalArgumentException,InvariantBroken{
			
		if (floor<0 || floor > NUMBER_OF_FLOORS-1) 
			throw new IllegalArgumentException("The floor specified is not valid");
		
		down[floor] = false;
		if (!invariant()) 
			throw new InvariantBroken("The direction request buttons are in an unsafe state");
		}
	
	/**
	 * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_noMoreRequestsAbove()}
	 * @param floor
	 * @return whether there is a request at a floor (up or down) above the specified floor
     * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public boolean noMoreRequestsAbove(int floor)throws IllegalArgumentException{
			
		if (floor<0 || floor >= NUMBER_OF_FLOORS) 
			throw new IllegalArgumentException("The number of floors specified is not in range");
		
		for (int i = floor+1; i < NUMBER_OF_FLOORS; i++){ 
			if (up[i] || down[i]) return false;}
		
		return true;
	}
	
	/**
     * Tested by {@link JUnit_DirectionRequestButtonsOnFloors#test_noMoreRequestsBelow()}
   	 * @param floor
	 * @return whether there is a request at a floor (up or down) below the specified floor
     * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public boolean noMoreRequestsBelow(int floor)throws IllegalArgumentException{
			
		if (floor<0 || floor >= NUMBER_OF_FLOORS) 
			throw new IllegalArgumentException("The number of floors specified is not in range");
		
		for (int i = floor-1; i >=0 ; i--){ 
			if (up[i] || down[i]) return false;
		}
		
		return true;
	}
	
	/**
	 * Typical string representation is of the form:
	 * <pre>
	 * Direction Requests For Elevator At Floor: 0- 1-UD 2- 3- 4- 5- 6- 7- 8- 9-
	 * </pre>
	 * In this example, there are 10 floors and up and down requests on the 1st floor
	 */
	public String toString (){
		
		String str = "Direction Requests For Elevator At Floor:";
			for (int floor=0; floor< NUMBER_OF_FLOORS;floor++){
				str = str + " "+floor+"-";
				if (up[floor]) str = str+"U";
				if (down[floor]) str = str+"D";}
				
			return str;
	}
	
	
}
