package models;

import interfaces.ControllerForElevator;
import interfaces.ElevatorForController;

/**
 * The stupid controller ensures that the elevator moves from bottom to top to bottom ... 
 * stopping at  every floor.
 * @author J Paul Gibson
 * @version 1
 */
public class StupidController implements ControllerForElevator{

	ElevatorForController elevator;
	
	public StupidController (ElevatorForController elevator) throws IllegalArgumentException{
		
		if (elevator == null) throw new IllegalArgumentException("The elevator must not be null");
		
		else this.elevator = elevator;	
	}
	

    /**
     * If the elevator is on the top floor then set the direction to down<br>
     * If the elevator is on the bottom floor then set the direction to up<br>
     * If the elevator is moving up then keep moving up until it reaches the top floor<br>
     * If the elevator is moving down then keep moving down until it reaches the top floor<br>
     * If the elevator is not moving then start it moving down towards the bottom floor<br>
     */
	public Direction calculateDirection() {
		
		if (elevator.currentFloor()==0) return Direction.UP;
		if (elevator.currentFloor()== elevator.topFloor()) return Direction.DOWN;
		if (elevator.currentDirection() == Direction.STAY) return Direction.DOWN;
		 return elevator.currentDirection();
	}

	/**
	 * Always stop at the next floor
	 */
	public boolean stopAtNextFloor(){
		return true;
	}


	public void bindToElevator(ElevatorForController elevator) throws IllegalArgumentException{
		
		if (elevator == null) throw new IllegalArgumentException("Cannot bind controller to a null elevator");
	}
	
	public String toString (){
		
		return "Stupid Controller";
	}

}
