package models;

import interfaces.ElevatorForController;

/**
 * The stupid controller ensures that the elevator moves from bottom to top to bottom ... 
 * stopping only at floor where requests are made.
 * @author J Paul Gibson
 * @version 1
 */
public class ImprovedStupidController extends StupidController{

	
	public ImprovedStupidController(ElevatorForController elevator) {
		super(elevator);
	}
	
	/**
	 * Only stop at floors where requests to stop have been made
	 */
	public boolean stopAtNextFloor(){
		int floor = elevator.currentFloor();
		Direction dir = elevator.currentDirection();
		
		if (elevator.floorPressedInElevator(floor)) return true;
		if (elevator.downPressedAtFloor(floor) && dir == Direction.DOWN) return true;
		if (elevator.upPressedAtFloor(floor) && dir == Direction.UP) return true;
		
		return false;
	}
}
