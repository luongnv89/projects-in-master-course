package models;

/**
 * The directions in which an elevator can move: down, up and stay
 * @author J Paul Gibson
 * @version 1
 */
public enum Direction {
	
	DOWN, STAY, UP

}
