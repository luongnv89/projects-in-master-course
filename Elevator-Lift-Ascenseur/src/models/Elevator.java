package models;

import interfaces.ElevatorSpecification;
import interfaces.ControllerForElevator;

public class Elevator implements ElevatorSpecification{
	
	
	public final int NUMBER_OF_FLOORS;
	
	private Direction currentDirection; 
	
	private int currentFloor;
	
	private DirectionRequestButtonsOnFloors  directionButtons;
	private FloorButtonsInElevator  floorButtons;
	
	private ControllerForElevator controller = null;
	
	public Elevator (int numberOfFloors)throws IllegalArgumentException{
			
		if (numberOfFloors <2) 
			throw new IllegalArgumentException("The number of floors must be greater than 2");
		NUMBER_OF_FLOORS = numberOfFloors;
		
		currentFloor =0;
		currentDirection = Direction.STAY;
		directionButtons = new DirectionRequestButtonsOnFloors(NUMBER_OF_FLOORS);
		floorButtons = new FloorButtonsInElevator(NUMBER_OF_FLOORS);

	}

	public Direction currentDirection(){ return currentDirection;}
	
	public int currentFloor(){ return currentFloor;}
	
	public int topFloor(){return NUMBER_OF_FLOORS-1;}
	
	public boolean updateFloor () {
		
		currentDirection = controller.calculateDirection();
		
		if (currentDirection == Direction.UP) currentFloor++;
		else if (currentDirection == Direction.DOWN) currentFloor--; 
		
		if (controller.stopAtNextFloor()){
			
			if (currentDirection == Direction.UP) directionButtons.cancelUpRequest(currentFloor);
			if (currentDirection == Direction.DOWN) directionButtons.cancelDownRequest(currentFloor);
			floorButtons.cancelFloorRequestInElevator(currentFloor);
			return true;
		}
		
		return false;
	}


	public boolean downPressedAtFloor(int floor) throws IllegalArgumentException {
		return directionButtons.downPressedAtFloor(floor);
	}


	public boolean upPressedAtFloor(int floor) throws IllegalArgumentException {
		return directionButtons.upPressedAtFloor(floor);
	}


	public boolean floorPressedInElevator(int floor) throws IllegalArgumentException {
		
		return floorButtons.floorPressedInElevator(floor);
	}


	public boolean noMoreRequestsBelow(int floor) throws IllegalArgumentException {

		return (directionButtons.noMoreRequestsBelow(floor) &&
				floorButtons.noMoreRequestsBelow(floor));
	}


	public boolean noMoreRequestsAbove(int floor) throws IllegalArgumentException {
		return (directionButtons.noMoreRequestsAbove(floor) &&
				floorButtons.noMoreRequestsAbove(floor));
	}


	public void pressUp(int floor) throws IllegalArgumentException {
		directionButtons.pressUp(floor);
		
	}


	public void pressDown(int floor) throws IllegalArgumentException {
		directionButtons.pressDown(floor);
		
	}


	public void pressFloorInElevator(int floor) throws IllegalArgumentException {
		floorButtons.pressFloorInElevator(floor);
		
	}

	public void installController(ControllerForElevator controller)	throws IllegalArgumentException {

		if (controller == null) 
			throw new IllegalArgumentException("The controller component must not be null");
		this.controller = controller;
		controller.bindToElevator(this);
	}

   public String toString(){
	   
	   String str ="\nElevator State:\n";
	   
	   
	   str = str+ "Direction = ";
	   if (currentDirection == Direction.UP) str = str+ "UP";
	   if (currentDirection == Direction.DOWN) str = str+ "DOWN";
	   if (currentDirection == Direction.STAY) str = str+ "STAY";
	   
	   str = str+"\nFloor = "+ currentFloor;
	   str = str+"\nNumber of floors = "+NUMBER_OF_FLOORS;
	   
	   str = str+ "\n"+floorButtons;
	   str = str+ "\n"+directionButtons;
	   
	   str = str +"\nController type = "+controller;
	   
	   return str;
   }

}
