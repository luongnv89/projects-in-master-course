package models;

import interfaces.ControllerForElevator;
import interfaces.ElevatorForController;

/**
 * The behaviour of an elevator when there is an emergency to be managed:<br>
 * the elevator must always stay at the current floor and stop at the next floor if already moving.<br>
 * Tested by {@link tests.JUnit_EmergencyController}
 * @author J Paul Gibson
 * @version 1
 */
public class EmergencyController implements ControllerForElevator{

	
    /**
     * In an emergency situation the controller must always stay at the current floor<br>
     * Tested by {@link tests.JUnit_EmergencyController#test_calculateDirection()}
     */
	public Direction calculateDirection(){	
		return Direction.STAY;
	}
	
	/**
	 * In an emergency situation the controller must stop at the next floor<br>
	 * Tested by {@link tests.JUnit_EmergencyController#test_stopAtNextFloor()}
	 */
	public boolean stopAtNextFloor(){
		return true;
	}

	/**
	 * In this emergency controller we do not need to read the state of the elevator and so we do not 
	 * need to bind the 2 components together. We keep the empty method to make the emergency behaviour 
	 * easy to change or update.
	 */
	public void bindToElevator(ElevatorForController elevator) throws IllegalArgumentException{
		
		if (elevator == null) throw new IllegalArgumentException("Cannot bind controller to a null elevator");
	}
	
	public String toString (){
		
		return "Emergency Controller";
	}

}
