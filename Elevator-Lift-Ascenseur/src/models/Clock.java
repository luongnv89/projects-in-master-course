package models;

import interfaces.ClockSpecification;

public class Clock implements ClockSpecification{

	int numberOfTicks;
	
	public Clock(){numberOfTicks=0;}
	
	public Clock(int numberOfTicks)throws IllegalArgumentException{
		setClock(numberOfTicks);
	}
	
	public boolean invariant (){return numberOfTicks  >= 0;}

	public void tick() {
		numberOfTicks++;	
	}

	public void tick(int x) throws IllegalArgumentException {
		if (x<0) throw new IllegalArgumentException("Time increment must be positive");
		numberOfTicks = numberOfTicks + x;
	}

	public void resetClock() {
        numberOfTicks = 0;
	}

	public void setClock(int x) throws IllegalArgumentException {
		if (x<0) throw new IllegalArgumentException("Time  must be positive");
		 numberOfTicks = x;
	}

	public int getTime() {
		return numberOfTicks;
	}
	
	/**
	 * Typical string format:
	 * <pre>
	 * Clock ticks = 0
	 * </pre>
	 */
	public String toString(){
		
		return "Clock ticks = "+numberOfTicks;
	}

}
