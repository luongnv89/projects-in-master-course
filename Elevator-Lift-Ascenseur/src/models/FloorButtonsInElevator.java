package models;

import tests.JUnit_FloorButtonsInElevator;
import tools.HasInvariant;
import tools.InvariantBroken;

/**
 * Tested by {@link tests.JUnit_FloorButtonsInElevator} 
 * and {@link tests.Validation_FloorButtonsInElevator}<br>
 * For managing the state of the floor requests inside the elevator
 * (see {@link interfaces.ElevatorSpecification})
 * @author J Paul Gibson
 * @version 1
 */
public class FloorButtonsInElevator implements HasInvariant{
	
	/**
	 * The number of floors is in range <code> 0 .. NUMBER_OF_FLOORS</code><br>
	 * This must be >=2
	 */
	private final int NUMBER_OF_FLOORS;
	
	/**
	 * Inside the elevator there is an array of floor request buttons that can be pressed.
	 */
	private boolean [] floorRequest;

	/**
	 * Initialises all floor requests (inside the elevator) to be false
	 * @param numberOfFloors is the number of floors in the lift system
	 * @throws IllegalArgumentException if the number of floors is <=1
	 * @throws InvariantBroken if not initialised in a safe state
	 */
	public FloorButtonsInElevator (int numberOfFloors) throws IllegalArgumentException{
			
		if (numberOfFloors <2) 
			throw new IllegalArgumentException("The number of floors must be greater than 2");
		NUMBER_OF_FLOORS = numberOfFloors;	
		
		floorRequest = new boolean [NUMBER_OF_FLOORS];
		
		for (int floor =0; floor<NUMBER_OF_FLOORS;floor++){
			floorRequest [floor] = false;
		}
		
		if (!invariant()) 
			throw new InvariantBroken("The floor request buttons have not been initialised in a safe state");
	}
	
	/**
	 * The following safety conditions are required:
	 * <ul>
	 * <li> number of floors must be at least 2 </li>
	 * <li> the number of floor buttons is the same as the number of floors</li>
	 * </ul>
	 * Tested by {@link JUnit_FloorButtonsInElevator#test_invariant()}
	 * @return true if all these safety conditions are met, and false otherwise. 
	 */
	public boolean invariant(){
		
		if (NUMBER_OF_FLOORS<2) return false;
		if (floorRequest.length != NUMBER_OF_FLOORS) return false;
		return true;
	}
	
	/**
	 * Tested by {@link JUnit_FloorButtonsInElevator#test_presses_cancels()}
	 * @param floor
	 * @return Whether there is currently a request to go to the specified floor (inside the elevator)
	 * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public boolean floorPressedInElevator (int floor) throws IllegalArgumentException{
			
		if (floor<0 || floor >= NUMBER_OF_FLOORS) 
			throw new IllegalArgumentException("The number of floors specified is not in range");
		return floorRequest[floor];}
	
	/**
	 * Tested by {@link JUnit_FloorButtonsInElevator#test_presses_cancels()}
	 * Update the state when a request is made for the specified floor (inside the elevator)
	 * @param floor
     * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public void pressFloorInElevator (int floor)throws IllegalArgumentException{
			
		if (floor<0 || floor >= NUMBER_OF_FLOORS) 
			throw new IllegalArgumentException("The number of floors specified is not in range");
		floorRequest[floor]= true;}

	/**
	 * Tested by {@link JUnit_FloorButtonsInElevator#test_presses_cancels()}
	 * Update the state when a request for the specified floor has been served
	 * @param floor
	 * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public void cancelFloorRequestInElevator (int floor)throws IllegalArgumentException{
			
		if (floor<0 || floor >= NUMBER_OF_FLOORS) 
			throw new IllegalArgumentException("The number of floors specified is not in range");
		floorRequest[floor]= false;}
	
	/**
	 * Tested by {@link JUnit_FloorButtonsInElevator#test_noMoreRequestsAbove()}
	 * @param floor
	 * @return whether there is a request for a floor (inside the elevator) above the specified floor
     * @throws IllegalArgumentException if the number of floors specified is not in range
	 */
	public boolean noMoreRequestsAbove(int floor)throws IllegalArgumentException{
			
		if (floor<0 || floor >= NUMBER_OF_FLOORS) 
			throw new IllegalArgumentException("The number of floors specified is not in range");
		
		for (int i = floor+1; i < NUMBER_OF_FLOORS; i++){ if (floorRequest[i]) return false;}
		
		return true;
	}
	
	/**
     * Tested by {@link JUnit_FloorButtonsInElevator#test_noMoreRequestsBelow()}
   	 * @param floor
	 * @return whether there is a request for a floor (inside the elevator) above the specified floor
     * @throws IllegalArgumentException if the number of floors specified is not in range

	 */
	public boolean noMoreRequestsBelow(int floor)throws IllegalArgumentException{
			
		if (floor<0 || floor >= NUMBER_OF_FLOORS) 
			throw new IllegalArgumentException("The number of floors specified is not in range");
		
		for (int i = floor-1; i >=0 ; i--){ 
			if (floorRequest[i]) return false;
		}
		
		return true;
	}
	
	/**
	 * Tested by {@link JUnit_FloorButtonsInElevator#test_toString()}<br>
	 * Lists the floors that have been requested inside the elevator<br>
	 * Typical output is of the form: <br>
	 * <pre>
	 * Requested Floors Inside Elevator: 2 4 7
	 * </pre>
	 * In this example, the floors requested (2,4 and 7) are listed in order and separated by single spaces
	 */
	public String toString(){
		String str="Requested Floors Inside Elevator:";
		for (int floor=0; floor< NUMBER_OF_FLOORS;floor++)
			if (floorRequest[floor]) str = str + " "+floor;
		return str;
		
	}
}
