package tests;


import junit.framework.Assert;

import org.junit.Test;

import models.Clock;

/**
 * Test for {@link Clock} extends {@link JUnit_ClockSpecification}
 * @author J Paul Gibson
 * @version 1
 */
public class JUnit_Clock extends JUnit_ClockSpecification{


	public void setUp() throws Exception {
		
		clockUnderTest = new Clock();
	}
	
	/**
	 * Unit test for {@link Clock#toString()}
	 */
	@Test
	public void test_toString(){
		
		Assert.assertEquals("Clock ticks = 0", clockUnderTest.toString());
	}

}
