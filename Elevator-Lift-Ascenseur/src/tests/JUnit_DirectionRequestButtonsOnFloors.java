package tests;

import models.DirectionRequestButtonsOnFloors;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for {@link DirectionRequestButtonsOnFloors} <br>
 * @author J Paul Gibson
 * @version 1
 */
public class JUnit_DirectionRequestButtonsOnFloors {

private final int NUMBER_OF_FLOORS = 10;
	
	DirectionRequestButtonsOnFloors directionRequestsUnderTest;
	
	@Before
	public  void setUp() throws Exception{
		
		directionRequestsUnderTest = new DirectionRequestButtonsOnFloors(NUMBER_OF_FLOORS);
	}
	
	@After
	public void tearDown() throws Exception {
		directionRequestsUnderTest = null;
	}

	/**
	 * Unit test for {@link DirectionRequestButtonsOnFloors#invariant()}<br>
	 * Tests that the constructor initialises the buttons in a safe state
	 */
	@Test
	public void test_invariant(){
		
		Assert.assertTrue(directionRequestsUnderTest.invariant());
	}
	/**
	 * Unit test for {@link DirectionRequestButtonsOnFloors#noMoreRequestsAbove(int )}
	 */
	@Test
	public void test_noMoreRequestsAbove(){
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++) 
			Assert.assertTrue(directionRequestsUnderTest.noMoreRequestsAbove(i));
		
		directionRequestsUnderTest.pressUp(NUMBER_OF_FLOORS/2);
		
		for (int i=0; i< NUMBER_OF_FLOORS/2;i++) 
			Assert.assertFalse(directionRequestsUnderTest.noMoreRequestsAbove(i));
		
		for (int i=NUMBER_OF_FLOORS/2; i< NUMBER_OF_FLOORS;i++) 
			Assert.assertTrue(directionRequestsUnderTest.noMoreRequestsAbove(i));
		
		Assert.assertTrue(directionRequestsUnderTest.invariant());
	}
	
	/**
	 * Unit test for {@link DirectionRequestButtonsOnFloors#noMoreRequestsBelow(int )}
	 */
	@Test
	public void test_noMoreRequestsBelow(){
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++) 
			Assert.assertTrue(directionRequestsUnderTest.noMoreRequestsBelow(i));
		
		directionRequestsUnderTest.pressDown(NUMBER_OF_FLOORS/2);
		
		for (int i=0; i<= NUMBER_OF_FLOORS/2;i++) 
			Assert.assertTrue(directionRequestsUnderTest.noMoreRequestsBelow(i));
		
		for (int i=NUMBER_OF_FLOORS/2+1; i< NUMBER_OF_FLOORS;i++) 
			Assert.assertFalse(directionRequestsUnderTest.noMoreRequestsBelow(i));
		
		Assert.assertTrue(directionRequestsUnderTest.invariant());
	}
	
	/**
	 * Unit test for {@link DirectionRequestButtonsOnFloors#toString()}
	 */
	@Test
	public void test_toString(){
		
		Assert.assertEquals("Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4- 5- 6- 7- 8- 9-", 
				            directionRequestsUnderTest.toString());
		
		directionRequestsUnderTest.pressDown(1);
		Assert.assertEquals("Direction Requests For Elevator At Floor: 0- 1-D 2- 3- 4- 5- 6- 7- 8- 9-", 
	            directionRequestsUnderTest.toString());
		
		directionRequestsUnderTest.pressUp(1);
		Assert.assertEquals("Direction Requests For Elevator At Floor: 0- 1-UD 2- 3- 4- 5- 6- 7- 8- 9-", 
	            directionRequestsUnderTest.toString());
		
		
		directionRequestsUnderTest.pressUp(NUMBER_OF_FLOORS/2);
		Assert.assertEquals("Direction Requests For Elevator At Floor: 0- 1-UD 2- 3- 4- 5-U 6- 7- 8- 9-", 
				directionRequestsUnderTest.toString());
		
		directionRequestsUnderTest.pressDown(NUMBER_OF_FLOORS/2);
		Assert.assertEquals("Direction Requests For Elevator At Floor: 0- 1-UD 2- 3- 4- 5-UD 6- 7- 8- 9-", 
				directionRequestsUnderTest.toString());
		
	}
	
	/**
	 * Test for correct integration of: 
	 * <ul>    <li> {@link DirectionRequestButtonsOnFloors#upPressedAtFloor(int)},
	 *         <li> {@link DirectionRequestButtonsOnFloors#pressUp(int)}, and
	 *         <li> {@link DirectionRequestButtonsOnFloors#cancelUpRequest(int)}
	 * </ul>
	 */
	@Test
	public void test_Up_presses_cancels(){
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			Assert.assertFalse(directionRequestsUnderTest.upPressedAtFloor(i));
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			directionRequestsUnderTest.pressUp(i);
		
		for (int i=0; i< NUMBER_OF_FLOORS-1;i++)
			Assert.assertTrue(directionRequestsUnderTest.upPressedAtFloor(i));
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			directionRequestsUnderTest.cancelUpRequest(i);
			
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			Assert.assertFalse(directionRequestsUnderTest.upPressedAtFloor(i));
		
		Assert.assertTrue(directionRequestsUnderTest.invariant());
	}

	/**
	 * Test for correct integration of: 
	 * <ul>    <li> {@link DirectionRequestButtonsOnFloors#downPressedAtFloor(int)},
	 *         <li> {@link DirectionRequestButtonsOnFloors#pressDown(int)}, and
	 *         <li> {@link DirectionRequestButtonsOnFloors#cancelDownRequest(int)}
	 * </ul>
	 */
	@Test
	public void test_Down_presses_cancels(){
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			Assert.assertFalse(directionRequestsUnderTest.downPressedAtFloor(i));
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			directionRequestsUnderTest.pressDown(i);
		
		for (int i=1; i< NUMBER_OF_FLOORS;i++)
			Assert.assertTrue(directionRequestsUnderTest.downPressedAtFloor(i));
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			directionRequestsUnderTest.cancelDownRequest(i);
			
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			Assert.assertFalse(directionRequestsUnderTest.downPressedAtFloor(i));
		
		Assert.assertTrue(directionRequestsUnderTest.invariant());
	}
}
