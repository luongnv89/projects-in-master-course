package tests;

import junit.framework.Assert;
import models.Direction;
import models.EmergencyController;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JUnit_EmergencyController {

	
EmergencyController emergencyControllerUnderTest;
	
	@Before
	public  void setUp() throws Exception{
		
		emergencyControllerUnderTest = new EmergencyController();
	}
	
	@After
	public void tearDown() throws Exception {
		emergencyControllerUnderTest = null;
	}

	/**
	 * Unit test for {@link EmergencyController#calculateDirection()}
	 */
	@Test
	public void test_calculateDirection(){
		Assert.assertEquals(Direction.STAY, emergencyControllerUnderTest.calculateDirection());
	}
	
	/**
	 * Unit test for {@link EmergencyController#stopAtNextFloor()()}
	 */
	@Test
	public void test_stopAtNextFloor(){
		Assert.assertTrue(emergencyControllerUnderTest.stopAtNextFloor());
	}
}
