package tests;

import java.util.Random;

import models.FloorButtonsInElevator;
import tools.DateHeader;
import tools.SeedRNGCommandLine;

/**
 * Validation tests for {@link FloorButtonsInElevator} <br>
 * A simple validation of the behaviour of the floor buttons in the elevator.<br>
 * We simulate 10 random events that change the state of an elevator with 5 floors <br>
 * The behaviour is random (the seed value can be provided as a command line parameter.<br>
 * Typical output:
 * <pre>
The seed used for the random number generator in the test is 0.
You can override this value by passing an integer value as a main argument parameter, if you so wish.


********************************************************************
Execution Date/Time 2013/01/18 16:12:18
********************************************************************
Requested Floors Inside Elevator:
pressFloorInElevator(3)

Requested Floors Inside Elevator: 3
floorPressedInElevator(4)= false
noMoreRequestsAbove(4)= true
noMoreRequestsBelow(4)= false
pressFloorInElevator(0)

Requested Floors Inside Elevator: 0 3
floorPressedInElevator(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
pressFloorInElevator(1)

Requested Floors Inside Elevator: 0 1 3
floorPressedInElevator(4)= false
noMoreRequestsAbove(4)= true
noMoreRequestsBelow(4)= false
pressFloorInElevator(2)

Requested Floors Inside Elevator: 0 1 2 3
floorPressedInElevator(2)= true
noMoreRequestsAbove(2)= false
noMoreRequestsBelow(2)= false
cancelFloorRequestInElevator(2)

Requested Floors Inside Elevator: 0 1 3
floorPressedInElevator(0)= true
noMoreRequestsAbove(0)= false
noMoreRequestsBelow(0)= true
pressFloorInElevator(4)

Requested Floors Inside Elevator: 0 1 3 4
floorPressedInElevator(0)= true
noMoreRequestsAbove(0)= false
noMoreRequestsBelow(0)= true
pressFloorInElevator(0)

Requested Floors Inside Elevator: 0 1 3 4
floorPressedInElevator(3)= true
noMoreRequestsAbove(3)= false
noMoreRequestsBelow(3)= false
cancelFloorRequestInElevator(4)

Requested Floors Inside Elevator: 0 1 3
floorPressedInElevator(2)= false
noMoreRequestsAbove(2)= false
noMoreRequestsBelow(2)= false
cancelFloorRequestInElevator(0)

Requested Floors Inside Elevator: 1 3
floorPressedInElevator(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
cancelFloorRequestInElevator(2)

Requested Floors Inside Elevator: 1 3
floorPressedInElevator(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
</pre>
 * 
 * @author J Paul Gibson
 * @version 1
 */
public class Validation_FloorButtonsInElevator {
	
	
	public static void main(String[] args) {
		
		final int NUMBER_OF_TEST_EVENTS = 10;
	    final int NUMBER_OF_TEST_FLOORS = 5;
		
		FloorButtonsInElevator floorButtons = new FloorButtonsInElevator(NUMBER_OF_TEST_FLOORS);
		
		
		Random rng = SeedRNGCommandLine.getRandom(args);
		System.out.println(DateHeader.dateString());
		System.out.println(floorButtons);
		
		for (int eventCount =0; eventCount< NUMBER_OF_TEST_EVENTS; eventCount++){
			
			int randomEvent;
			int randomFloor;
			
			randomEvent = rng.nextInt(2);
			randomFloor = rng.nextInt(NUMBER_OF_TEST_FLOORS);
			
			if ( randomEvent == 1){ floorButtons.pressFloorInElevator(randomFloor);
			                        System.out.println("pressFloorInElevator("+randomFloor+")");}
			                 else { floorButtons.cancelFloorRequestInElevator(randomFloor);
			                        System.out.println("cancelFloorRequestInElevator("+randomFloor+")");}
			
			System.out.println("\n"+floorButtons);
			
			randomFloor = rng.nextInt(NUMBER_OF_TEST_FLOORS);
			System.out.println("floorPressedInElevator("+randomFloor+")= "+ floorButtons.floorPressedInElevator(randomFloor));
			System.out.println("noMoreRequestsAbove("+randomFloor+")= "+ floorButtons.noMoreRequestsAbove(randomFloor));
			System.out.println("noMoreRequestsBelow("+randomFloor+")= "+ floorButtons.noMoreRequestsBelow(randomFloor));
			
		}
			
    }
	
}