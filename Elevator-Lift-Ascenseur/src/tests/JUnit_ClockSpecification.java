package tests;

import junit.framework.Assert;
import interfaces.ClockSpecification;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for {@link ClockSpecification}
 * @author J Paul Gibson
 * @version 1
 */
public abstract class JUnit_ClockSpecification {
	
	
	ClockSpecification clockUnderTest;
	
	/**
	 * Initialise  </code>clockUnderTest </code> as a concrete object to be tested
	 * @throws Exception
	 */
	@Before
	public  abstract void setUp() throws Exception;
	
	@After
	public void tearDown() throws Exception {
		clockUnderTest = null;
	}
	
	/**
	 * Unit test for {@link ClockSpecification#tick()}
	 */
	@Test
	public void test_tick(){
		int time = clockUnderTest.getTime();
		for (int  i =1; i<=100;i++){
		  clockUnderTest.tick();
		  Assert.assertEquals(time+i, clockUnderTest.getTime());}
		Assert.assertTrue(clockUnderTest.invariant());
	}

	/**
	 * Unit test for {@link ClockSpecification#tick(int )}
	 */
	@Test
	public void test_tick_int(){
		int time = clockUnderTest.getTime();
		clockUnderTest.tick(10);
		Assert.assertEquals(time+10, clockUnderTest.getTime());
		Assert.assertTrue(clockUnderTest.invariant());
	}
	
	/**
	 * Unit test for {@link ClockSpecification#tick(int ) exception case when argument is negative}
	 */
	@Test(expected=IllegalArgumentException.class)
	public void test_tick_int_exception(){
		
		clockUnderTest.tick(-10);
	}
	
	/**
	 * Unit test for {@link ClockSpecification#resetClock()}
	 */
	@Test
	public void test_resetClock(){
		
		clockUnderTest.resetClock();
		Assert.assertEquals(0, clockUnderTest.getTime());
		Assert.assertTrue(clockUnderTest.invariant());
	}
	
	/**
	 * Unit test for {@link ClockSpecification#setClock(int )}
	 */
	@Test
	public void test_setClock(){
		
		for (int  i =1; i<=100;i++){
		  clockUnderTest.setClock(i);
		  Assert.assertEquals(i, clockUnderTest.getTime());}
		Assert.assertTrue(clockUnderTest.invariant());
	}
	
	
	/**
	 * Unit test for {@link ClockSpecification#setClock(int ) exception case when argument is negative}
	 */
	@Test(expected=IllegalArgumentException.class)
	public void test_setClock_exception(){
		
		clockUnderTest.setClock(-10);
	}
	
	
	/**
	 * Unit test for {@link ClockSpecification#getTime()}
	 */
	@Test
	public void test_getTime(){
		
		  Assert.assertEquals(0, clockUnderTest.getTime());
	}
	
	/**
	 * Unit test for {@link ClockSpecification#invariant()}
	 */
	@Test
	public void test_invariant(){
		Assert.assertTrue(clockUnderTest.invariant());
	}
	

}
