package tests;

import java.util.Random;

import models.DirectionRequestButtonsOnFloors;
import tools.DateHeader;
import tools.SeedRNGCommandLine;

/**
 * Validation tests for {@link DirectionRequestButtonsOnFloors} <br>
 * A simple validation of the behaviour of the direction request buttons on each floor of the elevator system.<br>
 * We simulate 20 random events that change the state of an elevator with 5 floors <br>
 * The behaviour is random (the seed value can be provided as a command line parameter.<br>
 * Typical output:
 * <pre>
The seed used for the random number generator in the test is 0.
You can override this value by passing an integer value as a main argument parameter, if you so wish.


********************************************************************
Execution Date/Time 2013/01/22 18:13:38
********************************************************************
Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4-
pressDown(3)

Direction Requests For Elevator At Floor: 0- 1- 2- 3-D 4-
upPressedAtFloor(4)= false
downPressedAtFloor(4)= false
noMoreRequestsAbove(4)= true
noMoreRequestsBelow(4)= false
pressDown(0)

Direction Requests For Elevator At Floor: 0- 1- 2- 3-D 4-
upPressedAtFloor(3)= false
downPressedAtFloor(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= true
pressDown(1)

Direction Requests For Elevator At Floor: 0- 1-D 2- 3-D 4-
upPressedAtFloor(4)= false
downPressedAtFloor(4)= false
noMoreRequestsAbove(4)= true
noMoreRequestsBelow(4)= false
cancelUpRequest(2)

Direction Requests For Elevator At Floor: 0- 1-D 2- 3-D 4-
upPressedAtFloor(2)= false
downPressedAtFloor(2)= false
noMoreRequestsAbove(2)= false
noMoreRequestsBelow(2)= false
pressUp(2)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-D 4-
upPressedAtFloor(0)= false
downPressedAtFloor(0)= false
noMoreRequestsAbove(0)= false
noMoreRequestsBelow(0)= true
cancelUpRequest(4)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-D 4-
upPressedAtFloor(0)= false
downPressedAtFloor(0)= false
noMoreRequestsAbove(0)= false
noMoreRequestsBelow(0)= true
cancelUpRequest(0)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-D 4-
upPressedAtFloor(3)= false
downPressedAtFloor(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
pressUp(4)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-D 4-
upPressedAtFloor(2)= true
downPressedAtFloor(2)= false
noMoreRequestsAbove(2)= false
noMoreRequestsBelow(2)= false
cancelDownRequest(0)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-D 4-
upPressedAtFloor(3)= false
downPressedAtFloor(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
cancelDownRequest(2)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-D 4-
upPressedAtFloor(3)= false
downPressedAtFloor(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
cancelUpRequest(0)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-D 4-
upPressedAtFloor(2)= true
downPressedAtFloor(2)= false
noMoreRequestsAbove(2)= false
noMoreRequestsBelow(2)= false
pressUp(2)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-D 4-
upPressedAtFloor(2)= true
downPressedAtFloor(2)= false
noMoreRequestsAbove(2)= false
noMoreRequestsBelow(2)= false
pressUp(3)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-UD 4-
upPressedAtFloor(3)= true
downPressedAtFloor(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
cancelUpRequest(0)

Direction Requests For Elevator At Floor: 0- 1-D 2-U 3-UD 4-
upPressedAtFloor(0)= false
downPressedAtFloor(0)= false
noMoreRequestsAbove(0)= false
noMoreRequestsBelow(0)= true
pressUp(0)

Direction Requests For Elevator At Floor: 0-U 1-D 2-U 3-UD 4-
upPressedAtFloor(3)= true
downPressedAtFloor(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
cancelUpRequest(4)

Direction Requests For Elevator At Floor: 0-U 1-D 2-U 3-UD 4-
upPressedAtFloor(1)= false
downPressedAtFloor(1)= true
noMoreRequestsAbove(1)= false
noMoreRequestsBelow(1)= false
cancelUpRequest(2)

Direction Requests For Elevator At Floor: 0-U 1-D 2- 3-UD 4-
upPressedAtFloor(4)= false
downPressedAtFloor(4)= false
noMoreRequestsAbove(4)= true
noMoreRequestsBelow(4)= false
pressUp(1)

Direction Requests For Elevator At Floor: 0-U 1-UD 2- 3-UD 4-
upPressedAtFloor(1)= true
downPressedAtFloor(1)= true
noMoreRequestsAbove(1)= false
noMoreRequestsBelow(1)= false
cancelUpRequest(1)

Direction Requests For Elevator At Floor: 0-U 1-D 2- 3-UD 4-
upPressedAtFloor(3)= true
downPressedAtFloor(3)= true
noMoreRequestsAbove(3)= true
noMoreRequestsBelow(3)= false
pressDown(3)

Direction Requests For Elevator At Floor: 0-U 1-D 2- 3-UD 4-
upPressedAtFloor(2)= false
downPressedAtFloor(2)= false
noMoreRequestsAbove(2)= false
noMoreRequestsBelow(2)= false
 * </pre>
 *  
 * @author J Paul Gibson
 * @version 1
 */

public class Validation_DirectionRequestButtonsOnFloors {
	
public static void main(String[] args) {
		
		final int NUMBER_OF_TEST_EVENTS = 20;
	    final int NUMBER_OF_TEST_FLOORS = 5;
		
	    DirectionRequestButtonsOnFloors directionsButtons = new DirectionRequestButtonsOnFloors(NUMBER_OF_TEST_FLOORS);
		
		
		Random rng = SeedRNGCommandLine.getRandom(args);
		System.out.println(DateHeader.dateString());
		System.out.println(directionsButtons);
		
		for (int eventCount =0; eventCount< NUMBER_OF_TEST_EVENTS; eventCount++){
			
			int randomEvent;
			int randomFloor;
			
			randomEvent = rng.nextInt(4);
			randomFloor = rng.nextInt(NUMBER_OF_TEST_FLOORS);
			
			if ( randomEvent == 1){ directionsButtons.pressUp(randomFloor);
			                        System.out.println("pressUp("+randomFloor+")");}
			
		    else if ( randomEvent == 2){ directionsButtons.pressDown(randomFloor);
			                        System.out.println("pressDown("+randomFloor+")");}
			
		    else if ( randomEvent == 3){ directionsButtons.cancelUpRequest(randomFloor);
            System.out.println("cancelUpRequest("+randomFloor+")");}
			
		    else { directionsButtons.cancelDownRequest(randomFloor);
            System.out.println("cancelDownRequest("+randomFloor+")");}
			
			
			System.out.println("\n"+directionsButtons);
			
			randomFloor = rng.nextInt(NUMBER_OF_TEST_FLOORS);
			System.out.println("upPressedAtFloor("+randomFloor+")= "+ directionsButtons.upPressedAtFloor(randomFloor));
			System.out.println("downPressedAtFloor("+randomFloor+")= "+ directionsButtons.downPressedAtFloor(randomFloor));
			System.out.println("noMoreRequestsAbove("+randomFloor+")= "+ directionsButtons.noMoreRequestsAbove(randomFloor));
			System.out.println("noMoreRequestsBelow("+randomFloor+")= "+ directionsButtons.noMoreRequestsBelow(randomFloor));
			
		}
			
    }

}
