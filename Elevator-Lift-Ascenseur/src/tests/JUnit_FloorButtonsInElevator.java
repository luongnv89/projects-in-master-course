package tests;

import junit.framework.Assert;
import models.FloorButtonsInElevator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for {@link FloorButtonsInElevator} <br>
 * @author J Paul Gibson
 * @version 1
 */
public class JUnit_FloorButtonsInElevator {
	
	private final int NUMBER_OF_FLOORS = 10;
	
	FloorButtonsInElevator floorButtonsUnderTest;
	
	@Before
	public  void setUp() throws Exception{
		
		floorButtonsUnderTest = new FloorButtonsInElevator(NUMBER_OF_FLOORS);
	}
	
	@After
	public void tearDown() throws Exception {
		floorButtonsUnderTest = null;
	}

	/**
	 * Unit test for {@link FloorButtonsInElevator#invariant()}
	 */
	@Test
	public void test_invariant(){
		Assert.assertTrue(floorButtonsUnderTest.invariant());
	}
	
	/**
	 * Unit test for {@link FloorButtonsInElevator#noMoreRequestsAbove(int )}
	 */
	@Test
	public void test_noMoreRequestsAbove(){
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++) 
			Assert.assertTrue(floorButtonsUnderTest.noMoreRequestsAbove(i));
		
		floorButtonsUnderTest.pressFloorInElevator(NUMBER_OF_FLOORS/2);
		
		for (int i=0; i< NUMBER_OF_FLOORS/2;i++) 
			Assert.assertFalse(floorButtonsUnderTest.noMoreRequestsAbove(i));
		
		for (int i=NUMBER_OF_FLOORS/2; i< NUMBER_OF_FLOORS;i++) 
			Assert.assertTrue(floorButtonsUnderTest.noMoreRequestsAbove(i));
		
		Assert.assertTrue(floorButtonsUnderTest.invariant());
	}
	
	/**
	 * Unit test for {@link FloorButtonsInElevator#noMoreRequestsBelow(int )}
	 */
	@Test
	public void test_noMoreRequestsBelow(){
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++) 
			Assert.assertTrue(floorButtonsUnderTest.noMoreRequestsBelow(i));
		
		floorButtonsUnderTest.pressFloorInElevator(NUMBER_OF_FLOORS/2);
		
		for (int i=0; i<= NUMBER_OF_FLOORS/2;i++) 
			Assert.assertTrue(floorButtonsUnderTest.noMoreRequestsBelow(i));
		
		for (int i=NUMBER_OF_FLOORS/2+1; i< NUMBER_OF_FLOORS;i++) 
			Assert.assertFalse(floorButtonsUnderTest.noMoreRequestsBelow(i));
		
		Assert.assertTrue(floorButtonsUnderTest.invariant());
	}
	
	
	
	/**
	 * Test for correct integration of: 
	 * <ul>    <li> {@link FloorButtonsInElevator#floorPressedInElevator(int)},
	 *         <li> {@link FloorButtonsInElevator#pressFloorInElevator(int)}, and
	 *         <li> {@link FloorButtonsInElevator#cancelFloorRequestInElevator(int)}
	 * </ul>
	 */
	@Test
	public void test_presses_cancels(){
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			Assert.assertFalse(floorButtonsUnderTest.floorPressedInElevator(i));
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			floorButtonsUnderTest.pressFloorInElevator(i);
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			Assert.assertTrue(floorButtonsUnderTest.floorPressedInElevator(i));
		
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			floorButtonsUnderTest.cancelFloorRequestInElevator(i);
			
		for (int i=0; i< NUMBER_OF_FLOORS;i++)
			Assert.assertFalse(floorButtonsUnderTest.floorPressedInElevator(i));
		
		Assert.assertTrue(floorButtonsUnderTest.invariant());
	}
	
	/**
	 * Unit test for {@link FloorButtonsInElevator#toString()}
	 */
	@Test
	public void test_toString(){
		
		Assert.assertEquals("Requested Floors Inside Elevator:", 
				            floorButtonsUnderTest.toString());
		floorButtonsUnderTest.pressFloorInElevator(NUMBER_OF_FLOORS/2);
		Assert.assertEquals("Requested Floors Inside Elevator: "+ NUMBER_OF_FLOORS/2, 
				            floorButtonsUnderTest.toString());
		floorButtonsUnderTest.pressFloorInElevator(0);
		Assert.assertEquals("Requested Floors Inside Elevator: 0 "+ NUMBER_OF_FLOORS/2, 
				             floorButtonsUnderTest.toString());
		floorButtonsUnderTest.pressFloorInElevator(NUMBER_OF_FLOORS-1);
		Assert.assertEquals("Requested Floors Inside Elevator: 0 "+ NUMBER_OF_FLOORS/2+" "+(NUMBER_OF_FLOORS-1),
				            floorButtonsUnderTest.toString());
	}
}
