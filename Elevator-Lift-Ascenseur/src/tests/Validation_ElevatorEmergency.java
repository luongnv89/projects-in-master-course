package tests;

import models.Elevator;
import models.EmergencyController;


/**
 * Validation test for an {@link Elevator} controlled by {@link EmergencyController}<br>
 * Expected output:
 * <pre>

Elevator State:
Direction = STAY
Floor = 0
Number of floors = 5
Requested Floors Inside Elevator: 0 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3-U 4-
Controller type = Emergency Controller
Stopping at next floor

Elevator State:
Direction = STAY
Floor = 0
Number of floors = 5
Requested Floors Inside Elevator: 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3-U 4-
Controller type = Emergency Controller
Stopping at next floor

Elevator State:
Direction = STAY
Floor = 0
Number of floors = 5
Requested Floors Inside Elevator: 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3-U 4-
Controller type = Emergency Controller
Stopping at next floor

Elevator State:
Direction = STAY
Floor = 0
Number of floors = 5
Requested Floors Inside Elevator: 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3-U 4-
Controller type = Emergency Controller
Stopping at next floor

Elevator State:
Direction = STAY
Floor = 0
Number of floors = 5
Requested Floors Inside Elevator: 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3-U 4-
Controller type = Emergency Controller
 * </pre>
 * @author J Paul Gibson
 * @version 1
 */
public class Validation_ElevatorEmergency {

	
	public static void main(String[] args) {
		
		Elevator elevator = new Elevator(5);
		EmergencyController controller = new EmergencyController();
		elevator.installController(controller);
		
		elevator.pressUp(3);
		elevator.pressDown(2);
		elevator.pressFloorInElevator(0);
		elevator.pressFloorInElevator(4);
		System.out.println(elevator);
		
		for (int i = 0; i<4; i++){
		   if (elevator.updateFloor()) System.out.println("Stopping at next floor");
		                          else System.out.println("Passing  next floor");
		   System.out.println(elevator);
		}
		
	}
}
