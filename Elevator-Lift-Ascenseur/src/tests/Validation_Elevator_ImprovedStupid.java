package tests;

import models.Elevator;
import models.ImprovedStupidController;

/**
 *Validation test for an {@link Elevator} controlled by {@link ImprovedStupidController}<br>
 * Expected output:
 * <pre>

Elevator State:
Direction = STAY
Floor = 0
Number of floors = 5
Requested Floors Inside Elevator: 1 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3-U 4-
Controller type = Stupid Controller
Stopping at next floor

Elevator State:
Direction = UP
Floor = 1
Number of floors = 5
Requested Floors Inside Elevator: 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3-U 4-
Controller type = Stupid Controller
Passing  next floor

Elevator State:
Direction = UP
Floor = 2
Number of floors = 5
Requested Floors Inside Elevator: 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3-U 4-
Controller type = Stupid Controller
Stopping at next floor

Elevator State:
Direction = UP
Floor = 3
Number of floors = 5
Requested Floors Inside Elevator: 4
Direction Requests For Elevator At Floor: 0- 1- 2-D 3- 4-
Controller type = Stupid Controller
Stopping at next floor

Elevator State:
Direction = UP
Floor = 4
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2-D 3- 4-
Controller type = Stupid Controller
Passing  next floor

Elevator State:
Direction = DOWN
Floor = 3
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2-D 3- 4-
Controller type = Stupid Controller
Stopping at next floor

Elevator State:
Direction = DOWN
Floor = 2
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4-
Controller type = Stupid Controller
Passing  next floor

Elevator State:
Direction = DOWN
Floor = 1
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4-
Controller type = Stupid Controller
Passing  next floor

Elevator State:
Direction = DOWN
Floor = 0
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4-
Controller type = Stupid Controller
Passing  next floor

Elevator State:
Direction = UP
Floor = 1
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4-
Controller type = Stupid Controller
Passing  next floor

Elevator State:
Direction = UP
Floor = 2
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4-
Controller type = Stupid Controller
Passing  next floor

Elevator State:
Direction = UP
Floor = 3
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4-
Controller type = Stupid Controller
Passing  next floor

Elevator State:
Direction = UP
Floor = 4
Number of floors = 5
Requested Floors Inside Elevator:
Direction Requests For Elevator At Floor: 0- 1- 2- 3- 4-
Controller type = Stupid Controller
 * </pre>
 * @author J Paul Gibson
 * @version 1
 */
public class Validation_Elevator_ImprovedStupid {
	
public static void main(String[] args) {
		
		Elevator elevator = new Elevator(5);
		ImprovedStupidController controller = new ImprovedStupidController(elevator);
		elevator.installController(controller);
		
		elevator.pressUp(3);
		elevator.pressDown(2);
		elevator.pressFloorInElevator(1);
		elevator.pressFloorInElevator(4);
		System.out.println(elevator);
		
		for (int i = 0; i<12; i++){
		   if (elevator.updateFloor()) System.out.println("Stopping at next floor");
		                          else System.out.println("Passing  next floor");
		   System.out.println(elevator);
		}
		
	}

}
