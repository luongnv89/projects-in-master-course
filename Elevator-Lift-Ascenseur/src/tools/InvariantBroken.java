package tools;

/**
 * <code>InvariantBroken</code> is raised when the invariant of a class is not true in
 * the current state of the instance
 * @author J Paul Gibson
 * @version 1
 */
public class InvariantBroken extends RuntimeException {

private static final long serialVersionUID = 1L;

/**
   * Constructor of the exception raised when invariants are broken
   * @param message is used to explain which part of the invariant has been broken
   */
  public InvariantBroken(String message) {
    super(message);
  }
}

