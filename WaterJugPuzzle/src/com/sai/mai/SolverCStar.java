package com.sai.mai;

import java.util.ArrayList;

import com.sai.def.InvariantInterface;
import com.sai.def.Jar;
import com.sai.def.Move;
import com.sai.def.SolverAbstract;
import com.sai.def.SolverInterface;

/**
 * @author mainguyen All two jugs are empty. Then fill up a jug without throwing
 *         water in any jug.<br>
 *  Find all possibility can be solved by checking specific cases.<br>
 *  <ul>
 *  <li>The target is the size of one of jugs.
 *  <li>The target is the subtraction of sizes of two jugs.
 *  <li>The target is the multiply of the smaller jug.
 *  <li>The target is (jug2 - jug1%jug2) with jug1 > jug2.
 *  <ul>
 */
public class SolverCStar extends SolverAbstract implements SolverInterface,
		InvariantInterface {

	public SolverCStar(Jar jar1, Jar jar2) {
		jars = new ArrayList<Jar>();
		jars.add(jar1);
		jars.add(jar2);
	}

	public SolverCStar(SolverAbstract listJar) {
		super(listJar);
	}

	@Override
	public boolean solve(int target) {
		System.out.println("\nSolver for " + target);

		if (target == jars.get(0).getMaxQuantity()) {
			fillUp(jars.get(0));
			// this.showAllJugs();
			return true;

		} else if (target == jars.get(1).getMaxQuantity()) {
			fillUp(jars.get(1));
			// this.showAllJugs();
			return true;

		} else if (jars.get(0).getMaxQuantity() > jars.get(1).getMaxQuantity()) {

			if (target > jars.get(0).getMaxQuantity())
				return false;

			else if (target == (jars.get(0).getMaxQuantity() - jars.get(1)
					.getMaxQuantity())) {

				fillUp(jars.get(0));
				// this.showAllJugs();
				Move move = new Move(0, 1);
				moves.add(move);
				fillUp(jars.get(1));
				jars.get(0).setCurrentQuantity(
						jars.get(0).getCurrentQuantity()
								- jars.get(1).getMaxQuantity());
				// this.showAllJugs();
				return true;

			} else if (target % jars.get(1).getMaxQuantity() == 0) {

				while (target != jars.get(0).getCurrentQuantity()) {
					fillUp(jars.get(1));
					// this.showAllJugs();
					Move move = new Move(1, 0);
					moves.add(move);
					jars.get(0).setCurrentQuantity(
							jars.get(0).getCurrentQuantity()
									+ jars.get(1).getMaxQuantity());
					jars.get(1).setCurrentQuantity(0);
				}

				// this.showAllJugs();
				return true;

			} else if (target == (jars.get(1).getMaxQuantity() - (jars.get(0)
					.getMaxQuantity() % jars.get(1).getMaxQuantity()))) {

				while (jars.get(1).getCurrentQuantity() != target) {

					fillUp(jars.get(1));
					// this.showAllJugs();
					int temp = jars.get(0).getMaxQuantity()
							- jars.get(0).getCurrentQuantity();

					if (temp < jars.get(1).getCurrentQuantity()) {
						Move move = new Move(1, 0);
						moves.add(move);
						jars.get(0).setCurrentQuantity(
								jars.get(0).getCurrentQuantity() + temp);
						jars.get(1).setCurrentQuantity(
								jars.get(1).getCurrentQuantity() - temp);

					} else {
						Move move = new Move(1, 0);
						moves.add(move);
						jars.get(0).setCurrentQuantity(
								jars.get(0).getCurrentQuantity()
										+ jars.get(1).getCurrentQuantity());
						jars.get(1).setCurrentQuantity(0);
					}

				}
				// this.showAllJugs();
				return true;

			} else if (jars.get(0).getMaxQuantity() < jars.get(1)
					.getMaxQuantity()) {

				if (target > jars.get(1).getMaxQuantity())
					return false;

				else if (target == (jars.get(1).getMaxQuantity() - jars.get(0)
						.getMaxQuantity())) {

					fillUp(jars.get(1));
					// this.showAllJugs();
					Move move = new Move(1, 0);
					moves.add(move);
					fillUp(jars.get(0));
					jars.get(1).setCurrentQuantity(
							jars.get(1).getCurrentQuantity()
									- jars.get(0).getMaxQuantity());
					// this.showAllJugs();
					return true;

				} else if (target % jars.get(0).getMaxQuantity() == 0) {

					while (target != jars.get(1).getCurrentQuantity()) {
						fillUp(jars.get(0));
						// this.showAllJugs();
						Move move = new Move(0, 1);
						moves.add(move);
						jars.get(1).setCurrentQuantity(
								jars.get(1).getCurrentQuantity()
										+ jars.get(0).getMaxQuantity());
						jars.get(0).setCurrentQuantity(0);
					}
					// this.showAllJugs();
					return true;

				} else if (target == (jars.get(0).getMaxQuantity() - (jars.get(
						1).getMaxQuantity() % jars.get(0).getMaxQuantity()))) {

					while (jars.get(0).getCurrentQuantity() != target) {
						fillUp(jars.get(0));
						// this.showAllJugs();
						int temp = jars.get(1).getMaxQuantity()
								- jars.get(1).getCurrentQuantity();
						if (temp < jars.get(0).getCurrentQuantity()) {
							Move move = new Move(0, 1);
							moves.add(move);
							jars.get(1).setCurrentQuantity(
									jars.get(1).getCurrentQuantity() + temp);
							jars.get(0).setCurrentQuantity(
									jars.get(0).getCurrentQuantity() - temp);

						} else {
							Move move = new Move(0, 1);
							moves.add(move);
							jars.get(1).setCurrentQuantity(
									jars.get(1).getCurrentQuantity()
											+ jars.get(0).getCurrentQuantity());
							jars.get(0).setCurrentQuantity(0);
						}

					}
					// this.showAllJugs();
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean invariant() {
		for (Jar j : jars) {
			if (j == null) {
				return false;
			}
		}
		return super.invariant();
	}

	public void fillUp(Jar jr) {
		jr.setCurrentQuantity(jr.getMaxQuantity());
	}
}
