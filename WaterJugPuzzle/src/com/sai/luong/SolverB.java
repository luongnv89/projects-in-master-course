package com.sai.luong;

import java.util.ArrayList;

import com.sai.def.Jar;
import com.sai.def.Move;
import com.sai.def.SolverAbstract;

/**
 * Water Jug puzzle version B
 * 
 * @author crocode
 *
 */
public class SolverB extends SolverAbstract {

	/**
	 * Remark when found the solution
	 */
	private static boolean FOUND_SOLUTION = false;
	private int target = 0;

	/**
	 * List states of Solver which is traversed.
	 */
	private static ArrayList<String> listStates = new ArrayList<String>();

	/**
	 * debug = false if don't want to show the solver processing
	 */
	private boolean debug = false;
	
	public SolverB(Jar... jarLIst) {
		super(jarLIst);
	}

	public SolverB(SolverAbstract solverB) {
		super(solverB);
	}

	@Override
	public boolean solve(int target) {
		// Reset parameters
		if (this.target != target) {
			this.target = target;
			this.listStates.clear();
			FOUND_SOLUTION = false;
		}
		// Cannot solve
		if (!isSolvable(target)) {
			showMessage("Cannot solve!");
			return false;
		}

		// Don't have to do anything
		for (Jar jar : jars) {
			if (jar.getCurrentQuantity() == target) {
				FOUND_SOLUTION = true;
				return true;
			}
		}
		if (!FOUND_SOLUTION) {
			// Create new states

			for (Jar jar : jars) {
				// Create new states by change the contain of an non-empty jar
				if (jar.getCurrentQuantity() != 0) {

					// Create new states by move water from jar to an un-full
					// jar
					for (Jar jar2 : jars) {
						if (!jar2.equals(jar)
								&& jar2.getCurrentQuantity() < jar2
										.getMaxQuantity()) {
							SolverB moveJar = newSolverBByMoveJar(
									this.jars.indexOf(jar),
									this.jars.indexOf(jar2), this);
							moveJar.target = target;
							if (!listStates.contains(moveJar.saveState())) {
								listStates.add(moveJar.saveState());
								if (moveJar.solve(target)) {
									this.moves = moveJar.moves;
									// moveJar.showAllJugs();
									FOUND_SOLUTION = true;
									return true;
								}
							}
						}
					}

					if (!FOUND_SOLUTION) {
						// Create a new state by empty jar
						SolverB emptyJar = newSolverBByEmptyJar(jar, this);
						emptyJar.target = target;
						if (!listStates.contains(emptyJar.saveState())) {
							listStates.add(emptyJar.saveState());
							if (emptyJar.solve(target)) {
								this.moves = emptyJar.moves;
								// emptyJar.showAllJugs();
								FOUND_SOLUTION = true;
								return true;
							}
						}
					}

				}
			}

		}
		return FOUND_SOLUTION;
	}

	/**
	 * Create a new solver by move water from one nonEmpty jar to a non-full jar2.
	 * @param jar 
	 * @param jar2
	 * @param solverB
	 * @return
	 */
	private SolverB newSolverBByMoveJar(int jar, int jar2, SolverB solverB) {
		SolverB newSolverB = new SolverB(solverB);
		newSolverB.move(jar, jar2);
		return newSolverB;
	}

	/**
	 * Create a new solver by empty a non-empty jar
	 * @param jar
	 * @param solverB
	 * @return
	 */
	private SolverB newSolverBByEmptyJar(Jar jar, SolverB solverB) {
		SolverB newSolverB = new SolverB(solverB);
		newSolverB.emptyJar(solverB.jars.indexOf(jar));
		newSolverB.moves.add(new Move(solverB.jars.indexOf(jar), -1));
		return newSolverB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sai.def.SolverInterface#showAllJugs()
	 */

	/**
	 * Empty a jar has index is index
	 * @param index
	 */
	private void emptyJar(int index) {
		jars.get(index).setCurrentQuantity(0);
	}

	/**
	 * Get the total water of all jars in solver
	 * @return
	 */
	private int getTotal() {
		int total = 0;
		for (Jar jar : jars) {
			total += jar.getCurrentQuantity();
		}
		return total;
	}

	/**
	 * Before test a solver 
	 * @param target
	 * @return false if:
	 * <li> the total water of all the jar in solver is less than the target
	 * <li> there isn't any jar can contain target
	 */
	private boolean isSolvable(int target) {
		int total = getTotal();
		if (total < target) {
			showMessage("The total is less than target");
			return false;
		}

		if (target > maxContain()) {
			showMessage("There isn't any Jug can contain target!");
			return false;
		}

		return true;
	}

	private void showMessage(String string) {
		if (debug == true) {
			System.out.println(string);
		}

	}

	/**
	 * Return the max contain jar of solver
	 * @return
	 */
	private int maxContain() {
		int max = jars.get(0).getMaxQuantity();
		for (Jar jar : jars) {
			max = max > jar.getMaxQuantity() ? max : jar.getMaxQuantity();
		}
		return max;
	}

	/**
	 * Move water from jar has index jar1 to jar has index jar2
	 * @param jar1
	 * @param jar2
	 */
	private void move(int jar1, int jar2) {
		Move newMove = new Move(jar1, jar2);
		int srcCurrent = jars.get(jar1).getCurrentQuantity();
		int dstCurrent = jars.get(jar2).getCurrentQuantity();

		if (srcCurrent + dstCurrent <= jars.get(jar2).getMaxQuantity()) {
			jars.get(jar1).setCurrentQuantity(0);
			jars.get(jar2).setCurrentQuantity(srcCurrent + dstCurrent);
		}

		else {
			jars.get(jar1).setCurrentQuantity(
					srcCurrent + dstCurrent - jars.get(jar2).getMaxQuantity());
			jars.get(jar2).setCurrentQuantity(jars.get(jar2).getMaxQuantity());
		}
		this.moves.add(newMove);
	}

	/**
	 * Save sate of solver
	 * @return
	 */
	private String saveState() {
		String str = "";
		for (Jar jar : jars) {
			str = str + jar.toString() + " | ";
		}
		return str;
	}
}
