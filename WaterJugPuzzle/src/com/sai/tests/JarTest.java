/**
 * 
 */
package com.sai.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sai.def.Jar;

/**
 * @author NGUYEN VAN LUONG
 *
 */
public class JarTest {

	int MAX = 20;
	int MIN = 5;
	Jar defaultJar;
	Jar noInvariantJar;
	Jar copyJar;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		defaultJar = new Jar(MIN, MAX);
		noInvariantJar = new Jar(MAX, MIN);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		defaultJar = null;
		noInvariantJar = null;
	}

	/**
	 * Test method for {@link com.sai.def.Jar#Jar(int, int)}.
	 */

	/**
	 * Test method for {@link com.sai.def.Jar#getCurrentQuantity()}.
	 */
	@Test
	public void testGetCurrentQuantity() {
		assertEquals(defaultJar.getCurrentQuantity(), MIN);
	}

	/**
	 * Test method for {@link com.sai.def.Jar#setCurrentQuantity(int)}.
	 */
	@Test
	public void testSetCurrentQuantity() {
		defaultJar.setCurrentQuantity(MAX);
		assertEquals(defaultJar.getCurrentQuantity(), MAX);
	}

	/**
	 * Test method for {@link com.sai.def.Jar#getMaxQuantity()}.
	 */
	@Test
	public void testGetMaxQuantity() {
		assertEquals(defaultJar.getMaxQuantity(), MAX);
	}

	/**
	 * Test method for {@link com.sai.def.Jar#add(int)}.
	 */
	@Test
	public void testAddOverContaint() {
		defaultJar.add(MAX);
		assertEquals(defaultJar.getCurrentQuantity(), MAX);
	}

	/**
	 * Test method for {@link com.sai.def.Jar#add(int)}.
	 */
	@Test
	public void testAddContaint() {
		defaultJar.add((MAX - MIN) / 2);
		assertEquals(defaultJar.getCurrentQuantity(), MIN + (MAX - MIN) / 2);
	}

	/**
	 * Test method for {@link com.sai.def.Jar#remainingWaterAfterAdding(int)}.
	 */
	@Test
	public void testRemainingWaterAfterAddingAllWater() {
		assertEquals(defaultJar.remainingWaterAfterAdding(MIN), 0);
	}

	/**
	 * Test method for {@link com.sai.def.Jar#remainingWaterAfterAdding(int)}.
	 */
	@Test
	public void testRemainingWaterAfterAddingAPartOfWater() {
		assertEquals(defaultJar.remainingWaterAfterAdding(MAX), MIN);
	}

	/**
	 * Test method for {@link com.sai.def.Jar#canFillCompletely(int)}.
	 */
	@Test
	public void testCanFillCompletely() {
		assertTrue(defaultJar.canFillCompletely(MAX));
		assertFalse(defaultJar.canFillCompletely((MAX - MIN) / 2));
	}

	/**
	 * Test method for {@link com.sai.def.Jar#canAddCompletely(int)}.
	 */
	@Test
	public void testCanAddCompletely() {
		assertTrue(defaultJar.canAddCompletely((MAX - MIN) / 2));
		assertFalse(defaultJar.canAddCompletely(MAX));
	}

	/**
	 * Test method for {@link com.sai.def.Jar#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(defaultJar.invariant());
		assertFalse(noInvariantJar.invariant());
	}

	/**
	 * Test method for {@link com.sai.def.Jar#toString()}.
	 */
	@Test
	public void testToString() {
		assertEquals(defaultJar.toString(), " [" + MIN + "/" + MAX + "] ");
	}

}
