package com.sai.tests;

import org.junit.Before;

import com.sai.christos.SolverA;
import com.sai.def.Jar;

public class SolverATest extends SolverAbstractTest {

	@Before
	public void setUp() throws Exception {
		solver = new SolverA(new Jar(12, 12), new Jar(0, 8), new Jar(0, 5));
		solver.solve(7);
	}
}
