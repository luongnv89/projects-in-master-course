/**
 * 
 */
package com.sai.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sai.def.Move;

/**
 * @author NGUYEN VAN LUONG
 *
 */
public class MoveTest {

	int SOURCE = 5;
	int DESTINATIOn = 15;
	Move move;
	Move moveEmpty;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		move = new Move(SOURCE, DESTINATIOn);
		moveEmpty = new Move(SOURCE, -1);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		move = null;
	}

	/**
	 * Test method for {@link com.sai.def.Move#getSource()}.
	 */
	@Test
	public void testGetSource() {
		assertEquals(move.getSource(), SOURCE);
	}

	/**
	 * Test method for {@link com.sai.def.Move#getDestination()}.
	 */
	@Test
	public void testGetDestination() {
		assertEquals(move.getDestination(), DESTINATIOn);
	}

	/**

	/**
	 * Test method for {@link com.sai.def.Move#toString()}.
	 */
	@Test
	public void testToString() {
		assertTrue(moveEmpty.toString().equals("Empty jar: " + SOURCE));
		assertTrue(move.toString().equals(
				"from " + SOURCE + " to " + DESTINATIOn));
	}
}
