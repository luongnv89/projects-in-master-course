package com.sai.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ JarTest.class, MoveTest.class, SolverATest.class,
		SolverBTest.class, SolverCStarTest.class })
public class AllTests {

}
