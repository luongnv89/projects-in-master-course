package com.sai.tests;

import org.junit.Before;

import com.sai.def.Jar;
import com.sai.mai.SolverCStar;

public class SolverCStarTest extends SolverAbstractTest {

	@Before
	public void setUp() throws Exception {
		solver = new SolverCStar(new Jar(0, 12), new Jar(0, DEFAULT_CONTAIN));
		solver.solve(7);
	}

}
