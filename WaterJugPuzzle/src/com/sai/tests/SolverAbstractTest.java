/**
 * 
 */
package com.sai.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sai.def.SolverAbstract;

/**
 * @author NGUYEN VAN LUONG
 *
 */
public class SolverAbstractTest {

	protected SolverAbstract solver;
	protected int MAX = 15;
	protected int DEFAULT_CONTAIN = 5;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		solver = null;
	}

	/**
	 * Test method for {@link com.sai.def.SolverAbstract#animateSolution()}.
	 */
	@Test
	public void testAnimateSolution() {
		solver.animateSolution();
	}

	/**
	 * Test method for {@link com.sai.def.SolverAbstract#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(solver.invariant());
	}

	@Test
	public void testShowAllJugs() {
		solver.showAllJugs();
	}

	@Test
	public void testSolve() {
		assertTrue(solver.solve(DEFAULT_CONTAIN));
		assertFalse(solver.solve(MAX + 1));
	}
}
