/**
 * 
 */
package com.sai.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sai.def.SolverAll;

/**
 * Test for all version solvers 
 * 
 * @author NGUYEN VAN LUONG
 *
 */
public class SolverAllTest {

	SolverAll solver;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		solver = new SolverAll();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		solver = null;
	}

	/**
	 * 
	 * Test method for {@link com.sai.def.SolverAll#SolverAll(int, int)}.
	 * 
	 * 
	 */
	@Test
	public void testSolverAll() {
		solver.solve(3, 9);
	}

}
