package com.sai.tests;

import org.junit.Before;

import com.sai.def.Jar;
import com.sai.luong.SolverB;

public class SolverBTest extends SolverAbstractTest {

	@Before
	public void setUp() throws Exception {
		solver = new SolverB(new Jar(12, 12), new Jar(8, 8), new Jar(0, 7),
				new Jar(0, DEFAULT_CONTAIN));
		solver.solve(6);
	}
}
