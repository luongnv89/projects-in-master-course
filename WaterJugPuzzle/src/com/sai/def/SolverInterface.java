package com.sai.def;

public interface SolverInterface {
	public boolean solve(int target);
	public void animateSolution();
	public void showAllJugs();
}
