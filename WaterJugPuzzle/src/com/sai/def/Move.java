package com.sai.def;

public class Move {

	int source;
	int destination;

	/**
	 * Constructor
	 * @param s the source index 
	 * @param d the destination
	 */
	public Move(int s, int d) {
		source = s;
		destination = d;
	}

	public int getSource() {
		return source;
	}

	public int getDestination() {
		return destination;
	}

	@Override
	public String toString() {
		String result = "";
		if (destination == -1)
			result = "Empty jar: " + source;
		else
			result = "from " + source + " to " + destination;
		return result;
	}
}
