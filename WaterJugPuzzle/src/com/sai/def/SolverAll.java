/**
 * 
 */
package com.sai.def;

import java.util.Random;

import com.sai.christos.SolverA;
import com.sai.luong.SolverB;
import com.sai.mai.SolverCStar;

/**
 * @author NGUYEN VAN LUONG
 *
 */
public class SolverAll {

	final static int MIN_CONTAIN = 3;
	final static int MAX_CONTAIN = 20;
	final static int MIN_JARS = 2;
	final static int MAX_JARS = 10;

	SolverAbstract solver;
	int choice;
	int target;

	/**
	 * Solve a Water problem with :
	 * <li> Random number of jars
	 * <li> Random the max contain of jar
	 * <li> Random the number of full-jar
	 * @param choice to decided the version of Water Jug puzzle
	 * <li> choice =1 : Version A
	 * <li> choice =2 : Version B
	 * <li> choice =3 : Version C
	 * <li> choice =4 : Version C*
	 * <li> choice <1 or choice > 4 -> default for choice = 2
	 * @param target target of problem
	 */
	public void solve(int choice, int target) {
		if (choice < 1 || choice > 4) {
			System.out
					.println("Incorect choice! Choice must be in [1,2,3,4]. default choice = 2");
			choice = 2;
		}
		switch (choice) {
		case 4:
			System.out.println("Water jug puzzle : C*");
			solver = new SolverCStar(cStarSolver());
			break;
		case 1:
			System.out.println("Water jug puzzle : A");
			solver = new SolverA(arrayJars());
			break;
		case 2:
			System.out.println("Water jug puzzle : B");
			solver = new SolverB(arrayJars());
			break;
		case 3:
			System.out.println("Water jug puzzle : C");
			System.out.println("Waiting for Diem's solution.\nDefault choice");
			System.out.println("Water jug puzzle : B");
			solver = new SolverB(arrayJars());
			break;

		default:
			break;
		}
		System.out.println("The initial solver: ");
		solver.showAllJugs();
		System.out.println("Target: " + target);
		System.out.println("Solving.....");
		boolean isSolved = solver.solve(target);
		if (isSolved) {
			System.out.println("Animation for solver: ");
			solver.animateSolution();
		} else {
			System.out.println("Cannot solve!");
		}
	}

	/**
	 * Create a new SolverCStar
	 * @return
	 */
	private SolverAbstract cStarSolver() {
		Random ran = new Random();
		int jar1Max = MIN_CONTAIN + ran.nextInt(MAX_CONTAIN - MIN_CONTAIN);
		int jar2Max = MIN_CONTAIN + ran.nextInt(MAX_CONTAIN - MIN_CONTAIN);
		return new SolverCStar(new Jar(0, jar1Max), new Jar(0, jar2Max));
	}

	/**
	 * Create an array of Jars
	 * @return
	 */
	private Jar[] arrayJars() {
		Random ran = new Random();
		int numberOfJars = MIN_JARS + ran.nextInt(MAX_JARS - MIN_JARS);
		Jar[] listJar = new Jar[numberOfJars];
		for (int i = 0; i < numberOfJars; i++) {
			listJar[i] = createRandomJar();
		}
		return listJar;
	}

	/**
	 * Create randomly a jar
	 * @return
	 */
	private Jar createRandomJar() {

		Random ran = new Random();
		int jarMax = MIN_CONTAIN + ran.nextInt(MAX_CONTAIN - MIN_CONTAIN);
		int isFull = ran.nextInt(10);
		if (isFull > 5)
			return new Jar(jarMax, jarMax);
		else
			return new Jar(0, jarMax);
	}

}
