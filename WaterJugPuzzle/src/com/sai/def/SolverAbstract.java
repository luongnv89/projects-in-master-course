package com.sai.def;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sai.luong.SolverB;

public abstract class SolverAbstract implements SolverInterface,
		InvariantInterface {

	protected List<Jar> jars = null;
	protected List<Move> moves = new ArrayList<Move>();
	protected int target;

	public SolverAbstract(Jar... jarLIst) {
		jars = Arrays.asList(jarLIst);
	}

	public SolverAbstract(SolverAbstract other) {
		this.moves.addAll(other.moves);
		if (other.jars != null) {
			this.jars = new ArrayList<>();
			for (Jar jar : other.jars) {
				Jar newJar = new Jar(jar);
				this.jars.add(newJar);
			}
		}
	}

	@Override
	public void animateSolution() {
		System.out.println("\n**** SOLUTION FOR WATER JUG PUZZLE ****");
		showAllJugs();
		System.out.println("\nAnimation: ");
		for (Move m : moves) {
			System.out.println(m);
		}
		System.out.println("*** END ****");
	}

	@Override
	public boolean invariant() {
		for (Jar j : jars) {
			if (!j.invariant()) {
				return false;
			}
		}
		return (jars != null && jars.size() > 1 && moves != null);
	}

	@Override
	public void showAllJugs() {
		System.out.println();
		for (Jar jar : jars) {
			System.out.print(jar.toString());
		}
	}
}
