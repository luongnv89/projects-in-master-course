package com.sai.def;

public class Jar implements InvariantInterface {
	int currentQuantity;
	int maxQuantity;

	public Jar(Jar jar) {
		this.currentQuantity = jar.getCurrentQuantity();
		this.maxQuantity = jar.getMaxQuantity();
	}

	public Jar(int currentQuantity, int maxQuantity) {
		this.currentQuantity = currentQuantity;
		this.maxQuantity = maxQuantity;
	}

	public int getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(int currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public int getMaxQuantity() {
		return maxQuantity;
	}

	/**
	 * 
	 * @param addNumber
	 * @return the water that has been left off
	 */
	public int add(int addNumber) {
		if (addNumber + getCurrentQuantity() > getMaxQuantity()) {
			int remaining = (addNumber + getCurrentQuantity())
					- getMaxQuantity();
			setCurrentQuantity(getMaxQuantity());
			return remaining;
		} else {
			setCurrentQuantity(getCurrentQuantity() + addNumber);
			return 0;
		}

	}

	/**
	 * Returns the remaining water after adding the water from one {@link Jar} to the other.
	 * Does not actually make the addition, only returns the water that remains
	 * @param amountToBeAdded
	 * @return
	 */
	public int remainingWaterAfterAdding(int amountToBeAdded) {
		if (amountToBeAdded + getCurrentQuantity() > getMaxQuantity()) {
			int remaining = (amountToBeAdded + getCurrentQuantity())
					- getMaxQuantity();
			return remaining;
		}
		return 0;
	}

	/**
	 * Returns true if the jar can be filled completely if we add an amount of water.
	 * @param amountToBeAdded
	 * @return
	 */
	public boolean canFillCompletely(int amountToBeAdded) {
		return getCurrentQuantity() + amountToBeAdded >= getMaxQuantity();
	}

	/**
	 * Returns true if and only if we can add an amount of water to the jar and we do not
	 * surpass the maximum capacity
	 * @param amountToBeAdded
	 * @return
	 */
	public boolean canAddCompletely(int amountToBeAdded) {
		return getCurrentQuantity() + amountToBeAdded <= getMaxQuantity();
	}

	/**
	 * Returns wether the jar is full or not
	 * @return
	 */
	boolean isFull() {
		return getCurrentQuantity() == getMaxQuantity();
	}

	@Override
	public boolean invariant() {
		return (currentQuantity <= maxQuantity && currentQuantity >= 0);
	}

	@Override
	public String toString() {
		return " [" + currentQuantity + "/" + maxQuantity + "] ";
	}
}
