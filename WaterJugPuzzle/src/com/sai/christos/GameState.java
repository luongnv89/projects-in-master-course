package com.sai.christos;


public class GameState {
	
	int []gameState;
	
	public GameState(int stateNumber){
		gameState = new int[stateNumber];
	}
	
	public int getStateAt(int index){
		return gameState[index];
	}
	
	public void setState(int state, int index){
		gameState[index] = state;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		for (int i = 0; i < this.gameState.length; i++) {
			hash += i + gameState[i];
		}
		return 37 + this.gameState.length + hash;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!obj.getClass().equals(this.getClass())) {
			return false;
		}
		GameState state = (GameState)obj;
		if (state.getGameState().length != gameState.length) {
			return false;
		}
		for (int i = 0; i < gameState.length; i++) {
			if (!(getStateAt(i) == state.getStateAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	public int[] getGameState() {
		return gameState;
	}
}
