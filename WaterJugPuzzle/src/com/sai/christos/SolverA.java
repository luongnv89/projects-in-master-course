package com.sai.christos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.sai.def.Jar;
import com.sai.def.Move;
import com.sai.def.SolverAbstract;

/**
 * 
 * @author Christos Sotiriou
 *
 */
public class SolverA extends SolverAbstract {

	public List<GameState> visitedNodes = new ArrayList<GameState>();
	public static int EFFORTS_BEFORE_GIVING_UP = 100;

	public SolverA(Jar... jarLIst) {
		super(jarLIst);
	}

	public SolverA(SolverAbstract listJar) {
		super(listJar);
	}

	@Override
	public boolean solve(int target) {
		GameState originalGameState = getCurrentGameState();
		boolean solutionFound = false;

		int repetitions = EFFORTS_BEFORE_GIVING_UP;
		while (solutionFound == false && repetitions > 0) {
			System.out.println("---resetting----");
			restoreJarsFromGameState(originalGameState);
			visitedNodes.clear();
			moves.clear();
			solutionFound = worker(target);
			repetitions--;
		}

		return solutionFound;
	}

	/**
	 * Secondary worker thread. will make {@link #EFFORTS_BEFORE_GIVING_UP} tries before giving
	 * up and returning false as a viable solution to the puzzle
	 * @param target
	 * @return
	 */
	boolean worker(int target) {
		boolean shouldContinue = true;
		boolean solutionFound = false;
		Random r = new Random(new Date().getTime());
		while (shouldContinue == true && solutionFound == false) {
			if (reachedGoal(target)) {
				solutionFound = true;
			} else {
				/**
				 * In case where we are about to find a solution right now make this final move
				 */
				Jar[] combinationForTarget = solverCombinationByAddingJarContentsForTarget(target);

				if (combinationForTarget != null) {
					intelligentMoveFromSmallestToBiggest(
							combinationForTarget[0], combinationForTarget[1]);
					showAllJugs();
				} else {
					combinationForTarget = solverCombinationByCountingRemainingWaterAfterFilling(target);
					if (combinationForTarget != null) {
						intelligentMoveFromBiggestToSmallest(
								combinationForTarget[0],
								combinationForTarget[1]);
						showAllJugs();
					} else {
						boolean shouldExitMoveLoop = false;
						int repetitionsAllowed = EFFORTS_BEFORE_GIVING_UP;
						boolean hasReachedCircle = false;

						/**
						 * repeat until you find a move that doesn't reach a circle. Limit the search
						 * to 100 times max.
						 */
						while (repetitionsAllowed > 0
								&& shouldExitMoveLoop == false) {
							int randomFrom = Util.randomRange(0, 2, r);
							int randomTo = Util.randomRange(0, 2, r);

							System.out.println("moving from jag " + randomFrom
									+ " to " + randomTo);

							hasReachedCircle = moveFrom(getJagAt(randomFrom),
									getJagAt(randomTo));

							/**
							 * if the move was valid, exit
							 */
							if (!hasReachedCircle) {
								shouldExitMoveLoop = true;
							}
							repetitionsAllowed--;
						}

						this.showAllJugs();

						if (hasReachedCircle) {
							solutionFound = false;
							shouldContinue = false;
						}
					}
				}
			}
		}
		return solutionFound;
	}

	/**
	 * Returns an array that contains two jars, that when the water in them is exchanged
	 * the solution is found
	 * @param target
	 * @return
	 */
	Jar[] solverCombinationByAddingJarContentsForTarget(int target) {

		if (reachesGoalByAddition(biggestJar(), mediumJar(), target)) {
			return new Jar[] { biggestJar(), mediumJar() };
		}
		if (reachesGoalByAddition(biggestJar(), smallestJar(), target)) {
			return new Jar[] { biggestJar(), smallestJar() };
		}
		if (reachesGoalByAddition(smallestJar(), mediumJar(), target)) {
			return new Jar[] { smallestJar(), mediumJar() };
		}

		return null;
	}

	/**
	 * Returns a pair of Jars that can solve the puzzle immediately if
	 * one fills the other one.
	 * @param target
	 * @return a pair of jars if we can solve the puzzle. null if there is no such combination
	 */
	Jar[] solverCombinationByCountingRemainingWaterAfterFilling(int target) {
		if (reachesGoalByFilling(biggestJar(), mediumJar(), target)) {
			return new Jar[] { biggestJar(), mediumJar() };
		}
		if (reachesGoalByFilling(biggestJar(), smallestJar(), target)) {
			return new Jar[] { biggestJar(), smallestJar() };
		}
		if (reachesGoalByFilling(smallestJar(), mediumJar(), target)) {
			return new Jar[] { smallestJar(), mediumJar() };
		}
		return null;
	}

	/**
	 * Are these two jars capable of solving the riddle just by filling one another and looking
	 * at the remaining water in the first one?
	 * @param jarA
	 * @param jarB
	 * @param target
	 * @return
	 */
	boolean reachesGoalByFilling(Jar jarA, Jar jarB, int target) {
		if (jarA.remainingWaterAfterAdding(jarB.getCurrentQuantity()) == target
				|| jarB.remainingWaterAfterAdding(jarA.getCurrentQuantity()) == target
				|| (jarB.canFillCompletely(jarA.getCurrentQuantity()) && jarB
						.getMaxQuantity() == target)
				|| (jarA.canFillCompletely(jarB.getCurrentQuantity()) && jarA
						.getMaxQuantity() == target)) {
			return true;
		}
		return false;
	}

	/**
	 * can we add the water from one jar to the other and solve the riddle?
	 * @param jarA
	 * @param jarB
	 * @param target
	 * @return
	 */
	boolean reachesGoalByAddition(Jar jarA, Jar jarB, int target) {
		if ((jarA.canAddCompletely(jarB.getCurrentQuantity()) || jarB
				.canAddCompletely(jarA.getCurrentQuantity()))
				&& (jarA.getCurrentQuantity() + jarB.getCurrentQuantity()) == target) {
			return true;
		}
		return false;
	}

	/**
	 * Returns true if the riddle is solved.
	 * @param target how much water we need
	 * @return true if the game is won. Else, return false
	 */
	boolean reachedGoal(int target) {
		for (Jar j : jars) {
			if (j.getCurrentQuantity() == target) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Will move from the smallest jar to the biggest
	 * @param left
	 * @param right
	 */
	boolean intelligentMoveFromSmallestToBiggest(Jar left, Jar right) {
		if (left.getMaxQuantity() < right.getMaxQuantity()) {
			return moveFrom(left, right);
		} else {
			return moveFrom(right, left);
		}
	}

	/**
	 * will move water from the biggest jar to the smallest.
	 * @param left
	 * @param right
	 * @return
	 */
	boolean intelligentMoveFromBiggestToSmallest(Jar left, Jar right) {
		if (left.getMaxQuantity() > right.getMaxQuantity()) {
			return moveFrom(left, right);
		} else {
			return moveFrom(right, left);
		}
	}

	/**
	 * 
	 * @param source
	 * @param destination
	 * @return true in case we reached a circle
	 */
	boolean moveFrom(Jar source, Jar destination) {
		if (source == destination) {
			return true;
		}
		GameState previousGameState = getCurrentGameState();

		int remaining = destination.add(source.getCurrentQuantity());
		source.setCurrentQuantity(remaining);
		moves.add(new Move(indexOfJar(source), indexOfJar(destination)));

		GameState state = getCurrentGameState();
		if (visitedNodes.contains(state)) {
			restoreJarsFromGameState(previousGameState);
			moves.remove(moves.size() - 1);
			System.out.println("rolling back");
			return true;
		} else {
			this.visitedNodes.add(state);
			return false;
		}

	}

	/**
	 * Gets the biggest jar of the three
	 * @return
	 */
	Jar biggestJar() {
		Jar result = getJagAt(0);
		for (int i = 0; i < jars.size(); i++) {
			if (result.getMaxQuantity() < getJagAt(i).getMaxQuantity()) {
				result = getJagAt(i);
			}
		}
		return result;
	}

	/**
	 * Gets the smallest jar
	 * @return
	 */
	Jar smallestJar() {
		Jar result = getJagAt(0);
		for (int i = 0; i < jars.size(); i++) {
			if (result.getMaxQuantity() > getJagAt(i).getMaxQuantity()) {
				result = getJagAt(i);
			}
		}
		return result;
	}

	/**
	 * Gets the medium Jar
	 * @return
	 */
	Jar mediumJar() {
		Jar small = smallestJar();
		Jar big = biggestJar();
		for (Jar j : jars) {
			if (j != small && j != big) {
				return (Jar) j;
			}
		}
		return null;
	}

	int indexOfJar(Jar j) {
		return jars.indexOf(j);
	}

	/**
	 * Returns a {@link GameState} object, which is a "snapshot" of the current
	 * game state. It is used in rollback purposes
	 * @return
	 */
	GameState getCurrentGameState() {
		GameState result = new GameState(this.jars.size());
		for (int i = 0; i < this.jars.size(); i++) {
			result.setState(this.jars.get(i).getCurrentQuantity(), i);
		}
		return result;
	}

	/**
	 * Resets the game to the gamestate described by a {@link GameState} object
	 * @param gameState
	 */
	void restoreJarsFromGameState(GameState gameState) {
		for (int i = 0; i < gameState.getGameState().length; i++) {
			jars.get(i).setCurrentQuantity(gameState.getStateAt(i));
		}
	}

//	@Override
//	public void showAllJugs() {
//		for (int i = 0; i < jars.size(); i++) {
//			System.out.println(i + ":" + getJagAt(i).toString());
//		}
//		System.out.println("---");
//	}

	@Override
	public boolean invariant() {
		return super.invariant();
	}

	Jar getJagAt(int index) {
		return (Jar) jars.get(index);
	}

}
