package com.sai.christos;

import java.util.Collections;
import java.util.List;
import java.util.Random;


public class Util {

	/**
	 * Returns the index of an element inside a given integer array.
	 * Returns -1 if the value is not found.
	 * @param haystack
	 * @param needle
	 * @return
	 */
	public static int indexOf(int [] haystack, int needle){
		for (int i = 0; i < haystack.length; i++) {
			if(haystack[i] == needle)
				return i;
		}
		return -1;
	}
	
	

	/**
	 * Returns a shuffled array. Efficient. Uses the Fisher Yates shuffle method.
	 * @param listToShuffle
	 * @return
	 */
	public static void shuffleList (int [] listToShuffle, Random r){
		for (int i = listToShuffle.length-1; i>=0; i--) {
			int j = Util.randomRange(0, i, r);
			int temp  = listToShuffle[j];
			listToShuffle[j] = listToShuffle[i];
			listToShuffle[i] = temp;
		}
	}
	
	
	
	/**
	 * Returns an integer between values
	 * @param min the lower limit
	 * @param max the upper limit
	 * @param rand the {@link Random} instance to generate the random values from
	 * @return an integer between the ranges given
	 */
	public static int randomRange(int min, int max, Random rand){
		int randomNum = rand.nextInt(max - min + 1) + min;
		return randomNum;
	}

	
	/**
	 * Returns an integer between values
	 * @param min the lower limit
	 * @param max the upper limit
	 * @param rand the {@link Random} instance to generate the random values from
	 * @param exclusionValues the values that we do NOT want the result to have.
	 * @return an integer between the ranges given
	 */
	public static int randomRange(int min, int max, Random r, int[] exclusionValues){
		int randomNum = exclusionValues[0];
		while (indexOf(exclusionValues, randomNum) != -1) {
			randomNum = r.nextInt(max - min + 1) + min;
		}
		return randomNum;
	}
	
	
	
	public static int randomChoice(int [] arrayOfInts, Random r){
		return arrayOfInts[randomRange(0, arrayOfInts.length, r)];
	}
	
	
	
	/**
	 * returns true or false based on the percent given. For example,
	 * if percent is 5, then the chance of this function returning true is
	 * 5%.
	 * @param percent
	 * @return true or false, depending whose chances depend on the percentage passed as an argument
	 */
	public static boolean chancePercent(int percent){
		int randomNumber =  randomRange(0, 100, new Random());
		return randomNumber > (100 - percent);
	}
	
	
	
	/**
	 * Plain quicksort algorithm for int array
	 * @param array
	 * @param begin
	 * @param end
	 */
	static void quicksort(int array[], int begin, int end) {
	    int i = begin, j = end;
	    int pivot = array[end];
	    while (i <= j) {
	        while (array[i] < pivot)i++;
	        while (array[j] > pivot)j--;
	        if (i <= j) {
	            if (i != j) {             //this is a swap of values
	                array[i] ^= array[j]; //without a use of an extra
	                array[j] ^= array[i]; //variable, it is a swap using
	                array[i] ^= array[j]; //the XOR operator, i used it
	            }                         //so you can learn it because i
	            i++;                      //think it is fun.
	            j--;
	        } 
	    } 
	    if (begin < j)
	        quicksort(array, begin, j);
	    if (i < end)
	        quicksort(array, i, end);
	}
	
	
	/**
	 * Prints the contents of an int array to the comnand line.
	 * @param arr
	 */
	static public void printArray(int arr[]){
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
	
	
	/**
	 * Compares two arrays holding ints and returns true if their contents are the same
	 * @param arr1
	 * @param arr2
	 * @return true in case the contents of the two arrays are the same and in the same order. Otherwise, false.
	 */
	public static boolean compareArrays(int arr1[], int arr2[]){
		for (int i = 0; i < arr1.length; i++) {
			if (arr1[i] != arr2[i]) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Prints a two dimensional array holding integers.
	 * @param arr the array whose contents to print
	 */
	static public void printTwoDimensionalArray(int arr[][]){
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(" " + arr[i][j]);
			}
			System.out.println(" ");
		}
	}
	
	/**
	 * Copies the contents of one dimensional array to another. The origin is not changed,
	 * only the destination. 
	 * @param origin the array whose contents to copy
	 * @param destination the array where to copy the contents of the origin.
	 */
	static public void copyIntArray(int origin[], int destination[]){
		for (int i = 0; i < destination.length; i++) {
			destination[i] = origin[i];
		}
	}


	/**
	 * Shuffles a list.
	 * @param list the list to shuffle
	 */
	public static void shuffleList(List<?> list){
		long seed = System.nanoTime();
		Collections.shuffle(list, new Random(seed));
	}
	
	
	/**
	 * Copies data from one array to the other. The arrays must be of equal length and size
	 * @param source The source array
	 * @param destination The destination array;
	 */
	public static void copyTwoDimensionalArray(int [][] source, int[][] destination){
		for (int i = 0; i < source.length; i++) {
			for (int j = 0; j < source[i].length; j++) {
				destination[i][j] = source[i][j];
			}
		}
	}
	
	/**
	 * Will produce a new two dimensional array with the contents of the original. NOTE: it will
	 * only work correctly with orthogonal 2d arrays. If the source array has variable size in each
	 * line, it will not produce a correct result, and maybe an exception will be thrown.
	 *  
	 * @param source
	 * @return
	 */
	public static int[][] new2DArrayFromA2DArray(int [][] source){
		int [][] result = new int[source.length][source[0].length];
		copyTwoDimensionalArray(source, result);
		return result;
	}
}
