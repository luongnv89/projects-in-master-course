/**
 * 
 */
package louis.aspectj;

import java.util.ArrayList;
import louis.hello.*;

/**
 * @author luongnv89
 * 
 */
public aspect Profiler {
	ArrayList<Timer> listTimer = new ArrayList<>();

	pointcut methodExecuted() :execution(* *.*(..) ) && within(HelloAspectJ)  ;
	
	pointcut methodCall() : call(* *.*(..)) && within(HelloAspectJ);

	// long startTime;

	before() : methodCall() {
		Timer t = new Timer(thisJoinPoint.toString());
		t.setStartTime(System.nanoTime());
		listTimer.add(t);
		// startTime = System.currentTimeMillis();
	}

	after() returning() : methodCall() {
		for (int i = 0; i < listTimer.size(); i++) {
			if (listTimer.get(i).methodName
					.equals(thisJoinPoint.toString())) {
				listTimer.get(i).setEndTime(System.nanoTime());
				listTimer.get(i).totalExecutionTime();
				listTimer.remove(i);
				break;
			}
		}
		// System.out.println("Execution time: "
		// + String.valueOf(System.currentTimeMillis() - startTime));
	}

	private class Timer {
		long startTime;
		long endTime;
		String methodName;

		/**
		 * @param methodIndex
		 */
		public Timer(String methodName) {
			this.methodName = methodName;
		}

		/**
		 * @param startTime
		 *            the startTime to set
		 */
		public void setStartTime(long startTime) {
			this.startTime = startTime;
		}

		/**
		 * @param endTime
		 *            the endTime to set
		 */
		public void setEndTime(long endTime) {
			this.endTime = endTime;
		}

		public void totalExecutionTime() {
			System.out.println(methodName + " took "
					+ String.valueOf(endTime - startTime) + " nanoseconds");
		}

	}

}
