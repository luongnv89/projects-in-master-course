/**
 * 
 */
package louis.aspectj;
import louis.hello.*;

/**
 * @author luongnv89
 * 
 */
public aspect AspectJHelloAspectJ {
	pointcut myPointCut() : execution(* HelloAspectJ.sayHello(..));

	before() : myPointCut() {
		System.out.println("AspectJ say before sayHello() method:d.");
	}
}
