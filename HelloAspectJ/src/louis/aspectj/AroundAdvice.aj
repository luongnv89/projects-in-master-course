/**
 * 
 */
package louis.aspectj;

import louis.hello.*;

/**
 * @author luongnv89
 * 
 */
public aspect AroundAdvice {
	pointcut publicMethodCall() : execution(public * *.*(..)) && within(HelloAspectJ);

	Object around() : publicMethodCall(){
		long startTime = System.nanoTime();
		Object ret = proceed();
		long end = System.nanoTime();
		System.out.println(this.getClass().getName() + ": "
				+ thisJoinPointStaticPart.getSignature() + " took "
				+ (end - startTime) + " nanoseconds");
		return ret;
	}

}
