/**
 * 
 */
package louis.aspectj;

import louis.hello.*;

/**
 * @author luongnv89
 * 
 */
public aspect AspectJMethodCounter {
	int counter = 0;

	pointcut publicMethodCall() : execution(public * *.*(..)) && within(HelloAspectJ);

	pointcut afterMainMethod() : execution(public * *.main(..));

	after() returning() : publicMethodCall() {
		counter++;
	}

	after() returning() : afterMainMethod() {
		System.out.println(this.getClass().getName()
				+ ": Method call counter: " + counter);
	}
}
