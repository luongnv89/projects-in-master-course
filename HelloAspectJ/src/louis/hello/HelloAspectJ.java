/**
 * 
 */
package louis.hello;

/**
 * @author luongnv89
 * 
 */
public class HelloAspectJ {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		sayHello();

	}

	public static void sayHello() {
		System.out.println("Hello AspectJ!");

	}

}
