package mediators;

import models.Languages;

/**
 * {@link MediatorPartyGoer} represent a person in a party with mediator
 *
 */
public class MediatorPartyGoer {

	/**
	 * Languages which the partygoer can speak
	 */
	private final Languages[] LANGUAGES_SPOKEN;

	/**
	 * The party which the partygoer is joining
	 */
	public Mediator mediator;

	/**
	 * The name of partygoer
	 */
	public final String NAME;

	/**
	 * Create a new partygoer with name and languages which he can speak
	 * @param name
	 * @param spoken
	 */
	public MediatorPartyGoer(String name, Languages[] spoken) {

		mediator = null;
		NAME = name;
		if (spoken == null)
			LANGUAGES_SPOKEN = null;
		else {
			LANGUAGES_SPOKEN = new Languages[spoken.length];
			System.arraycopy(spoken, 0, LANGUAGES_SPOKEN, 0,
					LANGUAGES_SPOKEN.length);
		}
	}

	/**
	 * Partygoer join a party by register with a mediator
	 * @param partyToJoin
	 */
	public void joinParty(Mediator mediator) {
		this.mediator = mediator;
		this.mediator.registerPartyGoer(this);
	}

	/**
	 * Get the number of languages which partygoer can speak
	 * @return 0 if the partygoer cannot speak any language
	 */
	public int numberOfLanguagesSpoken() {
		if (LANGUAGES_SPOKEN == null)
			return 0;
		return LANGUAGES_SPOKEN.length;
	}

	/**
	 * Check Can the partgoer speak a specific language 
	 * @param language language to check 
	 * @return false if the partygoer cannot speak any language
	 * <li> false if the language to test isn't in languages which the partygoer can speak
	 * <li> true if the language to test is one of languages which the partygoer can speak
	 */
	public boolean speaksLanguage(Languages language) {

		if (LANGUAGES_SPOKEN == null)
			return false;
		for (int i = 0; i < LANGUAGES_SPOKEN.length; i++)
			if (language == LANGUAGES_SPOKEN[i])
				return true;

		return false;
	}

	/**
	 * Get the common languages between the partygoer with another partygoer
	 * @param pg another partygoer to check
	 * @return null if the partygoer cannot speak any language
	 * <li> null if the partygoer to check cannot speak any language
	 * <li> An Languages array contains languages which both two partygoer can speak  
	 */
	public Languages[] languagesInCommon(MediatorPartyGoer pg) {

		if (LANGUAGES_SPOKEN == null)
			return null;
		if (pg.numberOfLanguagesSpoken() == 0)
			return null;

		Languages[] result = null;
		int numberOfLanguagesInCommon = 0;

		for (int i = 0; i < LANGUAGES_SPOKEN.length; i++)
			if (pg.speaksLanguage(LANGUAGES_SPOKEN[i]))
				numberOfLanguagesInCommon++;

		int languageIndex = 0;
		if (numberOfLanguagesInCommon > 0) {
			result = new Languages[numberOfLanguagesInCommon];
			for (int i = 0; i < LANGUAGES_SPOKEN.length; i++)
				if (pg.speaksLanguage(LANGUAGES_SPOKEN[i]))
					result[languageIndex++] = LANGUAGES_SPOKEN[i];
		}

		return result;
	}

	/**
	 * Find translators for the partygoer with another partygoer in a party
	 * @param pg the partygoer need to talk
	 * @return null if two partygoers have some languages in common
	 * <li> null if two people aren't at the same party
	 * <li> the result of {@link Mediator#findTranslators(MediatorPartyGoer, MediatorPartyGoer)}
	 */
	public MediatorPartyGoer[] findTranslators(MediatorPartyGoer pg) {
		if (this.mediator == null || pg.mediator == null
				|| this.mediator != pg.mediator) {
			System.out.println("Two people aren't at the same party");
			return null;
		}
		if (this.languagesInCommon(pg) != null) {
			System.out.println(this.NAME + " and " + pg.NAME
					+ " don't need any translators");
			return null;
		} else
			return mediator.findTranslators(this, pg);
	}

	/**
	 * Get the string describe a partygoer
	 * */
	public String toString() {

		String str;
		if (mediator == null)
			str = NAME + " is a member of no party";
		else
			str = NAME + " is a member of party - " + mediator.party.getName()
					+ " -";

		str = str + " and speaks " + numberOfLanguagesSpoken() + " languages:";
		if (LANGUAGES_SPOKEN == null)
			return str;

		for (int i = 0; i < LANGUAGES_SPOKEN.length; i++)
			str = str + " " + LANGUAGES_SPOKEN[i];

		return str;

	}
}
