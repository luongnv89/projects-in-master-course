package mediators;

/**
 * {@link MediatorParty} represent a party has mediator
 * <br> A party has partygoers and name
 *
 */
public class MediatorParty {

	/**
	 * The name of party
	 */
	public String name;

	/**
	 * Create new party
	 * @param name
	 */
	public MediatorParty(String name) {
		this.name = name;
	}

	/**
	 * Get the name of party
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the describe string of party  
	 */
	@Override
	public String toString() {
		return "PartyMediator [name=" + name + "]";
	}

}
