package mediators.applications;

import mediators.Mediator;
import mediators.MediatorParty;
import mediators.MediatorPartyGoer;
import models.Languages;

public class MediatorSimulation {

	public static void main(String[] args) {

		MediatorParty party = new MediatorParty("FunParty");
		System.out.println(party);
		Mediator mediator = new Mediator();
		mediator.registerParty(party);

		Languages[] paulspeaks = { Languages.ENGLISH, Languages.FRENCH };
		MediatorPartyGoer paul = new MediatorPartyGoer("Paul", paulspeaks);
		mediator.registerPartyGoer(paul);
		System.out.println(mediator);

		Languages[] geoffspeaks = { Languages.GERMAN };
		MediatorPartyGoer geoff = new MediatorPartyGoer("Geoff", geoffspeaks);
		mediator.registerPartyGoer(geoff);
		System.out.println(mediator);

		Languages[] samspeaks = { Languages.ITALIAN, Languages.SPANISH,
				Languages.GERMAN };
		MediatorPartyGoer sam = new MediatorPartyGoer("Sam", samspeaks);
		mediator.registerPartyGoer(sam);
		System.out.println(mediator);

		Languages[] billspeaks = { Languages.ENGLISH, Languages.ITALIAN,
				Languages.SPANISH, Languages.FRENCH };
		MediatorPartyGoer bill = new MediatorPartyGoer("Bill", billspeaks);
		mediator.registerPartyGoer(bill);
		System.out.println(mediator);

		Languages[] tomspeaks = { Languages.ENGLISH, Languages.ITALIAN,
				Languages.GREEK };
		MediatorPartyGoer tom = new MediatorPartyGoer("Tom", tomspeaks);
		mediator.registerPartyGoer(tom);
		System.out.println(mediator);

		MediatorPartyGoer[] translators;

		if (tom.findTranslators(bill) == null)
			System.out
					.println("tom and bill can converse and dont need a translator\n");

		if (paul.findTranslators(geoff) == null)
			System.out
					.println("paul and geoff cannot converse as no translator available\n");

		translators = paul.findTranslators(sam);
		if (translators != null) {
			System.out
					.println("paul and sam can converse using one of these translators:");
			for (int i = 0; i < translators.length; i++)
				System.out.println(translators[i]);
		}

		System.out.println();

		translators = tom.findTranslators(geoff);
		System.out
				.println("tom and geoff can converse using one of these translators:");
		for (int i = 0; i < translators.length; i++)
			System.out.println(translators[i]);

	}

}
