/**
 * 
 */
package mediators;

import java.util.ArrayList;

/**
 * {@link Mediator} implement of {@link MediatorInterface}
 */
public class Mediator implements MediatorInterface {
	public MediatorParty party;
	public ArrayList<MediatorPartyGoer> listPartyGoers;

	/**
	 * @param party
	 */
	public Mediator() {
		party = null;
		listPartyGoers = new ArrayList<MediatorPartyGoer>();
	}

	@Override
	public boolean registerParty(MediatorParty party) {
		if (this.party != null) {
			System.out.println("The mediator is working for party: "
					+ this.party.getName());
			return false;
		} else {
			this.party = party;
			return true;
		}
	}

	@Override
	public boolean registerPartyGoer(MediatorPartyGoer partyGoerMediator) {
		if (party == null) {
			System.out.println("There isn't any party to join!");
			return false;
		} else {
			listPartyGoers.add(partyGoerMediator);
			partyGoerMediator.mediator = this;
			return true;
		}

	}

	@Override
	public MediatorPartyGoer[] findTranslators(MediatorPartyGoer pg1,
			MediatorPartyGoer pg2) {
		int numberOfTranslators = 0;

		for (MediatorPartyGoer pgindex : listPartyGoers)
			if (pgindex.languagesInCommon(pg1) != null
					&& pgindex.languagesInCommon(pg2) != null)
				numberOfTranslators++;

		// no translators found
		if (numberOfTranslators == 0)
			return null;

		int transindex = 0;
		MediatorPartyGoer[] translators = new MediatorPartyGoer[numberOfTranslators];
		for (MediatorPartyGoer pgindex : listPartyGoers)
			if (pgindex.languagesInCommon(pg1) != null
					&& pgindex.languagesInCommon(pg2) != null)
				translators[transindex++] = pgindex;

		return translators;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (party == null) {
			return "The mediator doesn't work at any party";
		} else {
			String str = "\nParty: " + party.toString();
			for (int i = 0; i < listPartyGoers.size(); i++) {
				str += "\nPartyGoer: " + listPartyGoers.get(i).toString();
			}
			return str;
		}
	}

}
