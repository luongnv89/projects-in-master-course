/**
 * 
 */
package mediators;

/**
 *  {@link MediatorInterface} is a mediator at a party
 *<br> Support two find translators to help them talk each other
 *
 */
public interface MediatorInterface {

	/**Register a party to mediator.
	 * <br> Each mediator support at only one party
	 * @param party
	 * @return <li> false if the mediator is working in another party
	 * <li> true if the mediator can working in party
	 */
	public boolean registerParty(MediatorParty party);

	/**
	 * Register a partygoer to mediator
	 * <br> Mediator manages all partygoers of the party
	 * @param partyGoer
	 * @return <li> false if there isn't any party to join
	 * <li> true if there is a party to join
	 */
	public boolean registerPartyGoer(MediatorPartyGoer partyGoer);

	/**
	 * Find translators for 2 partyGoers when they cannot talk in any common language 
	 * @param pg1
	 * @param pg2
	 * @return<li> null if there isn't any partygoer can translator for two partygoers
	 * <li> list partygoer who can translator for two partygoers
	 */
	public MediatorPartyGoer[] findTranslators(MediatorPartyGoer pg1,
			MediatorPartyGoer pg2);

}
