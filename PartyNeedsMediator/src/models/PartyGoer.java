package models;


/**
 * {@link PartyGoer} represent a person
 *
 */
public class PartyGoer {

	/**
	 * Languages which the partygoer can speak
	 */
	private final Languages[] LANGUAGES_SPOKEN;

	/**
	 * The party which the partygoer is joining
	 */
	private Party party;

	/**
	 * The name of partygoer
	 */
	public final String NAME;

	/**
	 * Create a new partygoer with name and languages which he can speak
	 * @param name
	 * @param spoken
	 */
	public PartyGoer(String name, Languages[] spoken) {

		party = null;
		NAME = name;
		if (spoken == null)
			LANGUAGES_SPOKEN = null;
		else {
			LANGUAGES_SPOKEN = new Languages[spoken.length];
			System.arraycopy(spoken, 0, LANGUAGES_SPOKEN, 0,
					LANGUAGES_SPOKEN.length);
		}
	}

	/**
	 * Partygoer join a party
	 * @param partyToJoin
	 */
	public void joinParty(Party partyToJoin) {
		party = partyToJoin;
	}

	/**
	 * Get the number of languages which partygoer can speak
	 * @return 0 if the partygoer cannot speak any language
	 */
	public int numberOfLanguagesSpoken() {
		if (LANGUAGES_SPOKEN == null)
			return 0;
		return LANGUAGES_SPOKEN.length;
	}

	/**
	 * Check Can the partgoer speak a specific language 
	 * @param language language to check 
	 * @return false if the partygoer cannot speak any language
	 * <li> false if the language to test isn't in languages which the partygoer can speak
	 * <li> true if the language to test is one of languages which the partygoer can speak
	 */
	public boolean speaksLanguage(Languages language) {

		if (LANGUAGES_SPOKEN == null)
			return false;
		for (int i = 0; i < LANGUAGES_SPOKEN.length; i++)
			if (language == LANGUAGES_SPOKEN[i])
				return true;

		return false;
	}

	/**
	 * Get the common languages between the partygoer with another partygoer
	 * @param pg another partygoer to check
	 * @return null if the partygoer cannot speak any language
	 * <li> null if the partygoer to check cannot speak any language
	 * <li> An Languages array contains languages which both two partygoer can speak  
	 */
	public Languages[] languagesInCommon(PartyGoer pg) {

		if (LANGUAGES_SPOKEN == null)
			return null;
		if (pg.numberOfLanguagesSpoken() == 0)
			return null;

		Languages[] result = null;
		int numberOfLanguagesInCommon = 0;

		for (int i = 0; i < LANGUAGES_SPOKEN.length; i++)
			if (pg.speaksLanguage(LANGUAGES_SPOKEN[i]))
				numberOfLanguagesInCommon++;

		int languageIndex = 0;
		if (numberOfLanguagesInCommon > 0) {
			result = new Languages[numberOfLanguagesInCommon];
			for (int i = 0; i < LANGUAGES_SPOKEN.length; i++)
				if (pg.speaksLanguage(LANGUAGES_SPOKEN[i]))
					result[languageIndex++] = LANGUAGES_SPOKEN[i];
		}

		return result;
	}

	/**
	 * Find translators for the partygoer with another partygoer in a party
	 * @param pg the partygoer need to talk
	 * @return null if two partygoers have some languages in common
	 * <li> null if there isn't any partygoer can translator for two partygoers
	 * <li> list partygoer who can translator for two partygoers
	 */
	public PartyGoer[] findTranslators(PartyGoer pg) {

		// if you have a language in common then there is no need for a translator
		if (languagesInCommon(pg) != null)
			return null;

		int numberOfTranslators = 0;

		for (PartyGoer pgindex : party.partyGoers)
			if (pgindex.languagesInCommon(this) != null
					&& pgindex.languagesInCommon(pg) != null)
				numberOfTranslators++;

		// no translators found
		if (numberOfTranslators == 0)
			return null;

		int transindex = 0;
		PartyGoer[] translators = new PartyGoer[numberOfTranslators];
		for (PartyGoer pgindex : party.partyGoers)
			if (pgindex.languagesInCommon(this) != null
					&& pgindex.languagesInCommon(pg) != null)
				translators[transindex++] = pgindex;

		return translators;

	}

	/**
	 * Get the string describe a partygoer
	 * */
	public String toString() {

		String str;
		if (party == null)
			str = NAME + " is a member of no party";
		else
			str = NAME + " is a member of party - " + party.getName() + " -";

		str = str + " and speaks " + numberOfLanguagesSpoken() + " languages:";
		if (LANGUAGES_SPOKEN == null)
			return str;

		for (int i = 0; i < LANGUAGES_SPOKEN.length; i++)
			str = str + " " + LANGUAGES_SPOKEN[i];

		return str;

	}
}
