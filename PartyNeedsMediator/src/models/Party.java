package models;

import java.util.Vector;

/**
 * {@link Party} represent a party
 * <br> A party has partygoers and name
 *
 */
public class Party {

	/**
	 * List all partygoers of party
	 */
	public Vector<PartyGoer> partyGoers;

	/**
	 * The name of party
	 */
	public String name;

	/**
	 * Create new party without any partygoer
	 * @param name
	 */
	public Party(String name) {

		this.name = name;
		partyGoers = new Vector<PartyGoer>();
	}

	/**
	 * Get the name of party
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Create new party with a list of partygoers (can be null)
	 * @param name
	 * @param partyPeople
	 */
	public Party(String name, PartyGoer[] partyPeople) {
		this.name = name;
		partyGoers = new Vector<PartyGoer>();
		if (partyPeople == null)
			return;

		for (int i = 0; i < partyPeople.length; i++) {
			partyGoers.add(partyPeople[i]);
			partyPeople[i].joinParty(this);
		}
	}

	/**
	 * Join a partygoer in the party
	 * @param pg
	 */
	public void joinParty(PartyGoer pg) {

		pg.joinParty(this);
		partyGoers.add(pg);

	}

	/**
	 * Get the describe string of party  
	 */
	public String toString() {
		String str = "People at the " + name + " party are -\n";
		for (PartyGoer pg : partyGoers)
			str = str + pg + "\n";
		return str;
	}
}
