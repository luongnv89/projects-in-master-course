package applications;

import models.Languages;
import models.Party;
import models.PartyGoer;

public class PartySimulation {
	
	public static void main(String[] args) {
		
		Party party = new Party("FunParty");
		System.out.println(party);
		
		Languages [] paulspeaks = 
		              {Languages.ENGLISH, Languages.FRENCH};
		PartyGoer paul = new PartyGoer ("Paul", paulspeaks);
		party.joinParty(paul);
		System.out.println(party);
		 
		Languages [] geoffspeaks = {Languages.GERMAN};
		PartyGoer geoff = new PartyGoer ("Geoff", geoffspeaks);
		party.joinParty(geoff);
		System.out.println(party);
		
		Languages [] samspeaks = {Languages.ITALIAN, Languages.SPANISH, Languages.GERMAN};
		PartyGoer sam = new PartyGoer ("Sam", samspeaks);
		party.joinParty(sam);
		System.out.println(party);
		
		
		Languages [] billspeaks = {Languages.ENGLISH, Languages.ITALIAN, Languages.SPANISH, Languages.FRENCH};
		PartyGoer bill = new PartyGoer ("Bill", billspeaks);
		party.joinParty(bill);
		System.out.println(party);
		
		Languages [] tomspeaks = {Languages.ENGLISH, Languages.ITALIAN, Languages.GREEK};
		PartyGoer tom = new PartyGoer ("Tom", tomspeaks);
		party.joinParty(tom);
		System.out.println(party);
		
		
		PartyGoer [] partyGoers = {tom, bill, sam, geoff, paul};
		party = new Party("FunParty2",partyGoers);
		System.out.println(party);
		
		PartyGoer [] translators;
		
		
		if (tom.findTranslators(bill)==null)
			System.out.println("tom and bill can converse and dont need a translator\n");
		
		if (paul.findTranslators(geoff)==null)
			System.out.println("paul and geoff cannot converse as no translator available\n");
		
		translators = paul.findTranslators(sam);
		System.out.println("paul and sam can converse using one of these translators:");
		for (int i=0; i <translators.length; i++)
			System.out.println(translators[i]);
		
		System.out.println();
		
		translators = tom.findTranslators(geoff);
		System.out.println("tom and geoff can converse using one of these translators:");
		for (int i=0; i <translators.length; i++)
			System.out.println(translators[i]);
		
	}

}
