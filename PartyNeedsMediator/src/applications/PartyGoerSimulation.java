package applications;

import models.Languages;
import models.PartyGoer;

public class PartyGoerSimulation {
 

	public static void main(String[] args) {
		
		Languages [] paulspeaks = {Languages.ENGLISH, Languages.FRENCH, Languages.SPANISH};
		PartyGoer pg1 = new PartyGoer ("Paul", paulspeaks);
		System.out.println(pg1); 
		for (Languages l : Languages.values())			
			if (!pg1.speaksLanguage(l)) System.out.println("Paul does not speak "+l);
		
		Languages [] geoffspeaks = {Languages.GREEK, Languages.ENGLISH, Languages.ITALIAN, Languages.SPANISH};
		PartyGoer pg2 = new PartyGoer ("Geoff", geoffspeaks);
		System.out.println(pg2);
	    for (Languages l : Languages.values())			
			if (!pg2.speaksLanguage(l)) System.out.println("Geoff does not speak "+l);
		
	    
		Languages [] dummyspeaks = null;
		PartyGoer pg3 = new PartyGoer ("Dummy", dummyspeaks);
		System.out.println(pg3);
	    for (Languages l : Languages.values())			
			if (!pg3.speaksLanguage(l)) System.out.println("Dummy does not speak "+l);
	    
	    
	    Languages [] common = pg1.languagesInCommon(pg2);
		System.out.print("Paul and Geoff can converse in ");
	    for (int i =0; i< common.length; i++) System.out.print( common[i]+" ");
	    System.out.println();
	    
	    common = pg2.languagesInCommon(pg1);
		System.out.print("Geoff and Paul can converse in ");
	    for (int i =0; i< common.length; i++) System.out.print( common[i]+" ");
	    System.out.println();
	    
	    common = pg2.languagesInCommon(pg2);
		System.out.print("Geoff and Geoff can converse in ");
	    for (int i =0; i< common.length; i++) System.out.print( common[i]+" ");
	    System.out.println();
	    
	    if (pg1.languagesInCommon(pg3)==null)
	     System.out.println("Paul and Dummy cannot converse");
	    
	    if (pg3.languagesInCommon(pg2)==null)
		     System.out.println("Dummy and Geoff cannot converse");
	    
	    
	}

}
