/**
 * 
 */
package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import mediators.Mediator;
import mediators.MediatorParty;
import mediators.MediatorPartyGoer;
import models.Languages;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link MediatorPartyGoer}
 *
 */
public class MediatorPartyGoerTest {

	MediatorPartyGoer pgNolanguage;
	MediatorPartyGoer pgEF;
	MediatorPartyGoer pgGI;
	MediatorPartyGoer pgEI;
	MediatorPartyGoer pgS;
	MediatorPartyGoer pgNoparty;

	Mediator mPartySomePeople;
	MediatorParty partySomePeople;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		partySomePeople = new MediatorParty("Party Some People");
		pgNolanguage = new MediatorPartyGoer("PG NoLanguage", null);

		Languages[] pgEFSpeak = { Languages.ENGLISH, Languages.FRENCH };
		pgEF = new MediatorPartyGoer("PG EnglishFrench", pgEFSpeak);

		Languages[] pgGISpeak = { Languages.GERMAN, Languages.ITALIAN };
		pgGI = new MediatorPartyGoer("PG EnglishFrench", pgGISpeak);

		Languages[] pgEISpeak = { Languages.ENGLISH, Languages.ITALIAN };
		pgEI = new MediatorPartyGoer("PG EnglishFrench", pgEISpeak);

		Languages[] pgSSpeak = { Languages.SPANISH };
		pgS = new MediatorPartyGoer("PG EnglishFrench", pgSSpeak);

		pgNoparty = new MediatorPartyGoer("No Party", null);

		mPartySomePeople = new Mediator();

		mPartySomePeople.registerParty(partySomePeople);

		pgNolanguage.joinParty(mPartySomePeople);
		pgEF.joinParty(mPartySomePeople);
		pgGI.joinParty(mPartySomePeople);
		pgEI.joinParty(mPartySomePeople);
		pgS.joinParty(mPartySomePeople);

	}

	/**
	 * Test method for {@link mediators.MediatorPartyGoer#MediatorPartyGoer(java.lang.String, models.Languages[])}.
	 */
	@Test
	public void testMediatorPartyGoer() {
		assertTrue(pgNolanguage.NAME.equals("PG NoLanguage"));
		assertTrue(pgEF.NAME.equals("PG EnglishFrench"));
	}

	/**
	 * Test method for {@link mediators.MediatorPartyGoer#joinParty(mediators.Mediator)}.
	 */
	@Test
	public void testJoinParty() {
		System.out.println("PG haven't joined the party yet:"
				+ pgNoparty.toString());

		pgNoparty.joinParty(mPartySomePeople);

		System.out.println("PG already have joined the party yet:"
				+ pgNoparty.toString());
	}

	/**
	 * Test method for {@link mediators.MediatorPartyGoer#numberOfLanguagesSpoken()}.
	 */
	@Test
	public void testNumberOfLanguagesSpoken() {
		assertTrue(pgNolanguage.numberOfLanguagesSpoken() == 0);
		assertTrue(pgS.numberOfLanguagesSpoken() == 1);
		assertTrue(pgEF.numberOfLanguagesSpoken() == 2);
	}

	/**
	 * Test method for {@link mediators.MediatorPartyGoer#speaksLanguage(models.Languages)}.
	 */
	@Test
	public void testSpeaksLanguage() {
		assertFalse(pgNolanguage.speaksLanguage(Languages.ENGLISH));
		assertFalse(pgEF.speaksLanguage(Languages.GERMAN));
		assertTrue(pgEF.speaksLanguage(Languages.ENGLISH));
		assertTrue(pgEF.speaksLanguage(Languages.FRENCH));
	}

	/**
	 * Test method for {@link mediators.MediatorPartyGoer#languagesInCommon(mediators.MediatorPartyGoer)}.
	 */
	@Test
	public void testLanguagesInCommon() {
		assertNull(pgNolanguage.languagesInCommon(pgEF));
		assertNull(pgEI.languagesInCommon(pgNolanguage));
		assertNotNull(pgEI.languagesInCommon(pgEF));
	}

	/**
	 * Test method for {@link mediators.MediatorPartyGoer#findTranslators(mediators.MediatorPartyGoer)}.
	 */
	@Test
	public void testFindTranslators() {
		assertNull(pgNolanguage.findTranslators(pgEF));
		assertNull(pgS.findTranslators(pgEF));
		assertNull(pgEF.findTranslators(pgEI));
		assertNotNull(pgEF.findTranslators(pgGI));
	}

	/**
	 * Test method for {@link mediators.MediatorPartyGoer#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(pgNolanguage.toString());
		System.out.println(pgNoparty.toString());
		System.out.println(pgEF.toString());
	}

}
