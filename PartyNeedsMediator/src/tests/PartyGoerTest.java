/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import models.Languages;
import models.Party;
import models.PartyGoer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link PartyGoer}
 *
 */
public class PartyGoerTest {
	PartyGoer pgNolanguage;
	PartyGoer pgEF;
	PartyGoer pgGI;
	PartyGoer pgEI;
	PartyGoer pgS;
	PartyGoer pgNoparty;
	Party party;
 
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		pgNolanguage = new PartyGoer("PG NoLanguage", null);

		Languages[] pgEFSpeak = { Languages.ENGLISH, Languages.FRENCH };
		pgEF = new PartyGoer("PG EnglishFrench", pgEFSpeak);

		Languages[] pgGISpeak = { Languages.GERMAN, Languages.ITALIAN };
		pgGI = new PartyGoer("PG EnglishFrench", pgGISpeak);

		Languages[] pgEISpeak = { Languages.ENGLISH, Languages.ITALIAN };
		pgEI = new PartyGoer("PG EnglishFrench", pgEISpeak);

		Languages[] pgSSpeak = { Languages.SPANISH };
		pgS = new PartyGoer("PG EnglishFrench", pgSSpeak);

		PartyGoer[] pgs = { pgNolanguage, pgEF, pgGI, pgEI, pgS };
		party = new Party("Party to Join", pgs);

		pgNoparty = new PartyGoer("PG No Party", null);

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		pgNolanguage = null;
		pgEF = null;
		pgGI = null;
		pgEI = null;
		pgS = null;
		party = null;
		pgNoparty = null;
	}

	/**
	 * Test method for {@link models.PartyGoer#PartyGoer(java.lang.String, models.Languages[])}.
	 */
	@Test
	public void testPartyGoer() {
		assertTrue(pgNolanguage.NAME.equals("PG NoLanguage"));
		assertTrue(pgEF.NAME.equals("PG EnglishFrench"));
	}

	/**
	 * Test method for {@link models.PartyGoer#joinParty(models.Party)}.
	 */
	@Test
	public void testJoinParty() {
		System.out.println("PG haven't joined the party yet:"
				+ pgNoparty.toString());

		pgNoparty.joinParty(party);

		System.out.println("PG already have joined the party yet:"
				+ pgNoparty.toString());

	}

	/**
	 * Test method for {@link models.PartyGoer#numberOfLanguagesSpoken()}.
	 */
	@Test
	public void testNumberOfLanguagesSpoken() {
		assertTrue(pgNolanguage.numberOfLanguagesSpoken() == 0);
		assertTrue(pgS.numberOfLanguagesSpoken() == 1);
		assertTrue(pgEF.numberOfLanguagesSpoken() == 2);
	}

	/**
	 * Test method for {@link models.PartyGoer#speaksLanguage(models.Languages)}.
	 */
	@Test
	public void testSpeaksLanguage() {
		assertFalse(pgNolanguage.speaksLanguage(Languages.ENGLISH));
		assertFalse(pgEF.speaksLanguage(Languages.GERMAN));
		assertTrue(pgEF.speaksLanguage(Languages.ENGLISH));
		assertTrue(pgEF.speaksLanguage(Languages.FRENCH));
	}

	/**
	 * Test method for {@link models.PartyGoer#languagesInCommon(models.PartyGoer)}.
	 */
	@Test
	public void testLanguagesInCommon() {
		assertNull(pgNolanguage.languagesInCommon(pgEF));
		assertNull(pgEI.languagesInCommon(pgNolanguage));
		assertNotNull(pgEI.languagesInCommon(pgEF));
	}

	/**
	 * Test method for {@link models.PartyGoer#findTranslators(models.PartyGoer)}.
	 */
	@Test
	public void testFindTranslators() {
		assertNull(pgNolanguage.findTranslators(pgEF));
		assertNull(pgS.findTranslators(pgEF));
		assertNull(pgEF.findTranslators(pgEI));
		assertNotNull(pgEF.findTranslators(pgGI));
	}

	/**
	 * Test method for {@link models.PartyGoer#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(pgNolanguage.toString());
		System.out.println(pgNoparty.toString());
		System.out.println(pgEF.toString());
	}

}
