package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LanguagesTest.class, MediatorPartyGoerTest.class,
		MediatorPartyTest.class, MediatorTest.class, PartyGoerTest.class,
		PartyTest.class })
public class AllTests {

}
