/**
 * 
 */
package tests;

import static org.junit.Assert.assertTrue;
import mediators.MediatorParty;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link MediatorParty}
 *
 */
public class MediatorPartyTest {
	MediatorParty party;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		party = new MediatorParty("Party with Mediator");
	}

	/**
	 * Test method for {@link mediators.MediatorParty#MediatorParty(java.lang.String)}.
	 */
	@Test
	public void testMediatorParty() {
		assertTrue(party.getName().equals("Party with Mediator"));
		System.out.println(party.toString());
	}

	/**
	 * Test method for {@link mediators.MediatorParty#getName()}.
	 */
	@Test
	public void testGetName() {
		assertTrue(party.getName().equals("Party with Mediator"));
	}

	/**
	 * Test method for {@link mediators.MediatorParty#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(party.toString());
	}

}
