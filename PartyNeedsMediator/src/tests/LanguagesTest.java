/**
 * 
 */
package tests;

import static org.junit.Assert.assertTrue;
import models.Languages;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link Languages} enum
 *
 */
public class LanguagesTest {

	Languages[] l;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		l = new Languages[6];
		l[0] = Languages.ENGLISH;
		l[1] = Languages.FRENCH;
		l[2] = Languages.GERMAN;
		l[3] = Languages.GREEK;
		l[4] = Languages.ITALIAN;
		l[5] = Languages.SPANISH;
	}

	/**
	 * Test method for {@link models.Languages#toString()}.
	 */
	@Test
	public void testToString() {
		assertTrue(l[0].toString().equals("English"));
		assertTrue(l[1].toString().equals("French"));
		assertTrue(l[2].toString().equals("German"));
		assertTrue(l[3].toString().equals("Greek"));
		assertTrue(l[4].toString().equals("Italian"));
		assertTrue(l[5].toString().equals("Spanish"));
	}

}
