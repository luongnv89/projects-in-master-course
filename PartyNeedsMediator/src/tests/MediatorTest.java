/**
 * 
 */
package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import mediators.Mediator;
import mediators.MediatorParty;
import mediators.MediatorPartyGoer;
import models.Languages;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link Mediator}
 *
 */
public class MediatorTest {

	Mediator mNoParty;

	Mediator mPartyNoPeople;

	Mediator mPartySomePeople;

	MediatorPartyGoer pgNolanguage;
	MediatorPartyGoer pgEF;
	MediatorPartyGoer pgGI;
	MediatorPartyGoer pgEI;
	MediatorPartyGoer pgS;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mNoParty = new Mediator();

		MediatorParty partyNoPeople = new MediatorParty("Party No People");
		mPartyNoPeople = new Mediator();
		mPartyNoPeople.registerParty(partyNoPeople);

		MediatorParty partySomePeople = new MediatorParty("Party Some People");
		pgNolanguage = new MediatorPartyGoer("PG NoLanguage", null);

		Languages[] pgEFSpeak = { Languages.ENGLISH, Languages.FRENCH };
		pgEF = new MediatorPartyGoer("PG EnglishFrench", pgEFSpeak);

		Languages[] pgGISpeak = { Languages.GERMAN, Languages.ITALIAN };
		pgGI = new MediatorPartyGoer("PG EnglishFrench", pgGISpeak);

		Languages[] pgEISpeak = { Languages.ENGLISH, Languages.ITALIAN };
		pgEI = new MediatorPartyGoer("PG EnglishFrench", pgEISpeak);

		Languages[] pgSSpeak = { Languages.SPANISH };
		pgS = new MediatorPartyGoer("PG EnglishFrench", pgSSpeak);

		mPartySomePeople = new Mediator();

		mPartySomePeople.registerParty(partySomePeople);

		mPartySomePeople.registerPartyGoer(pgNolanguage);
		mPartySomePeople.registerPartyGoer(pgEF);
		mPartySomePeople.registerPartyGoer(pgEI);
		mPartySomePeople.registerPartyGoer(pgGI);
		mPartySomePeople.registerPartyGoer(pgS);

	}

	/**
	 * Test method for {@link mediators.Mediator#Mediator()}.
	 */
	@Test
	public void testMediator() {
		assertNull(mNoParty.party);
		assertNotNull(mPartyNoPeople.party);
	}

	/**
	 * Test method for {@link mediators.Mediator#registerParty(mediators.MediatorParty)}.
	 */
	@Test
	public void testRegisterParty() {
		MediatorParty party = new MediatorParty("Party to register");
		assertTrue(mNoParty.registerParty(party));
		assertFalse(mPartyNoPeople.registerParty(party));
		assertFalse(mPartySomePeople.registerParty(party));
	}

	/**
	 * Test method for {@link mediators.Mediator#registerPartyGoer(mediators.MediatorPartyGoer)}.
	 */
	@Test
	public void testRegisterPartyGoer() {
		MediatorPartyGoer partyGoer = new MediatorPartyGoer(
				"PartyGoer to register", null);
		assertFalse(mNoParty.registerPartyGoer(partyGoer));
		assertTrue(mPartyNoPeople.registerPartyGoer(partyGoer));
		assertTrue(mPartySomePeople.registerPartyGoer(partyGoer));
	}

	/**
	 * Test method for {@link mediators.Mediator#findTranslators(mediators.MediatorPartyGoer, mediators.MediatorPartyGoer)}.
	 */
	@Test
	public void testFindTranslators() {
		assertNull(mPartySomePeople.findTranslators(pgNolanguage, pgEF));
		assertNull(mPartySomePeople.findTranslators(pgS, pgEF));
		assertNotNull(mPartySomePeople.findTranslators(pgEF, pgGI));
	}

}
