/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import models.Languages;
import models.Party;
import models.PartyGoer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link Party} class
 *
 */
public class PartyTest {

	Party partyNoPeople;
	Party partyWithSomePeople;
	Party partyNoPeople2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		partyNoPeople = new Party("Party No People");

		Languages[] pg1Speaks = { Languages.ENGLISH, Languages.FRENCH };
		PartyGoer pg1 = new PartyGoer("PartyGoer1", pg1Speaks);

		Languages[] pg2Speaks = { Languages.ENGLISH, Languages.ITALIAN };
		PartyGoer pg2 = new PartyGoer("PartyGoer1", pg2Speaks);

		PartyGoer[] pgs = { pg1, pg2 };
		partyWithSomePeople = new Party("Party With Some People", pgs);

		partyNoPeople2 = new Party("Party No People 2", null);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		partyNoPeople = null;
		partyWithSomePeople = null;
	}

	/**
	 * Test method for {@link models.Party#Party(java.lang.String)}.
	 */
	@Test
	public void testPartyString() {
		assertTrue(partyNoPeople.partyGoers.size() == 0);
	}

	/**
	 * Test method for {@link models.Party#getName()}.
	 */
	@Test
	public void testGetName() {
		assertTrue(partyNoPeople.getName().equals("Party No People"));
		assertTrue(partyWithSomePeople.getName().equals(
				"Party With Some People"));
	}

	/**
	 * Test method for {@link models.Party#Party(java.lang.String, models.PartyGoer[])}.
	 */
	@Test
	public void testPartyStringPartyGoerArray() {
		assertTrue(partyWithSomePeople.partyGoers.size() == 2);
		assertTrue(partyNoPeople2.partyGoers.size() == 0);
	}

	/**
	 * Test method for {@link models.Party#joinParty(models.PartyGoer)}.
	 */
	@Test
	public void testJoinParty() {
		Languages[] pg3Speaks = { Languages.GREEK };
		PartyGoer pg3 = new PartyGoer("PartyGoer 3", pg3Speaks);

		partyNoPeople.joinParty(pg3);
		assertTrue(partyNoPeople.partyGoers.size() == 1);

		partyWithSomePeople.joinParty(pg3);
		assertTrue(partyWithSomePeople.partyGoers.size() == 3);

		partyNoPeople2.joinParty(pg3);
		assertTrue(partyNoPeople2.partyGoers.size() == 1);

	}

	/**
	 * Test method for {@link models.Party#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(partyNoPeople.toString());
		System.out.println(partyWithSomePeople.toString());
	}

}
