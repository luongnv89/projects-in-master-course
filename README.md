#Projects in Master course#

Some of my Java projects in my Master course

* 15 Puzzle Game
* AspectJ examples
* Char sequence implementation
* Decorator Pattern
* Dices MVC model
* Electrical Dice
* Encrypto Graphic
* Fitnite Sequence
* Java Exceptions
* JSOUP examples
* Library Testing
* Lift problem
* Match
* MiniMax XO
* N Puzzle
* Next Date
* Observer Pattern
* Paddle For Game MVC model
* Palindrome
* Party Need Mediator
* Paul Dice
* Poker
* Generic Algorithms
* Tic tac toe Refactor
* Unreliable Bounded Queue
* Unreliable Bounded Stack
* Water Jug Puzzle
* Web Services
* XML examples

-------------------------------
Nguyen Van Luong - luongnv89@gmail.com - [http://luongnv.info](http://luongnv.info)