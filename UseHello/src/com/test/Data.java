package com.test;

/**
 * Map a language with its message
 * @author luongnv89
 *
 */
public class Data {
	String language;
	String msg;

	public Data(String lang, String m) {
		language = lang;
		msg = m;
	}

}
