package com.test;

/**
 * Interface
 * @author luongnv89
 *
 */
public interface LanguagesMap {
	/**
	 * Get the message with the input language
	 * @param lang The input language
	 * @return the message of the input language
	 */
	String map(String lang);
}
