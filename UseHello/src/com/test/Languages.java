package com.test;

import java.util.ArrayList;

/**
 * Languages database to map
 * @author luongnv89
 *
 */
public class Languages implements LanguagesMap{
	/**
	 * List languages in database
	 */
	ArrayList<Data> listLanguages = new ArrayList<Data>();

	/**
	 * Add one more language with its message
	 * @param language
	 * @param msg
	 */
	public void add(String language, String msg) {
		Data newLanguage = new Data(language, msg);
		listLanguages.add(newLanguage);
	}
	
	public String map(String lang) {
		for (int i = 0; i < listLanguages.size(); i++) {
			if (lang.equalsIgnoreCase(listLanguages.get(i).language))
				return listLanguages.get(i).msg;
		}
		return "Sorry, I don't find your languages";
	}
	
}
