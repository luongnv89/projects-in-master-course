package com.test;

/**
 * Class with the lang can be set
 * @author luongnv89
 *
 */
class HelloWorld1 {
	String lang;

	public void setMsg(String msg) {
		this.lang = msg;
	}

	public HelloWorld1(String m) {
		lang = m;
	}

	/**
	 * Say hello
	 * @param langg Language database
	 */
	public void printHello(Languages langg) {
		System.out.println(langg.map(lang));
	}
}
