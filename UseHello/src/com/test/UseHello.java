package com.test;

public class UseHello {
	public static void main(String[] args) {
		// Input language database
		Languages listLang = new Languages();
		listLang.add("French", "Bonjour Monde");
		listLang.add("English", "Hello World");
		listLang.add("Vietnam", "Chao the gioi");

		// The HelloWorld1 class
		HelloWorld1 myHello = new HelloWorld1("French");
		myHello.printHello(listLang);
		myHello.setMsg("Vietnam");
		myHello.printHello(listLang);
		myHello.setMsg("English");
		myHello.printHello(listLang);
		myHello.setMsg("Greek");
		myHello.printHello(listLang);

		System.out.println("\n******************************\n");

		// The HelloWorld2 class
		HelloWorld2 myHelloWorld2 = new HelloWorld2();
		myHelloWorld2.printHello("French", listLang);
		myHelloWorld2.printHello("Vietnam", listLang);
		myHelloWorld2.printHello("English", listLang);
		myHelloWorld2.printHello("Greek", listLang);
	}

}
