package com.test;

/**
 * Functional class
 * 
 * @author luongnv89
 * 
 */
class HelloWorld2 {

	/**
	 * Say hello
	 * 
	 * @param msg
	 *            The lang
	 * @param lang
	 *            Language database
	 */
	public void printHello(String msg, Languages lang) {
		System.out.println(lang.map(msg));

	};
}