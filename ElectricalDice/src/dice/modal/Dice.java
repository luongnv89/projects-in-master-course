package dice.modal;

import java.util.Random;

import dice.interf.DiceInterface;

/**
 * 
 * The Dice class describe a dice.
 * 
 * @version 1.0
 * @author luongnv89
 * 
 */
public class Dice extends DiceWithStatistic implements DiceInterface {
	/**
	 * The number of side of Dice
	 */
	int size;
	/**
	 * The value of last roll. lastRoll = 0 when the Dice is initialed.
	 */
	int lastRoll = 0;
	/**
	 * Count the times the Dice is rolled.
	 */
	int numberRolls = 0;

	public Dice() {
		super();
		this.size = Config.SIDE_DEFAULT;

	}

	public Dice(int inputSize) {
		super(inputSize);
		if (inputSize < Config.SIDE_MIN || inputSize > Config.SIDE_MAX)
			this.size = Config.SIDE_DEFAULT;
		else
			this.size = inputSize;

	}

	public Dice(Dice otherDice) {
		super(otherDice);
		this.size = otherDice.size;

	}

	/**
	 * Roll Dice
	 */
	public void roll() {
		if (!this.invariant())
			System.out.println("\nThe dice is invalid!");
		else {
			numberRolls++;
			Random ran = new Random();
			this.lastRoll = 1 + ran.nextInt(size);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dice.interf.DiceInterface#invariant()
	 */
	@Override
	public boolean invariant() {
		if (this.size < Config.SIDE_MIN || this.size > Config.SIDE_MAX
				|| lastRoll < 0 || lastRoll > this.size)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dice.interf.DiceInterface#equals(dice.modal.Dice)
	 */
	@Override
	public boolean equals(Dice otherDice) {
		if (this.size == otherDice.size && this.lastRoll == otherDice.lastRoll)
			return true;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Sides of Dice is " + this.size + ". Last roll is "
				+ this.lastRoll+". Toal number of roll = "+this.numberRolls;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getLastRoll() {
		return lastRoll;
	}

	public int getNumberRolls() {
		return numberRolls;
	}

}
