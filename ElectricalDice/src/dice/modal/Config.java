package dice.modal;

public final class Config {
	static final int SIDE_DEFAULT = 6;
	static final int SIDE_MIN = 3;
	static final int SIDE_MAX = 36;

	boolean INVARIANT_CLASS = (SIDE_MIN >= 0 && SIDE_DEFAULT >= SIDE_MIN && SIDE_DEFAULT < SIDE_MAX);
}
