package dice.modal;

/**
 * Contain only static variable of Dice class
 * @version 1.0
 * @author luongnv89
 *
 */
/**
 * @author luongnv89
 * 
 */
public class DiceWithStatistic {
	/**
	 * The number of instance of Dice
	 */
	private static int NUMBERDICES = 0;

	/**
	 * Initial a default Dice
	 */
	public DiceWithStatistic() {
		DiceWithStatistic.NUMBERDICES++;
	}

	/**
	 * Initial a Dice with the number of side is input parameter
	 * 
	 * @param inputSize
	 */
	public DiceWithStatistic(int inputSize) {
		DiceWithStatistic.NUMBERDICES++;
	}

	/**
	 * Initial a Dice with another Dice is input parameter. The new Dice will
	 * has the same side, but the last roll is 0.
	 * 
	 * @param otherDice
	 */
	public DiceWithStatistic(DiceWithStatistic otherDice) {
		DiceWithStatistic.NUMBERDICES++;
	}

	/**
	 * Get the number of instances are exist
	 * 
	 * @return The number of instances are exist
	 */
	public static int getNUMBERDICES() {
		return NUMBERDICES;
	}
}
