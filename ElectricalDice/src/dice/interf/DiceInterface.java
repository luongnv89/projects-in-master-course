package dice.interf;

import dice.modal.Dice;

/**
 * Dice provide two interface
 * 
 * @version 1.0
 * @author luongnv89
 * 
 */
public interface DiceInterface {
	/**
	 * Test the validation of a Dice. If the sides of Dice is less than SIDE_MIN
	 * or bigger than SIDE_MAX then the Dice is invalid. If the value of last
	 * roll is less than 0 or bigger than the sides of Dice then the Dice is
	 * invalid.
	 * 
	 * @return True if Dice is valid
	 */
	boolean invariant();

	/**
	 * Compare current Dice with another Dice
	 * 
	 * @param otherDice
	 *            Other dice to compare
	 * @return True if two Dice are same side and last roll values.
	 */
	boolean equals(Dice otherDice);
}
