package dice.tests;

import org.junit.Test;

import dice.modal.Dice;
import dice.modal.DiceWithStatistic;

/**
 * Test for {@link Dice} class.
 * 
 * @author luongnv89
 * 
 */
public class DiceTest {

	Dice dice;

	/**
	 * Test initial a Dice by default constructor {@link Dice#Dice()}
	 */
	@Test
	public void testDefault() {
		dice = new Dice();
		System.out.println("*** A new Dice: " + dice.toString());
		// System.out.println(DiceWithStatistic.getNUMBERDICES());
	}

	/**
	 * Test initial a Dice with an invalid input of sides {@link Dice#Dice(int)}
	 */
	@Test
	public void testInputSizeLessThan() {
		dice = new Dice(2);
		System.out.println("*** A new Dice: " + dice.toString());
		// System.out.println(DiceWithStatistic.getNUMBERDICES());
	}

	/**
	 * Test initial a Dice with an invalid input of sides {@link Dice#Dice(Side)}
	 */
	@Test
	public void testInputSizeBiggerThan() {
		dice = new Dice(37);
		System.out.println("*** A new Dice: " + dice.toString());
		// System.out.println(DiceWithStatistic.getNUMBERDICES());
	}

	/**
	 * Test initial a Dice with a valid input of sides
	 */
	@Test
	public void testInputValidSize() {
		dice = new Dice(10);
		System.out.println("*** A new Dice: " + dice.toString());
		// System.out.println(DiceWithStatistic.getNUMBERDICES());
	}

	/**
	 * Test roll method of Dice
	 */
	@Test
	public void testRoll() {
		dice = new Dice(10);
		System.out.println("\nBEFORE ROLL: ");
		System.out.println("*** A new Dice: " + dice.toString());
		// System.out.println(DiceWithStatistic.getNUMBERDICES());
		dice.roll();
		System.out.println("\nAFTER ROLL: " + dice.toString());
	}

	/**
	 * Test initial a Dice with another Dice
	 */
	@Test
	public void testCoppyDice() {
		dice = new Dice(10);
		System.out.println("\nBEFORE ROLL: ");
		System.out.println("*** A new Dice: " + dice.toString());
		// System.out.println(DiceWithStatistic.getNUMBERDICES());
		Dice dice2 = new Dice(dice);
		System.out.println("*** A new Dice: " + dice2.toString());
		// System.out.println(DiceWithStatistic.getNUMBERDICES());

		dice.roll();
		System.out.println("\nAFTER ROLL: " + dice.toString());
		Dice dice3 = new Dice(dice);
		System.out.println("*** A new Dice: " + dice3.toString());
		// System.out.println(DiceWithStatistic.getNUMBERDICES());

	}

	/**
	 * Test roll a Dice many times
	 */
	@Test
	public void testMultiRolls() {
		dice = new Dice(10);
		System.out.println("\nBEFORE ROLL: ");
		System.out.println("*** A new Dice: " + dice.toString());
		for (int i = 0; i < 9; i++) {
			System.out.print((i + 1) + ":|");
			dice.roll();
			int lastRoll = dice.getLastRoll();
			for (int j = 0; j < lastRoll; j++) {
				System.out.print("* ");
			}
			System.out.print("(" + lastRoll + ")");
			System.out.println();
		}
		System.out.println("\nAFTER ROLL: " + dice.toString());
	}

	/**
	 * Test compare two Dices.
	 */
	@Test
	public void testEqual() {
		dice = new Dice();
		System.out.println("*** A new Dice: " + dice.toString());
		System.out.println("Compare itself: " + dice.equals(dice));
		Dice dice2 = new Dice(dice);

		dice.roll();
		dice2.roll();
		System.out.println(dice.toString());
		System.out.println("*** A new Dice: " + dice2.toString());
		System.out.println("Compare another dice: " + dice.equals(dice2));
		System.out.println("\n***The number of Dices are initialed : "
				+ DiceWithStatistic.getNUMBERDICES());

	}

	@Test
	public void hasCodeTest() {
		dice = new Dice();
		System.out.println(dice.hashCode());
		Dice newDice = new Dice(dice);
		System.out.println("\n"+newDice.hashCode());
		Dice dice2 = dice;
		System.out.println("\n"+dice2.hashCode());
		System.out.println(dice.toString());
		System.out.println(dice2.toString());
		System.out.println(newDice.toString());
		System.out.println(dice.equals(dice2));
	}
	
	@Test
	public void calHashCode(){
		dice = new Dice();
		System.out.println(hashCode(dice));
		System.out.println(hashCode(new Dice(dice)));
	}

	private int hashCode(Dice dice2) {
		// TODO Auto-generated method stub
		int hash = 37;
		hash = 41*hash + dice2.getLastRoll();
		hash = 41*hash + dice2.getSize();
		return hash;
	}
}
