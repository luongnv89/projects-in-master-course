package mediatheque.document;

public interface HasInvariant {
	
	  public int dureeEmprunt(); 
	  public double tarifEmprunt();

}
