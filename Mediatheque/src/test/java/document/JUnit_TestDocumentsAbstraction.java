package document;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mediatheque.Genre;
import mediatheque.Localisation;
import mediatheque.OperationImpossible;
import mediatheque.document.Document;

import org.junit.Test;

import util.InvariantBroken;

public class JUnit_TestDocumentsAbstraction {

	/**
	 * Document can be borrowed
	 */
	protected mediatheque.document.Document canBorrow;
	/**
	 * Document can Not be borrowed
	 */
	protected mediatheque.document.Document cannotBorrow;
	/**
	 * where the document is.
	 */
	protected Localisation position;
	
	/**
	 * The type of this document
	 */
	protected Genre type;

	/**
	 * Test
	 * {@link mediatheque.document.Document#Document(String, mediatheque.Localisation, String, String, String, mediatheque.Genre)}
	 * . Test <code>invariant()</code> and is <code>estEmpruntable()</code>
	 * features of Documents
	 */
	@Test
	public void testConstructor() {
		assertTrue(canBorrow.invariant());
		assertTrue(canBorrow.estEmpruntable());

		assertTrue(cannotBorrow.invariant());
		assertFalse(cannotBorrow.estEmpruntable());
	}
	
	/**
	 * Test {@link Document#estEmpruntable()}
	 * <ul>
	 * <li> isBorrowed can be borrowed
	 * <li> notBorrowedAndBorrowable can be borrowed
	 * <li> notBorrowable can NOT be borrowed
	 * <ul>
	 */
	@Test
	public void testBorrowable(){
		assertTrue(canBorrow.estEmpruntable());
		assertFalse(cannotBorrow.estEmpruntable());
	}

//	/**
//	 * Test {@link Document#estEmprunte()}
//	 * <ul>
//	 * <li> isBorrowed is borrowed
//	 * <li> notBorrowedAndBorrowable is NOT borrowed
//	 * <li> notBorrowable is NOT borrowed
//	 * <ul>
//	 */
//	@Test
//	public void testBorrowed(){
//		assertTrue(isBorrowed.estEmprunte());
//		assertFalse(notBorrowedAndBorrowable.estEmprunte());
//		assertFalse(notBorrowable.estEmprunte());
//	}
	
	/**
	 * Test {@link Document#equals(Object)}
	 * <ul>
	 * <li> A Document is equal to itself.
	 * <li> 2 any Documents are different, then they are not equal.
	 * <ul>
	 */
	@Test
	public void testEqual(){
		assertTrue(canBorrow.equals(canBorrow));
		assertFalse(canBorrow.equals(cannotBorrow));
	}
	
	/**
	 * Test {@link Document#toString()}, with <code>isBorrowed</code> Document
	 */
	@Test
	public void testToString(){
		System.out.println(canBorrow.toString());
	}
	
	/**
	 * Test {@link Document#metConsultable()}, 
	 * checks <code>estEmpruntable</code> after metConsultable().
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testmetConsultableOK() throws OperationImpossible, InvariantBroken{
		assertTrue(canBorrow.estEmpruntable());
		canBorrow.metConsultable();
		assertFalse(canBorrow.estEmpruntable());
	}
	
	/**
	 * Test {@link Document#metConsultable()}, 
	 * Not OK because metConsultable a document can not be borrowed.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testmetConsultableKo1() throws OperationImpossible, InvariantBroken{
		cannotBorrow.metConsultable();
	}
	/**
	 * Test {@link Document#metConsultable()}, 
	 * Not OK because metConsultable a document is borrowed.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testmetConsultableKo2() throws OperationImpossible, InvariantBroken{
		canBorrow.emprunter();
		canBorrow.metConsultable();
	}
	
	/**
	 * Test {@link Document#metEmpruntable()}, 
	 * It's OK, because the document is currently not allowed to borrow.
	 * checks <code>nonEmpruntable</code> after metEmpruntable().
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testmetEmpruntableOK() throws OperationImpossible, InvariantBroken{
		assertFalse(cannotBorrow.estEmpruntable());
		cannotBorrow.metEmpruntable();
		assertTrue(cannotBorrow.estEmpruntable());
	}
	
	/**
	 * Test {@link Document#metEmpruntable()}, 
	 * It's NOT OK, because the document is already  allowed to borrow.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testmetEmpruntableKo() throws OperationImpossible, InvariantBroken{
		canBorrow.metEmpruntable();
	}
	
	/**
	 * Test {@link Document#getAuteur()}
	 */
	@Test
	public void testgetAuthor() {
		assertEquals("Nguyen", canBorrow.getAuteur());
		assertEquals("Nguyen", cannotBorrow.getAuteur());
	}

	/**
	 * Test {@link Document#getAnnee()}
	 */
	@Test
	public void testgetYear() {
		assertEquals("2000", canBorrow.getAnnee());
		assertEquals("2010", cannotBorrow.getAnnee());
	}
	/**
	 * Test {@link Document#getGenre()}
	 */
	@Test
	public void testgetGenre() {		
		assertEquals(type, canBorrow.getGenre());
		assertEquals(type, cannotBorrow.getGenre());
	}
	
	/**
	 * Test {@link Document#getLocalisation()}
	 */
	@Test
	public void testgetLocation() {
		assertEquals(position, canBorrow.getLocalisation());
		assertEquals(position, cannotBorrow.getLocalisation());
	}
	
	/**
	 * Test {@link Document#estEmpruntable()}.
	 * <code>canBorrow</code> will return true.
	 * <code>cannotBorrow</code> will return false.
	 */
	@Test
	public void testEstEmpruntable(){
		assertTrue(canBorrow.estEmpruntable());
		assertFalse(cannotBorrow.estEmpruntable());
	}
	
	@Test
	public void testEstEmprunte() throws InvariantBroken, OperationImpossible{
		canBorrow.emprunter();
		assertTrue(canBorrow.estEmprunte());
	}
	
	/**
	 * Test {@link Document#emprunter()},
	 * OK when borrows a document allowed to borrow.
	 * After that, the number of borrowed document increases 1 and the document is changed to borrowed status.
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test
	public void testEmprunterOK() throws InvariantBroken, OperationImpossible{
		int nbBorrow = canBorrow.getNbEmprunts();
		canBorrow.emprunter();
		assertEquals(nbBorrow+1, canBorrow.getNbEmprunts());
		assertTrue(canBorrow.estEmprunte());
	}
	
	/**
	 * Test {@link Document#emprunter()},
	 * NOT OK when borrows a document is already borrowed.
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test(expected=OperationImpossible.class)
	public void testEmprunterKo1() throws InvariantBroken, OperationImpossible{
		canBorrow.emprunter();
		canBorrow.emprunter();
	}
	
	/**
	 * Test {@link Document#emprunter()},
	 * NOT OK when borrows a document is not allowed to borrow.
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test(expected=OperationImpossible.class)
	public void testEmprunterKo2() throws InvariantBroken, OperationImpossible{
		cannotBorrow.emprunter();
	}
	
	/**
	 * Test {@link Document#restituer()},
	 * OK when return a document which is borrowed.
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test
	public void testRestituerOK() throws InvariantBroken, OperationImpossible{
		canBorrow.emprunter();
		assertTrue(canBorrow.estEmprunte());
		canBorrow.restituer();
		assertFalse(canBorrow.estEmprunte());
	}
	
	/**
	 * Test {@link Document#restituer()},
	 * Not OK when return a document which is not allowed to borrow..
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test(expected=OperationImpossible.class)
	public void testRestituerKo1() throws InvariantBroken, OperationImpossible{
		cannotBorrow.restituer();
	}
	
	/**
	 * Test {@link Document#restituer()},
	 * Not OK when return a document which is not borrowed.
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test(expected=OperationImpossible.class)
	public void testRestituerKo2() throws InvariantBroken, OperationImpossible{
		canBorrow.restituer();
	}
	
	/**
	 * Test {@link Document#afficherStatDocument()}
	 * display the result on the screen.
	 */
	@Test
	public void testafficherStatDocument(){
		canBorrow.afficherStatDocument();
	}
	
	/**
	 * Test {@link Document#afficherStatistiques()}
	 * display the result on the screen.
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testAfficherStatistiques(){
		canBorrow.afficherStatistiques();
	}
}
