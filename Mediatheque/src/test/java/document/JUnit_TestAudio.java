package document;

import static org.junit.Assert.*;

import mediatheque.Genre;
import mediatheque.Localisation;
import mediatheque.document.Audio;
import mediatheque.document.Document;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JUnit_TestAudio extends JUnit_TestDocumentsAbstraction {

	@Before
	public void setUp() throws Exception {
		type = new Genre("audio");
		position = new Localisation("Audio", "Languages");
		
		canBorrow = new Audio("111", position,
				"French fast audio", "Nguyen", "2000", type,
				"Audio");
		canBorrow.metEmpruntable();
		
		cannotBorrow = new Audio("222",position,
				"French audio", "Nguyen", "2010", type,
				"Audio");
	}

	@After
	public void tearDown() throws Exception {
		canBorrow = null;
		cannotBorrow = null;
		position = null;
		type = null;
	}

	/**
	 * Test {@link Document#getTitre()}
	 */
	@Test
	public void testGetTitle() {
		assertEquals("French fast audio", canBorrow.getTitre());
		assertEquals("French audio", cannotBorrow.getTitre());
	}
	
		
	/**
	 * Test {@link Document#getCode()}
	 */
	@Test
	public void testgetCode() {
		assertEquals("111", canBorrow.getCode());
		assertEquals("222", cannotBorrow.getCode());
	}
	
	
	/**
	 * Test {@link Audio#dureeEmprunt()}. 
	 * For each Audio, we can borrow 28 days.
	 */
	@Test
	public void testdureeEmprunt() {
		assertEquals(28, canBorrow.dureeEmprunt());
		assertEquals(28, cannotBorrow.dureeEmprunt());
	}
	
	/**
	 * Test {@link Audio#tarifEmprunt()}
	 */
	@Test
	public void testtarifEmprunt() {
		assertEquals(1.0, canBorrow.tarifEmprunt(),0);
		assertEquals(1.0, cannotBorrow.tarifEmprunt(),0);
	}
}
