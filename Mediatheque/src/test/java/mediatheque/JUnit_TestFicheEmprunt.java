package mediatheque;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mediatheque.client.Client;
import mediatheque.document.Document;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Datutil;
import util.InvariantBroken;

/**
 * @author mainguyen
 * Test class {@link FicheEmprunt}.
 */
public class JUnit_TestFicheEmprunt {
	FicheEmprunt fiche;
	Mediatheque media;	
	Client cli;
	Document doc;
	String nom = "Nguyen";
	String pre = "Mai";
	String code = "11111";
	
	@Before
	public void setUp() throws Exception {		
		media = new Mediatheque("TestMediatheque");
		cli = media.chercherClient(nom, pre);
		doc = media.chercherDocument(code);
		doc.metEmpruntable();
		fiche = new FicheEmprunt(media, cli,doc);
	}

	@After
	public void tearDown() throws Exception {
		media = null;
		fiche = null;
		cli = null;
		doc = null;
	}
	
	/**
	 * Test {@link FicheEmprunt#FicheEmprunt(Mediatheque, mediatheque.client.Client, mediatheque.document.Document)}
	 */
	@Test
	public void testConstructor() {
		assertTrue(fiche.correspond(cli, doc));
		assertTrue(doc.estEmprunte());
	}

	/**
	 * Test {@link FicheEmprunt#verifier()}
	 * @throws OperationImpossible 
	 */
	@Test
	public void testverifier() throws OperationImpossible{
		fiche.verifier();
	}
	
	/**
	 * Test {@link FicheEmprunt#modifierClient(Client)}.
	 * After modifying, the client of this now is the new client.
	 */
	@Test
	public void testModifierClient(){
		Client newClient = media.getClientAt(0);
		fiche.modifierClient(newClient);
		assertTrue(fiche.getClient().equals(newClient));
	}
	
	@Test
	public void testRestituer() throws InvariantBroken, OperationImpossible{
		int nbDocBorrw = fiche.getClient().getNbEmpruntsEnCours();
		fiche.restituer();
		assertFalse(fiche.getDocument().estEmprunte());
		assertEquals(nbDocBorrw - 1, fiche.getClient().getNbEmpruntsEnCours());
	}
	
	@Test
	public void testDate(){
		System.out.println("dateEmprunt: "+fiche.getDateEmprunt());
		assertTrue(Datutil.dateDuJour().equals(fiche.getDateEmprunt()));
		System.out.println("dateLimite: "+fiche.getDateLimite());
		System.out.println("depasse: "+fiche.getDepasse());
		System.out.println("Borrowing time: "+fiche.getDureeEmprunt());
		
	}
	
	/**
	 * Test {@link FicheEmprunt#changementCategorie()}
	 * @throws OperationImpossible
	 */
	@Test
	public void testChangementCategorie() throws OperationImpossible{
		assertFalse(fiche.changementCategorie());
	}
	
	/**
	 * Test {@link FicheEmprunt#toString()}.
	 * the result: "1111" par "Nguyen" le 31/05/13 pour le 07/12/18;
	 */
	@Test
	public void testToString(){
		System.out.print(fiche.toString());
	}
	
	/**
	 * Test {@link FicheEmprunt#afficherStatistiques()}.
	 * Expected: "Nombre total d'emprunts = 8"
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testafficherStatistiques(){
		fiche.afficherStatistiques();
	}
}
