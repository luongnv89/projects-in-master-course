package mediatheque;

import static org.junit.Assert.*;

import mediatheque.client.Client;
import mediatheque.document.Document;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author mainguyen
 * Test class {@link LettreRappel}.
 * Purpose is to see the result on the screen.
 */
public class JUnit_TestLettreRappel{
	LettreRappel lettre;
	
	Mediatheque media;
	FicheEmprunt fiche;
	Client cli;
	Document doc;
	String nom = "Nguyen";
	String pre = "Mai";
	String code = "11111";

	@Before
	public void setUp() throws Exception {
		media = new Mediatheque("TestMediatheque");
		cli = media.chercherClient(nom, pre);
		doc = media.chercherDocument(code);
		doc.metEmpruntable();
		fiche = new FicheEmprunt(media, cli,doc);
		lettre = new LettreRappel("Int mediatheque", fiche);
	}

	@After
	public void tearDown() throws Exception {
		media = null;
		fiche = null;
		cli = null;
		doc = null;
		lettre = null;
	}

	@Test
	public void testConstructor() {
		assertTrue(lettre.getDateRappel().equals(fiche.getDateLimite()));
	}

	/**
	 * Test {@link LettreRappel#relancer()}.
	 * Just to see the format will display in the screen.
	 */
	@Test
	public void testRelancer(){
		lettre.relancer();
	}
}
