package mediatheque;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author mainguyen <br>
 *Test {@link Genre} class. Check methods in the class works correctly.
 */
public class JUnit_TestGenre {

	Genre book;
	Genre video;
	
	@Before
	public void setUp() throws Exception {
		book = new Genre("Book");
		video = new Genre("");
	}

	@After
	public void tearDown() throws Exception {
		book = null;
		video = null;
	}

	/**
	 * Test {@link Genre#Genre(String)}
	 */
	@Test
	public void testConstructor() {
		assertEquals( book.getNom(), "Book");
		assertEquals(book.getNbEmprunts(), 0);
		
		assertEquals( video.getNom(), "");
		assertEquals(video.getNbEmprunts(), 0);
	}
	
	/**
	 * Test {@link Genre#toString()}.
	 * The result should look like:
	 * "Genre: "+ Book +", nbemprunts:"+ 0
	 */
	@Test
	public void testToString(){
		assertEquals("Genre: "+ book.getNom()+", nbemprunts:"+book.getNbEmprunts(), book.toString());
	}

	/**
	 * Test {@link Genre#emprunter()} with <code>book</code>
	 */
	@Test
	public void testEmprunter(){
		int nbemprunts = book.getNbEmprunts();
		book.emprunter();
		assertEquals(nbemprunts+1, book.getNbEmprunts());
	}
	
	/**
	 * Test {@link Genre#modifier(String)}. Modify video to audio
	 */
	@Test
	public void testModifier(){
		String audio = "Audio";
		video.modifier(audio);
		assertEquals(audio, video.getNom());
	}
	
	/**
	 * Test {@link Genre#equals(Object)}
	 */
	@Test
	public void testEquals(){
		Genre audio = new Genre("Audio");
		
		assertTrue(book.equals(book));
		assertFalse(book.equals(audio));
		audio.emprunter();
		assertFalse(book.equals(audio));
		assertFalse(book.equals(video));
		assertFalse(video.equals(book));
	}
}
