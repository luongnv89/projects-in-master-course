package mediatheque;

import static org.junit.Assert.*;

import mediatheque.client.CategorieClient;
import mediatheque.client.Client;
import mediatheque.document.Document;
import mediatheque.document.Livre;
import mediatheque.document.Video;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.InvariantBroken;

/**
 * @author mainguyen
 * Test class {@link Mediatheque}. Test methods in the class run correctly.
 */
public class JUnit_TestMediatheque {

	Mediatheque mediathequeEmpty;
	Mediatheque mediathequeNotEmpty;

	@Before
	public void setUp() throws Exception {
		mediathequeEmpty = new Mediatheque("TestMediatheque");
		mediathequeNotEmpty = new Mediatheque("mediaTest");
	}

	@After
	public void tearDown() throws Exception {
		mediathequeEmpty = null;
		mediathequeNotEmpty = null;
	}

	/**
	 * Test {@link Mediatheque#metConsultable(String)}. Initialize an empty
	 * library
	 */
	@Test
	public void testConstructor() {
		assertEquals("TestMediatheque", mediathequeEmpty.getNom());
		assertEquals("mediaTest", mediathequeNotEmpty.getNom());
	}

	/**
	 * Test {@link Mediatheque#empty()} class. After calling
	 * <code>empty()</code>, all data of this object are empty.
	 */
	@Test
	public void testEmpty() {
		mediathequeEmpty.empty();
		assertEquals(0, mediathequeEmpty.getCategoriesSize());
		assertEquals(0, mediathequeEmpty.getClientsSize());
		assertEquals(0, mediathequeEmpty.getDocumentsSize());
		assertEquals(0, mediathequeEmpty.getFicheEmpruntsSize());
		assertEquals(0, mediathequeEmpty.getGenresSize());
		assertEquals(0, mediathequeEmpty.getLocalisationsSize());
	}

	/**
	 * Test {@link Mediatheque#ajouterGenre(String)}. <br>
	 * OK when adds into <code>mediathequeEmpty</code> <li>Book <li>Video <li>
	 * Audio And saves to file Which are not in the list yet.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testAjouterGenreOK() throws OperationImpossible {
		mediathequeEmpty.empty();
		mediathequeEmpty.ajouterGenre("Book");
		mediathequeEmpty.ajouterGenre("Video");
		mediathequeEmpty.ajouterGenre("Audio");
		assertEquals(3, mediathequeEmpty.getGenresSize());
		mediathequeEmpty.saveToFile();
	}

	/**
	 * Test {@link Mediatheque#ajouterGenre(String)}. <br>
	 * NOT OK. Because we expect an Exception when add Genre which is already in
	 * the list.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testAjouterGenreKO() throws OperationImpossible {
		mediathequeEmpty.ajouterGenre("Book");
	}

	/**
	 * Test {@link Mediatheque#chercherGenre(String)} with
	 * <code>mediathequeEmpty</code>. Search a genre which is in library. Search
	 * a genre which is Not in the library
	 */
	@Test
	public void testChercherGenre() {
		String search1 = "Book";
		Genre gBook = new Genre(search1);
		assertTrue(mediathequeEmpty.chercherGenre(search1).equals(gBook));

		String search2 = "Livre";
		assertEquals(null, mediathequeEmpty.chercherGenre(search2));
	}

	/**
	 * Test {@link Mediatheque#modifierGenre(String, String)}.<br>
	 * OK, we remove Genre exists.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testModifierGenreOK() throws OperationImpossible {
		String new1 = "Livre";
		String old1 = "Book";

		mediathequeEmpty.modifierGenre(old1, new1);
		assertEquals(null, mediathequeEmpty.chercherGenre(old1));
		assertTrue(new Genre(new1).equals(mediathequeEmpty.chercherGenre(new1)));
	}

	/**
	 * Test {@link Mediatheque#modifierGenre(String, String)}.<br>
	 * NOT OK, we cannot remove Genre which does NOT exist.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testModifierGenreKO() throws OperationImpossible {
		String new2 = "Ordinateur";
		String old2 = "Computer";
		mediathequeEmpty.modifierGenre(old2, new2);
	}

	/**
	 * Test {@link Mediatheque#supprimerGenre(String)}.<br>
	 * OK when removes Genre which is in the list of Genre.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testSupprimerGenreOK() throws OperationImpossible {
		String remove = "Audio";
		mediathequeEmpty.supprimerGenre(remove);
		assertEquals(null, mediathequeEmpty.chercherGenre(remove));
		assertEquals(2, mediathequeEmpty.getGenresSize());

	}

	/**
	 * Test {@link Mediatheque#supprimerGenre(String)}.<br>
	 * NOT OK when removes Genre which is NOT in the list of Genre.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testSupprimerGenreKO() throws OperationImpossible {
		String remove = "Livre";
		mediathequeEmpty.supprimerGenre(remove);

	}

	/**
	 * Test {@link Mediatheque#listerGenres()} with
	 * <code>mediathequeEmpty</code>. List all Genres inside the list:<br>
	 * Genre: Book, nbemprunts:0<br>
	 * Genre: Video, nbemprunts:0<br>
	 * Genre: Audio, nbemprunts:0<br>
	 */
	@Test
	public void testListerGenres() {
		System.out.println("\nTest --- list of Genre!");
		mediathequeEmpty.listerGenres();
	}

	/**
	 * Test {@link Mediatheque#getGenreAt(int)} with
	 * <code>mediathequeEmpty</code>. get Genre at 0 is "Book".
	 */
	@Test
	public void testGetGenreAt() {
		assertTrue(new Genre("Book").equals(mediathequeEmpty.getGenreAt(0)));
	}

	/**
	 * Test {@link Mediatheque#getGenresSize()} with
	 * <code>mediathequeEmpty</code>. The current size of Genre is 3.
	 */
	@Test
	public void testGetGenresSize() {
		assertEquals(3, mediathequeEmpty.getGenresSize());
	}

	/**
	 * Test {@link Mediatheque#ajouterLocalisation(String, String)} with
	 * <code>mediathequeEmpty</code>.<br>
	 * OK with a Location is NOT in the list of Location.
	 * 
	 * @throws OperationImpossible
	 * 
	 */
	@Test
	public void testAjouterLocalisationOK() throws OperationImpossible {
		int locSize = mediathequeEmpty.getLocalisationsSize();
		mediathequeEmpty.ajouterLocalisation("Economic", "Trend");
		mediathequeEmpty.ajouterLocalisation("Languages", "English");
		assertEquals(locSize + 2, mediathequeEmpty.getLocalisationsSize());
		mediathequeEmpty.saveToFile();

	}

	/**
	 * Test {@link Mediatheque#ajouterLocalisation(String, String)} with
	 * <code>mediathequeEmpty</code>.<br>
	 * NOT OK with a Location already exists.
	 * 
	 * @throws OperationImpossible
	 * 
	 */
	@Test(expected = OperationImpossible.class)
	public void testAjouterLocalisationKO() throws OperationImpossible {
		mediathequeEmpty.ajouterLocalisation("Economic", "Trend");
	}

	/**
	 * Test {@link Mediatheque#chercherLocalisation(String, String)}. Search a
	 * Localisition is in the list and one is not in the list.
	 */
	@Test
	public void testChercherLocalisation() {
		String salle1 = "Economic";
		String rayon1 = "Trend";
		assertTrue(new Localisation(salle1, rayon1).equals(mediathequeEmpty
				.chercherLocalisation(salle1, rayon1)));

		String rayon2 = "Marketing";
		assertEquals(null,
				mediathequeEmpty.chercherLocalisation(salle1, rayon2));
	}

	/**
	 * Test
	 * {@link Mediatheque#modifierLocalisation(Localisation, String, String)}.<br>
	 * OK with a Location exists in the list.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testModifierLocalisation() throws OperationImpossible {
		Localisation loc = new Localisation("Economic", "Trend");
		String salle = "Economic";
		String modifiedRayon = "Marketing";
		mediathequeEmpty.modifierLocalisation(loc, salle, modifiedRayon);
		assertEquals(null,
				mediathequeEmpty.chercherLocalisation("Economic", "Trend"));
		assertTrue(new Localisation(salle, modifiedRayon)
				.equals(mediathequeEmpty.chercherLocalisation(salle,
						modifiedRayon)));
	}

	/**
	 * Test
	 * {@link Mediatheque#modifierLocalisation(Localisation, String, String)}.<br>
	 * NOT OK with a Location NOT exist in the list.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testModifierLocalisationKO() throws OperationImpossible {
		Localisation loc = new Localisation("Economic", "Marketing");
		String salle = "Economic";
		String modifiedRayon = "Finance";
		mediathequeEmpty.modifierLocalisation(loc, salle, modifiedRayon);
	}

	/**
	 * Test {@link Mediatheque#supprimerLocalisation(String, String)} with
	 * <code>mediathequeEmpty</code>.<br>
	 * OK with a Location exists in the list. After removing, we no longer
	 * search for it.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testSupprimerLocalisationOK() throws OperationImpossible {
		String rvSalle = "Economic";
		String rvRayon = "Trend";
		int locSize = mediathequeEmpty.getLocalisationsSize();
		mediathequeEmpty.supprimerLocalisation(rvSalle, rvRayon);
		assertEquals(null,
				mediathequeEmpty.chercherLocalisation(rvSalle, rvRayon));
		assertEquals(locSize - 1, mediathequeEmpty.getLocalisationsSize());
	}

	/**
	 * Test {@link Mediatheque#supprimerLocalisation(String, String)} with
	 * <code>mediathequeEmpty</code> <br>
	 * NOT OK because we remove a Location which does NOT exist.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testSupprimerLocalisationKO() throws OperationImpossible {
		String rvSalle = "Economic";
		String rvRayon = "Marketing";
		mediathequeEmpty.supprimerLocalisation(rvSalle, rvRayon);
	}

	/**
	 * Test {@link Mediatheque#listerLocalisations()} with
	 * <code>mediathequeEmpty</code>. The result should be:<br>
	 * Mediatheque INTMediatheque listage des localisations au 30/05/13<br>
	 * Salle/Rayon : Economic/Marketing<br>
	 * Salle/Rayon : Economic/Finance<br>
	 * Salle/Rayon : Technology/Web<br>
	 * Salle/Rayon : Technology/Java<br>
	 * Salle/Rayon : Languages/English<br>
	 * Salle/Rayon : Languages/French<br>
	 */
	@Test
	public void testListerLocalisations() {
		System.out.println("\nTest --- list of localisition!");
		mediathequeEmpty.listerLocalisations();
	}

	/**
	 * Test {@link Mediatheque#getLocalisationAt(int)}. The first localisition
	 * in <code>mediathequeEmpty</code> is Economic/Marketing.
	 */
	@Test
	public void testGetLocalisationAt() {
		Localisation loc = new Localisation("Economic", "Trend");
		assertEquals(loc, mediathequeEmpty.getLocalisationAt(0));
	}

	/**
	 * Test
	 * {@link Mediatheque#ajouterCatClient(String, int, double, double, double, boolean)}
	 * . Adds new categories of client in <code>mediathequeEmpty</code>:<br>
	 * Normal <br>
	 * Discount <br>
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testAjouterCatClientOK() throws OperationImpossible {
		mediathequeEmpty.ajouterCatClient("Normal", 2, 1, 48, 1, false);
		mediathequeEmpty.ajouterCatClient("Discount", 2, 1, 28, 0.5, true);
		mediathequeEmpty.saveToFile();
		assertEquals(2, mediathequeEmpty.getCategoriesSize());
	}

	/**
	 * Test
	 * {@link Mediatheque#ajouterCatClient(String, int, double, double, double, boolean)}
	 * NOT OK. Adds a category client "Normal" already exists.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testAjouterCatClientKO() throws OperationImpossible {
		mediathequeEmpty.ajouterCatClient("Normal", 2, 1, 48, 1, false);
	}

	/**
	 * Test {@link Mediatheque#chercherCatClient(String)}. Search for Normal
	 * which is in the list. And search for Subscribe which is not in the list.
	 */
	@Test
	public void testChercherCatClient() {
		String chCatClient1 = "Discount";
		String chCatClient2 = "Subscribe";
		assertTrue(mediathequeEmpty.getCategorieAt(1).equals(
				mediathequeEmpty.chercherCatClient(chCatClient1)));
		assertEquals(null, mediathequeEmpty.chercherCatClient(chCatClient2));
	}

	/**
	 * Test {@link Mediatheque#supprimerCatClient(String)} is OK. Because the
	 * removed category client is existed.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testSupprimerCatClientOK() throws OperationImpossible {
		String rvCatClient1 = "Discount";
		mediathequeEmpty.supprimerCatClient(rvCatClient1);
		assertEquals(null, mediathequeEmpty.chercherCatClient(rvCatClient1));
	}

	/**
	 * Test {@link Mediatheque#supprimerCatClient(String)} is not OK. Because
	 * the removed category client is NOT existed.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testSupprimerCatClientKO() throws OperationImpossible {
		String rvCatClient2 = "Subscribe";
		mediathequeEmpty.supprimerCatClient(rvCatClient2);
	}

	/**
	 * Test
	 * {@link Mediatheque#modifierCatClient(CategorieClient, String, int, double, double, double, boolean)}
	 * . The modified category client is in the list.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testmodifierCatClientOK() throws OperationImpossible {
		CategorieClient co = new CategorieClient("Normal", 2, 1, 48, 1, false);
		mediathequeEmpty.modifierCatClient(co, "Discount", 1, 2, 28, 0.5, true);
		assertEquals("Discount", co.getNom());
		assertTrue(co.getCodeReducUtilise());

	}

	/**
	 * Test
	 * {@link Mediatheque#modifierCatClient(CategorieClient, String, int, double, double, double, boolean)}
	 * . The modified category client is Not in the list.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testmodifierCatClientKO() throws OperationImpossible {
		CategorieClient co = new CategorieClient("Subscribe", 2, 1, 48, 1,
				false);
		mediathequeEmpty.modifierCatClient(co, "Discount", 1, 2, 28, 0.5, true);

	}

	/**
	 * Test {@link Mediatheque#listerCatsClient()}
	 */
	@Test
	public void testListerCatsClient() {
		System.out.println("\nTest --- list of category of client!");
		mediathequeEmpty.listerCatsClient();
	}

	/**
	 * Test {@link Mediatheque#getCategorieAt(int)}
	 */
	@Test
	public void testGetCategorieAt() {
		assertNull(mediathequeEmpty.getCategorieAt(3));
	}

	/**
	 * Test {@link Mediatheque#ajouterDocument(mediatheque.document.Document)}
	 * The added document is Not in the list
	 * 
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test
	public void testAjouterDocumentOK() throws OperationImpossible,
			InvariantBroken {
		int docSize = mediathequeEmpty.getDocumentsSize();
		Document doc = new Livre("11111", new Localisation("Economic", "Trend"),
				"Grobal economy, 3 edition", "Ammel", "2009",
				new Genre("Book"), 245);
		Document video = new Video("00001", new Localisation("Economic", "Trend"),
				"Grobal economy, 3 edition", "Ammel", "2009",
				new Genre("Video"), 45, "nxb");
		mediathequeEmpty.ajouterDocument(doc);
		mediathequeEmpty.ajouterDocument(video);
		mediathequeEmpty.saveToFile();
		assertEquals(docSize + 2, mediathequeEmpty.getDocumentsSize());
	}

	/**
	 * Test {@link Mediatheque#ajouterDocument(mediatheque.document.Document)}
	 * Not OK when adds a document is already existed in the list
	 * 
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testAjouterDocumentKO() throws OperationImpossible,
			InvariantBroken {
		Document doc = new Livre("11111", new Localisation("Economic", "Trend"),
				"Grobal economy, 3 edition", "Ammel", "2009",
				new Genre("Book"), 245);
		mediathequeEmpty.ajouterDocument(doc);
	}

	/**
	 * Test {@link Mediatheque#chercherDocument(String)}
	 */
	@Test
	public void testChercherDocument() {
		String docCode1 = "11111";
		String docCode2 = "111";
		assertNotNull(mediathequeEmpty.chercherDocument(docCode1));
		assertEquals(null, mediathequeEmpty.chercherDocument(docCode2));

	}

	/**
	 * Test {@link Mediatheque#retirerDocument(String)}. The tested document is
	 * in the list.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testRetirerDocumentOK() throws OperationImpossible {
		String rmDoc = "00001";
		mediathequeEmpty.retirerDocument(rmDoc);
		assertNull(mediathequeEmpty.chercherDocument(rmDoc));
	}

	/**
	 * Test {@link Mediatheque#retirerDocument(String)} The tested document is
	 * NOT in the list.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testRetirerDocumentKO() throws OperationImpossible {
		String rmDoc = "2222";
		mediathequeEmpty.retirerDocument(rmDoc);
	}

	/**
	 * Test {@link Mediatheque#listerDocuments()}. Shows all Documents are
	 * recently in the list
	 */
	@Test
	public void testlisterDocuments() {
		System.out.println("\nTest --- list of documents!");
		mediathequeEmpty.listerDocuments();
	}

	/**
	 * Test {@link Mediatheque#metEmpruntable(String)}. The document is
	 * available in the list.
	 * 
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test
	public void testmetEmpruntableOK() throws OperationImpossible,
			InvariantBroken {
		String code = "11111";
		Document doc = mediathequeEmpty.chercherDocument(code);
		mediathequeEmpty.metEmpruntable(code);
		assertTrue(doc.estEmpruntable());
	}

	/**
	 * Test {@link Mediatheque#metEmpruntable(String)}. The document is NOT
	 * available in the list.
	 * 
	 * @throws InvariantBroken
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testmetEmpruntableKO() throws OperationImpossible,
			InvariantBroken {
		String code = "2222";
		mediathequeEmpty.metEmpruntable(code);
	}

	/**
	 * Test {@link Mediatheque#metConsultable(String)}.<br>
	 * OK with a Document can currently borrow.
	 * 
	 * @throws OperationImpossible
	 * @throws InvariantBroken
	 */
	@Test
	public void testmetConsultableOK() throws OperationImpossible,
			InvariantBroken {
		String code = "11111";
		Document doc = mediathequeEmpty.chercherDocument(code);
		mediathequeEmpty.metEmpruntable(code);
		mediathequeEmpty.metConsultable(code);
		assertFalse(doc.estEmpruntable());
	}

	/**
	 * Test {@link Mediatheque#metConsultable(String)}.<br>
	 * NOT OK with a Document is already not allowed to borrow.
	 * 
	 * @throws OperationImpossible
	 * @throws InvariantBroken
	 */
	@Test(expected = OperationImpossible.class)
	public void testmetConsultableKO1() throws OperationImpossible,
			InvariantBroken {
		String code = "1111";
		mediathequeEmpty.metConsultable(code);
	}

	/**
	 * Test {@link Mediatheque#metConsultable(String)}.<br>
	 * NOT OK with a Document doesn't exist.
	 * 
	 * @throws OperationImpossible
	 * @throws InvariantBroken
	 */
	@Test(expected = OperationImpossible.class)
	public void testmetConsultableKO2() throws OperationImpossible,
			InvariantBroken {
		String code = "2222";
		mediathequeEmpty.metConsultable(code);
	}

	/**
	 * Test {@link Mediatheque#getDocumentAt(int)}. There is documents in the
	 * list. So that we can get at the index = 0. But we can not get a document
	 * at the index > the size of the list.
	 */
	@Test
	public void testGetDocumentAt() {
		assertNotNull(mediathequeEmpty.getCategorieAt(0));
		assertNull(mediathequeEmpty.getCategorieAt(mediathequeEmpty
				.getDocumentsSize() + 1));
	}

	/**
	 * Test {@link Mediatheque#inscrire(String, String, String, String)} and
	 * {@link Mediatheque#inscrire(String, String, String, String, int)}.<br>
	 * OK when adds clients are not in the list of Client yet.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testInscrireOK() throws OperationImpossible {
		String catClient1 = "Normal";
		String catClient2 = "Discount";
		int sizeClient = mediathequeEmpty.getClientsSize();
		mediathequeEmpty.inscrire("Nguyen", "Mai", "Evry", catClient1);
		mediathequeEmpty.inscrire("Nguyen", "Luong", "Evry", catClient2, 1000);
		mediathequeEmpty.saveToFile();
		assertEquals(sizeClient + 2, mediathequeEmpty.getClientsSize());

	}

	/**
	 * Test {@link Mediatheque#inscrire(String, String, String, String)} and
	 * {@link Mediatheque#inscrire(String, String, String, String, int)}.<br>
	 * NOT OK when add clients are already in the list. 
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testInscrireKO1() throws OperationImpossible {
		String catClient1 = "Subscribe";
		mediathequeEmpty.inscrire("Nguyen", "Mai", "Evry", catClient1);

	}
	
	/**
	 * Test {@link Mediatheque#inscrire(String, String, String, String)} and
	 * {@link Mediatheque#inscrire(String, String, String, String, int)}.<br>
	 * NOT OK when add clients which the category of
	 * client does not exist.
	 * 
	 * @throws OperationImpossible
	 */
	@Test(expected = OperationImpossible.class)
	public void testInscrireKO2() throws OperationImpossible {
		String catClient2 = "Subscribe";
		mediathequeEmpty.inscrire("Nguyen", "Van Luong", "Evry", catClient2, 1000);

	}

	/**
	 * Test {@link Mediatheque#resilier(String, String)} OK. Remove successfully
	 * a client who is in the list. After removing, we cannot search the client
	 * any more.
	 * 
	 * @throws OperationImpossible
	 * 
	 */
	@Test
	public void testResilierOK() throws OperationImpossible {
		String nom = "Nguyen";
		String pre = "Mai";
		mediathequeEmpty.resilier(nom, pre);
		assertNull(mediathequeEmpty.chercherClient(nom, pre));
	}

	/**
	 * Test {@link Mediatheque#resilier(String, String)} OK. Remove fail a
	 * client who is NOT in the list. Expected an exception.
	 * 
	 * @throws OperationImpossible
	 * 
	 */
	@Test(expected = OperationImpossible.class)
	public void testResilierKO() throws OperationImpossible {
		String nom = "Ngo";
		String pre = "Mai";
		mediathequeEmpty.resilier(nom, pre);
	}

	/**
	 * Test
	 * {@link Mediatheque#modifierClient(Client, String, String, String, String, int)}.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testModifierClient() throws OperationImpossible {
		String oldnom = "Nguyen";
		String oldpre = "Mai";
		String newnom = "Nguyen";
		String newpre = "Thi Mai";
		String newcat = "Discount";
		String newadd = "Paris";
		Client cl = mediathequeEmpty.chercherClient(oldnom, oldpre);
		mediathequeEmpty.modifierClient(cl, newnom, newpre, newadd, newcat,
				2000);
		assertEquals(newnom, cl.getNom());
		assertTrue(cl.getCategorie().equals(
				mediathequeEmpty.chercherCatClient(newcat)));
	}

	/**
	 * Test {@link Mediatheque#changerCategorie(String, String, String, int)}.<br>
	 * OK when change cat of a client who is in the list to another cat which exists too.
	 * @throws OperationImpossible
	 */
	@Test
	public void testChangerCategorieOK() throws OperationImpossible {
		String nom = "Nguyen";
		String pre = "Mai";
		String newcat = "Discount";
		mediathequeEmpty.changerCategorie(nom, pre, newcat, 2000);
		assertTrue(mediathequeEmpty.chercherClient(nom, pre).getCategorie()
				.equals(mediathequeEmpty.chercherCatClient(newcat)));
	}
	
	/**
	 * Test {@link Mediatheque#changerCategorie(String, String, String, int)}.<br>
	 * NOT OK when change cat of a client who NOT is in the list.
	 * @throws OperationImpossible
	 */
	@Test(expected=OperationImpossible.class)
	public void testChangerCategorieKO1() throws OperationImpossible {
		String nom = "Nguyen";
		String pre1 = "Thi Mai";
		String newcat = "Discount";
		mediathequeEmpty.changerCategorie(nom, pre1, newcat, 2000);
	}
	
	/**
	 * Test {@link Mediatheque#changerCategorie(String, String, String, int)}.<br>
	 * NOT OK when change to another cat which Not exist.
	 * @throws OperationImpossible
	 */
	@Test(expected=OperationImpossible.class)
	public void testChangerCategorieKO2() throws OperationImpossible {
		String nom = "Nguyen";
		String pre2 = "Mai";
		String newcat = "Subscribe";
		mediathequeEmpty.changerCategorie(nom, pre2, newcat, 2000);
	}
	
	/**
	 * Test {@link Mediatheque#changerCodeReduction(String, String, int)}.<br>
	 * OK when change code of a existing client who has the code reduce.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testChangerCodeReductionOK() throws OperationImpossible{
		String nom = "Nguyen";
		String pre = "Luong";
		mediathequeEmpty.changerCodeReduction(nom, pre, 2000);
		
	}
	
	/**
	 * Test {@link Mediatheque#changerCodeReduction(String, String, int)}.<br>
	 * NOT OK when change a client doesn't have the code reduce.
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testChangerCodeReductionKO1() throws OperationImpossible{
		String nom = "Nguyen";
		String pre1 = "Mai";
		mediathequeEmpty.changerCodeReduction(nom, pre1, 2000);
		
	}
	
	/**
	 * Test {@link Mediatheque#changerCodeReduction(String, String, int)}.<br>
	 * NOT OK when change code of a client who Not exist.
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testChangerCodeReductionKO2() throws OperationImpossible{
		String nom = "Nguyen";
		String pre2 = "Van Luong";
		mediathequeEmpty.changerCodeReduction(nom, pre2, 2000);
		
	}
	
	/**
	 * Test {@link Mediatheque#chercherClient(String, String)}. And {@link Mediatheque#findClient(String, String)}.
	 * test a client exists, return an object.
	 * test a client not exist, return null.
	 */
	@Test
	public void testChercherClient(){
		assertNotNull(mediathequeEmpty.chercherClient("Nguyen", "Mai"));
		assertNull(mediathequeEmpty.chercherClient("Nguyen", "Thi Mai"));
		assertNotNull(mediathequeEmpty.findClient("Nguyen", "Mai"));
		assertNull(mediathequeEmpty.findClient("Nguyen", "Thi Mai"));
	}
	
	/**
	 * Test {@link Mediatheque#listerClients()}
	 */
	@Test
	public void testlisterClients(){
		System.out.println("\nTest --- list of client!");
		mediathequeEmpty.listerClients();
	}
	
	/**
	 * Test {@link Mediatheque#existeClient(CategorieClient)}.
	 * test a existing clients with the cat, return true.
	 * test no client has the cat, return false.
	 */
	@Test
	public void testexisteClient(){
		String catOK = "Normal";
		String catKO = "Subscribe";
		assertTrue(mediathequeEmpty.existeClient(mediathequeEmpty.chercherCatClient(catOK)));
		assertFalse(mediathequeEmpty.existeClient(mediathequeEmpty.chercherCatClient(catKO)));
	}
	
	/**
	 * Test {@link Mediatheque#getClientAt(int)}.
	 * We know the client with the index = 1 has nom=Nguyen and pre=Mai.
	 * Test get the client the index = 1.
	 */
	@Test
	public void testGetClientAt(){
		Client cl = mediathequeEmpty.chercherClient("Nguyen", "Mai");
		assertTrue(cl.equals(mediathequeEmpty.getClientAt(1)));
	}
	
	/**
	 * Test {@link Mediatheque#emprunter(String, String, String)}.<br>
	 * OK when allows a client borrows a document which can be borrowed.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testEmprunterOK() throws OperationImpossible, InvariantBroken{
		String nom = "Nguyen";
		String pre = "Mai";
		String code = "11111";
		mediathequeEmpty.chercherDocument(code).metEmpruntable();
		mediathequeEmpty.emprunter(nom, pre, code);
		//mediathequeEmpty.saveToFile();
	}
	
	/**
	 * Test {@link Mediatheque#emprunter(String, String, String)}.<br>
	 * Not OK when a non-existing client borrows a document.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testEmprunterKO1() throws OperationImpossible, InvariantBroken{
		String nomNonExist = "Nguyen";
		String preNonExist = "Thi Mai";
		String codeOK = "00001";
		mediathequeEmpty.chercherDocument(codeOK).metEmpruntable();
		mediathequeEmpty.emprunter(nomNonExist, preNonExist, codeOK);
	}
	
	/**
	 * Test {@link Mediatheque#emprunter(String, String, String)}.<br>
	 * Not OK, a document not allowed to borrow.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testEmprunterKO2() throws OperationImpossible, InvariantBroken{
		String nom = "Nguyen";
		String pre = "Mai";
		String codeKO = "11111";
		mediathequeEmpty.emprunter(nom, pre, codeKO);
	}
	
	/**
	 * Test {@link Mediatheque#emprunter(String, String, String)}.<br>
	 * Not OK, borrow a document which are already borrowed.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testEmprunterKO3() throws OperationImpossible, InvariantBroken{
		String codeOK = "00001";
		String nom = "Nguyen";
		String pre = "Mai";
		mediathequeEmpty.emprunter(nom, pre, codeOK);
	}
	
	/**
	 * Test {@link Mediatheque#restituer(String, String, String)}.<br>
	 * OK, An existing client returns a document which he borrowed.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test
	public void testRestituerOK() throws OperationImpossible, InvariantBroken{
		String nom = "Nguyen";
		String pre = "Mai";
		String code = "11111";
//		mediathequeEmpty.emprunter(nom, pre, code);
		mediathequeEmpty.restituer(nom, pre, code);
	}
	
	/**
	 * Test {@link Mediatheque#restituer(String, String, String)}.<br>
	 * NOT OK, An existing client return a document which is not existing.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testRestituerKO1() throws OperationImpossible, InvariantBroken{
		String nom = "Nguyen";
		String pre = "Mai";
		String codeNonExist = "12345";
		mediathequeEmpty.restituer(nom, pre, codeNonExist);
	}
	
	/**
	 * Test {@link Mediatheque#restituer(String, String, String)}.<br>
	 * NOT OK, a client who is not existing.
	 * @throws InvariantBroken 
	 * @throws OperationImpossible 
	 */
	@Test(expected=OperationImpossible.class)
	public void testRestituerKO2() throws OperationImpossible, InvariantBroken{
		String nomNonExist = "Nguyen";
		String preNonExist = "Mai";
		String code = "00001";
		mediathequeEmpty.restituer(nomNonExist, preNonExist, code);
	}
	
	/**
	 * Test {@link Mediatheque#verifier()}
	 */
	@Test
	public void testVerifier(){
		mediathequeEmpty.verifier();
	}
	
	/**
	 * Test {@link Mediatheque#listerFicheEmprunts()}.
	 */
	@Test
	public void testListerFicheEmprunts(){
		System.out.println("\nTest --- list of borrowing!");
		mediathequeEmpty.listerFicheEmprunts();
	}
	
	@Test
	public void testgetFicheEmpruntAt(){
		assertNotNull(mediathequeEmpty.getFicheEmpruntAt(0));
	}
}
