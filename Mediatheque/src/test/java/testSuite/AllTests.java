package testSuite;


import mediatheque.JUnit_TestFicheEmprunt;
import mediatheque.JUnit_TestGenre;
import mediatheque.JUnit_TestLettreRappel;
import mediatheque.JUnit_TestLocalisation;
import mediatheque.JUnit_TestMediatheque;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import client.JUnit_TestCategorieClient;
import client.JUnit_TestClient;
import client.JUnit_TestHastClient;

import document.JUnit_TestAudio;
import document.JUnit_TestLivre;
import document.JUnit_TestVideo;

@RunWith(Suite.class)
@SuiteClasses({ JUnit_TestAudio.class, JUnit_TestLivre.class,
		JUnit_TestVideo.class, JUnit_TestClient.class,
		JUnit_TestCategorieClient.class, JUnit_TestHastClient.class,
		JUnit_TestGenre.class, JUnit_TestLocalisation.class,
		JUnit_TestMediatheque.class, JUnit_TestFicheEmprunt.class,
		JUnit_TestLettreRappel.class })
public class AllTests {

}

