package models;

import java.util.ArrayList;

import specifications.JugsSpecification;
import utils.MessageConsole;

public class Jugs implements JugsSpecification {

	protected final int NUMBER_OF_JUGS;

	protected int[] jugs;

	protected int[] jugSizes;

	//NGUYEN VAN LUONG - ADD
	protected int target = 0;

	/**
	 * List states of Solver which is traversed.
	 */
	protected ArrayList<String> listStates = new ArrayList<String>();

	protected ArrayList<Move> listMoves = new ArrayList<Move>();

	protected String finalState = "";

	// NGUYEN VAN LUONG - END
	
//	public static Counter counter = new Counter();


	public Jugs(int[] jugSizes) throws IllegalArgumentException {
//		counter.addMethodCalled("Jugs");
		if (jugSizes == null)
			throw (new IllegalArgumentException(
					"The jug sizes array argument must be initialised"));

		for (int jug = 0; jug < jugSizes.length; jug++)
			if (jugSizes[jug] <= 0)
				throw (new IllegalArgumentException(
						"The jug sizes must be positive integers"));

		this.NUMBER_OF_JUGS = jugSizes.length;
		jugs = new int[NUMBER_OF_JUGS];
		this.jugSizes = new int[NUMBER_OF_JUGS];

		System.arraycopy(jugSizes, 0, this.jugSizes, 0, NUMBER_OF_JUGS);
	}

	public void initialise(int jugToFill) throws IllegalArgumentException {
//		counter.addMethodCalled("initialise");
		if (jugToFill < 0 || jugToFill >= NUMBER_OF_JUGS)
			throw (new IllegalArgumentException("The jug indice is not valid"));

		for (int jug = 0; jug < NUMBER_OF_JUGS; jug++)
			jugs[jug] = 0;
		jugs[jugToFill] = jugSizes[jugToFill];
	}

	public int numberOfJugs() {
//		counter.addMethodCalled("numberOfJugs");
		return NUMBER_OF_JUGS;
	}

	public int amountInJug(int jugToMeasure) throws IllegalArgumentException {
//		counter.addMethodCalled("amountInJug");
		if (jugToMeasure < 0 || jugToMeasure >= NUMBER_OF_JUGS)
			throw (new IllegalArgumentException("The jug indice is not valid"));

		return jugs[jugToMeasure];
	}

	public int sizeOfJug(int jugToMeasure) throws IllegalArgumentException {
//		counter.addMethodCalled("sizeOfJug");
		if (jugToMeasure < 0 || jugToMeasure >= NUMBER_OF_JUGS)
			throw (new IllegalArgumentException("The jug indice is not valid"));

		return jugSizes[jugToMeasure];
	}

	public void moveBetweenJugs(int from, int to)
			throws IllegalArgumentException {
//		counter.addMethodCalled("moveBetweenJugs");
		if (from < 0 || from >= NUMBER_OF_JUGS || to < 0
				|| to >= NUMBER_OF_JUGS)
			throw (new IllegalArgumentException(
					"One of the jug indices is not valid"));

		int amountToMove = jugSizes[to] - jugs[to];

		if (amountToMove > jugs[from])
			amountToMove = jugs[from];

		jugs[from] = jugs[from] - amountToMove;
		jugs[to] = jugs[to] + amountToMove;

	}

	public boolean measures(int quantity) throws IllegalArgumentException {
//		counter.addMethodCalled("measures");
		if (quantity <= 0)
			throw (new IllegalArgumentException(
					"The quantity has to be a positive integer"));

		for (int jug = 0; jug < NUMBER_OF_JUGS; jug++)
			if (jugs[jug] == quantity)
				return true;

		return false;

	}

	public String toString() {
//		counter.addMethodCalled("toString");
		String str = "Number of Jugs = " + NUMBER_OF_JUGS;
		for (int jug = 0; jug < NUMBER_OF_JUGS; jug++)
			str = str + "\n Jug " + jug + " size = " + jugSizes[jug]
					+ " amount = " + jugs[jug];
		return str;
	}

	// Add more
	@Override
	public boolean solve(int target) throws IllegalArgumentException {
//		counter.addMethodCalled("solve");
		if (target < 0)
			throw (new IllegalArgumentException("The target value is invalid"));

		// Reset parameters
		if (this.target != target) {
			this.target = target;
			listStates.clear();
			listStates.add(stateToString());
		}
		// Cannot solve
		if (!thereIsASolution(target)) {
			MessageConsole.debugMessage("Cannot solve!");
			return false;
		}

		// Don't have to do anything
		if (measures(target)) {
			finalState = stateToString();
			return true;
		}
		// Create new states
		for (int i = 0; i < numberOfJugs(); i++) {

			// Create new states by change the contain of an non-empty jar
			if (amountInJug(i) > 0) {
				for (int j = 0; j < numberOfJugs(); j++) {
					if (j != i && (amountInJug(j) < sizeOfJug(j))) {
						int[] preState = arrayCopy(jugs);
						moveBetweenJugs(i, j);
						if (!listStates.contains(stateToString())) {
							listStates.add(stateToString());
							Move move = new Move(i, j);
							if (solve(target)) {
								listMoves.add(move);
								return true;
							}
						}
						jugs = arrayCopy(preState);
					}
				}
			}
		}
		return false;

	}

	/**
	 * @param jugs 
	 * @return
	 */
	protected int[] arrayCopy(int[] jugs) {
//		counter.addMethodCalled("arrayCopy");
		int[] initialState;
		initialState = new int[numberOfJugs()];
		for (int k = 0; k < numberOfJugs(); k++) {
			initialState[k] = jugs[k];
		}
		return initialState;
	}

	@Override
	public String findSolutionA(int target) throws IllegalArgumentException {
//		counter.addMethodCalled("findSolutionA");
		if (target < 0)
			throw (new IllegalArgumentException("The target value is invalid"));
		String solution = problemInfro();
		//Save initial state of problem
		int[] initialState;
		initialState = arrayCopy(jugs);

		//Find the solution
		listStates.clear();
		listMoves.clear();
		if (this.solve(target)) {
			if (listMoves.isEmpty()) {
				solution += "\nFound a solution!\nYou don't need make any move!";
			} else {
				solution += "\nFound a solution by the sequence of move: \n";
				for (int i = listMoves.size() - 1; i >= 0; i--) {
					solution += listMoves.get(i).toString() + "\n";
				}
			}
			solution += "\nFinal state will be: " + finalState;
		} else {
			solution += "\nCouldn't find the solution";
		}
		//Recuse the initial state
		for (int i = 0; i < numberOfJugs(); i++) {
			this.jugs[i] = initialState[i];
		}
		listStates.clear();
		listMoves.clear();
		return solution;
	}

	@Override
	public ArrayList<Move> findSolutionB(int target)
			throws IllegalArgumentException {
		
//		counter.addMethodCalled("findSolutionB");
		if (target < 0)
			throw (new IllegalArgumentException("The target value is invalid"));

		ArrayList<Move> listMoveOfSolution = new ArrayList<Move>();
		//Save initial state of problem
		int[] initialState;
		initialState = arrayCopy(jugs);

		//Find the solution
		listStates.clear();
		listMoves.clear();
		if (this.solve(target)) {
			if (!listMoves.isEmpty()) {
				for (int i = (listMoves.size() - 1); i >= 0; i--) {
					listMoveOfSolution.add(listMoves.get(i));
				}
			}
		}
		//Recuse the initial state
		for (int i = 0; i < numberOfJugs(); i++) {
			this.jugs[i] = initialState[i];
		}
		listStates.clear();
		listMoves.clear();
		return listMoveOfSolution;
	}

	@Override
	public String solveA(int target) throws IllegalArgumentException {
//		counter.addMethodCalled("solveA");
		if (target < 0)
			throw (new IllegalArgumentException("The target value is invalid"));
		String solution = problemInfro();

		//Find the solution
		listStates.clear();
		listMoves.clear();
		if (this.solve(target)) {
			if (listMoves.isEmpty()) {
				solution += "\nFound a solution!\nYou don't need make any move!";
			} else {
				solution += "\nFound a solution by the sequence of move: \n";
				for (int i = listMoves.size() - 1; i >= 0; i--) {
					solution += listMoves.get(i).toString() + "\n";
				}
			}
			solution += "\nFinal state: " + finalState;
		} else {
			solution += "\nCouldn't find the solution";
		}
		return solution;
	}

	@Override
	public ArrayList<Move> solveB(int target) throws IllegalArgumentException {
//		counter.addMethodCalled("solveB");
		if (target < 0)
			throw (new IllegalArgumentException("The target value is invalid"));
		ArrayList<Move> listMoveOfSolution = new ArrayList<Move>();

		//Find the solution
		listStates.clear();
		listMoves.clear();
		if (this.solve(target)) {
			if (!listMoves.isEmpty()) {
				for (int i = (listMoves.size() - 1); i >= 0; i--) {
					listMoveOfSolution.add(listMoves.get(i));
				}
			}
		}
		listMoves.clear();
		listMoves.addAll(listMoveOfSolution);
		return listMoves;
	}

	@Override
	public void doSequences(ArrayList<Move> sequences)
			throws IllegalArgumentException {
//		counter.addMethodCalled("doSequences");
		MessageConsole.setINFOR(true);
		MessageConsole.inforMessage(problemInfro());
		MessageConsole.inforMessage(stateToString());
		for (int i = 0; i < sequences.size(); i++) {
			Move move = sequences.get(i);
			MessageConsole.inforMessage(move.toString());
			moveBetweenJugs(move.from, move.to);
			MessageConsole.inforMessage(stateToString());
		}
		MessageConsole.setINFOR(false);

	}

	@Override
	public boolean thereIsASolution(int q) throws IllegalArgumentException {
//		counter.addMethodCalled("thereIsASolution");
		if (q < 0)
			throw (new IllegalArgumentException("The target value is invalid"));

		if (q > maxSizeOfJugs()) {
			MessageConsole
					.inforMessage("There isn't any Jug can contain target!");
			return false;
		}

		return true;
	}

	/**
	 * Save the state of problem to a String to cut the node which solver has visited.
	 * @return
	 */
	protected String stateToString() {
//		counter.addMethodCalled("stateToString");
		String state = "";
		for (int i = 0; i < numberOfJugs(); i++) {
			state = state + "[" + String.valueOf(amountInJug(i)) + "/"
					+ String.valueOf(sizeOfJug(i)) + "] ";
		}
		return state;
	}

	/**
	 * Get the max size of jug
	 * @return
	 */
	protected int maxSizeOfJugs() {
//		counter.addMethodCalled("maxSizeOfJugs");
		int max = sizeOfJug(0);
		for (int i = 1; i < numberOfJugs(); i++) {
			max = max > sizeOfJug(i) ? max : sizeOfJug(i);
		}
		return max;
	}

	/**
	 * Get the information of problem
	 * @return
	 */
	protected String problemInfro() {
//		counter.addMethodCalled("problemInfro");
		String infor = "\n\n*****Initial state: " + stateToString()
				+ "\nTarget : " + target;
		return infor;
	}
	//NGUYEN VAN LUONG - END
	public boolean invariant() {
//		counter.addMethodCalled("invariant");
		if (NUMBER_OF_JUGS <= 0)
			return false;
		if (jugs == null | jugSizes == null)
			return false;
		if (jugs.length != NUMBER_OF_JUGS)
			return false;
		if (jugSizes.length != NUMBER_OF_JUGS)
			return false;

		for (int jug = 0; jug < jugSizes.length; jug++)
			if (jugSizes[jug] <= 0)
				return false;

		for (int jug = 0; jug < jugs.length; jug++)
			if (jugs[jug] < 0)
				return false;

		for (int jug = 0; jug < jugs.length; jug++)
			if (jugs[jug] > jugSizes[jug])
				return false;

		return true;

	}

	public void sayHello(){
		System.out.println("HELLOOOOO!");
	}
}
