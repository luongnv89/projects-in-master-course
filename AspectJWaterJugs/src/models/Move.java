/**
 * 
 */
package models;

public class Move {
	int from;
	int to;

	public Move(int from, int to) {
		this.from = from;
		this.to = to;
	}

	public String toString() {
		if (from == -1) {
			return "Fill to jug: " + to;
		} else if (to == -1) {
			return "Empty jug: " + from;
		} else {
			return "Move from " + from + " to " + to;
		}
	}
}