package specifications;

public interface JugsWithHoseSpecification extends JugsSpecification {
	
	public void fillfromHose(int jug) throws IllegalArgumentException;;

}
