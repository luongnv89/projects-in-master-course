package utils;

/**
 * {@link MessageConsole} manage the message which will show on the console
 *
 */
public class MessageConsole {
	static boolean DEBUG = true;
	static boolean INFOR = true;
	static boolean ERROR = true;

	/**
	 * @param dEBUG the dEBUG to set
	 */
	public static void setDEBUG(boolean enableDebugMessage) {
		DEBUG = enableDebugMessage;
	}

	/**
	 * @param iNFOR the iNFOR to set
	 */
	public static void setINFOR(boolean enableInforMessage) {
		INFOR = enableInforMessage;
	}

	/**
	 * @param eXCEPTION the eXCEPTION to set
	 */
	public static void setEXCEPTION(boolean enableErrorMessage) {
		ERROR = enableErrorMessage;
	}

	public static void debugMessage(String debugMsg) {
		if (DEBUG) {
			System.out.println("[Debug_msg] " + debugMsg);
		}
	}

	public static void inforMessage(String inforMsg) {
		if (INFOR) {
			System.out.println("[Infor_msg] " + inforMsg);
		}
	}

	public static void errorMessage(String errorMsg) {
		if (ERROR) {
			System.out.println("[Error_msg] " + errorMsg);
		}
	}

}
