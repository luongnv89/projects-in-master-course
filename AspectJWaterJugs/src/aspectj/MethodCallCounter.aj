package aspectj;

import java.util.ArrayList;

/**
 * @author luongnv89
 * 
 */
public aspect MethodCallCounter extends MainPointCut{
	pointcut methodCallCounter():call(* *.*(..)) && !within(MethodCallCounter);

	

	ArrayList<MethodIndex> listMethods = new ArrayList<>();

	long startTime;
	int currentMethod;
	before(): methodCallCounter(){
		boolean exist = false;
		for (int i = 0; i < listMethods.size(); i++) {
			if (listMethods.get(i).getMethodName()
					.equals(thisJoinPoint.toString())) {
				exist = true;
				listMethods.get(i).increaseCounter();
				currentMethod = i;
				break;
			}
		}

		if (!exist) {
			listMethods.add(new MethodIndex(thisJoinPoint.toString()));
			currentMethod = listMethods.size()-1;
		}
		startTime = System.nanoTime();
		
	}

	after() returning: methodCallCounter(){
		listMethods.get(currentMethod).addExecutedTime(System.nanoTime()-startTime);
	}
	
	after() returning: mainMethodEnd(){
		System.out.println("Method counter: \n");
		for (int i = 0; i < listMethods.size(); i++) {
			System.out.println(listMethods.get(i).toString() + "\n");
		}
	}

}

