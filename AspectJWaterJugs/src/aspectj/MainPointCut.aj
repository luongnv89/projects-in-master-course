/**
 * 
 */
package aspectj;

import java.util.ArrayList;

/**
 * @author luongnv89
 *
 */
public abstract aspect MainPointCut {
	protected ArrayList<MethodIndex> listMethods = new ArrayList<>();
	pointcut mainMethodEnd():execution(* *.main(..));
	pointcut methodCallCounter():call(* *.*(..)) && !within(MethodCallCounter);
}
