/**
 * 
 */
package aspectj;

/**
 * @author luongnv89
 *
 */
class MethodIndex {
	String methodName;
	int counter = 0;
	long executedTime = 0;

	/**
	 * @param methodName
	 */

	public MethodIndex(String methodName) {
		this.methodName = methodName;
		this.counter = 1;
		executedTime = 0;
	}

	public void increaseCounter() {
		counter++;
	}
	
	public void addExecutedTime(long time){
		executedTime+=time;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return methodName + "is called: " + counter
				+ " times. Total executed time: " + this.executedTime+" nanoseconds";
	}

}
