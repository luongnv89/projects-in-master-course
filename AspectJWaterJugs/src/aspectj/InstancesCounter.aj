/**
 * 
 */
package aspectj;
import models.Jugs;

/**
 * @author luongnv89
 *
 */
public aspect InstancesCounter extends MainPointCut{
	pointcut constructorCall():call(public Jugs.new(..));
	
	static int numberOfJugs = 0;
	
	after() returning: constructorCall(){
		System.out.println("A Jugs object is created!");
		numberOfJugs++;
	}
	
	after() returning: mainMethodEnd(){
		System.out.println("Number of Jugs are created: " + numberOfJugs);
	}
}
