/**
 * 
 */
package aspectj;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import models.Jugs;

/**
 * @author luongnv89
 * 
 */
public aspect InvariantLog extends MainPointCut {
	pointcut checkInvariant(Jugs j) : target(j)&&call(* *.*(..))&&!(within(MainPointCut)||within(InstancesCounter)||within(MethodCallCounter)||within(InvariantLog));

	before(Jugs j) : checkInvariant(j){
		boolean invar = j.invariant();
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(
					"logs/invariantLogging.txt", true));
			out.write(thisJoinPoint.toString() + ": " + String.valueOf(invar)
					+ "\n");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
