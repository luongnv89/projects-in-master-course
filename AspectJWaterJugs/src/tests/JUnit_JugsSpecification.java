package tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import specifications.JugsSpecification;

public abstract class JUnit_JugsSpecification {

	JugsSpecification jugsUnderTest;

	/**
	 * Initialise the jugsUnderTest to 3 jugs with sizes 8 5 and 3
	 */
	@Before
	public abstract void setUp();

	@After
	public void tearDown() {

		jugsUnderTest = null;
	}

	@Test
	public void testConstructor() {

		Assert.assertTrue(jugsUnderTest.invariant());

		Assert.assertEquals(3, jugsUnderTest.numberOfJugs());

		Assert.assertEquals(8, jugsUnderTest.sizeOfJug(0));
		Assert.assertEquals(5, jugsUnderTest.sizeOfJug(1));
		Assert.assertEquals(3, jugsUnderTest.sizeOfJug(2));

	}

	@Test
	public void testnumberOfJugs() {

		Assert.assertEquals(3, jugsUnderTest.numberOfJugs());

	}

	@Test
	public void testsizeOfJug() {

		Assert.assertEquals(8, jugsUnderTest.sizeOfJug(0));
		Assert.assertEquals(5, jugsUnderTest.sizeOfJug(1));
		Assert.assertEquals(3, jugsUnderTest.sizeOfJug(2));

	}

	@Test
	public void testinitialise() {

		jugsUnderTest.initialise(0);
		Assert.assertEquals(jugsUnderTest.sizeOfJug(0),
				jugsUnderTest.amountInJug(0));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(1));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(2));
		Assert.assertTrue(jugsUnderTest.invariant());

		jugsUnderTest.initialise(1);
		Assert.assertEquals(jugsUnderTest.sizeOfJug(1),
				jugsUnderTest.amountInJug(1));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(0));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(2));
		Assert.assertTrue(jugsUnderTest.invariant());

		jugsUnderTest.initialise(2);
		Assert.assertEquals(jugsUnderTest.sizeOfJug(2),
				jugsUnderTest.amountInJug(2));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(0));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(1));
		Assert.assertTrue(jugsUnderTest.invariant());

	}

	@Test
	public void testamountInJug() {

		Assert.assertEquals(0, jugsUnderTest.amountInJug(0));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(1));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(2));

	}

	@Test
	public void testmeasures() {

		Assert.assertFalse(jugsUnderTest.measures(8));
		Assert.assertFalse(jugsUnderTest.measures(5));
		Assert.assertFalse(jugsUnderTest.measures(3));

		jugsUnderTest.initialise(0);
		Assert.assertTrue(jugsUnderTest.measures(8));
		Assert.assertFalse(jugsUnderTest.measures(5));
		Assert.assertFalse(jugsUnderTest.measures(3));

		jugsUnderTest.initialise(1);
		Assert.assertFalse(jugsUnderTest.measures(8));
		Assert.assertTrue(jugsUnderTest.measures(5));
		Assert.assertFalse(jugsUnderTest.measures(3));

		jugsUnderTest.initialise(2);
		Assert.assertFalse(jugsUnderTest.measures(8));
		Assert.assertFalse(jugsUnderTest.measures(5));
		Assert.assertTrue(jugsUnderTest.measures(3));
	}

	@Test
	public void testmoveBetweenJugs() {

		jugsUnderTest.initialise(0);

		// moving when from is empty changes nothing
		jugsUnderTest.moveBetweenJugs(1, 0);
		Assert.assertEquals(8, jugsUnderTest.amountInJug(0));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(1));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(2));
		Assert.assertTrue(jugsUnderTest.invariant());

		// fill up the to jug leaving some behind in from
		jugsUnderTest.moveBetweenJugs(0, 1);
		Assert.assertEquals(3, jugsUnderTest.amountInJug(0));
		Assert.assertEquals(5, jugsUnderTest.amountInJug(1));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(2));
		Assert.assertTrue(jugsUnderTest.invariant());

		// moving when to is full changes nothing
		jugsUnderTest.moveBetweenJugs(0, 1);
		Assert.assertEquals(3, jugsUnderTest.amountInJug(0));
		Assert.assertEquals(5, jugsUnderTest.amountInJug(1));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(2));
		Assert.assertTrue(jugsUnderTest.invariant());

		// fill up the to jug leaving some behind in from
		jugsUnderTest.moveBetweenJugs(1, 2);
		Assert.assertEquals(3, jugsUnderTest.amountInJug(0));
		Assert.assertEquals(2, jugsUnderTest.amountInJug(1));
		Assert.assertEquals(3, jugsUnderTest.amountInJug(2));

		// moving all out of from without filling to
		jugsUnderTest.moveBetweenJugs(2, 0);
		Assert.assertEquals(6, jugsUnderTest.amountInJug(0));
		Assert.assertEquals(2, jugsUnderTest.amountInJug(1));
		Assert.assertEquals(0, jugsUnderTest.amountInJug(2));
		Assert.assertTrue(jugsUnderTest.invariant());

	}

	/**
	 * Test for {@link jugsUnderTestSpecification#solveA(int)}
	 */
	@Test
	public void testSolve() {

		System.out.println("\n*****TEST FOR  solve()\n");

		jugsUnderTest.initialise(0);
		Assert.assertTrue(jugsUnderTest.solve(8));

		jugsUnderTest.initialise(1);
		Assert.assertTrue(jugsUnderTest.solve(5));

		jugsUnderTest.initialise(0);
		Assert.assertTrue(jugsUnderTest.solve(2));

	}

	/**
	 * Test for {@link JugsSpecification#findSolutionA(int)}
	 */
	@Test
	public void testFindSolutionA() {

		System.out.println("\n*****TEST FOR  findSolutionA()\n");


		jugsUnderTest.initialise(0);
		System.out.println(jugsUnderTest.findSolutionA(8));

		jugsUnderTest.initialise(1);
		System.out.println(jugsUnderTest.findSolutionA(5));

		jugsUnderTest.initialise(0);
		System.out.println(jugsUnderTest.findSolutionA(2));

	}

	/**
	 * Test for {@link JugsSpecification#solveA(int)}
	 */
	@Test
	public void testSolveA() {
		System.out.println("\n*****TEST FOR SolveA()\n");

		System.out.println(jugsUnderTest.solveA(4));

		jugsUnderTest.initialise(2);
		System.out.println(jugsUnderTest.solveA(4));

		jugsUnderTest.initialise(0);
		System.out.println(jugsUnderTest.solveA(8));

		jugsUnderTest.initialise(1);
		System.out.println(jugsUnderTest.solveA(5));

		jugsUnderTest.initialise(0);
		System.out.println(jugsUnderTest.solveA(2));

	}

	/**
	 * Test for {@link JugsSpecification#findSolutionB(int)}
	 */
	@Test
	public void testFindSolutionB() {
		System.out.println("\n*****TEST FOR findSolutionB()\n");

		System.out.println(jugsUnderTest.findSolutionB(4));

		jugsUnderTest.initialise(2);
		System.out.println(jugsUnderTest.findSolutionB(4));

		jugsUnderTest.initialise(0);
		System.out.println(jugsUnderTest.findSolutionB(8));

		jugsUnderTest.initialise(1);
		System.out.println(jugsUnderTest.findSolutionB(5));

		jugsUnderTest.initialise(0);
		System.out.println(jugsUnderTest.findSolutionB(2));

	}

	/**
	 * Test for {@link JugsSpecification#solveB(int)}
	 */
	@Test
	public void testSolveB() {
		System.out.println("\n*****TEST FOR solveB()\n");

		System.out.println(jugsUnderTest.solveB(4));

		jugsUnderTest.initialise(2);
		System.out.println(jugsUnderTest.solveB(4));

		jugsUnderTest.initialise(0);
		System.out.println(jugsUnderTest.solveB(8));

		jugsUnderTest.initialise(1);
		System.out.println(jugsUnderTest.solveB(5));

		jugsUnderTest.initialise(0);
		System.out.println(jugsUnderTest.solveB(2));

	}

	@Test
	public void testDoSequences() {
		System.out.println("\nTEST FOR doSequences()\n");

		jugsUnderTest.doSequences(jugsUnderTest.findSolutionB(4));

		jugsUnderTest.initialise(2);
		jugsUnderTest.doSequences(jugsUnderTest.findSolutionB(4));

		jugsUnderTest.initialise(0);
		jugsUnderTest.doSequences(jugsUnderTest.findSolutionB(8));

		jugsUnderTest.initialise(1);
		jugsUnderTest.doSequences(jugsUnderTest.findSolutionB(5));

		jugsUnderTest.initialise(0);
		jugsUnderTest.doSequences(jugsUnderTest.findSolutionB(2));

	}
}
