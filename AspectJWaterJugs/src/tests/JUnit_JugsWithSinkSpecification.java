package tests;

import org.junit.Assert;
import org.junit.Test;

import specifications.JugsWithSinkSpecification;

public abstract class JUnit_JugsWithSinkSpecification extends
		JUnit_JugsSpecification {

	/**
	 * Test for {@link JugsWithSinkSpecification#emptyToSink(int)}
	 */
	@Test
	public void testemptyToSink() {

		JugsWithSinkSpecification jugsWithSink = (JugsWithSinkSpecification) jugsUnderTest;

		jugsWithSink.initialise(0);

		jugsWithSink.emptyToSink(0);
		Assert.assertEquals(0, jugsWithSink.amountInJug(0));
		Assert.assertEquals(0, jugsWithSink.amountInJug(1));
		Assert.assertEquals(0, jugsWithSink.amountInJug(2));
		Assert.assertTrue(jugsWithSink.invariant());

	}

}
