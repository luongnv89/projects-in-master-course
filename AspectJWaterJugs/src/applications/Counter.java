/**
 * 
 */
package applications;

import java.util.ArrayList;

/**
 * Count the number of times a method is called
 * 
 * @author louis
 * 
 */
public class Counter {
	/**
	 * 
	 */
	ArrayList<Method> listMethodCallCounters;

	public Counter() {
		listMethodCallCounters = new ArrayList<Method>();
	}

	public void addMethodCalled(String methodName) {
		boolean exist = false;
		for (int i = 0; i < listMethodCallCounters.size(); i++) {
			if (listMethodCallCounters.get(i).getMethodName()
					.equals(methodName)) {
				int nbCall = listMethodCallCounters.get(i).getNumberOfCall();
				listMethodCallCounters.get(i).setNumberOfCall(nbCall + 1);
				exist = true;
				break;
			}
		}

		if (!exist) {
			listMethodCallCounters.add(new Method(methodName, 1));
		}
	}
	
	public void showMethodCall(){
		for(int i=0;i<listMethodCallCounters.size();i++){
			System.out.println(listMethodCallCounters.get(i).getMethodName()+"(): "+listMethodCallCounters.get(i).getNumberOfCall());
		}
	}

}

class Method {
	String methodName;
	int numberOfCall;

	public Method(String methodName, int numberOfCall) {
		this.methodName = methodName;
		this.numberOfCall = numberOfCall;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param numberOfCall
	 *            the numberOfCall to set
	 */
	public void setNumberOfCall(int numberOfCall) {
		this.numberOfCall = numberOfCall;
	}

	/**
	 * @return the numberOfCall
	 */
	public int getNumberOfCall() {
		return numberOfCall;
	}

}
