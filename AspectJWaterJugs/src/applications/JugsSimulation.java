package applications;

import models.Jugs;

public class JugsSimulation {


	final static int NUMBER_OF_MOVES = 200;
	
	public static void main(String[] args) {
		
		int [] sizes = new int [] {8, 5, 3};
		Jugs jugs = new Jugs (sizes);
		System.out.println(jugs);
		
		jugs.initialise(0);
		System.out.println("jugs.initialise(0)");
		System.out.println(jugs);
		
		int randomTo;
		int randomFrom;
		
		for (int move =0; move < NUMBER_OF_MOVES; move++){
			
		randomTo = (int) (Math.random()*jugs.numberOfJugs());
		randomFrom = (int) (Math.random()*jugs.numberOfJugs());
		jugs.moveBetweenJugs(randomFrom, randomTo);
		System.out.println("jugs.moveBetweenJugs("+randomFrom+", "+randomTo+")");
		System.out.println(jugs);

		}
		
		
//		System.out.println("\n\n****Number of method call: ");
//		Jugs.counter.showMethodCall();
	}

}
