package models;

import java.util.Observable;

import abstractions.XOBoardObservableSpecification;
import abstractions.XOBoardSpecification;



public class XOBoard extends Observable implements XOBoardObservableSpecification{
	
	

	private String namePlayer1;
	

	private String namePlayer2;
	

	private boolean firstPlayerIsX;

	private boolean nextPlayerIsX;
	

	private int playCount;
	

	private char board[][] = new char [XO_BOARD_SIZE][XO_BOARD_SIZE];
	

	public XOBoard(){
		
		init("Player 1", "Player 2", true);
		}


	public XOBoard(boolean xStarts){
		
		init("Player 1", "Player 2", xStarts);
		}


	public XOBoard(String nameP1, String nameP2, boolean xStarts){

		System.out.println("Constructing XOBoard with P1="+nameP1+" P2="+nameP2+" xStarts ="+xStarts);
		init(nameP1, nameP2, xStarts);
		}

	public void init (String nameP1, String nameP2, boolean xStarts){
		
		namePlayer1 = nameP1;
		namePlayer2 = nameP2;
		firstPlayerIsX = xStarts;
		nextPlayerIsX = firstPlayerIsX;
	    init();

	}

	public void init() {
		  for (int i =0; i<XO_BOARD_SIZE; i++)
			     for (int j=0; j<XO_BOARD_SIZE; j++)
			    	 board[i][j] = EMPTY_CHAR;
		  playCount=0;
		  setChanged(); notifyObservers();
	}
	
	public char shapeNextPlayer (){
		
		if (nextPlayerIsX) return 'X'; else return 'O';
	}
	
	private int nextPlayer() {
		
		return 1+playCount%2;
	}
	
	public String nameNextPlayer() {

		if (nextPlayer() == 1) return namePlayer1; else return namePlayer2;
	}
	
	public String get_namePlayer1() {

		return namePlayer1;
	}
	
	public String get_namePlayer2() {

		return namePlayer2;
	}
	
	public boolean get_nextPlayerIsX() {
		
		return nextPlayerIsX;
	}
	
	public boolean xStarted (){
		
		return firstPlayerIsX;
	}
	
	public char shapeFirstPlayer() {

		if (firstPlayerIsX) return 'X'; else return 'O';
	}


	public char shapeSecondPlayer() {

		if (firstPlayerIsX) return 'O'; else return 'X';
	}
	

   /**
    * @return If the board is in a safe state, i.e. contains only empty spaces, Xs or Os
    * @todo Strengthen the invariant property
    */
    public boolean invariant(){

      for (int i =0; i<XO_BOARD_SIZE; i++)
                      {
                        for (int j=0; j<XO_BOARD_SIZE; j++)
                            {
                             if (board[i][j] != EMPTY_CHAR &&
                                 board[i][j] != X_CHAR &&
                                 board[i][j] != O_CHAR) return false;
                            };
                      };
     return true;
    }

    public char charAt (int pos) throws IllegalArgumentException{
     
    	if (pos<1 || pos> XO_BOARD_SIZE*XO_BOARD_SIZE) 
   	     throw (new IllegalArgumentException("Specified position - " + pos +" is not in the board"));
        int position  = pos-1;
        int row = position/XO_BOARD_SIZE;
        int column = position%XO_BOARD_SIZE;
        return charAt(row,column);
    }


    public char charAt (int i, int j) throws IllegalArgumentException{
    	
      if (j<0 ||j>XO_BOARD_SIZE-1 ||i>XO_BOARD_SIZE-1||i<0) 
	     throw (new IllegalArgumentException("Specified position is not in the board"));
      return board[i][j];
     }

    public boolean checkFull(){
    	
      return (playCount==9);
    }

    public boolean checkWin (){
    	
      return checkwin(X_CHAR) || checkwin(O_CHAR);
    }

/**
 * @param ch is the character for which we check 3-in-a-row (horizontal, diagonal or vertical)
 * @return if 3-in-a-row on board
 */
public boolean checkwin (char ch){
boolean rows = false;
boolean cols = false;
boolean diag = false;
for (int i = 0;i<3; i++){
       if (    (board[i][0] == ch) && (board[i][0] == board[i][1]) && (board[i][1] == board[i][2])
          ) rows=true;

       if (    (board[0][i] == ch) && (board[0][i] == board[1][i]) && (board[1][i] == board[2][i])
          ) cols=true;
    }
if ( (   (board[0][0]==ch) && (board[1][1] == ch)  && (board[2][2] == ch)
     )
   ||
     ( (board[2][0]==ch)   && (board[1][1] == ch)  && (board[0][2] == ch)
     )
   ) diag = true;

return (rows || cols || diag);
}

public boolean checkWinX() {
	return checkwin('X');
}

public boolean checkWinO() {
	return checkwin('O');
}

public void setNamePlayer1(String name){
	namePlayer1=name;setChanged(); notifyObservers();
}

public void setNamePlayer2(String name){
	namePlayer2=name;setChanged(); notifyObservers();
}

public void flip(int i, int j){	
	 if (board[i][j] == X_CHAR) board[i][j] = O_CHAR;
     else if (board[i][j] == O_CHAR) board[i][j] =X_CHAR;
	 nextPlayerIsX = ! nextPlayerIsX;
	 setChanged(); notifyObservers();
}

public void flipXO(){
	
	for (int i = 0;i<XO_BOARD_SIZE; i++)
	   for (int j=0; j<XO_BOARD_SIZE; j++)
		   if (board[i][j] == X_CHAR) board[i][j] = O_CHAR;
		     else if (board[i][j] == O_CHAR) board[i][j] =X_CHAR;
	
	firstPlayerIsX = ! firstPlayerIsX;
	nextPlayerIsX = ! nextPlayerIsX;
	setChanged(); notifyObservers();
	}

public  int randomSpace () throws IllegalStateException{
	
	if (checkFull()) throw (new IllegalStateException("No more space left on board"));
	int pos;
	do 
		pos = (int)(Math.random()*XO_BOARD_SIZE*XO_BOARD_SIZE)+1;
	while (charAt(pos)!=EMPTY_CHAR);	
	return pos;
}


private void playInRandomSpaceOnBoard(char c)throws IllegalStateException{ 
	int pos = randomSpace()-1; board[pos/3][pos%3] = c;
	nextPlayerIsX = ! nextPlayerIsX;
	playCount++;
	setChanged(); notifyObservers();
	}

public void playXInRandomSpaceOnBoard() throws IllegalStateException{ 
	playInRandomSpaceOnBoard('X');}

public void playOInRandomSpaceOnBoard()throws IllegalStateException{ 
	playInRandomSpaceOnBoard('O');}

public void playUnobserved(int i, int j, char c) throws IllegalArgumentException{
	if (i<0 || i>XO_BOARD_SIZE-1 || j<0 || j>XO_BOARD_SIZE-1) 
		throw (new IllegalArgumentException("Position specified is not in the board"));
	board[i][j] = c; 
	}

private void play(int i, int j, char c) throws IllegalArgumentException{
	if (i<0 || i>XO_BOARD_SIZE-1 || j<0 || j>XO_BOARD_SIZE-1) 
		throw (new IllegalArgumentException("Position specified is not in the board"));
	if (board [i][j] ==EMPTY_CHAR) board[i][j] = c; 
	    else throw (new IllegalArgumentException("Position specified is already taken"));
	nextPlayerIsX = ! nextPlayerIsX;
	playCount++;
	setChanged(); notifyObservers(); 
	}

public void playX(int i, int j) throws IllegalArgumentException{
      play(i,j,X_CHAR);
}

public void playO(int i, int j) throws IllegalArgumentException{
	  play(i,j,O_CHAR);
}

public void unset(int i, int j) throws IllegalArgumentException{
	  playUnobserved(i,j,EMPTY_CHAR);
}

public void forceObserverNotification(){
	setChanged(); notifyObservers();
}

public void playX(int pos){playX( (pos-1)/3, (pos-1)%3);}

public void playO(int pos){playO( (pos-1)/3, (pos-1)%3);}

public boolean isFree(int i, int j) throws IllegalArgumentException{
     return charAt(i,j)==EMPTY_CHAR;
     }

public String toString(){
String str= "\n";

for (int i =0; i<XO_BOARD_SIZE; i++)
                      {
                        for (int j=0; j<XO_BOARD_SIZE; j++)
                            {
                             str = str+board[i][j];
                             if (j<XO_BOARD_SIZE-1) str = str+'|'; else str=str+"\n";
                            };
                      if (i<XO_BOARD_SIZE-1) str = str+"-----\n";
                      };
                      
                      if (checkWinO())str = str+ "\nO has won";
                      if (checkWinX())str = str+ "\nX has won";
return str;
}

public void play(int i, int j) throws IllegalArgumentException {
	if (nextPlayerIsX) playX(i,j); else playO(i,j);	
}

public void play(int pos) throws IllegalArgumentException {
	if (nextPlayerIsX) playX(pos); else playO(pos);
}

public boolean checkGameOver() {
	return (checkFull() || checkWin());
}





}

