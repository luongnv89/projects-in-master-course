package models;



public class XOGameConfiguration {
	
	public char player1Shape;
	public char player2Shape;

	  
	public int numberOfGames;
	public int wins1;
	public int wins2;
	public boolean doesPlayer1StartNext;

	
	public XOGameConfiguration (){
		
		numberOfGames =0;
		wins1=0;
		wins2=0;
		doesPlayer1StartNext = false;

		player1Shape='X';
		player2Shape='O';
	}
	
	public void flipShapes(){
		if (player1Shape=='X'){player1Shape='O'; player2Shape='X';}
		else {player1Shape='X'; player2Shape='O';}
	}
	
	public void resetScores(){
		numberOfGames = 0;
		wins1 = 0;
		wins2 = 0;
	}
	
	
    public int playerStartingNext(){
		
		if (doesPlayer1StartNext) return 1; else return 2;
	}
    
    public void changeStartingPlayer(){
    	doesPlayer1StartNext =! doesPlayer1StartNext;
    }

}
