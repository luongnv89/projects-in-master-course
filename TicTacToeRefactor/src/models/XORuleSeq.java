package models;


import models.rules.XORandomPlay;
import abstractions.XORule;

public class XORuleSeq {

public XORule defaultrule;
public XORule first;
public XORuleSeq nextSeq;
public XORuleSeq previousSeq;

public XORuleSeq (){
defaultrule = new XORandomPlay();
first = defaultrule;
nextSeq = null;
previousSeq = null;
}

public XORuleSeq(XORule rule){
defaultrule = rule;
first = defaultrule;
nextSeq = null;
previousSeq = null;
}

public void resetToDefault(){
first = defaultrule;
nextSeq = null;
previousSeq = null;
}

public boolean contains(XORule rule){
if ( (rule.toString()).equals(first.toString()) ) return true;
if (nextSeq==null) return false;
else return nextSeq.contains(rule);
}

// attempts to play in board and returns the rule eventually applied
public XORule apply(XOBoard game, char ch){
	
boolean applied = false;
XORule current = first;
if (ch!='X'&&ch!='O') return null;
if (game.checkFull()) return null;
applied = current.apply(game, ch);
if (applied) return current;
else return nextSeq.apply(game,ch);
}

public void addBackRule(XORule rule){

if (nextSeq==null){
XORuleSeq newlastseq = new XORuleSeq(rule);
newlastseq.previousSeq = this;
nextSeq = newlastseq;}
else nextSeq.addBackRule(rule);
}


public void addFrontRule(XORule rule){

XORuleSeq oldfrontseq = new XORuleSeq(first);
oldfrontseq.nextSeq = nextSeq;
oldfrontseq.previousSeq = this;
first = rule;
nextSeq = oldfrontseq;
previousSeq = null;
if (oldfrontseq.nextSeq!=null)
   oldfrontseq.nextSeq.previousSeq=oldfrontseq;
}

public String toString(){
String str = "XORuleSeq: ";
str = str+(first.toString());
if (nextSeq !=null) str = str+(nextSeq.toString());
else str = str+ "EMPTY";
return str;
}



}