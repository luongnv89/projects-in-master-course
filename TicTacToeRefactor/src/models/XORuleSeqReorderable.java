package models;

import java.util.Observable;

import models.rules.XORandomPlay;
import abstractions.XORule;


public class XORuleSeqReorderable extends Observable{
	
	public XORule defaultrule;
	public XORule first;
	public XORuleSeqReorderable nextSeq;
	public XORuleSeqReorderable previousSeq;

public XORuleSeqReorderable selected;

public XORuleSeqReorderable (){
	defaultrule = new XORandomPlay();
	first = defaultrule;
	nextSeq = null;
	previousSeq = null;
	selected = this;
	}

	public XORuleSeqReorderable(XORule rule){
	defaultrule = rule;
	first = defaultrule;
	nextSeq = null;
	previousSeq = null;
	selected = this;
	}

	public void resetToDefault(){
	first = defaultrule;
	nextSeq = null;
	previousSeq = null;
	selected = this;
	setChanged(); notifyObservers();
	}

	public boolean contains(XORule rule){
	if ( (rule.toString()).equals(first.toString()) ) return true;
	if (nextSeq==null) return false;
	else return nextSeq.contains(rule);
	}

	// attempts to play in board and returns the rule eventually applied
	public XORule apply(XOBoard game, char ch){
		
	boolean applied = false;
	XORule current = first;
	if (ch!='X'&&ch!='O') return null;
	if (game.checkFull()) return null;
	applied = current.apply(game, ch);
	if (applied) return current;
	else return nextSeq.apply(game,ch);
	}

	public void addBackRule(XORule rule){

	if (nextSeq==null){
	XORuleSeqReorderable newlastseq = new XORuleSeqReorderable(rule);
	newlastseq.previousSeq = this;
	nextSeq = newlastseq;}
	else nextSeq.addBackRule(rule);
	setChanged(); notifyObservers();
	}


	public void addFrontRule(XORule rule){

	XORuleSeqReorderable oldfrontseq = new XORuleSeqReorderable(first);
	oldfrontseq.nextSeq = nextSeq;
	oldfrontseq.previousSeq = this;
	first = rule;
	nextSeq = oldfrontseq;
	previousSeq = null;
	if (oldfrontseq.nextSeq!=null)
	   oldfrontseq.nextSeq.previousSeq=oldfrontseq;
	setChanged(); notifyObservers();
	}

	public String toString(){
	String str = "XORuleSeq: ";
	str = str+(first.toString());
	if (nextSeq !=null) str = str+(nextSeq.toString());
	else str = str+ "EMPTY";
	str = str+"\nSelected is "+(selected.first.toString());
	return str;
	}

public void changeSelectR(){
if (selected.nextSeq!=null) selected = selected.nextSeq;
setChanged(); notifyObservers();
}

public void changeSelectL(){
if (selected.previousSeq!=null) selected = selected.previousSeq;
setChanged(); notifyObservers();
} 


public void swapSelectL(){
XORule temp;
if (selected.previousSeq!=null){
temp = selected.first;
selected.first = selected.previousSeq.first;
selected.previousSeq.first = temp;
selected = selected.previousSeq;
setChanged(); notifyObservers();
} 
}

public void swapSelectR(){
XORule temp;
if (selected.nextSeq!=null){
temp = selected.first;
selected.first = selected.nextSeq.first;
selected.nextSeq.first = temp;
selected = selected.nextSeq;
setChanged(); notifyObservers();
} 
}



}