package models.rules;



import models.XOBoard;
import abstractions.XORule;


public class XOInOrderPlay extends XORule{

public boolean apply(XOBoard game, char ch){

if (ch!='X'&&ch!='O') return false;
if (game.checkFull()) return false;
for (int i = 0;i<3; i++)
   for (int j=0; j<3; j++)
       if (game.charAt(i,j) ==' '){
          if (ch=='X') game.playX(i,j); else game.playO(i,j);
          return true;}
return false;
}

public XOInOrderPlay (String str){name ="Test_XOInOrderPlay: "+str;}
public XOInOrderPlay (){name = "Test_XOInOrderPlay: ";}

public void rename (String str){name ="Test_XOInOrderPlay: "+str;}


}