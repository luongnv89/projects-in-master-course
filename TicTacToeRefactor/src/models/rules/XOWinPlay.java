package models.rules;

import models.XOBoard;
import abstractions.XORule;



public class XOWinPlay extends XORule{

private boolean winPlay(XOBoard game, char ch){
boolean foundwin = false;
for (int i = 0;i<3; i++)
   for (int j=0; j<3; j++)
       if ((game.charAt(i,j) ==' ')) 
          {if (ch == 'X') {game.playUnobserved(i,j,'X'); foundwin = game.checkWinX();}
                     else {game.playUnobserved(i,j,'O'); foundwin = game.checkWinO();}
           if (!foundwin) game.unset(i,j); 
           else {game.forceObserverNotification(); return true;}
          }
return false;
}



public boolean apply(XOBoard game, char ch){
boolean foundwin;
if (ch!='X'&&ch!='O') return false;
if (game.checkFull()) return false;
foundwin = winPlay(game, ch);
return foundwin;
}

public XOWinPlay (String str){name ="XOWinPlay: "+str;}
public XOWinPlay (){name = "XOWinPlay: ";}

public void rename (String str){name ="XOWinPlay: "+str;}

}