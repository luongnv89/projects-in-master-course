package models.rules;

import models.XOBoard;
import abstractions.XORule;


public class XOMiddlePlay extends XORule{

public boolean apply(XOBoard game, char ch){
if (ch!='X'&&ch!='O') return false;
if (game.charAt(1,1)!=' ') return false;
if (ch=='X') game.playX(1,1); else game.playO(1,1);
return true;
}

public XOMiddlePlay (String str){name ="XOMiddlePlay: "+str;}
public XOMiddlePlay (){name = "XOMiddlePlay: ";}

public void rename (String str){name ="XOMiddlePlay: "+str;}



}