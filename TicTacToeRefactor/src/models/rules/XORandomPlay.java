package models.rules;

import models.XOBoard;
import abstractions.XORule;


public class XORandomPlay extends XORule{


public boolean apply(XOBoard game, char ch){
	
	
if (ch!='X'&&ch!='O') return false;
if (game.checkFull()) return false;
else if (ch =='X') game.playXInRandomSpaceOnBoard();
else if (ch =='O') game.playOInRandomSpaceOnBoard();
return true;
}


public XORandomPlay (String str){name ="XORandomPlay: "+str;}

public XORandomPlay (){name = "XORandomPlay: ";}

public void rename (String str){name ="XORandomPlay: "+str;}




}