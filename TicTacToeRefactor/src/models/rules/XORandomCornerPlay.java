package models.rules;


import models.XOBoard;
import abstractions.XORule;

public class XORandomCornerPlay extends XORule{

public boolean apply(XOBoard game, char ch){
if (ch!='X'&&ch!='O') return false;

int choice;
int count=0;

for (int i = 1; i<10; i+=2) if (i!=5 &&game.charAt(i)==' ') count++;

if (count ==0) return false;

do{
choice = 1+((int)(Math.random()*5))*2;
} while  (choice==5 || game.charAt(choice)!=' ');
if (ch=='X') game.playX(choice); else game.playO(choice);
return true;
}

public XORandomCornerPlay (String str){name ="XORandomCornerPlay: "+str;}
public XORandomCornerPlay (){name = "XORandomCornerPlay: ";}

public void rename (String str){name ="XORandomCormnerPlay: "+str;}



}