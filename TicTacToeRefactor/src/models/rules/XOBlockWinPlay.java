package models.rules;

import models.XOBoard;
import abstractions.XORule;

public class XOBlockWinPlay extends XORule{
	

		private boolean noLosePlay(XOBoard game, char ch){
		boolean foundloss = false;
		for (int i = 0;i<3; i++)
		   for (int j=0; j<3; j++){
		       if (game.charAt(i,j) ==' ') 
		          {if (ch=='O') {game.playUnobserved(i,j,'X'); foundloss = game.checkWinX();}
		                   else {game.playUnobserved(i,j,'O'); foundloss = game.checkWinO();}
		           game.unset(i,j);
		           if (foundloss && ch=='O') {game.playO(i,j); return true;}
		           if (foundloss && ch=='X') {game.playX(i,j); return true;}
		          }
		   }	          
		return false;
		}

		public boolean apply(XOBoard game, char ch){
			
		boolean blockedwin;
		if (ch!='X'&&ch!='O') return false;
		if (game.checkFull()) return false;
		blockedwin = noLosePlay(game, ch);
		return blockedwin;
		}

		public XOBlockWinPlay (String str){name ="XOBlockWinPlay: "+str;}
		public XOBlockWinPlay (){name = "XOBlockWinPlay: ";}

		public void rename (String str){name ="XOBlockWinPlay: "+str;}


		public static void main(String[] args){

		XOBoard game = new XOBoard();

		boolean applied;

		game.playX(0,0);
		game.playX(2,2);


		XORule rule;
		XOBlockWinPlay blockwinplay  = new XOBlockWinPlay();
		rule = blockwinplay;
		applied = rule.apply(game,'O');
		if (applied) System.out.println("blocked win"); else System.out.println("not blocked win");
		System.out.println(game);
		}

		

}
