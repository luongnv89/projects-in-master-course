package models.rules;


import models.XOBoard;
import abstractions.XORule;



public class XORandomEdgePlay extends XORule{

public boolean apply(XOBoard game, char ch){
if (ch!='X'&&ch!='O') return false;

int choice;
int count=0;

for (int i = 2; i<9; i+=2) if (game.charAt(i)==' ') count++;

if (count ==0) return false;

do{
choice = ((int)(Math.random()*4)+1)*2;
} while  (game.charAt(choice)!=' ');
if (ch=='X') game.playX(choice); else game.playO(choice);
return true;
}

public XORandomEdgePlay (String str){name ="XORandomEdgePlay: "+str;}
public XORandomEdgePlay (){name = "XORandomEdgePlay: ";}

public void rename (String str){name ="XORandomEdgePlay: "+str;}



}