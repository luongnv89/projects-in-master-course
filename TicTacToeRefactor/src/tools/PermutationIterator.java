package tools;

import java.util.*;


public class PermutationIterator implements Iterator<int[]>{

 
 private final int n;
 

 private int[] perm;
 

 private int[] dirs;


 public PermutationIterator(int size) throws IllegalArgumentException{
     n = size;
     if (n<=0) throw (new IllegalArgumentException("The size must be a positive integer"));
     
     perm = new int[n];
     dirs = new int[n];
     for(int i = 0; i < n; i++) {
           perm[i] = i;
           dirs[i] = -1;
         }
     dirs[0] = 0;
 }
 

 public int[] next() {
	 
	 if (perm ==null) return null;
     
	 // The return value is a copy of perm, before it is updated
	 int [] result = new int [perm.length];
	 for(int j = 0; j < result.length; j++) result[j] = perm[j];
     
     // find the largest element with non zero direction
     int i=-1, e=-1;
     for(int j = 0; j < n; j++)
         if ((dirs[j] != 0) && (perm[j] > e)) {
             e = perm[j];
             i = j;
         }

     if (i == -1) // no more permutations
         {perm = (dirs = null); 
          return result;
         }

     // swap with the adjacent element specified by the direction value 
     int k = i + dirs[i];
     swap(i, k, dirs);
     swap(i, k, perm);
     // if at the start/end or the next element in the direction is bigger then  reset the direction.
     if ((k == 0) || (k == n-1) || (perm[k + dirs[k]] > e))
         dirs[k] = 0;

     // set directions to all greater elements
     for(int j = 0; j < n; j++)
         if (perm[j] > e)
             dirs[j] = (j < k) ? +1 : -1;
  
     return result;
 }


 public boolean hasNext() {
	 return (perm!=null);
 }

 
 public void remove() {
     throw new UnsupportedOperationException();
 }

 private static String intArrayAsString (int[] array){
	 if (array ==null) return "null";
	 String str="[ ";
	 for (int j = 0; j <array.length; j++) str = str +array[j]+" ";
	 return str+ "]";
 }

 public String toString (){
	  
	String str ="\nperm = "+intArrayAsString(perm);
	str = str+ "\ndirs = "+intArrayAsString(dirs);	
	return str+"\n";
 }
 
 protected static void swap(int i, int j, int[] arr) {
     int v = arr[i];
     arr[i] = arr[j];
     arr[j] = v;
 }

}
