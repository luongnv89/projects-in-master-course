package applet;


/*
<applet code=XOApplet.class width = 660  height = 440 ></applet>

*/


/* XOApplet class for playing noughts and crosses 

Paul Gibson
April 2000
Version 1

*/

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Event;
import java.awt.GridLayout;

import models.XOBoard;
import models.XORuleSeqReorderable;
import models.rules.XOBlockWinPlay;
import models.rules.XOMiddlePlay;
import models.rules.XORandomCornerPlay;
import models.rules.XOWinPlay;



public class XOApplet extends Applet  {

XOBoard game = new XOBoard();
XOCanvas canvas = new XOCanvas(this);
XOControl control = new XOControl(this);
XOHistory history = new XOHistory();
XORuleCanPan playerRules = new XORuleCanPan();

AudioClip beep,ding,joy,strange,yahoo1,yahoo2;
boolean Pwon;
boolean Cwon;
boolean screenlock;
int scoreC;
int scoreP;
int scoreD;

public void resize(){

}

public void init(){

setSize(660,440);
Pwon = false;
Cwon = false;
screenlock = true;
scoreC= 0;
scoreD=-1;// 
scoreP= 0;
setLayout(new GridLayout(2,2));
add(canvas);
add(control);
add(playerRules);
add(history);
beep = getAudioClip(getCodeBase(),"ImagesSounds/beep.au");
ding = getAudioClip(getCodeBase(),"ImagesSounds/ding.au");
joy = getAudioClip(getCodeBase(),"ImagesSounds/joy.au");
strange = getAudioClip(getCodeBase(),"ImagesSounds/return.au");
yahoo1 = getAudioClip(getCodeBase(),"ImagesSounds/yahoo1.au");
yahoo2 = getAudioClip(getCodeBase(),"ImagesSounds/yahoo2.au");
newgame();
         
} // END METHOD init

// The game playing level

public void play (int lvl, char ch){
XORuleSeqReorderable player = new XORuleSeqReorderable();

if (lvl==1)     {player.addFrontRule(new XORandomCornerPlay());
                 player.addFrontRule(new XOMiddlePlay()); 
                 
}
else if (lvl==2){player.addFrontRule(new XOWinPlay()); 
                 player.addFrontRule(new XOMiddlePlay()); 
}
else if (lvl==3){player.addFrontRule(new XOWinPlay()); 
                 player.addFrontRule(new XOBlockWinPlay());
                 player.addFrontRule(new XOMiddlePlay()); 
}
else if (lvl==4){player.addFrontRule(new XORandomCornerPlay()); 
                 player.addFrontRule(new XOBlockWinPlay()); 
                 player.addFrontRule(new XOWinPlay());
                 player.addFrontRule(new XOMiddlePlay()); 
}
if (lvl !=5) player.apply(game,ch);
else playerRules.apply(game,ch);
}

public void beepSound(){if (control.sounds) beep.play();};
public void dingSound(){if (control.sounds) ding.play();};
public void joySound(){if (control.sounds) joy.play();};
public void strangeSound(){if (control.sounds) strange.play();};
public void yahoo1Sound(){if (control.sounds) yahoo1.play();};
public void yahoo2Sound(){if (control.sounds) yahoo2.play();};

public boolean mouseUp(Event evt, int x, int y){

int boardheight = 200;
int boardwidth = 330;

if (screenlock) history.append("\nPress NEW GAME to start");
else{
int c = x*3/boardwidth;
int r = y*3/boardheight;

if (c>2 || c<0 || r>2 || r<0) {
history.append("\nIn the board please\n");
dingSound();
}

else {
      if (game.charAt(r,c)!=' ' ) history.append("\nPosition already taken\n");
         else {
               char charP;
               if (control.shape) charP ='X'; else charP = 'O';
               if (charP=='X') game.playX(r,c); else game.playO(r,c);
               if (game.checkwin(charP)){
                       history.append("\nWell done ... you have won.");
                       yahoo2Sound();
                       Pwon = true; 
                       screenlock = true;}
               else if (game.checkFull()){
                  history.append("\n\nGame drawn.\n"); 
                  screenlock = true; joySound();
                  }                               
             else {  screenlock = true;
                       char compshape;
                       if (control.shape) compshape ='O'; else compshape = 'X';
                       play(control.level,compshape); canvas.repaint();
                       if (game.checkwin(compshape)){
                             history.append("\nBad luck  ... I have won.\n");
                       yahoo1Sound();
                             Cwon = true; screenlock = true;}
                         else if (game.checkFull()){
                            history.append("\nGame drawn.\n"); screenlock = true;}
                         else screenlock = false;
                        
                    
                    }
             }
   }
}

canvas.repaint();
return true;
}

public void help(){history.appendText("\nHELP - PLEASE CONTACT THE TEACHER FOR HELP\n");}

public void newgame(){
beepSound();

history.appendText("\n\nNEW GAME\n");
game.init();
if (Pwon) {history.append("\nWinning last game gains you a point!\n");;
           scoreP++;}
else if (Cwon) {history.append("\nLosing last game gives computer a point!\n");;
           scoreC++;}
else {scoreD++;}
Pwon = false; Cwon= false;
if (control.starter){screenlock = false;
                     history.append("\nClick in the board to play.\n");}
else {
  screenlock = true;
  char compshape;
  if (control.shape) compshape ='O'; else compshape = 'X';
  play(control.level,compshape);
  screenlock = false;
}
  canvas.repaint();
}


public void replay(){history.append("\nREPLAY - not yet implemented\n");}

public void undo(){history.append("\nUNDO MOVE - not yet implemented\n");}


public void reset(){
strangeSound();
history.setText("\nRESET SCORE - OK let's start from scratch\n");
scoreP= 0; scoreC =0;scoreD=-1;
newgame();canvas.repaint();
}



} // ENDCLASS XOApplet