package applet;





import java.awt.Button;
import java.awt.Choice;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import models.XORuleSeqReorderable;
import models.rules.XOBlockWinPlay;
import models.rules.XOInOrderPlay;
import models.rules.XOMiddlePlay;
import models.rules.XORandomCornerPlay;
import models.rules.XORandomEdgePlay;
import models.rules.XOWinPlay;


class XORulePanel extends Panel implements ActionListener, ItemListener{
  
  XORuleSeqReorderable rules;
  XORuleCanvas canvas;

  Button selectR = new Button("select v");
  Button selectL = new Button("select ^");
  Button swapR = new Button("swap v");
  Button swapL = new Button("swap ^");
  Button reset = new Button("default");
  Choice ruleList = new Choice();
  Label title1 = new Label("Opponent");
  Label title2 =  new Label("RULES");

XORulePanel (XORuleSeqReorderable r, XORuleCanvas c){

  rules =r; canvas = c;

  setLayout(new GridLayout(5,2,1,1));

  add(title1); add(title2);
  add(selectL);    selectL.addActionListener(this);
  add(swapL);      swapL.addActionListener(this);
  add(selectR);    selectR.addActionListener(this);
  add(swapR);      swapR.addActionListener(this);
  add(new Label("  RESET TO: "));
  add(reset);      reset.addActionListener(this);
  add(new Label ("  ADD RULE: "));
  ruleList.addItem("Block Win");
  ruleList.addItem("Any Corner");
  ruleList.addItem("Any Edge");
  ruleList.addItem("Middle");
  ruleList.addItem("In Order");
  ruleList.addItem("Play Win");
  add(ruleList);  ruleList.addItemListener(this);
}

 public void itemStateChanged(ItemEvent e){
   String str = e.getItem().toString();
    if (str.equals("Block Win")  && !rules.contains(new XOBlockWinPlay()) ){
       rules.addFrontRule(new XOBlockWinPlay());}
    else if (str.equals("Any Corner") && !rules.contains(new XORandomCornerPlay())){
       rules.addFrontRule(new XORandomCornerPlay());}
    else if (str.equals("Any Edge") && !rules.contains(new XORandomEdgePlay())){
       rules.addFrontRule(new XORandomEdgePlay());}
    else if (str.equals("Middle") && !rules.contains(new XOMiddlePlay())){
       rules.addFrontRule(new XOMiddlePlay());}
    else if (str.equals("In Order") && !rules.contains(new XOInOrderPlay())){
       rules.addFrontRule(new XOInOrderPlay());}
    else if (str.equals("Play Win") && !rules.contains(new XOWinPlay())){
       rules.addFrontRule(new XOWinPlay());}
    canvas.repaint();
   }

public void actionPerformed(ActionEvent event)
 { 
   if (event.getActionCommand().startsWith("swap v") ){
      rules.swapSelectR();
      }
   if (event.getActionCommand().startsWith("swap ^") ){
      rules.swapSelectL();
      }
   if (event.getActionCommand().startsWith("select v") ){
      rules.changeSelectR();
      }
   if (event.getActionCommand().startsWith("select ^") ){
      rules.changeSelectL();
      }
   if (event.getActionCommand().startsWith("default") ){
      rules.resetToDefault();
      }
   canvas.repaint();
 }

} // ENDCLASS XORulePanel;