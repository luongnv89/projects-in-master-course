package applet;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Panel;

import models.XOBoard;
import models.XORuleSeqReorderable;
import abstractions.XORule;


class XORuleCanPan extends Panel
{ 
XORulePanel control;
XORuleCanvas XOcanvas;
XORuleSeqReorderable rules;

XORuleCanPan()
  { super();
    setSize(350,450);
    setBackground(Color.yellow);
    rules = new XORuleSeqReorderable();

    XOcanvas = new XORuleCanvas(rules);
    control = new XORulePanel(rules, XOcanvas);
    setLayout(new BorderLayout());
    add(control, BorderLayout.EAST);
    add(XOcanvas, BorderLayout.CENTER);
    setVisible(true);
  }

 public XORule apply (XOBoard game, char ch){
  return rules.apply(game, ch);
 }

 public static void main(String[] args)
  { XORuleCanPan XOplayer = new XORuleCanPan();
  }
}