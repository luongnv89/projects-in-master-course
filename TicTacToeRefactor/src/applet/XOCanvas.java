package applet;


import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import models.XOBoard;

class XOCanvas extends Canvas{

  XOApplet parent;
  XOBoard game;

  Image ImageO;
  Image ImageX;

int boardheight = 200;
int boardwidth = 330;

XOCanvas(XOApplet myparent){

  parent = myparent;
  setBackground(Color.gray);
  resize(220,330);
  game = myparent.game;
}


private void drawBoard(Graphics g, int width, int height){


ImageO = parent.getImage(parent.getCodeBase(), "ImagesSounds/O.gif");
ImageX = parent.getImage(parent.getCodeBase(), "ImagesSounds/X.gif");  

g.setColor(Color.green);
int yoff = boardheight/3;
int xoff = boardwidth/3;
g.drawLine(xoff, 0, xoff, boardheight);
g.drawLine(2*xoff, 0, 2*xoff, boardheight);
g.drawLine(0, yoff, boardwidth, yoff);
g.drawLine(0, 2*yoff, boardwidth, 2*yoff);


for (int r = 0 ; r < 3 ; r++) {
for (int c = 0 ; c < 3 ; c++) {
	if (game.charAt(r,c) =='O') 
            g.drawImage(ImageO, c*xoff + 40, r*yoff + 12, this);
             
        else if (game.charAt(r,c) =='X') 
            g.drawImage(ImageX, c*xoff + 40, r*yoff +12, this);
     }	
    }  

}

public void paint(Graphics g) {

Dimension d = size();

g.setColor(Color.yellow);
g.fillRect(5,d.height-16,d.width-10,14);

g.setColor(Color.red);
g.drawString("        Draws: "+ parent.scoreD+
             "        Computer: "+parent.scoreC+ 
             "        Player: "+parent.scoreP, 5, d.height-5);

g.setColor(Color.red);

drawBoard(g, d.width, d.height-18);


}     // END METHOD paint

} // ENDCLASS XOCanvas
