package applet;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;

import models.XORuleSeqReorderable;
import models.XORuleSeq;



class XORuleCanvas extends Canvas
{ 

XORuleSeqReorderable rules;

XORuleCanvas (XORuleSeqReorderable r){

  rules =r;
  setBackground(Color.yellow);

}

public void paint(Graphics g) {

g.setColor(Color.gray);
g.fillRect(5,5,145,210);

g.setColor(Color.green);
g.drawString("XORuleSeqReorderable FIRST",10,20);


int verticalInc =0;
XORuleSeqReorderable temp;
temp = rules;
do{
if (rules.selected == temp) 
{g.setColor(Color.orange); g.drawString("<-",140,40+verticalInc);}
 else g.setColor(Color.blue);
g.drawString(temp.first.toString(),10,40+verticalInc);
temp = temp.nextSeq;
verticalInc+=20;
}
while (temp!=null);
g.setColor(Color.green);
g.drawString("XORuleSeqReorderable LAST",10,40+verticalInc);

}     // END METHOD paint

}
