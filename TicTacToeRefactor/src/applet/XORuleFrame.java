package applet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;

import models.XORuleSeqReorderable;


@SuppressWarnings("serial")
class XORuleFrame extends Frame
{ 
XORulePanel control;
XORuleCanvas xoRuleCanvas;
XORuleSeqReorderable rules;

XORuleFrame(String s)
  { super(s);
    setSize(350,300);
    setResizable(false);
    setBackground(Color.yellow);
    rules = new XORuleSeqReorderable();

    xoRuleCanvas = new XORuleCanvas(rules);
    control = new XORulePanel(rules, xoRuleCanvas);
    setLayout(new BorderLayout());
    add(control, BorderLayout.EAST);
    add(xoRuleCanvas, BorderLayout.CENTER);
    setVisible(true);
  }


 public static void main(String[] args)
  { new XORuleFrame("XORuleFrame");
  }
}