package applet;


import java.awt.Button;
import java.awt.Choice;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class XOControl extends Panel implements ActionListener, ItemListener{

  XOApplet parent;

  Button help = new Button("HELP");
  Button newgame = new Button("NEW GAME");
  Button reset = new Button("RESET SCORE");


  Choice shapeC = new Choice();
  boolean shape; // true for X, false for O

  Choice starterC = new Choice();
  boolean starter; // true for player, false for computer

  Choice soundsC = new Choice();
  boolean sounds; // true for yes, false for no

  Choice levelC = new Choice();
  int level;  //0 - bad ... 4 - great, 5 - rule based player

XOControl (XOApplet myparent){

  setSize(350,200);
  parent = myparent;
  setLayout(new GridLayout(6,2,5,5));

  add(new Label ("  GAME CONTROLS:"));

  add(help);  help.addActionListener(this);
  add(newgame);   newgame.addActionListener(this);
  add(reset);   reset.addActionListener(this);

  add(new Label ("  Player's Shape:"));
  shape = true;
  shapeC.addItem("X");
  shapeC.addItem("O");
  add(shapeC); shapeC.addItemListener(this);

  add(new Label ("  Who Starts:"));
  starter = true;
  starterC.addItem("Player");
  starterC.addItem("Computer");
  add(starterC); starterC.addItemListener(this);

  add(new Label ("  Sounds:"));
  sounds = true;
  soundsC.addItem("Yes");
  soundsC.addItem("No");
  add(soundsC); soundsC.addItemListener(this);

  add(new Label ("  Opponent Player:"));
  level = 0;
  levelC.addItem("Bad");
  levelC.addItem("Poor");
  levelC.addItem("Average");
  levelC.addItem("Good");
  levelC.addItem("Great");
  levelC.addItem("Rules");
  add(levelC); levelC.addItemListener(this);
}

public void actionPerformed(ActionEvent event)
 { 
   if (event.getActionCommand().equals("HELP") ){
      parent.help();
      }
   else if (event.getActionCommand().equals("NEW GAME") ){
      parent.newgame();
      }
   else if (event.getActionCommand().equals("RESET SCORE") ){
      parent.reset();
      }
 }

 public void itemStateChanged(ItemEvent e){

    String str = e.getItem().toString();
    if (str.equals("X")){if (!shape) shape=true; parent.game.flipXO();
                             parent.canvas.repaint();}
    else if (str.equals("O")){if (shape) shape=false; parent.game.flipXO();
                             parent.canvas.repaint();}
    else if (str.equals("Player")){starter =true;}
    else if (str.equals("Computer")){starter=false;}
    else if (str.equals("Bad")){level =0;}
    else if (str.equals("Poor")){level =1;}
    else if (str.equals("Average")){level =2;}
    else if (str.equals("Good")){level =3;}
    else if (str.equals("Great")){level =4;}
    else if (str.equals("Rules")){level =5;}
    else if (str.equals("Yes")){sounds =true;}
    else if (str.equals("No")){sounds=false;};

   }



} // ENDCLASS XOControl
