package abstractions;

import models.XOBoard;

public abstract class XORule {

	protected String name;

	public abstract boolean apply(XOBoard game, char ch);

	public XORule(String str) {
		name = "XORule: " + str;
	}

	public XORule() {
		name = "XORule: ";
	}

	public void rename(String str) {
		name = "XORule: " + str;
	}

	public String toString() {
		return name;
	}

}
