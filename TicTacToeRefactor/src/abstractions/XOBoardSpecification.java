package abstractions;

import tools.HasInvariant;

public interface XOBoardSpecification extends HasInvariant{


	 static final char EMPTY_CHAR = ' ';
	 

	 static final char X_CHAR = 'X';
	 
	
	 static final char O_CHAR = 'O';
	 

	 static final int XO_BOARD_SIZE = 3;
	

	 public void init ();
	 

	 public void init (String name1, String name2, boolean xStarts);
	 

	 public char charAt (int pos) throws IllegalArgumentException;
	 

	 public char charAt(int i, int j) throws IllegalArgumentException;
	 

	 public void flipXO();
	 

	 public boolean checkFull();
	 
	
	 public boolean checkWin ();
	 

	 public boolean checkGameOver();
	 
	
	 public boolean checkWinX ();
	 
 
	public boolean checkWinO ();
	

	public boolean isFree(int i, int j) throws IllegalArgumentException;
	 

	 public  int randomSpace () throws IllegalStateException;
	 

	 public void playXInRandomSpaceOnBoard() throws IllegalStateException;

	 public void playOInRandomSpaceOnBoard() throws IllegalStateException;
	 

	 public void playX(int i, int j) throws IllegalArgumentException;
	 

	 public void playO(int i, int j) throws IllegalArgumentException;
	 
	 

	 public void play(int i, int j) throws IllegalArgumentException;
	 

	 public void playX (int pos) throws IllegalArgumentException;
	 

	 public void playO (int pos) throws IllegalArgumentException;
	 

	 public void play (int pos) throws IllegalArgumentException;
	 
	
	 public char shapeNextPlayer ();
	 

	 public String nameNextPlayer();

	 public boolean xStarted ();
	 
	
	 public boolean get_nextPlayerIsX ();
	 
	
	 public char shapeFirstPlayer ();
	 

	 public String get_namePlayer1();
	 

	 public char shapeSecondPlayer ();
	 

	 public String get_namePlayer2();
}
