package abstractions;

import java.util.Observer;

public interface XOBoardObservableSpecification extends XOBoardSpecification{

	public void addObserver (Observer observer);
}
