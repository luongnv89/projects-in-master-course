package fit.process;

import java.util.Scanner;

public class FitniteNoObject {

	public static void main(String args[]) {
		// int[] seq1 = null;
		// int[] seq2 = null;
		// input(seq1);
		// input(seq2);\
		int[] seq1 = { 1, 2, 3 };
		int[] seq2 = { 6, 7 };
		int[] seq3 = overlap(seq1, seq2);
		int[] seq4;
		if (seq1[0] > seq2[0])
			seq4 = interior(seq1, seq2);
		else
			seq4 = interior(seq2, seq1);
		show(seq1);
		show(seq2);
		show(seq3);
		show(seq4);
	}

	private static int[] interior(int[] seq1, int[] seq2) {
		int[] res;
		int k = 0;
		res = new int[seq1.length < seq2.length ? seq2.length : seq1.length];
		if (seq1[seq1.length - 1] >= seq2[0])
			res = null;
		else {
			int temp = seq1[seq1.length - 1] + 1;
			res[k] = temp;
			while (temp == seq2[0]) {
				k++;
				temp++;
				res[k] = temp;
			}
		}
		return res;
	}

	private static int[] overlap(int[] seq1, int[] seq2) {
		int[] res;
		int k = 0;
		res = new int[seq1.length > seq2.length ? seq2.length : seq1.length];
		for (int i = 0; i < seq1.length; i++) {
			for (int j = 0; j < seq2.length; j++) {
				if (seq1[i] == seq2[j]) {
					res[k] = seq1[i];
					k++;
				}
			}
		}
		return res;
	}

	// private static void input(int[] seq) {
	// Scanner in = new Scanner(System.in);
	// int len;
	// System.out.println("Input the length of sequence:\n");
	// len = in.nextInt();
	// seq = new int[len];
	// for (int i = 0; i < len; i++) {
	// System.out.println("Input value for element number " + i + " :\n");
	// seq[i] = in.nextInt();
	// }
	// }

	private static void show(int[] seq1) {
		if (seq1.length == 0) {
			System.out.println("null");
		} else {
			for (int i = 0; i < seq1.length; i++) {
				System.out.print(" " + seq1[i]);
			}
		}
		System.out.println();
	}
}
