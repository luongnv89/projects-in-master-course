package fit.process;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int s1_start = 7, s1_end = 10;
		int s2_start = 1, s2_end = 5;
		// Interior
		int s4_start;
		int s4_end;
		if (s1_end < s2_start) {
			System.out.println("There isn't overlap!");
			s4_start = s1_end + 1;
			s4_end = s2_start - 1;
			System.out.println("Interior: " + s4_start + " ... " + s4_end);
		} else if (s1_start > s2_end) {
			System.out.println("There isn't overlap!");
			s4_start = s2_end + 1;
			s4_end = s1_start - 1;
			System.out.println("Interior: " + s4_start + " ... " + s4_end);
		} else {
			// overlap
			System.out.println("There isn't interior!");
			int s3_start = s1_start > s2_start ? s1_start : s2_start;
			int s3_end = s1_end < s2_end ? s1_end : s2_end;
			System.out.println("Overlap: " + s3_start + " ... " + s3_end);
		}
	}

}
