package minimax.test;

import minimax.model.Game;
import minimax.model.HandPlayer;
import minimax.model.MaxPlayer;
import minimax.model.XOBoard;
import minimax.specific.Player;

import org.junit.Test;

/**
 * 
 * Test for {@link Game} class
 * 
 * @author luongnv89
 * 
 */
public class GameTest {

	Game game;
	/**
	 * User go first or not
	 */
	boolean playerFirst = false;

	/**
	 * User vs Max player
	 */
	@Test
	public void test() {
		createGame();
		game.play(playerFirst);
	}
	
	/**
	 * Create a game with board, player1, player2
	 */
	private void createGame() {
		XOBoard board = new XOBoard(3);
		Player player1 = new MaxPlayer(board);
		Player player2 = new HandPlayer(board, player1.getId() == 1 ? 2 : 1);
		game = new Game(board, player1, player2);

	}
}

