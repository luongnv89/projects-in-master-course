package minimax.test;

import minimax.model.MinPlayer;
import minimax.model.XOBoard;
import minimax.specific.Player;

import org.junit.Test;

public class PlayerTest {

	Player player;

	@Test
	public void test() {
		XOBoard board = new XOBoard(createXOBoard());
		player = new MinPlayer(board);
		player.run();
		board.show();
		System.out.println("Valid: " + board.valid() + " Score: "
				+ board.getScore() + " Winner: " + board.getWinner());
	}

	/**
	 * Create a XOBoard
	 */
	private XOBoard createXOBoard() {
		int[] s = { 0, 1, 0, 0, 2, 1, 1, 2, 2 };
		XOBoard board = new XOBoard(s, 3);
		board.show();
		System.out.println("Valid: " + board.valid() + " Score: "
				+ board.getScore() + " Winner: " + board.getWinner());
		return board;
	}

}
