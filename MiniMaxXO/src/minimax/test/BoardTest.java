package minimax.test;

import minimax.model.XOBoard;

import org.junit.Test;

public class BoardTest {
	XOBoard board;
	int side;

	/**
	 * Test {@link XOBoard} class
	 * 
	 */
	@Test
	public final void testBoard1() {
		board = new XOBoard(3);
		board.show();
		System.out.println("Valid: " + board.valid() + " Score: "
				+ board.getScore() + " Winner: " + board.getWinner());
		board.update(2, 2, 1);
		board.show();
		System.out.println("Valid: " + board.valid() + " Score: "
				+ board.getScore() + " Winner: " + board.getWinner());
	}

	/**
	 * Test board with the state is given by user
	 */
	@Test
	public final void testBoard2() {
		int[] s = { 1, 1, 1, 0, 2, 0, 1, 2, 2 };
		board = new XOBoard(s, 3);
		board.show();
		System.out.println("Valid: " + board.valid() + " Score: "
				+ board.getScore() + " Winner: " + board.getWinner());
	}

	@Test
	public final void testBoard3() {
		int[] s = { 1, 1, 1, 0, 2, 0, 1, 2, 2 };
		board = new XOBoard(s, 3);
		board.show();
		System.out.println("Valid: " + board.valid() + " Score: "
				+ board.getScore() + " Winner: " + board.getWinner());

		XOBoard board2 = new XOBoard(board);
		board2.show();
		System.out.println("Valid: " + board.valid() + " Score: "
				+ board.getScore() + " Winner: " + board.getWinner());
	}

	@Test
	public final void testBoard4() {
		int[][] s = { { 1, 1, 1 }, 
				      { 0, 2, 0 }, 
				      { 1, 2, 2 } };
		board = new XOBoard(s, 3);
		board.show();
		System.out.println("Valid: " + board.valid() + " \nScore: "
				+ board.getScore() + " \nWinner: " + board.getWinner());
	}
}
