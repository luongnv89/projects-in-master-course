package minimax.model;

import java.util.Scanner;

import minimax.specific.Player;

/**
 * User play game!
 * 
 * @author luongnv89
 * 
 */
public class HandPlayer extends Player {

	public HandPlayer(XOBoard board, int id) {
		super(board, id);
	}

	@Override
	public void run() {
		boolean valid = false;
		while (!valid) {
			Scanner in = new Scanner(System.in);
			System.out.println("Input the position of your choice:");
			System.out.println("Row: ");
			int x = in.nextInt();
			System.out.println("Column: ");
			int y = in.nextInt();
			if (board.getStates(x, y) == 0) {
				valid = true;
				board.update(x, y, id);
			} else {
				System.out
						.println("Incorrect! This position is already!Please input again!");
			}
		}
		board.getScore();
	}

	/**
	 * Set id for user. If the other player is 1, id of user will be 2 and
	 * otherwise
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

}
