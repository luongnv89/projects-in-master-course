package minimax.model;

import minimax.specific.Player;

/**
 * Game
 * 
 * @author luongnv89
 * 
 */
public class Game {
	private XOBoard board = new XOBoard(3);
	private Player player1;
	private Player player2;

	/**
	 * Create a game
	 * 
	 * @para@Overridem board
	 * @param player
	 *            Min or Max player
	 */
	public Game(XOBoard board, Player player1, Player player2) {
		this.board = new XOBoard(board);
		this.player1 = player1;
		this.player2 = player2;
	}

	/**
	 * Play game
	 * 
	 * @param playerFirst
	 *            If playerFirst = true, that means player1 go first!
	 */
	public void play(boolean player1First) {
		boolean stop = false;
		board.show();
		while (!stop) {
			if (player1First) {
				player1.run();
				board.getScore();
				board.show();
				if ((board.getWinner() != 0) || board.isDrawn()) {
					stop = true;
				} else {
					player2.run();
					board.show();
					if ((board.getWinner() != 0) || board.isDrawn())
						stop = true;
				}
			} else {
				player2.run();
				board.show();
				if ((board.getWinner() != 0) || board.isDrawn())
					stop = true;
				else {
					player1.run();
					board.getScore();
					board.show();
					if ((board.getWinner() != 0) || board.isDrawn())
						stop = true;
				}
			}
		}
		if (board.isDrawn())
			System.out.println("Draw game!");
		else
			System.out.println("Winner is player: " + board.getWinner());
	}

}
