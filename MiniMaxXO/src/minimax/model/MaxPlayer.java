package minimax.model;

import minimax.specific.Player;


/**
 * Player is a Max player (id = 2)
 * @author luongnv89
 *
 */
public class MaxPlayer extends Player {

	public MaxPlayer(XOBoard board) {
		super(board, 2);
	}
}
