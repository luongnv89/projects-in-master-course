package minimax.model;

import minimax.specific.Player;


/**
 * Player is min player (id = 1)
 * @author luongnv89
 *
 */
public class MinPlayer extends Player {

	public MinPlayer(XOBoard board) {
		super(board, 1);
	}
}
