package minimax.specific;

import minimax.model.XOBoard;

/**
 * The class player
 * 
 * @author luongnv89
 * 
 */
public class Player {
	protected int id;
	protected XOBoard board;
	int m;

	/**
	 * Create a player
	 * 
	 * @param otherBoard
	 *            Board
	 * @param otherID
	 *            Id of player
	 */
	public Player(XOBoard otherBoard, int otherID) {
		this.id = otherID;
		this.board = new XOBoard(otherBoard);
	}

	/**
	 * Play one step
	 */
	public void run() {
		if (id == 1)
			m = 1000;
		else
			m = -1000;
		int x = 0, y = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board.getStates(i, j) == 0) {
					board.update(i, j, id);
					if ((id == 1 && m > board.getScore())
							|| (id == 2 && m < board.getScore())) {
						m = board.getScore();
						x = i;
						y = j;
						if (board.getWinner() != 0) {
							break;
						}
					}
					board.reset(i, j);
				}
			}
			if (board.getWinner() != 0) {
				break;
			}
		}
		if (board.getWinner() == 0) {
			board.update(x, y, id);
		}

	};

	/**
	 * Get the id of player
	 * 
	 * @return 1 - Min player, 2 - Max player, id of Hand player belong to the enemies
	 */
	public int getId() {
		return id;
	};
}
