package minimax.specific;

/**
 * The Board specific
 * 
 * @author luongnv89
 * 
 */
public class Board {
	protected int[][] states;
	int side;

	/**
	 * Initial a board without any states
	 * 
	 * @param side
	 *            The side of board
	 */
	public Board(int side) {
		this.side = side;
		states = new int[side][side];
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				states[i][j] = 0;
			}
		}
	}

	/**
	 * Create a board with the state of board is given by user
	 * 
	 * @param s
	 *            The state of board
	 * @param side
	 *            The side of board
	 */
	public Board(int[][] s, int side) {
		this.side = side;
		states = new int[side][side];
		if (side != Math.sqrt(s.length)) {
			System.out.println("Invalid side!");
		} else {
			for (int i = 0; i < side; i++) {
				for (int j = 0; j < side; j++) {
					if (s[i][j] != 1 && s[i][j] != 2)
						System.out.println("Wrong player!");
					else
						states[i][j] = s[i][j];
				}
			}
		}
	}

	/**
	 * Create a board from another board
	 * 
	 * @param otherboard
	 */
	public Board(Board otherboard) {
		this.side = otherboard.side;
		this.states = otherboard.states;
	}

	/**
	 * Create a board with the state of board is given by user
	 * 
	 * @param s
	 *            The state of board
	 * @param side
	 *            The side of board
	 */
	public Board(int[] s, int side) {
		this.side = side;
		states = new int[side][side];
		if (side != Math.sqrt(s.length)) {
			System.out.println("Invalid side!");
		} else {
			for (int i = 0; i < side; i++) {
				for (int j = 0; j < side; j++) {
					if (s[i * side + j] == 1 || s[i * side + j] == 2
							|| s[i * side + j] == 0)
						states[i][j] = s[i * side + j];
					else
						System.out.println("Wrong player!");
				}
			}
		}
	}

	/**
	 * Update the state of board
	 * 
	 * @param x
	 *            The x-cordinator
	 * @param y
	 *            The y-cordinator
	 * @param player
	 *            The player
	 */
	public void update(int x, int y, int player) {
		if (x >= side || x < 0 || y > side || y < 0)
			System.out.println("Out of board!");
		else {
			if (states[x][y] != 0)
				System.out.println("Error! This piece already had player!");
			else {
				if (player == 1 || player == 2)
					states[x][y] = player;
				else {
					System.out.println("Error! Wrong player!");
				}
			}
		}
	}

	/**
	 * Show the state of board
	 */
	public void show() {
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				System.out.print(" " + states[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Check a board is valid or invalid
	 * 
	 * @return True if the board is valid
	 */
	public boolean valid() {
		int nP1 = 0;
		int nP2 = 0;
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				if (states[i][j] == 0 || states[i][j] == 1 || states[i][j] == 2) {
					if (states[i][j] == 1)
						nP1++;
					if (states[i][j] == 2)
						nP2++;
				} else {
					System.out.println("Wrong piece!");
					return false;
				}
			}
		}
		if (Math.abs(nP1 - nP2) > 1) {
			System.out.println("Wrong turn!");
			return false;
		}
		return true;
	}

	/**
	 * Get the value of a piece on board
	 * 
	 * @param i
	 * @param j
	 * @return player
	 */
	public int getStates(int i, int j) {
		return states[i][j];
	}

	/**
	 * Reset the value of a piece
	 * 
	 * @param x
	 * @param y
	 */
	public void reset(int x, int y) {
		this.states[x][y] = 0;
	}

}
