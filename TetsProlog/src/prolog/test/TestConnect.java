package prolog.test;

import jpl.Query;

public class TestConnect {

	public static void main(String[] args) {
		Query q;
		q = new Query("consult('foo.pl')");
		System.err.println(q.hasSolution());
		q = new Query("foo(a)");
		System.err.println(q.hasSolution());
		q = new Query("foo(b)");
		System.err.println(q.hasSolution());
		q = new Query("foo(c)");
		System.err.println(q.hasSolution());
		q = new Query("foo(X)");
		System.err.println(q.hasSolution());

		while (q.hasMoreElements()) {
			System.err.println(q.nextElement());
		}
		System.out.println(System.getProperties());
	}
}