/**
 * 
 */
package tools;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * StoreException when doing pop(),push() or head() <br>
 * 
 * @author luongnv89
 * 
 */
public class StoreException extends Exception {
	/**
	 * File to save exception log
	 */
	static String logFile = "log.txt";

	/**
	 * @param logFile
	 *            the logFile to set
	 */
	public static void setLogFile(String path) {
		logFile = path;
	}

	public StoreException(String message, UnReliableException action) {
		// System.out.println(String.valueOf(hashCode)
		// + "\tUnReliableStackException: " + String.valueOf(action));
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(logFile,
					true));
			out.write(message + ":" + action + "\n");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write an {@link StoreException} to log file
	 * 
	 * @param message
	 * @param action
	 */
	public void writeException(String message, UnReliableException action) {
		// System.out.println(String.valueOf(hashCode)
		// + "\tUnReliableStackException: " + String.valueOf(action));
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(logFile,
					true));
			out.write(message + ":" + action + "\n");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Type of StoreException
	 * 
	 * @author luongnv89
	 * 
	 */
	public enum UnReliableException {
		POP, PUSH, HEAD
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
