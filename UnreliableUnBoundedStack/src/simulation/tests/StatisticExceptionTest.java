/**
 * 
 */
package simulation.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simulation.StatisticException;

/**
 * Test for {@link StatisticException}
 * @author luongnv89
 * 
 */
public class StatisticExceptionTest {
	StatisticException statisticException;
	String objectID = "123ID";

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		statisticException = new StatisticException(objectID);
	}


	/**
	 * Test method for
	 * {@link simulation.StatisticException#addException(java.lang.String)}
	 * .
	 */
	@Test
	public void testAddException() {
		System.out.println(statisticException.toString());
		statisticException.addException(objectID+" Exception POP");
		System.out.println(statisticException.toString());
		statisticException.addException(objectID+" Exception PUSH");
		System.out.println(statisticException.toString());
		statisticException.addException(objectID+" Exception HEAD");
		System.out.println(statisticException.toString());
	}

	/**
	 * Test method for {@link simulation.StatisticException#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(statisticException.toString());
	}

	/**
	 * Test method for {@link simulation.StatisticException#getObjectID()}
	 * .
	 */
	@Test
	public void testGetObjectID() {
		assertTrue(objectID.equals(statisticException.getObjectID()));
	}

}
