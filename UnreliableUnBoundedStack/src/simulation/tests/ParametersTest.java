/**
 * 
 */
package simulation.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import simulation.Parameters;

/**
 * Test for {@link Parameters}
 * @author luongnv89
 * 
 */
public class ParametersTest {
	Parameters param;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		param = new Parameters(100, 200, 300, 4, 5, 6);
	}

	/**
	 * Test method for {@link simulation.Parameters#toString()}.
	 */
	@Test
	public void testToString() {
		System.out.println(param.toString());
	}

	/**
	 * Test method for {@link simulation.Parameters#getPushProblemFreq()}.
	 */
	@Test
	public void testGetPushProblemFreq() {
		assertEquals(100, param.getPushProblemFreq());
	}

	/**
	 * Test method for {@link simulation.Parameters#getPopProblemFreq()}.
	 */
	@Test
	public void testGetPopProblemFreq() {
		assertEquals(200, param.getPopProblemFreq());
	}

	/**
	 * Test method for {@link simulation.Parameters#getHeadProblemFreq()}.
	 */
	@Test
	public void testGetHeadProblemFreq() {
		assertEquals(300, param.getHeadProblemFreq());
	}

	/**
	 * Test method for {@link simulation.Parameters#getDoPushFreq()}.
	 */
	@Test
	public void testGetDoPushFreq() {
		assertEquals(4, param.getDoPushFreq());
	}

	/**
	 * Test method for {@link simulation.Parameters#getDoPopFreq()}.
	 */
	@Test
	public void testGetDoPopFreq() {
		assertEquals(5, param.getDoPopFreq());
	}

	/**
	 * Test method for {@link simulation.Parameters#getDoHeadFreq()}.
	 */
	@Test
	public void testGetDoHeadFreq() {
		assertEquals(6, param.getDoHeadFreq());
	}

}
