/**
 * 
 */
package simulation.tests;

import org.junit.Before;
import org.junit.Test;

import simulation.EnvironmentConfig;

/**
 * Test for {@link EnvironmentConfig}
 * 
 * @author luongnv89
 * 
 */
public class EnvironmentConfigTest {
	EnvironmentConfig environmentConfig;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		environmentConfig = new EnvironmentConfig();
	}

	/**
	 * Test method for
	 * {@link simulation.EnvironmentConfig#getListParameters()}.
	 */
	@Test
	public void testGetListParameters() {
		System.out.println("Test get list Parameters:");
		for (int i = 0; i < environmentConfig.getListParameters().size(); i++) {
			System.out.println(i + ": "
					+ environmentConfig.getListParameters().get(i).toString());
		}
	}

//	/**
//	 * Test method for
//	 * {@link stack.simulation.EnvironmentConfig#chooseEnvironment()}.
//	 */
//	@Test
//	public void testChooseEnvironment() {
//		int choose = environmentConfig.chooseEnvironment();
//		if (choose != environmentConfig.getListParameters().size())
//			System.out.println("Your choice: "
//					+ choose
//					+ ": "
//					+ environmentConfig.getListParameters().get(choose)
//							.toString());
//	}

}
