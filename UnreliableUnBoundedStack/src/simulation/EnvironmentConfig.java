/**
 * 
 */
package simulation;

import java.util.ArrayList;
import java.util.Scanner;

import simulation.tests.EnvironmentConfigTest;
import specification.StoreAbstract;

/**
 * Manage the paramenters of testing environment. <br>
 * Test by {@link EnvironmentConfigTest}
 * 
 * @author luongnv89
 * 
 */
public class EnvironmentConfig {
	/**
	 * Number of times the method {@link StoreAbstract#doSomething()} is called
	 */
	/**
	 * default problem frequency of {@link StoreAbstract#POP_PROBLEM_FREQ} and
	 * {@link StoreAbstract#PUSH_PROBLEM_FREQ} and
	 * {@link StoreAbstract#HEAD_PROBLEM_FREQ}
	 */
	private int problemFrequency = 1000;
	/**
	 * maximum problem frequency of {@link StoreAbstract#POP_PROBLEM_FREQ} and
	 * {@link StoreAbstract#PUSH_PROBLEM_FREQ} and
	 * {@link StoreAbstract#HEAD_PROBLEM_FREQ}
	 */
	private int problemMaxFrequency = 10000;
	/**
	 * default frequency of {@link StoreAbstract#DO_PUSH_FREQ} and
	 * {@link StoreAbstract#DO_POP_FREQ} and {@link StoreAbstract#DO_HEAD_FREQ}
	 */
	private int doFrequency = 1;
	/**
	 * maximum frequency of {@link StoreAbstract#DO_PUSH_FREQ} and
	 * {@link StoreAbstract#DO_POP_FREQ} and {@link StoreAbstract#DO_HEAD_FREQ}
	 */
	private int doMaxFrequency = 10;

	/**
	 * List all testing enviroment parameters which used to test
	 */
	ArrayList<Parameters> listParameters = new ArrayList<>();

	/**
	 * 
	 */
	public EnvironmentConfig() {
		createParameters();
	}

	/**
	 * Create {@link EnvironmentConfig#listParameters}
	 */
	private void createParameters() {

		// PROBLEM FREQUENCY
		listParameters.add(new Parameters(Integer.MAX_VALUE, Integer.MAX_VALUE,
				Integer.MAX_VALUE, doFrequency, doFrequency, doFrequency));

		listParameters.add(new Parameters(problemFrequency, Integer.MAX_VALUE,
				Integer.MAX_VALUE, doFrequency, doFrequency, doFrequency));
		listParameters.add(new Parameters(Integer.MAX_VALUE, problemFrequency,
				Integer.MAX_VALUE, doFrequency, doFrequency, doFrequency));
		listParameters.add(new Parameters(Integer.MAX_VALUE, Integer.MAX_VALUE,
				problemFrequency, doFrequency, doFrequency, doFrequency));

		listParameters.add(new Parameters(problemFrequency, problemFrequency,
				problemFrequency, doFrequency, doFrequency, doFrequency));

		listParameters.add(new Parameters(problemMaxFrequency,
				problemFrequency, problemFrequency, doFrequency, doFrequency,
				doFrequency));
		listParameters.add(new Parameters(problemFrequency,
				problemMaxFrequency, problemFrequency, doFrequency,
				doFrequency, doFrequency));
		listParameters.add(new Parameters(problemFrequency, problemFrequency,
				problemMaxFrequency, doFrequency, doFrequency, doFrequency));

		// DO FREQUENCY
		listParameters.add(new Parameters(problemFrequency, problemFrequency,
				problemFrequency, doMaxFrequency, doFrequency, doFrequency));
		listParameters.add(new Parameters(problemFrequency, problemFrequency,
				problemFrequency, doFrequency, doMaxFrequency, doFrequency));
		listParameters.add(new Parameters(problemFrequency, problemFrequency,
				problemFrequency, doFrequency, doFrequency, doMaxFrequency));

	}

	/**
	 * @return the listParameters
	 */
	public ArrayList<Parameters> getListParameters() {
		return listParameters;
	}

	/**
	 * Choose testing environment parameter to test
	 * 
	 * @return
	 */
	public int chooseEnvironment() {
		boolean ok = false;
		int env = 0;

		for (int i = 0; i < listParameters.size(); i++) {
			System.out.println(i + ": " + listParameters.get(i).toString());
		}
		System.out.println(listParameters.size()
				+ ": Test for all configurations");
		while (!ok) {
			System.out.print("Choose the environment for test:");
			Scanner in = new Scanner(System.in);
			env = in.nextInt();
			if (env < 0 || env > listParameters.size()) {
				System.out.println("Input must be bigger than 0 and less than "
						+ listParameters.size());
				System.out.println("Please input again!");
			} else {
				ok = true;
			}
		}
		return env;
	}

}
