package simulation;

import simulation.tests.ParametersTest;
import specification.StoreAbstract;

/**
 * The environment parameters for testing <br>
 * Test by {@link ParametersTest}
 * 
 * @author luongnv89
 * 
 */
public class Parameters {
	/**
	 * See {@link StoreAbstract#PUSH_PROBLEM_FREQ}
	 */
	int pushProblemFreq;
	/**
	 * See {@link StoreAbstract#POP_PROBLEM_FREQ}
	 */
	int popProblemFreq;
	/**
	 * See {@link StoreAbstract#HEAD_PROBLEM_FREQ}
	 */
	int headProblemFreq;
	/**
	 * See {@link StoreAbstract#DO_PUSH_FREQ}
	 */
	int doPushFreq;
	/**
	 * See {@link StoreAbstract#DO_POP_FREQ}
	 */
	int doPopFreq;
	/**
	 * See {@link StoreAbstract#HEAD_PUSH_FREQ}
	 */
	int doHeadFreq;

	/**
	 * Create a parameter
	 * @param pushProblemFreq
	 * @param popProblemFreq
	 * @param headProblemFreq
	 * @param doPushFreq
	 * @param doPopFreq
	 * @param doHeadFreq
	 */
	public Parameters(int pushProblemFreq, int popProblemFreq,
			int headProblemFreq, int doPushFreq, int doPopFreq, int doHeadFreq) {
		this.pushProblemFreq = pushProblemFreq;
		this.popProblemFreq = popProblemFreq;
		this.headProblemFreq = headProblemFreq;
		this.doPushFreq = doPushFreq;
		this.doPopFreq = doPopFreq;
		this.doHeadFreq = doHeadFreq;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "pushProb:" + pushProblemFreq + "\t popProb:" + popProblemFreq
				+ "\t headProb:" + headProblemFreq + "\t doPush:" + doPushFreq
				+ "\t doPop:" + doPopFreq + "\t doHead:" + doHeadFreq;
	}

	/**
	 * @return the pushProblemFreq
	 */
	public int getPushProblemFreq() {
		return pushProblemFreq;
	}

	/**
	 * @return the popProblemFreq
	 */
	public int getPopProblemFreq() {
		return popProblemFreq;
	}

	/**
	 * @return the headProblemFreq
	 */
	public int getHeadProblemFreq() {
		return headProblemFreq;
	}

	/**
	 * @return the doPushFreq
	 */
	public int getDoPushFreq() {
		return doPushFreq;
	}

	/**
	 * @return the doPopFreq
	 */
	public int getDoPopFreq() {
		return doPopFreq;
	}

	/**
	 * @return the doHeadFreq
	 */
	public int getDoHeadFreq() {
		return doHeadFreq;
	}
}
