/**
 * 
 */
package simulation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import models.QueueMainTemp;
import models.QueuePair;
import models.QueuePopPush;
import models.ReliableStack;
import models.ShareEmptyStack;
import models.UnreliableStack;
import specification.StoreAbstract;
import tools.DateHeader;
import tools.StoreException;

/**
 * Simulation to test
 * 
 * @author luongnv89
 * 
 */
public class Simulation {
	/**
	 * Number of action {@link StoreAbstract#doSomething()} or
	 * {@link QueuePair#doSomething()}
	 */
	private static final int NUMBER_TEST = 1000000;
	/**
	 * Number element of initial Store, or queue
	 */
	private static final int NUMBER_ELEMENTS_INITIAL = 1000;
	/**
	 * see {@link EnvironmentConfig}
	 */
	static EnvironmentConfig environmentConfig;
	/**
	 * Path to save statistic result
	 */
	static String statisticFile = "Statistic.txt";

	// public static ArrayList<StatisticException> listStatisticException = new
	// ArrayList<>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		environmentConfig = new EnvironmentConfig();
		int type;
		int env;

		if (args.length == 0) {
			type = chooseTypeOfStore();

			env = environmentConfig.chooseEnvironment();
		} else {
			type = Integer.parseInt(args[0]);
			env = Integer.parseInt(args[1]);
		}

		if (env == environmentConfig.getListParameters().size()) {
			for (int i = 0; i < environmentConfig.getListParameters().size(); i++) {
				System.out.println("Configuration test: "
						+ environmentConfig.getListParameters().get(i)
								.toString());
				writeToFile("\n******************", statisticFile);
				writeToFile("Configuration test: "
						+ environmentConfig.getListParameters().get(i)
								.toString(), statisticFile);
				runTest(type, environmentConfig.getListParameters().get(i));
			}
		} else {
			System.out
					.println("Configuration test: "
							+ environmentConfig.getListParameters().get(env)
									.toString());
			writeToFile("\n******************", statisticFile);
			writeToFile(
					"Configuration test: "
							+ environmentConfig.getListParameters().get(env)
									.toString(), statisticFile);
			runTest(type, environmentConfig.getListParameters().get(env));
		}
	}

	/**
	 * Run 5 test for a type of Store with a specific parameter <br>
	 * For each type of Store and parameter, do 5 different test
	 * 
	 * @param type
	 * @param param
	 *            see {@link Parameters}
	 */
	public static void runTest(int type, Parameters param) {
		StoreAbstract.setupProblemFrequencies(param.getPushProblemFreq(),
				param.getPopProblemFreq(), param.getHeadProblemFreq());
		StoreAbstract.setupDoFrequencies(param.getDoPushFreq(),
				param.getDoPopFreq(), param.getDoHeadFreq());
		for (int i = 0; i < 5; i++) {
			System.out.println("Testing ... " + i);
			writeToFile("--------------\n", statisticFile);
			writeToFile("Testing ... " + i, statisticFile);
			runTestUnReliable(type);
		}
	}

	/**
	 * Run a test for a type of Store with a specific parameter
	 * 
	 * @param type
	 */
	private static void runTestUnReliable(int type) {
		ShareEmptyStack.setupShareStack(new ReliableStack());
		QueuePair queuePair = null;
		StoreAbstract store = null;

		if (type == 10) {
			ShareEmptyStack.setupShareStack(new UnreliableStack());
			queuePair = new QueuePair(new QueuePopPush(new UnreliableStack(
					initialElements())), new QueuePopPush(new UnreliableStack(
					initialElements())));
			System.out.println("Test unreliable QueuePair");
			writeToFile("Test unreliable QueuePair", statisticFile);
		} else {
			store = createStore(type);
		}

		String logFile = null;
		if (type != 10)
			logFile = "log_" + String.valueOf(store.hashCode()) + ".txt";
		else
			logFile = "log_" + String.valueOf(queuePair.hashCode()) + ".txt";
		StoreException.setLogFile(logFile);

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(logFile,
					true));
			out.write(DateHeader.dateString() + "\n");
			System.out.println("Exception loggin at: " + logFile);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		long startTime = System.currentTimeMillis();
		for (int i = 0; i < NUMBER_TEST; i++) {
			if (type != 10)
				store.doSomething();
			else {
				queuePair.doSomething();
			}
		}
		System.out.println("Total time: "
				+ String.valueOf(System.currentTimeMillis() - startTime));
		countStatisticException(logFile);
	}

	/**
	 * Create store to test
	 * 
	 * @param type
	 *            type of Store
	 * @return the store to test
	 */
	private static StoreAbstract createStore(int type) {
		StoreAbstract store = null;
		switch (type) {
		case 1:
			store = new UnreliableStack(initialElements());
			System.out.println("Test UnReliableStack");
			writeToFile("Test UnReliableStack", statisticFile);
			break;
		case 2:
			store = new QueuePopPush(initialElements());
			System.out.println("Test reliable QueuePopPush");
			writeToFile("Test reliable QueuePopPush", statisticFile);
			break;
		case 3:
			store = new QueuePopPush(new UnreliableStack(initialElements()));
			System.out
					.println("Test unreliable QueuePopPush: Component stack is unreliable");
			writeToFile(
					"Test unreliable QueuePopPush: Component stack is unreliable",
					statisticFile);
			break;
		case 4:
			store = new QueuePopPush(initialElements());
			ShareEmptyStack.setupShareStack(new UnreliableStack());
			System.out
					.println("Test unreliable QueuePopPush: Aggreatation stack is unreliable");
			writeToFile(
					"Test unreliable QueuePopPush: Aggreatation stack is unreliable",
					statisticFile);
			break;
		case 5:
			store = new QueuePopPush(new UnreliableStack(initialElements()));
			ShareEmptyStack.setupShareStack(new UnreliableStack());
			System.out
					.println("Test unreliable QueuePopPush: Two stacks are unreliable");
			writeToFile(
					"Test unreliable QueuePopPush: Two stacks are unreliable",
					statisticFile);
			break;
		case 6:
			store = new QueueMainTemp(initialElements());
			System.out.println("Test reliable QueueMainTemp");
			writeToFile("Test reliable QueueMainTemp", statisticFile);
			break;
		case 7:
			store = new QueueMainTemp(new UnreliableStack(initialElements()));
			System.out
					.println("Test unreliable QueueMainTemp: Component stack is unreliable");
			writeToFile(
					"Test unreliable QueueMainTemp: Component stack is unreliable",
					statisticFile);
			break;
		case 8:
			store = new QueueMainTemp(initialElements());
			ShareEmptyStack.setupShareStack(new UnreliableStack());
			System.out
					.println("Test unreliable QueueMainTemp: Aggreatation stack is unreliable");
			writeToFile(
					"Test unreliable QueueMainTemp: Aggreatation stack is unreliable",
					statisticFile);
			break;
		case 9:
			store = new QueueMainTemp(new UnreliableStack(initialElements()));
			ShareEmptyStack.setupShareStack(new UnreliableStack());
			System.out
					.println("Test unreliable QueueMainTemp: Two stacks are unreliable");
			writeToFile(
					"Test unreliable QueueMainTemp: Two stacks are unreliable",
					statisticFile);
			break;
		default:
			System.out.println("Wrong type:" + type);
			break;
		}
		return store;
	}

	/**
	 * Choose type of store from input
	 * 
	 * @return
	 */
	private static int chooseTypeOfStore() {
		boolean ok = false;
		int type = 0;
		System.out.println("1: Test UnReliableStack");
		System.out.println("2: Test reliable QueuePopPush");
		System.out
				.println("3: Test unreliable QueuePopPush: Component stack is unreliable");
		System.out
				.println("4: Test unreliable QueuePopPush: Aggreatation stack is unreliable");
		System.out
				.println("5: Test unreliable QueuePopPush: Two stacks are unreliable");
		System.out.println("6: Test reliable QueueMainTemp");
		System.out
				.println("7: Test unreliable QueueMainTemp: Component stack is unreliable");
		System.out
				.println("8: Test unreliable QueueMainTemp: Aggreatation stack is unreliable");
		System.out
				.println("9: Test unreliable QueueMainTemp: Two stacks are unreliable");
		System.out.println("10: Test unreliable QueuePair");
		while (!ok) {
			System.out.print("Choose the type of store for test:");
			Scanner in = new Scanner(System.in);
			type = in.nextInt();
			if (type < 0 || type >= 11) {
				System.out
						.println("Input must be bigger than 0 and less than 11");
				System.out.println("Please input again!");
			} else {
				ok = true;
			}
		}
		return type;
	}

	/**
	 * Initial an Integer array
	 * 
	 * @return
	 */
	private static int[] initialElements() {
		int[] array = new int[NUMBER_ELEMENTS_INITIAL];
		for (int i = 0; i < array.length; i++) {
			array[i] = (new Random()).nextInt(10);
		}
		return array;
	}

	/**
	 * Write a string to a file
	 * 
	 * @param string
	 * @param statisticFile
	 */
	private static void writeToFile(String string, String statisticFile) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(
					statisticFile, true));
			out.write(string + "\n");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Analyze statistic of exception in an input file
	 * @param path
	 */
	public static void countStatisticException(String path) {
		ArrayList<StatisticException> listStatisticException = new ArrayList<>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line = br.readLine();
			int nbLine = 0;
			while (line != null) {
				nbLine++;
				if (nbLine >= 5) {
					boolean exist = false;
					for (int i = 0; i < listStatisticException.size(); i++) {
						if (line.contains(listStatisticException.get(i)
								.getObjectID())) {
							listStatisticException.get(i).addException(line);
							exist = true;
							break;
						}
					}
					if (!exist) {
						String id = line.replace(":POP", "")
								.replace(":PUSH", "").replace(":HEAD", "");
						StatisticException newStatisticException = new StatisticException(
								id);
						newStatisticException.addException(line);
						listStatisticException.add(newStatisticException);
					}
				}
				line = br.readLine();
			}
			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		// System.out.println("Statistic of exception at: " + path);
		saveStatistic(listStatisticException, path);
	}

	/**
	 * Write statistic analyze to a file
	 * @param listStatisticException
	 * @param path
	 */
	private static void saveStatistic(
			ArrayList<StatisticException> listStatisticException, String path) {
		writeToFile("Statistic analyze: ", statisticFile);
		for (int i = 0; i < listStatisticException.size(); i++) {
			writeToFile(listStatisticException.get(i).toString(), statisticFile);
			// System.out.println(listStatisticException.get(i).toString());
		}

	}

}
