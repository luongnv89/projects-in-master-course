/**
 * 
 */
package simulation;

import simulation.tests.StatisticExceptionTest;
import tools.StoreException.UnReliableException;

/**
 * Present the statistic Exception of an Object <br>
 * Test by {@link StatisticExceptionTest}
 * 
 * @author luongnv89
 * 
 */
public class StatisticException {
	/**
	 * Id of object, created by composing type of object and hascode() of
	 * object.
	 */
	String objectID;
	/**
	 * Total number of push() exception
	 */
	int nbPushProblem;
	/**
	 * Total number of pop() exception
	 */
	int nbPopProblem;
	/**
	 * Total number of head() exception
	 */
	int nbHeadProblem;
	/**
	 * Total number of unknown exception
	 */
	int unknownProblem;

	/**
	 * @param objectID
	 */
	public StatisticException(String objectID) {
		this.objectID = objectID;
		nbHeadProblem = 0;
		nbPopProblem = 0;
		nbPushProblem = 0;
	}

	/**
	 * Add new exception of object
	 * 
	 * @param exception
	 */
	public void addException(String exception) {
		if (exception.contains(String.valueOf(UnReliableException.PUSH)))
			nbPushProblem++;
		else if (exception.contains(String.valueOf(UnReliableException.POP)))
			nbPopProblem++;
		else if (exception.contains(String.valueOf(UnReliableException.HEAD)))
			nbHeadProblem++;
		else
			unknownProblem++;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return objectID + " \t " + nbPushProblem + " \t " + nbPopProblem
				+ " \t " + nbHeadProblem + " \t " + unknownProblem + "\n";
	}

	/**
	 * @return id of object
	 */
	public String getObjectID() {
		// TODO Auto-generated method stub
		return this.objectID;
	}

}
