package specification;

import tests.StoreInterfaceTest;
import tools.InvariantBroken;
import tools.StoreException;

/**
 * Store interface
 * <br>Test by {@link StoreInterfaceTest}
 * @author konal89
 * 
 */
public interface StoreInterface {

	/**
	 * Push an element into store
	 * 
	 * @param x
	 *            element
	 * @throws StoreException 
	 */
	public void push(int x) throws StoreException;

	/**
	 * Get the head element of store
	 * 
	 * @return value of the head element <li>0: if the store is empty
	 * @throws StoreException 
	 * @throws InvariantBroken
	 */
	public int head() throws StoreException;

	/**
	 * Remove the head element of store If the store is empty, this method will
	 * do nothing
	 * @throws StoreException 
	 * 
	 */
	public void pop() throws StoreException;

	/**
	 * Get the number of elements of store
	 * 
	 * @return the number of elements of store <li>0 if store is empty
	 */
	public int getNumberOfElement();

	/**
	 * Check a store is empty or not
	 * 
	 * @return true if store empty <li>false otherwise
	 */
	public boolean isEmpty();

	/**
	 * Check a store is invariant or not
	 * 
	 * @return true if store is invariant
	 */
	public boolean invariant();

	/**
	 * Show the content of store, <li>for example: {1,2,3,4,5} <li>if store is a
	 * stack -> head = 5 <li>if store is a queue -> head = 1
	 */
	public void show();

}
