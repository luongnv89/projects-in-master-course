package specification;

import java.util.Random;

import tools.StoreException;

/**
 * Store abstract
 * @author konal89
 * 
 */
public abstract class StoreAbstract implements StoreInterface {

	/**
	 * The number of elements of store
	 */
	protected int numberOfElement;

	/**
	 * Create an empty store
	 */
	public StoreAbstract() {
		numberOfElement = 0;
	}

	public int getNumberOfElement() {
		return numberOfElement;
	}

	public boolean isEmpty() {
		return getNumberOfElement() == 0;
	}

	// Statistic part
	/**
	 * How often, on average, the {@link UnreliableBoundedQueue1#push} method
	 * has a problematic delay.<br>
	 * Every time push is called, there is a probability of
	 * <code>  1/PUSH_PROBLEM_FREQ </code> that the execution is delayed by
	 * {@link UnreliableBoundedQueue1#DELAY} milliseconds.
	 */
	/**
	 * Frequency of push() exception.
	 */
	public static int PUSH_PROBLEM_FREQ = 0;
	/**
	 * Frequency of pop() exception.
	 */
	public static int POP_PROBLEM_FREQ = 0;
	/**
	 * Frequency of head() exception.
	 */
	public static int HEAD_PROBLEM_FREQ = 0;

	/**
	 * Frequency of doing push() when call {@link StoreAbstract#doSomething()}
	 */
	public static int DO_PUSH_FREQ = 1;
	/**
	 * Frequency of doing pop() when call {@link StoreAbstract#doSomething()}
	 */
	public static int DO_POP_FREQ = 1;
	/**
	 * Frequency of doing head() when call {@link StoreAbstract#doSomething()}
	 */
	public static int DO_HEAD_FREQ = 1;

	/**
	 * When call {@link StoreAbstract#doSomething()} is call, Store will do
	 * anything, which is depend of the frequency of push(), pop(),head()
	 */
	public void doSomething() {
		int total = DO_PUSH_FREQ + DO_POP_FREQ + DO_HEAD_FREQ;
		int action = (new Random()).nextInt(100);
		if (action < DO_PUSH_FREQ * 100 / total) {
			try {
				push(1);
			} catch (StoreException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		} else if (action < (DO_PUSH_FREQ + DO_POP_FREQ) * 100 / total) {
			try {
				pop();
			} catch (StoreException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		} else {
			try {
				head();
			} catch (StoreException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		}

	}

	/**
	 * Setup problem frequency
	 * @param pushFrequency
	 * @param popFrequency
	 * @param headFrequency
	 */
	public static void setupProblemFrequencies(int pushFrequency,
			int popFrequency, int headFrequency) {
		PUSH_PROBLEM_FREQ = pushFrequency;
		POP_PROBLEM_FREQ = popFrequency;
		HEAD_PROBLEM_FREQ = headFrequency;
	}

	/**
	 * Setup doing something frequency
	 * @param pushF
	 * @param popF
	 * @param headF
	 */
	public static void setupDoFrequencies(int pushF, int popF, int headF) {
		DO_PUSH_FREQ = pushF;
		DO_POP_FREQ = popF;
		DO_HEAD_FREQ = headF;
	}

	// public void problemFrequency(int frequency)
	// throws StoreException {
	// Random RNG = new Random();
	// if (RNG.nextInt(frequency) == 0)
	// throw new StoreException();
	// }
}
