package specification;

import models.ReliableStack;
import models.ShareEmptyStack;
import tools.StoreException;

/**
 * Queue is implemented from two stack
 * 
 * @author luongnv89
 * 
 */
public abstract class QueueAbstract extends StoreAbstract {

	/**
	 * Component stack of queue
	 */
	protected ReliableStack qStack;

	/**
	 * @param qStack
	 */
	public QueueAbstract(ReliableStack qStack) {
		this.qStack = qStack;
		numberOfElement = qStack.getNumberOfElement();
	}

	/**
	 * Default constructor, the queue is empty
	 */
	public QueueAbstract() {
		super();
		qStack = new ReliableStack();
	}

	/**
	 * Constructor a store with the size and the elements is input by user <li>
	 * if the input size is unbound -> default size <li>if the length of array
	 * of elements is bigger than the size of store -> store is empty
	 * 
	 * @param elems
	 *            The array of elements
	 * @param size2
	 *            the size of store
	 */
	public QueueAbstract(int[] elem) {
		qStack = new ReliableStack(elem);
		numberOfElement = elem.length;
	}

	@Override
	public void show() {
		qStack.show();

	}

	/**
	 * Move all elements from stack of queue to {@link ShareEmptyStack#shareStack}
	 * @throws StoreException
	 */
	public void moveToShareStack() throws StoreException {
		while (!qStack.isEmpty()) {
			ShareEmptyStack.shareStack.push(qStack.head());
			qStack.pop();
			numberOfElement--;
		}
	}

	/**
	 * Push back all elements from {@link ShareEmptyStack#shareStack} to stack of queue
	 * @throws StoreException
	 */
	public void pushBackFromShareStack() throws StoreException {
		while (!ShareEmptyStack.shareStack.isEmpty()) {
			qStack.push(ShareEmptyStack.shareStack.head());
			numberOfElement++;
			ShareEmptyStack.shareStack.pop();
		}
	}

}
