/**
 * 
 */
package models;

import tests.IntPairTest;

/**
 * Pair of two Integer number
 * Test by {@link IntPairTest}
 * @author luongnv89
 * 
 */
public class IntPair {

	/**
	 * First Integer number
	 */
	int first;
	/**
	 * Second Integer number
	 */
	int second;

	/**
	 * @param first
	 * @param second
	 */
	public IntPair(int first, int second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * @return the first
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * @return the second
	 */
	public int getSecond() {
		return second;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "(" + first + "," + second + ")";
	}

	/**
	 * Compare two {@link IntPair}
	 * @param other
	 * @return true if two {@link IntPair} have the same value of {@link IntPair#first} and {@link IntPair#second}
	 * <br>false otherwise
	 */
	public boolean equals(IntPair other) {
		return (getFirst() == other.getFirst())
				&& (getSecond() == other.getSecond());
	}

}
