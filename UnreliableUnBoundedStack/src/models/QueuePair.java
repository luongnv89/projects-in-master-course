/**
 * 
 */
package models;

import java.util.Random;

import specification.QueueAbstract;
import specification.StoreAbstract;
import tests.QueuePairReliableTest;
import tools.HasInvariant;
import tools.StoreException;
import tools.StoreException.UnReliableException;

/**
 * Implement a Queue of integer paris, using two {@link QueueAbstract}.
 * <br>Test by {@link QueuePairReliableTest}
 * @author luongnv89
 * 
 */
public class QueuePair implements HasInvariant {

	/**
	 * First queue of QueuePair
	 */
	QueueAbstract first;
	/**
	 * Second queue of QueuePair
	 */
	QueueAbstract second;

	/**
	 * @param first
	 * @param second
	 */
	public QueuePair(QueueAbstract first, QueueAbstract second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * Push an integer pair into QueuePair
	 * @param p an {@link IntPair} to push into QueuePair
	 * @throws StoreException if there is some exception when two queue of QueuePair push
	 */
	public void push(IntPair p) throws StoreException{
		try {
			first.push(p.getFirst());
			second.push(p.getSecond());
		} catch (StoreException e) {
			// System.out.println("QueuePairException: "
			// + UnReliableException.PUSH);
			e.writeException("QueuePair \t " + this.hashCode(),
					UnReliableException.PUSH);
			System.out.println("QueuePair \t " + this.hashCode() + ": "
					+ UnReliableException.PUSH);
		}

	}

	/**
	 * Get the head element of QueuePair
	 * @return an {@link IntPair} which is at the head of QueuePair
	 * @throws StoreException
	 */
	public IntPair head() throws StoreException {
		try {
			return new IntPair(first.head(), second.head());
		} catch (StoreException e) {
			e.writeException("QueuePair \t " + this.hashCode(),
					UnReliableException.HEAD);
			System.out.println("QueuePair \t " + this.hashCode() + ": "
					+ UnReliableException.HEAD);
			// System.out.println("QueuePairException: "
			// + UnReliableException.HEAD);
			return null;
		}
	}

	/**
	 * pop an element from QueuePair
	 * @throws StoreException
	 */
	public void pop()  throws StoreException{
		try {
			first.pop();
			second.pop();
		} catch (StoreException e) {
			e.writeException("QueuePair \t " + this.hashCode(),
					UnReliableException.POP);
			System.out.println("QueuePair \t " + this.hashCode() + ": "
					+ UnReliableException.POP);
			// System.out
			// .println("QueuePairException: " + UnReliableException.POP);
		}

	}

	/* (non-Javadoc)
	 * @see tools.HasInvariant#invariant()
	 */
	public boolean invariant() {
		return first.getNumberOfElement() == second.getNumberOfElement();
	}

	/**Get number of element of QueuePair
	 * @return number of element of QueuePair
	 */
	public int getNumberOfElement() {
		return first.getNumberOfElement();
	}

	/**
	 * Check isEmpty of QueuePair
	 * @return
	 * true if QueuePair is empty
	 * <br>false otherwise
	 */
	public boolean isEmpty() {
		return first.getNumberOfElement() == 0;
	}

	/**
	 * QueuePair do something.
	 * <br> see {@link StoreAbstract#doSomething()}
	 */
	public void doSomething() {
		int total = StoreAbstract.DO_PUSH_FREQ + StoreAbstract.DO_POP_FREQ
				+ StoreAbstract.DO_HEAD_FREQ;
		int action = (new Random()).nextInt(100);
		if (action < StoreAbstract.DO_PUSH_FREQ * 100 / total) {
			 try {
			push(new IntPair(1, 2));
			 } catch (StoreException e) {
//			 e.writeException("QueuePair \t " + this.hashCode(),
//			 UnReliableException.PUSH);
			 System.out.println("QueuePair \t " + this.hashCode()+": "+
			 UnReliableException.PUSH);
			 }
		} else if (action < (StoreAbstract.DO_PUSH_FREQ + StoreAbstract.DO_POP_FREQ)
				* 100 / total) {
			 try {
			pop();
			 } catch (StoreException e) {
			 System.out.println("QueuePair \t " + this.hashCode()+": "+
			 UnReliableException.POP);
//			 e.writeException("QueuePair \t " + this.hashCode(),
//			 UnReliableException.POP);
			 }
		} else {
			 try {
			head();
			 } catch (StoreException e) {
//			 // TODO Auto-generated catch block
			 System.out.println("QueuePair \t " + this.hashCode()+": "+
			 UnReliableException.HEAD);
//			 e.writeException("QueuePair \t " + this.hashCode(),
//			 UnReliableException.HEAD);
			
			 }
		}

	}

}
