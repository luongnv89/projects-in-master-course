/**
 * 
 */
package models;

import specification.QueueAbstract;
import tests.QueueMainTempReliableStackTest;
import tools.StoreException;
import tools.StoreException.UnReliableException;

/**
 * 
 * The queue has two stack components which we will name as a mainstack and a
 * tempstack and a boolean representing whether or not the mainstack is ready to
 * push. (If it is not ready to push then we say that it is ready to pop). When
 * ready to push, if a push request is made of the queue then this element is
 * pushed directly onto the mainstack, if a pop is requested then all the
 * elements are moved from the mainstack to the tempstack, the mainstack and
 * tempstack are swapped, the state is changed to ready to pop and the element
 * popped off the mainstack
 * <br> Using hybrid design:
 * <li> mainstack: is a component of queue
 * <li> tempstack: is an aggregation of queue. From {@link ShareEmptyStack#shareStack} 
 * 
 * <br>Test by {@link QueueMainTempReliableStackTest}
 * 
 * @author luongnv89
 * 
 */
public class QueueMainTemp extends QueueAbstract {

	/**
	 * Ready to Push present the current status of queue
	 * <br> If true: all elements are currently in mainstack
	 * <br>false: all elements are currently in tempstack, to push new element into queue, need to push back all elements from tempstack to mainstack
	 */
	boolean readyToPush = true;

	/**
	 * @param qStack
	 */
	public QueueMainTemp(ReliableStack qStack) {
		super(qStack);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param elem
	 */
	public QueueMainTemp(int[] elem) {
		super(elem);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#push(int)
	 */
	@Override
	public void push(int x) {
		try {
			if (!isReadyToPush()) {
				pushBackFromShareStack();
				readyToPush = true;
			}
			qStack.push(x);
		} catch (StoreException e) {
			e.writeException("QueueMainTemp \t " + this.hashCode(),
					UnReliableException.PUSH);
			// System.out.println("QueueMainTempException: "
			// + UnReliableException.PUSH);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#head()
	 */
	@Override
	public int head() {
		try {
			if (isReadyToPush()) {
				moveToShareStack();
				readyToPush = false;
			}

			return ShareEmptyStack.shareStack.head();
		} catch (StoreException e) {
			e.writeException("QueueMainTemp \t " + this.hashCode(),
					UnReliableException.HEAD);
			// System.out.println("QueueMainTempException: "
			// + UnReliableException.HEAD);
			return Integer.MIN_VALUE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#pop()
	 */
	@Override
	public void pop() {
		try {
			if (isReadyToPush()) {
				moveToShareStack();
				readyToPush = false;
			}
			ShareEmptyStack.shareStack.pop();
		} catch (StoreException e) {
			// System.out.println("QueueMainTempException: "
			// + UnReliableException.POP);
			e.writeException("QueueMainTemp \t " + this.hashCode(),
					UnReliableException.POP);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#invariant()
	 */
	@Override
	public boolean invariant() {
		return qStack.invariant();
	}

	/**
	 * @return the readyToPush
	 */
	public boolean isReadyToPush() {
		return readyToPush;
	}

}
