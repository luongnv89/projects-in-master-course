/**
 * 
 */
package models;

import specification.QueueAbstract;
import tests.QueuePopPush2ReliableTest;
import tools.StoreException;
import tools.StoreException.UnReliableException;

/**
 * The queue is specified as having two stack components which we will name as a
 * pushstack and a popstack. When a push request is made of the queue then this
 * element is pushed directly onto the pushstack. When a pop request , if
 * popstack is not empty, just pop off an element of popstack, otherwise move
 * all elements from the pushstack on to the popstack then pop off the last
 * element of the popstack. <br>
 * Using composition design: <li>pushstack: is a component of queue <li>
 * popstack: is a component of queue. From {@link ShareEmptyStack#shareStack} <br>
 * Test by {@link QueuePopPush2ReliableTest}
 * 
 * @author luongnv89
 * 
 */
public class QueuePopPush2 extends QueueAbstract {

	/**
	 * Pop stack
	 */
	ReliableStack popStack;

	/**
	 * @param qStack
	 */
	public QueuePopPush2(ReliableStack qStack, ReliableStack pStack) {
		super(qStack);
		popStack = pStack;
		numberOfElement = qStack.getNumberOfElement()
				+ pStack.getNumberOfElement();

		// TODO Auto-generated constructor stub
	}

	/**
	 * @param elem
	 */
	public QueuePopPush2(int[] elem) {
		super(elem);
		popStack = new ReliableStack();
		numberOfElement = elem.length;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#push(int)
	 */
	@Override
	public void push(int x) throws StoreException {
		try {
			qStack.push(x);
			numberOfElement++;
		} catch (StoreException e) {
			e.writeException("QueuePopPush2 \t" + this.hashCode(),
					UnReliableException.PUSH);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#head()
	 */
	@Override
	public int head() throws StoreException {
		try {
			if (popStack.isEmpty()) {
				moveToPopStack();
			}
			return popStack.head();
		} catch (StoreException e) {
			e.writeException("QueuePopPush2 \t" + this.hashCode(),
					UnReliableException.HEAD);
			return Integer.MIN_VALUE;
		}
	}

	private void moveToPopStack() throws StoreException {
		while (!qStack.isEmpty()) {
			popStack.push(qStack.head());
			qStack.pop();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#pop()
	 */
	@Override
	public void pop() throws StoreException {
		try {
			if (popStack.isEmpty()) {
				moveToPopStack();
			}
			popStack.pop();
		} catch (StoreException e) {
			e.writeException("QueuePopPush2 \t" + this.hashCode(),
					UnReliableException.HEAD);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#invariant()
	 */
	@Override
	public boolean invariant() {
		return (numberOfElement == qStack.getNumberOfElement()
				+ popStack.getNumberOfElement())
				&& qStack.invariant() && popStack.invariant();
	}

}
