package models;

import java.util.ArrayList;

import specification.StoreAbstract;
import tests.ReliableStackTest;
import tools.StoreException;

/**
 * A Reliable Stack with the elements is stored in an array list.
 * <br>
 * Test by {@link ReliableStackTest}
 * @author luongnv89
 * 
 */
public class ReliableStack extends StoreAbstract {

	/**
	 * Arraylist store the element
	 */
	protected ArrayList<Integer> listElement;

	/**
	 * Default constructor
	 */
	public ReliableStack() {
		super();
		listElement = new ArrayList<Integer>();
	}

	/**
	 * Constructor a store with the size and the elements is input by user <li>
	 * if the input size is unbound -> default size <li>if the length of array
	 * of elements is bigger than the size of store -> store is empty
	 * 
	 * @param elems
	 *            The array of elements
	 * @param size2
	 *            the size of store
	 */
	public ReliableStack(int[] elems) {
		listElement = new ArrayList<Integer>();
		for (int i = 0; i < elems.length; i++) {
			listElement.add(elems[i]);
		}
		numberOfElement = listElement.size();
	}


	@Override
	public void push(int x) throws StoreException{
		numberOfElement++;
		listElement.add(x);

	}

	@Override
	public int head() throws StoreException{
		if (this.isEmpty()) {
			// System.out.println("Store is empty!");
			return Integer.MIN_VALUE;
		} else {
			return listElement.get(numberOfElement - 1);
		}
	}

	@Override
	public void pop() throws StoreException{
		if (this.isEmpty()) {
			// System.out.println("Store is empty!");
		} else {
			listElement.remove(numberOfElement - 1);
			numberOfElement--;
		}
	}

	@Override
	public void show() {
		for (int i = 0; i < this.getNumberOfElement(); i++) {
			System.out.print(" " + listElement.get(i));
		}
		System.out.println();

	}

	@Override
	public boolean invariant() {
		if (numberOfElement == 0 && listElement.isEmpty())
			return true;
		if (numberOfElement >= 0 && !listElement.isEmpty())
			return true;
		return false;
	}

}
