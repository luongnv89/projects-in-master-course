package models;

import java.util.Random;

import tools.StoreException;
import tools.StoreException.UnReliableException;

/**
 * The elements of stack is stored in an array
 * 
 * @author luongnv89
 * 
 */
public class UnreliableStack extends ReliableStack {

	/**
	 * Default constructor, the size of store is default, the store is empty
	 */
	public UnreliableStack() {
		super();
	}

	/**
	 * Constructor a store with the size and the elements is input by user <li>
	 * if the input size is unbound -> default size <li>if the length of array
	 * of elements is bigger than the size of store -> store is empty
	 * 
	 * @param elems
	 *            The array of elements
	 * @param size2
	 *            the size of store
	 */
	public UnreliableStack(int[] elems) {
		super(elems);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.model.ReliableStack#push(int)
	 */
	@Override
	public void push(int x) throws StoreException {
		// try {
		// problemFrequency(PUSH_PROBLEM_FREQ);
		// super.push(x);
		// } catch (StoreException e) {
		// e.writeException("UnReliableStack \t" + this.hashCode(),
		// UnReliableException.PUSH);
		// }
		Random RNG = new Random();
		if (RNG.nextInt(PUSH_PROBLEM_FREQ) == 0)
			throw new StoreException("UnReliableStack \t" + this.hashCode(),
					UnReliableException.PUSH);
		else
			super.push(x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.model.ReliableStack#head()
	 */
	@Override
	public int head() throws StoreException {
		// try {
		// problemFrequency(HEAD_PROBLEM_FREQ);
		// return super.head();
		// } catch (StoreException e) {
		// e.writeException("UnReliableStack \t" + this.hashCode(),
		// UnReliableException.HEAD);
		// return Integer.MIN_VALUE;
		// }

		Random RNG = new Random();
		if (RNG.nextInt(HEAD_PROBLEM_FREQ) == 0)
			throw new StoreException("UnReliableStack \t" + this.hashCode(),
					UnReliableException.HEAD);
		else
			return super.head();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.model.ReliableStack#pop()
	 */
	@Override
	public void pop() throws StoreException {
		// try {
		// problemFrequency(POP_PROBLEM_FREQ);
		// super.pop();
		// } catch (StoreException e) {
		// e.writeException("UnReliableStack \t" + this.hashCode(),
		// UnReliableException.POP);
		// }
		Random RNG = new Random();
		if (RNG.nextInt(POP_PROBLEM_FREQ) == 0)
			throw new StoreException("UnReliableStack \t" + this.hashCode(),
					UnReliableException.POP);
		else
			super.pop();
	}

}
