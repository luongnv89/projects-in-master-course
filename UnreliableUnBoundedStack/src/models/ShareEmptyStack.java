/**
 * 
 */
package models;

/**
 * Content the aggregation stack of {@link QueuePopPush} and
 * {@link QueueMainTemp} <br>
 * By default, the {@link ShareEmptyStack#shareStack} is a {@link ReliableStack} <br>
 * the {@link ShareEmptyStack#shareStack} can be changed to be an
 * {@link UnreliableStack} by
 * {@link ShareEmptyStack#setupShareStack(ReliableStack)}
 * 
 * @author luongnv89
 * 
 */
public class ShareEmptyStack {

	/**
	 * shareStack need to set static for implementing hybrid design for
	 * {@link QueuePair}
	 */
	public static ReliableStack shareStack = new ReliableStack();

	/**
	 * Change type of shareStack
	 * 
	 * @param s
	 */
	public static void setupShareStack(ReliableStack s) {
		shareStack = s;
	}
}
