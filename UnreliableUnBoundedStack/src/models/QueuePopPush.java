/**
 * 
 */
package models;

import specification.QueueAbstract;
import tests.QueuePopPushReliableStackTest;
import tools.StoreException;
import tools.StoreException.UnReliableException;

/**
 * The queue is specified as having two stack components which we will name as a
 * pushstack and a popstack. When a push request is made of the queue then this
 * element is pushed directly onto the pushstack. When a pop request is made of
 * the queue then move all elements from the pushstack on to the popstack then
 * pop off the last element of the popstack and then move all the elements back
 * on to the pushstack.
 * <br> Using hybrid design:
 * <li> pushstack: is a component of queue
 * <li> popstack: is an aggregation of queue. From {@link ShareEmptyStack#shareStack} 
 * <br>Test by {@link QueuePopPushReliableStackTest}
 * @author luongnv89
 * 
 */
public class QueuePopPush extends QueueAbstract {

	/**
	 * @param qStack
	 */
	public QueuePopPush(ReliableStack qStack) {
		super(qStack);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param elem
	 */
	public QueuePopPush(int[] elem) {
		super(elem);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#push(int)
	 */
	@Override
	public void push(int x) throws StoreException{
		try {
			qStack.push(x);
			numberOfElement++;
		} catch (StoreException e) {
			e.writeException("QueuePopPush \t " + this.hashCode(),
					UnReliableException.PUSH);
			// System.out.println(String.valueOf(this.hashCode())
			// + "\tQueuePopPush: PushException");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#head()
	 */
	@Override
	public int head()  throws StoreException{
		try {
			// Move all elements of qStack to shareStack
			moveToShareStack();
			// Get value of the last element of shareStack which is the element
			// need
			// to pop
			// of queue
			int head = ShareEmptyStack.shareStack.head();
			// Push back all elements of qStack after pop
			pushBackFromShareStack();
			return head;
		} catch (StoreException e) {
			e.writeException("QueuePopPush \t " + this.hashCode(),
					UnReliableException.HEAD);
			// System.out.println(String.valueOf(this.hashCode())
			// + "\tQueuePopPush:HeadException");
			return Integer.MIN_VALUE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#pop()
	 */
	@Override
	public void pop()  throws StoreException{
		// Move all elements of qStack to shareStack
		try {
			moveToShareStack();
			// Pop the last element of shareStack which is the element need to
			// pop
			// of queue
			ShareEmptyStack.shareStack.pop();
			// Push back all elements of qStack after pop
			pushBackFromShareStack();
			numberOfElement--;
		} catch (StoreException e) {
			e.writeException("QueuePopPush \t " + this.hashCode(),
					UnReliableException.POP);
			// System.out.println(String.valueOf(this.hashCode())
			// + "\tQueuePopPush:PopException");
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.specification.StoreInterface#invariant()
	 */
	@Override
	public boolean invariant() {
		return qStack.invariant();
	}

}
