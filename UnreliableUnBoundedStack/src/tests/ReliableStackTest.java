/**
 * 
 */
package tests;

import static org.junit.Assert.*;
import models.ReliableStack;

import org.junit.Before;
import org.junit.Test;

import tools.InvariantBroken;
import tools.StoreException;

/**
 * Test for {@link ReliableStack}
 * @author luongnv89
 * 
 */
public class ReliableStackTest extends StoreInterfaceTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		store = new ReliableStack();
		storeNotEmpty = new ReliableStack(elements);
		invariantStore = new ReliableStack();
	}

	/**
	 * Test method for {@link models.ReliableStack#ReliableStack()}.
	 */
	@Test
	public void testReliableStack() {
		ReliableStack stack = new ReliableStack();
		assertTrue(stack.invariant());
	}

	/**
	 * Test method for {@link models.ReliableStack#ReliableStack(int[])}.
	 */
	@Test
	public void testReliableStackIntArray() {
		ReliableStack stack = new ReliableStack(elements);
		assertTrue(stack.invariant());
	}

	// /**
	// * Test method for
	// * {@link
	// stack.model.ReliableStack#ReliableStack(stack.model.ReliableStack)}
	// * .
	// */
	// @Test
	// public void testReliableStackReliableStack() {
	// fail("Not yet implemented");
	// }

	/**
	 * Test method for {@link models.ReliableStack#push(int)}.
	 * 
	 * @throws StoreException
	 * @throws InvariantBroken
	 */
	@Test
	public void testPush() throws InvariantBroken, StoreException {
		store.push(1);
		assertEquals(1, store.head());

	}

	/**
	 * Test method for {@link models.ReliableStack#head()}.
	 * @throws StoreException 
	 */
	@Test
	public void testHead() throws StoreException {
		assertEquals(store.head(), Integer.MIN_VALUE);
		assertEquals(storeNotEmpty.head(), 5);
	}

	/**
	 * Test method for {@link models.ReliableStack#pop()}.
	 * @throws StoreException 
	 */
	@Test
	public void testPop() throws StoreException {
		assertEquals(storeNotEmpty.head(), 5);
		storeNotEmpty.pop();
		assertEquals(storeNotEmpty.head(), 4);
	}

}
