/**
 * 
 */
package tests;

import models.QueuePopPush2;
import models.ReliableStack;

import org.junit.Before;

/**
 * @author luongnv89
 * 
 */
public class QueuePopPush2ReliableTest extends QueueAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		shareStack = new ReliableStack();
		qStack = new ReliableStack();
		store = new QueuePopPush2(qStack, new ReliableStack());
		storeNotEmpty = new QueuePopPush2(elements);
	}

}
