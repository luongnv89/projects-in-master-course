/**
 * 
 */
package tests;

import static org.junit.Assert.*;
import models.IntPair;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link IntPair} class
 * 
 * @author luongnv89
 * 
 */
public class IntPairTest {

	IntPair p;
	int first = 1;
	int second = 2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		p = new IntPair(first, second);
	}

	/**
	 * Test method for {@link models.IntPair#getFirst()}.
	 */
	@Test
	public void testGetFirst() {
		assertEquals(first, p.getFirst());
	}

	/**
	 * Test method for {@link models.IntPair#getSecond()}.
	 */
	@Test
	public void testGetSecond() {
		assertEquals(second, p.getSecond());
	}

	/**
	 * Test method for {@link models.IntPair#toString()}.
	 */
	@Test
	public void testToString() {
		assertTrue(p.toString().equals("(" + first + "," + second + ")"));
	}

	/**
	 * Test method for {@link models.IntPair#equals(IntPair)}.
	 */
	@Test
	public void testEqual() {
		assertTrue(p.equals(new IntPair(first, second)));
	}

}
