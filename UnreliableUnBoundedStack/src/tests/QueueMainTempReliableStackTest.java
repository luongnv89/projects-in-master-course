/**
 * 
 */
package tests;

import models.QueueMainTemp;
import models.ReliableStack;
import models.ShareEmptyStack;

import org.junit.Before;

/**
 * Test a {@link QueueMainTemp} reliable
 * @author luongnv89
 *
 */
public class QueueMainTempReliableStackTest extends QueueAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		shareStack = new ReliableStack();
		ShareEmptyStack.setupShareStack(shareStack);
		qStack = new ReliableStack();
		store = new QueueMainTemp(qStack);
		storeNotEmpty = new QueueMainTemp(elements);
	}

}
