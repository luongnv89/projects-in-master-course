/**
 * 
 */
package tests;

import static org.junit.Assert.assertEquals;
import models.UnreliableStack;

import org.junit.Before;
import org.junit.Test;

/**
 * Test for {@link UnreliableStack}
 * 
 * @author luongnv89
 * 
 */
public class UnreliableStackTest extends ReliableStackTest {

	/*
	 * (non-Javadoc)
	 * 
	 * @see stack.tests.ReliableStackTest#setUp()
	 */
	@Before
	public void setUp() throws Exception {
		store = new UnreliableStack();
		storeNotEmpty = new UnreliableStack(elements);
	}

	/**
	 * Test method for
	 * {@link models.UnreliableStack#setupProblemFrequencies(int, int, int)}
	 * .
	 */
	@Test
	public void testSetupProblemFrequencies() {
		assertEquals(0, UnreliableStack.PUSH_PROBLEM_FREQ);
		assertEquals(0, UnreliableStack.POP_PROBLEM_FREQ);
		assertEquals(0, UnreliableStack.HEAD_PROBLEM_FREQ);

		UnreliableStack.setupProblemFrequencies(100, 1000, 10000);
		assertEquals(100, UnreliableStack.PUSH_PROBLEM_FREQ);
		assertEquals(1000, UnreliableStack.POP_PROBLEM_FREQ);
		assertEquals(10000, UnreliableStack.HEAD_PROBLEM_FREQ);
	}
}
