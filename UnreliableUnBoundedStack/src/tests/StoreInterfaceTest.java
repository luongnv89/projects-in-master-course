/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import specification.StoreInterface;

/**
 * Test for {@link StoreInterface}
 * @author luongnv89
 * 
 */
public class StoreInterfaceTest {

	protected StoreInterface store;

	protected StoreInterface storeNotEmpty;

	protected StoreInterface invariantStore;

	protected static int[] elements = { 1, 2, 3, 4, 5 };

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		store = null;
		invariantStore = null;
		storeNotEmpty = null;
	}

	/**
	 * Test method for
	 * {@link specification.StoreInterface#getNumberOfElement()}.
	 */
	@Test
	public void testGetNumberOfElement() {
		assertEquals(store.getNumberOfElement(), 0);
		assertEquals(storeNotEmpty.getNumberOfElement(), 5);
	}

	/**
	 * Test method for {@link specification.StoreInterface#isEmpty()}.
	 */
	@Test
	public void testIsEmpty() {
		assertTrue(store.isEmpty());
		assertFalse(storeNotEmpty.isEmpty());
	}

	/**
	 * Test method for {@link specification.StoreInterface#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(store.invariant());
		assertTrue(storeNotEmpty.invariant());
		// assertFalse(invariantStore.invariant());
	}

	/**
	 * Test method for {@link specification.StoreInterface#show()}.
	 */
	@Test
	public void testShow() {
		System.out.println("Content of empty store: ");
		store.show();

		System.out.println("Content of not empty store: ");
		storeNotEmpty.show();
	}

}
