/**
 * 
 */
package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import models.IntPair;
import models.QueuePair;
import models.QueuePopPush;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import specification.QueueAbstract;
import tools.StoreException;

/**
 * Test a {@link QueuePair} reliable
 * @author luongnv89
 * 
 */
public class QueuePairReliableTest {

	QueueAbstract first;
	QueueAbstract second;
	QueuePair queue;
	QueuePair queueNotEmpty;
	QueuePair queueInvariant;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		first = new QueuePopPush(getIntArray(10, 1));
		second = new QueuePopPush(getIntArray(11, 2));
		queue = new QueuePair(new QueuePopPush(getIntArray(0, 1)),
				new QueuePopPush(getIntArray(0, 1)));
		queueInvariant = new QueuePair(first, second);
		queueNotEmpty = new QueuePair(new QueuePopPush(getIntArray(10, 1)),
				new QueuePopPush(getIntArray(10, 2)));

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		first = null;
		second = null;
		queue = null;
		queueInvariant = null;
		queueNotEmpty = null;
	}

	/**
	 * Test method for {@link models.QueuePair#QueuePair()}.
	 */
	@Test
	public void testQueuePair() {
		assertTrue(queue.invariant());
	}

	/**
	 * Test method for {@link models.QueuePair#push(models.IntPair)}.
	 * 
	 * @throws StoreException
	 */
	@Test
	public void testPush() throws StoreException {
		assertTrue(queue.isEmpty());
		queue.push(new IntPair(1, 2));
		assertTrue(queue.head().equals(new IntPair(1, 2)));
		assertFalse(queue.isEmpty());
	}

	/**
	 * Test method for {@link models.QueuePair#head()}.
	 * 
	 * @throws StoreException
	 */
	@Test
	public void testHead() throws StoreException {
		assertTrue(queueNotEmpty.head().equals(new IntPair(0, 0)));
	}

	/**
	 * Test method for {@link models.QueuePair#pop()}.
	 * 
	 * @throws StoreException
	 */
	@Test
	public void testPop() throws StoreException {
		assertTrue(queueNotEmpty.head().equals(new IntPair(0, 0)));
		queueNotEmpty.pop();
		assertTrue(queueNotEmpty.head().equals(new IntPair(1, 2)));
	}

	/**
	 * Test method for {@link models.QueuePair#invariant()}.
	 */
	@Test
	public void testInvariant() {
		assertTrue(queue.invariant());
		assertTrue(queueNotEmpty.invariant());
		assertFalse(queueInvariant.invariant());
	}

	/**
	 * Test method for {@link models.QueuePair#getNumberOfElement()}.
	 */
	@Test
	public void testGetNumberOfElement() {
		assertEquals(0, queue.getNumberOfElement());
		assertEquals(10, queueNotEmpty.getNumberOfElement());
	}

	/**
	 * Test method for {@link models.QueuePair#isEmpty()}.
	 */
	@Test
	public void testIsEmpty() {
		assertTrue(queue.isEmpty());
		assertFalse(queueNotEmpty.isEmpty());
	}

	public int[] getIntArray(int size, int value) {
		int[] array = new int[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = value * i;
		}
		return array;
	}

}
