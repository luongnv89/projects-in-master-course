/**
 * 
 */
package tests;

import models.QueuePopPush;
import models.ReliableStack;
import models.ShareEmptyStack;

import org.junit.Before;

/**
 * Test a {@link QueuePopPush} reliable
 * @author luongnv89
 * 
 */
public class QueuePopPushReliableStackTest extends QueueAbstractTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		shareStack = new ReliableStack();
		ShareEmptyStack.setupShareStack(shareStack);
		qStack = new ReliableStack();
		store = new QueuePopPush(qStack);
		storeNotEmpty = new QueuePopPush(elements);

	}
}
