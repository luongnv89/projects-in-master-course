/**
 * 
 */
package tests;

import static org.junit.Assert.*;
import models.ReliableStack;

import org.junit.Test;

import specification.QueueAbstract;
import specification.StoreInterface;
import tools.InvariantBroken;
import tools.StoreException;

/**
 * Test for {@link QueueAbstract}
 * @author luongnv89
 * 
 */
public abstract class QueueAbstractTest extends StoreInterfaceTest {

	protected ReliableStack shareStack;
	protected ReliableStack qStack;
	protected QueueAbstract queue;
	/**
	 * Test method for {@link models.QueuePopPush#QueuePopPush()}.
	 */
	@Test
	public void testQueuePopPush() {
		assertTrue(store.invariant());
		assertTrue(store.isEmpty());
	}

	/**
	 * Test method for {@link models.QueuePopPush#QueuePopPush(int[])}.
	 * @throws StoreException 
	 * @throws InvariantBroken 
	 */
	@Test
	public void testQueuePopPushIntArray() throws InvariantBroken, StoreException {
		assertFalse(storeNotEmpty.isEmpty());
		assertTrue(storeNotEmpty.invariant());
		assertEquals(1, storeNotEmpty.head());
	}

	/**
	 * Test method for
	 * {@link specification.QueueAbstract#moveToShareStack()}.
	 * @throws StoreException 
	 */
	@Test
	public void testMoveToShareStack() throws StoreException {
		((QueueAbstract) storeNotEmpty).moveToShareStack();
		assertTrue(storeNotEmpty.isEmpty());
	}

	/**
	 * Test method for
	 * {@link specification.QueueAbstract#pushBackFromShareStack()}.
	 * @throws StoreException 
	 */
	@Test
	public void testPushBackFromShareStack() throws StoreException {
		((QueueAbstract) storeNotEmpty).moveToShareStack();
		assertTrue(storeNotEmpty.isEmpty());
		((QueueAbstract) storeNotEmpty).pushBackFromShareStack();
		assertFalse(storeNotEmpty.isEmpty());
	}

	/**
	 * Test method for {@link StoreInterface#push(int)}.
	 * @throws StoreException 
	 * @throws InvariantBroken 
	 */
	@Test
	public void testPush() throws InvariantBroken, StoreException {
		store.push(1);
		assertEquals(1, store.head());
		store.push(2);
		assertEquals(1, store.head());

	}

	/**
	 * Test method for {@link StoreInterface#head()}.
	 * @throws StoreException 
	 * @throws InvariantBroken 
	 */
	@Test
	public void testHead() throws InvariantBroken, StoreException {
		assertEquals(store.head(), Integer.MIN_VALUE);
		assertEquals(storeNotEmpty.head(), 1);
	}

	/**
	 * Test method for {@link StoreInterface#pop()}.
	 * @throws StoreException 
	 * @throws InvariantBroken 
	 */
	@Test
	public void testPop() throws InvariantBroken, StoreException {
		assertEquals(storeNotEmpty.head(), 1);
		storeNotEmpty.pop();
		assertEquals(storeNotEmpty.head(), 2);
	}
}
