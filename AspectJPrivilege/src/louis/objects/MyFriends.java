/**
 * 
 */
package louis.objects;

/**
 * @author luongnv89
 *
 */
public class MyFriends {
	private String privateFriend;

	/**
	 * @param privateFriend
	 */
	public MyFriends(String privateFriend) {
		super();
		this.privateFriend = privateFriend;
	}

	/**
	 * @return the privateFriend
	 */
	public String getPrivateFriend() {
		return privateFriend;
	}

	/**
	 * @param privateFriend the privateFriend to set
	 */
	public void setPrivateFriend(String privateFriend) {
		this.privateFriend = privateFriend;
	}
	
	public void sayHello(){
		System.out.println("Hello! You cannot know my private friend");
	}
	
}
