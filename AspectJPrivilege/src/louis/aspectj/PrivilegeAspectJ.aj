/**
 * 
 */
package louis.aspectj;

import louis.objects.MyFriends;

/**
 * @author luongnv89
 * 
 */
privileged public aspect PrivilegeAspectJ {

	pointcut getFriendsCall() : execution(public * MyFriends.*(..));

	before(MyFriends f): this(f) && getFriendsCall(){
		System.out.println("My best friends is: " + f.privateFriend);
	}
}
