package tests;

import junit.framework.Assert;
import models.UnreliableBoundedQueue4;

public class JUnit_UnreliableBoundedQueue4 extends JUnit_ReliableBoundedQueue{

	public void setUp() throws Exception {
		boundedQ = new UnreliableBoundedQueue4(3);
		Assert.assertTrue(boundedQ.is_empty());
		Assert.assertEquals(3, boundedQ.get_size());	
	}
}