package tests;

import junit.framework.Assert;
import models.UnreliableBoundedQueue3;


public class JUnit_UnreliableBoundedQueue3 extends JUnit_ReliableBoundedQueue{

	public void setUp() throws Exception {
		boundedQ = new UnreliableBoundedQueue3(3);
		Assert.assertTrue(boundedQ.is_empty());
		Assert.assertEquals(3, boundedQ.get_size());	
	}
}
