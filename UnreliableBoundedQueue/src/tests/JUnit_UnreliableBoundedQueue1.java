package tests;

import junit.framework.Assert;
import models.UnreliableBoundedQueue1;

public class JUnit_UnreliableBoundedQueue1 extends JUnit_ReliableBoundedQueue{

		public void setUp() throws Exception {
			boundedQ = new UnreliableBoundedQueue1(3);
			Assert.assertTrue(boundedQ.is_empty());
			Assert.assertEquals(3, boundedQ.get_size());	
		}
}
