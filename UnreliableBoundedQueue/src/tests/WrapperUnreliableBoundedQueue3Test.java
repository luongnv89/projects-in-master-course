/**
 * 
 */
package tests;

import junit.framework.Assert;
import models.WrapperUnreliableBoundedQueue1;
import models.WrapperUnreliableBoundedQueue3;

/**
 * Test for {@link WrapperUnreliableBoundedQueue1} class
 * 
 * @author luongnv89
 * 
 */
public class WrapperUnreliableBoundedQueue3Test extends
		JUnit_ReliableBoundedQueue {

	public void setUp() throws Exception {
		boundedQ = new WrapperUnreliableBoundedQueue3(3);
		Assert.assertTrue(boundedQ.is_empty());
		Assert.assertEquals(3, boundedQ.get_size());
	}

}
