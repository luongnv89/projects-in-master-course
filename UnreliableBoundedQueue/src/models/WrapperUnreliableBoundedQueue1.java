/**
 * 
 */
package models;

/**
 * @author luongnv89
 * 
 */
public class WrapperUnreliableBoundedQueue1 extends UnreliableBoundedQueue1 {

	public WrapperUnreliableBoundedQueue1(int bound)
			throws IllegalArgumentException {
		super(bound);
		MyTimerThread timerThread = new MyTimerThread(Thread.currentThread());
		timerThread.start();
	}

}
