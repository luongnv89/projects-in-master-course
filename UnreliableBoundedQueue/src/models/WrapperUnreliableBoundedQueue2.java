/**
 * 
 */
package models;

/**
 * @author luongnv89
 * 
 */
public class WrapperUnreliableBoundedQueue2 extends UnreliableBoundedQueue2 {

	MyTimerThread monitorThread;

	public WrapperUnreliableBoundedQueue2(int bound)
			throws IllegalArgumentException {
		super(bound);
		final Thread currThread = Thread.currentThread();
		MyTimerThread monitorThread = new MyTimerThread(currThread);
		monitorThread.start();
	}
}
