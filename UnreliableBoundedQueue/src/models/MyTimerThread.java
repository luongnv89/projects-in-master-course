/**
 * 
 */
package models;

/**
 * Monitor the executed time of thread
 * 
 * @author luongnv89
 * 
 */
public class MyTimerThread extends Thread {
	Thread mainThread;

	/**
	 * @param startTime
	 * @param mainThread
	 */
	public MyTimerThread(Thread mainThread) {
		this.mainThread = mainThread;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while (!mainThread.interrupted()) {
			if (mainThread.getState() == Thread.State.TIMED_WAITING) {
				System.out.println(String.valueOf(mainThread.getId()) + ": "
						+ mainThread.getState() + " - INTERUPTED!");
				mainThread.interrupt();
			}

		}
	}

}
