/**
 * 
 */
package models;

import abstractions.BoundedQueueSpecification;

/**
 * @author luongnv89
 * 
 */
public class WrapperUnreliableBoundedQueue3 implements
		BoundedQueueSpecification {

	UnreliableBoundedQueue3 un3;

	public WrapperUnreliableBoundedQueue3(int sizeBound)
			throws IllegalArgumentException {
		un3 = new UnreliableBoundedQueue3(sizeBound);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.UnreliableBoundedQueue1#push(int)
	 */
	@Override
	public void push(int x) {
		// TODO Auto-generated method stub
		boolean reliable = false;
		String expectOutput = un3.toString() + x+" ";
		while (!reliable) {
			UnreliableBoundedQueue3 temp = un3;
			System.out.println("Before: " + expectOutput);
			temp.push(x);
			System.out.println("After: " + temp.toString());
			if (expectOutput.replace(" ", "") != temp.toString().replace(" ", "")) {
				System.out.println("Wrong value!");
			} else {
				System.out.println("Correct value!");
				reliable = true;
				un3 = temp;
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.ReliableBoundedQueue#head()
	 */
	@Override
	public int head() {
		// TODO Auto-generated method stub
		return un3.head();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.ReliableBoundedQueue#pop()
	 */
	@Override
	public void pop() {
		// TODO Auto-generated method stub
		un3.pop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return un3.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.ReliableBoundedQueue#is_full()
	 */
	@Override
	public boolean is_full() {
		// TODO Auto-generated method stub
		return un3.is_full();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.ReliableBoundedQueue#is_empty()
	 */
	@Override
	public boolean is_empty() {
		// TODO Auto-generated method stub
		return un3.is_empty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.ReliableBoundedQueue#get_size()
	 */
	@Override
	public int get_size() {
		// TODO Auto-generated method stub
		return un3.get_size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see models.ReliableBoundedQueue#invariant()
	 */
	@Override
	public boolean invariant() {
		// TODO Auto-generated method stub
		return un3.invariant();
	}

}
