package models;

import abstracts.DocumentAbstract;

public class CDsDocuments extends DocumentAbstract {

	private DocumentGenre genre;
	private DocumentGenre classify;

	public CDsDocuments(String string3, int i, String string, String string2,
			int j, DocumentGenre classic, DocumentGenre opera) {
		super(string3, i, string, string2, j);
		genre = classic;
		classify = opera;
	}

	@Override
	public DocumentGenre getGenre() {
		return genre;
	}

	public DocumentGenre getClassification() {
		return classify;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return super.equals(obj);
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CDsDocuments other = (CDsDocuments) obj;
		if (classify != other.classify)
			return false;
		if (genre != other.genre)
			return false;
		return super.equals(obj);
	}

}
