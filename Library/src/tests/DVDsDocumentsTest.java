package tests;

import static org.junit.Assert.assertEquals;
import models.DVDsDocuments;
import models.DocumentGenre;

import org.junit.BeforeClass;
import org.junit.Test;

public class DVDsDocumentsTest extends DocumentAbstractTest {

	@BeforeClass
	public static void setUp() throws Exception {
		document1 = new DVDsDocuments("DVDs1", 6, "DVD Paradise",
				"Britney Spear", 2007, DocumentGenre.COMEDY, 120, 1);
		document2 = new DVDsDocuments("DVDs1", 6, "DVD Paradise",
				"Britney Spear", 2007, DocumentGenre.COMEDY, 120, 1);
	}

	@Test
	public void testGetMethod() {
		assertEquals(document1.getID(), "DVDs1");
		assertEquals(document1.getLocation(), 6);
		assertEquals(document1.getTitle(), "DVD Paradise");
		assertEquals(document1.getAuthor(), "Britney Spear");
		assertEquals(document1.getYear(), 2007);
		assertEquals(((DVDsDocuments) document1).getGenre(),
				DocumentGenre.COMEDY);
		assertEquals(((DVDsDocuments) document1).getDuration(), 120);
		assertEquals(((DVDsDocuments) document1).getBroadCast(), 1);
	}

}
