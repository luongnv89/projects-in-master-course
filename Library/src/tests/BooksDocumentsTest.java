package tests;

import static org.junit.Assert.assertEquals;
import models.DocumentGenre;
import models.BooksDocuments;

import org.junit.Before;
import org.junit.Test;

public class BooksDocumentsTest extends DocumentAbstractTest {

	@Before
	public void setUp() throws Exception {
		document1 = new BooksDocuments("BOOK1", 7, "Robinson Cruiso",
				"Jack London", 2001, DocumentGenre.NOVEL, 130);
		document2 = new BooksDocuments("BOOK1", 7, "Robinson Cruiso",
				"Jack London", 2001, DocumentGenre.NOVEL, 130);
	}

	@Test
	public void testGetMethod() {
		assertEquals(document1.getID(), "BOOK1");
		assertEquals(document1.getLocation(), 7);
		assertEquals(document1.getTitle(), "Robinson Cruiso");
		assertEquals(document1.getAuthor(), "Jack London");
		assertEquals(document1.getYear(), 2001);
		assertEquals(((BooksDocuments) document1).getGenre(), DocumentGenre.NOVEL);
		assertEquals(((BooksDocuments) document1).getPages(), 130);
	}

}
