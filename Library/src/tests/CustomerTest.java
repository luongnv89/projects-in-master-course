package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import models.Customer;

import org.junit.BeforeClass;
import org.junit.Test;

public class CustomerTest {

	static Customer normalRate;
	static Customer normalRate2;
	static Customer discount;
	static Customer subscriber;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		normalRate = new Customer("user1", "NGUYEN", "VAN A",
				"Telecom Sudparis1", 1);
		normalRate2 = new Customer("user11", "NGUYEN", "VAN A",
				"Telecom Sudparis1", 1);
		discount = new Customer("user2", "LE", "VAN B", "Telecom Sudparis2", 2);
		subscriber = new Customer("user3", "TRAN", "THI C",
				"Telecom Sudparis3", 3);

	}

	@Test
	public void testConstructor() {
		assertEquals(normalRate.getName(), "NGUYEN");
		assertEquals(normalRate.getSurName(), "VAN A");
		assertEquals(normalRate.getAddress(), "Telecom Sudparis1");
		assertEquals(normalRate.getCategory(), 1);
		assertEquals(normalRate.getID(), "user1");
	}

	@Test
	public void testEquals() {
		assertTrue(normalRate2.equals(normalRate2));
		assertFalse(normalRate.equals(discount));
		assertFalse(normalRate.equals(null));
		assertFalse(normalRate.equals(68));
		// assertFalse(normalRate.equals(new Customer(null, "NGUYEN", "VAN A",
		// "Telecom Sudparis1", 1)));
		// assertFalse(normalRate.equals(new Customer("user3", "NGUYEN",
		// "VAN A",
		// "Telecom Sudparis1", 1)));
		// assertFalse(normalRate.equals(new Customer("user1", null, "VAN A",
		// "Telecom Sudparis1", 1)));
		// assertFalse(normalRate.equals(new Customer("user1", "LE", "VAN A",
		// "Telecom Sudparis1", 1)));
		// assertFalse(normalRate.equals(new Customer("user1", "NGUYEN",
		// "NGUYEN",
		// "Telecom Sudparis1", 1)));
		// assertFalse(normalRate.equals(new Customer("user1", "NGUYEN", null,
		// "Telecom Sudparis1", 1)));
		//
		// assertFalse(normalRate.equals(new Customer("user1", "NGUYEN",
		// "VAN A",
		// "Telecom Sudparis2", 1)));
		// assertFalse(normalRate.equals(new Customer("user1", "NGUYEN",
		// "VAN A",
		// null, 1)));
		//
		// assertFalse(normalRate.equals(new Customer("user1", "NGUYEN",
		// "VAN A",
		// "Telecom Sudparis1", 2)));
		// assertFalse(normalRate.equals(new Customer("user1", "NGUYEN",
		// "VAN A",
		// "Telecom Sudparis1", (Integer) null)));
	}

	@Test
	public void testUpdate() {
		normalRate2.updateName("LE");
		normalRate2.updateSurName("VAN B");
		normalRate2.updateAddress("Telecom Sudparis2");
		normalRate2.updateCategory(2);

		assertEquals(normalRate2.getName(), "LE");
		assertEquals(normalRate2.getSurName(), "VAN B");
		assertEquals(normalRate2.getAddress(), "Telecom Sudparis2");
		assertEquals(normalRate2.getCategory(), 2);
		assertEquals(normalRate2.getID(), "user11");
	}
}
