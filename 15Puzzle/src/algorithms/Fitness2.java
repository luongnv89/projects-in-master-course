package algorithms;

import models.Board;
import models.FitnessFunction;

/**
 * Using the fitness function number 1
 * @author crocode
 *
 */
public class Fitness2 extends FitnessFunction{
	
	public Fitness2(Board b) {
		super(b);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see models.FitnessFunction#fitness()
	 * The fitness function number 1
	 */
	@Override
	public float fitness() {
		// TODO Auto-generated method stub
		System.out.println("This is fitness function 2");
		return 0;
	}
	
}
