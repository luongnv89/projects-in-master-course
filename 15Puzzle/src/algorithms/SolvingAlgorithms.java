package algorithms;

import java.util.ArrayList;

import models.FitnessFunction;

/**
 * Find the way to improve the fitness value.
 * 
 * @author crocode
 * @version 1
 */
public class SolvingAlgorithms {

	/**
	 * The fitness function is used to find the solution
	 * */
	FitnessFunction fit;
	/**
	 * The number of steps we have to move
	 * */
	int nSteps;

	/**
	 * The path of moves to solve
	 * */
	ArrayList<Integer> path = new ArrayList<Integer>();

	public FitnessFunction getFit() {
		return fit;
	}

	public void setFit(FitnessFunction fit) {
		this.fit = fit;
	}

	public int getnSteps() {
		return nSteps;
	}

	public void setnSteps(int nSteps) {
		this.nSteps = nSteps;
	}

	public ArrayList<Integer> getPath() {
		return path;
	}

	public void setParth(ArrayList<Integer> path) {
		this.path = path;
	}

	public SolvingAlgorithms(FitnessFunction fitness) {
		// TODO Auto-generated constructor stub
		fit = fitness;
	}

	/**
	 * Algorithms for find the solution using the fitness function.
	 */
	public void solveProble() {
		fit.fitness();
		System.out.println("Set path");
		System.out.println("Set steps");
	}

}
