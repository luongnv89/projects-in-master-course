package tests;

import models.Board;
import algorithms.Fitness1;
import algorithms.Fitness2;
import algorithms.Fitness3;
import algorithms.SolvingAlgorithms;

public class SolvingTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final Board board = new Board();
		board.setRandomState();
		SolvingAlgorithms slv1 = new SolvingAlgorithms(new Fitness1(board));
		SolvingAlgorithms slv2 = new SolvingAlgorithms(new Fitness2(board));
		SolvingAlgorithms slv3 = new SolvingAlgorithms(new Fitness3(board));
		slv1.solveProble();
		slv2.solveProble();
		slv3.solveProble();
	}

}
