package tests;

import models.Board;
import models.FitnessFunction;
import algorithms.Fitness1;
import algorithms.Fitness2;
import algorithms.Fitness3;

public class FitnessTest {
	public static void main(String args[]) {
		Board board = new Board();
		board.setRandomState();

		FitnessFunction f1 = new Fitness1(board);
		FitnessFunction f2 = new Fitness2(board);
		FitnessFunction f3 = new Fitness3(board);
		f1.fitness();
		f2.fitness();
		f3.fitness();
	}
}
