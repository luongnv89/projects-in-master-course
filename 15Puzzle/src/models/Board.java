package models;

import java.util.ArrayList;
import java.util.Random;

import specification.BoardSpecification;

public class Board implements BoardSpecification {

	/**
	 * The element of board 4x4 </br> From left to right </br> From top to
	 * bottom
	 * */
	public ArrayList<Integer> listElement = new ArrayList<Integer>();

	int misPoint = 15;

	public int getMisPoint() {
		return misPoint;
	}

	public void setMisPoint(int misPoint) {
		this.misPoint = misPoint;
	}

	/**
	 * The default constructor function of the Board class
	 */
	public Board() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see specification.BoardSpecification#setRandomState()
	 */

	@Override
	public void setRandomState() {
		// TODO Auto-generated method stub
		ArrayList<Integer> listInteger = new ArrayList<Integer>();
		for (int i = 0; i < 15; i++) {
			listInteger.add(i + 1);
		}

		Random ran = new Random();
		for (int i = 0; i < 15; i++) {
			int numIndex = ran.nextInt(listInteger.size());
			listElement.add(listInteger.get(numIndex));
			listInteger.remove(listInteger.get(numIndex));
		}
		listElement.add(0);

	}

	/**
	 * 
	 * Algorithms for check a board is solvable.
	 * 
	 * @see specification.BoardSpecification#isSolvable()
	 * 
	 *      (non-Javadoc)
	 * @see specification.BoardSpecification#isSolvable()
	 */
	@Override
	public boolean isSolvable() {
		// TODO Auto-generated method stub
		boolean solvable = false;
		return solvable;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see specification.BoardSpecification#changeState()
	 */
	@Override
	public void changeState(int position) {
		// TODO Auto-generated method stub
		listElement.set(misPoint, listElement.get(position));
		listElement.set(position, 0);

	}

	@Override
	public void setRandomCorrectState() {
		// TODO Auto-generated method stub
		
	}

}
