package models;

/**
 * Using fitness function
 * 
 * @author crocode
 * 
 */
public abstract class FitnessFunction {

	Board board = new Board();

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	float fitness;

	public float getFitness() {
		return fitness;
	}

	public void setFitness(float fitness) {
		this.fitness = fitness;
	}

	/**
	 * @param b
	 *            The board
	 */
	public FitnessFunction(Board b) {
		// TODO Auto-generated constructor stub
		board.listElement.addAll(b.listElement);
	}

	public FitnessFunction() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return The fitness value
	 */
	public abstract float fitness();
}
