package specification;

/**
 * The specification of a 15-puzzle board </br>Size: 4x4 </br>15 element: 1->15
 * </br> one element is missing.
 * 
 * @author crocode
 * 
 */
public interface BoardSpecification {
	/**
	 * Fill randomly 15 element of board with values from 1 to 15 </br> The last
	 * element is missing.
	 * */
	public void setRandomState();

	/**
	 * 
	 * */
	public void setRandomCorrectState();

	/**
	 * Check board can be solvable or unsolvable
	 * */
	public boolean isSolvable();

	/**
	 * The state of board is changed when some element was moved.
	 */
	public void changeState(int position);

}
