import mediatheque.document.*;
import mediatheque.*;
import util.Console;
import util.Datutil;
import util.InvariantBroken;

/**
 * Menu d'utilisation du package mediatheque.
 */
/**
 * @author denis
 * 
 */
public class Menu {

    /**
     * Reference sur l'instance de mediatheque.
     */
    static Mediatheque media = null;

    /**
     * Fonction main.
     * @throws InvariantBroken 
     */
    public static void main(String[] args) throws InvariantBroken {
        if (args.length == 0) {
            throw new IllegalArgumentException("Main needs at least its name as an argument");
        }
        media = new Mediatheque(args[0]);
        if (media == null) {
            System.out.println("No media data");
        }
        try {

            int choix;
            while ((choix = menu()) > 0) {
                switch (choix) {
                    case 1:
                        emprunterDocument();
                        break;
                    case 2:
                        restituerDocument();
                        break;
                    case 3:
                        media.verifier();
                        break;
                    case 4:
                        menuLister();
                        break;
                    case 5:
                        menuDocument();
                        break;
                    case 6:
                        menuClient();
                        break;
                    case 7:
                        menuLocalisation();
                        break;
                    case 8:
                        menuGenre();
                        break;
                    case 9:
                        Datutil.addAuJour(7);
                        break;
                    default:
                        break;
                }

            }
            boolean res = saveMedia(media);
            if (!res) {
                System.out.println("Unable to save media data");
            }
        } catch (OperationImpossible op) {
            System.out.println("Erreur Menu : " + op);
        }
    }

    /**
     * Affichage d'un menu.
     * 
     * @return code du menu (0 ==> quit)
     */
    static int menu() {
        System.out.println("\n" + Datutil.dateToString(Datutil.dateDuJour())
                + ": Menu");
        System.out.println(" 1- Emprunter un document");
        System.out.println(" 2- Restituer un document");
        System.out.println(" 3- Verifier les emprunts");
        System.out.println(" 4- Affichages");
        System.out.println(" 5- Operations sur un document");
        System.out.println(" 6- Operations sur un client");
        System.out.println(" 7- Operations sur une localisation");
        System.out.println(" 8- Operations sur un genre");
        System.out.println(" 9- Avancer la date de 7 jours");
        System.out.println(" 0- quitter");
        int choix = 0;
        try {
            choix = Console.readInt("\nEntrer le choix :");
        } catch (Exception e) {
            System.err.println("Erreur de saisie");
        }
        return choix;
    }

    /**
     * Menu pour les listes.
     */
    static void menuLister() {
        int choix = 0;
        do {
            System.out.println("Que voulez vous lister ?");
            System.out.println(" 1- Clients");
            System.out.println(" 2- Documents");
            System.out.println(" 3- Fiches d'Emprunt");
            System.out.println(" 4- Localisations");
            System.out.println(" 5- Genres");
            try {
                choix = Console.readInt("\nEntrer le choix :");
            } catch (Exception e) {
                System.err.println("Erreur de saisie");
            }
        } while (!(choix > 0 && choix < 6));
        switch (choix) {
            case 1:
                media.listerClients();
                break;
            case 2:
                media.listerDocuments();
                break;
            case 3:
                media.listerFicheEmprunts();
                break;
            case 4:
                media.listerLocalisations();
                break;
            case 5:
                media.listerGenres();
                break;
            default:
                break;
        }
    }

    /**
     * Menu de gestion des genres.
     */
    static void menuGenre() {
        int choix = 0;
        do {
            System.out.println("Que voulez vous faire ?");
            System.out.println(" 1- Ajouter un Genre");
            System.out.println(" 2- Supprimer un Genre");
            System.out.println(" 3- Lister les Genre");
            try {
                choix = Console.readInt("\nEntrer le choix :");
            } catch (Exception e) {
                System.err.println("Erreur de saisie");
            }
        } while (!(choix > 0 && choix < 4));
        try {
            switch (choix) {
                case 1:
                    ajouterGenre();
                    break;
                case 2:
                    supprimerGenre();
                    break;
                case 3:
                    media.listerGenres();
                    break;
                default:
                    break;
            }
        } catch (OperationImpossible o) {
            System.err.println("Erreur \"" + o.getMessage() + "\"");
        }
    }

    /**
     * Ajouter un genre.
     * 
     * @throws OperationImpossible
     *             exception levee.
     */
    static void ajouterGenre() throws OperationImpossible {
        System.out.println("Ajouter un genre");
        String genre = Console.readLine("Nom du nouveau Genre :");
        media.ajouterGenre(genre);
    }

    /**
     * Supprimer un genre.
     * 
     * @throws OperationImpossible
     *             exception levee.
     */
    static void supprimerGenre() throws OperationImpossible {
        System.out.println("Supprimer un genre");
        String genre = Console.readLine("Nom du Genre a supprimer :");
        media.ajouterGenre(genre);
    }

    /**
     * Menu pour les localisations.
     */
    static void menuLocalisation() {
        int choix = 0;
        do {
            System.out.println("Que voulez vous faire ?");
            System.out.println(" 1- Ajouter une Localisation");
            System.out.println(" 2- Supprimer une Localisation");
            System.out.println(" 3- Lister les localisations");
            try {
                choix = Console.readInt("\nEntrer le choix :");
            } catch (Exception e) {
                System.err.println("Erreur de saisie");
            }
        } while (!(choix > 0 && choix < 4));
        try {
            switch (choix) {
                case 1:
                    ajouterLocalisation();
                    break;
                case 2:
                    supprimerLocalisation();
                    break;
                case 3:
                    media.listerLocalisations();
                    break;
                default:
                    break;
            }
        } catch (OperationImpossible o) {
            System.err.println("Erreur \"" + o.getMessage() + "\"");
        }
    }

    /**
     * Ajouter une localisation.
     * 
     * @throws OperationImpossible
     *             exception levee.
     */
    static void ajouterLocalisation() throws OperationImpossible {
        System.out.println("Ajouter une Localisation");
        String salle = Console.readLine("Nom de la salle :");
        String rayon = Console.readLine("Nom du rayon :");
        media.ajouterLocalisation(salle, rayon);
    }

    /**
     * Supprimer une localisation.
     * 
     * @throws OperationImpossible
     *             exception levee.
     */
    static void supprimerLocalisation() throws OperationImpossible {
        System.out.println("Supprimer une Localisation");
        String salle = Console.readLine("Nom de la salle :");
        String rayon = Console.readLine("Nom du rayon :");
        media.supprimerLocalisation(salle, rayon);
    }

    /**
     * Menu pour les documents.
     * @throws InvariantBroken 
     */
    static void menuDocument() throws InvariantBroken {
        int choix = 0;
        do {
            System.out.println("Que voulez vous faire sur un document ?");
            System.out.println(" 1- Ajouter un document");
            System.out.println(" 2- Retirer un document");
            System.out.println(" 3- Autoriser l'emprunt d'un document");
            System.out.println(" 4- Interdire l'emprunt d'un document");
            System.out.println(" 5- Lister les documents");
            try {
                choix = Console.readInt("\nEntrer le choix :");
            } catch (Exception e) {
                System.err.println("Erreur de saisie");
            }
        } while (!(choix > 0 && choix < 6));
        try {
            switch (choix) {
                case 1:
                    ajouterDocument();
                    break;
                case 2:
                    retirerDocument();
                    break;
                case 3:
                    empruntable();
                    break;
                case 4:
                    nonEmpruntable();
                    break;
                case 5:
                    media.listerDocuments();
                    break;
                default:
                    break;
            }
        } catch (OperationImpossible o) {
            System.err.println("Erreur \"" + o.getMessage() + "\"");
        }
    }

    /**
     * Menu de choix du type de document.
     * 
     * @return Le type.
     */
    static int menudoc() {
        int choix = 0;
        do {
            System.out.println("Type du document ");
            System.out.println(" 1- Audio");
            System.out.println(" 2- Video");
            System.out.println(" 3- Livre");
            try {
                choix = Console.readInt("\nEntrer le choix :");
            } catch (Exception e) {
                System.err.println("Erreur de saisie");
            }
        } while (!(choix > 0 && choix < 4));
        return choix;
    }

    /**
     * Ajouter un document.
     * 
     * @throws OperationImpossible
     *             exception levee.
     * @throws InvariantBroken 
     */
    static void ajouterDocument() throws OperationImpossible, InvariantBroken {
        Document doc = null;
        System.out.println("Ajouter un document");
        String code = Console.readLine("  Code du document :");
        String salle = Console.readLine(" Salle  :");
        String rayon = Console.readLine(" Rayon  :");
        String titre = Console.readLine("  Titre :");
        String auteur = Console.readLine(" Auteur  :");
        String annee = Console.readLine("  Annee :");
        String genre = Console.readLine("  Genre :");
        int typdoc = menudoc();
        Localisation loc;
        Genre g;
        loc = media.chercherLocalisation(salle, rayon);
        g = media.chercherGenre(genre);
        switch (typdoc) {
            case 1:
                String classif = Console.readLine("  Classification :");
                doc = new Audio(code, loc, titre, auteur, annee, g, classif);
                break;
            case 2:
                int duree = Console.readInt("  Duree :");
                String mleg = Console.readLine("  Mention legale:");
                doc = new Video(code, loc, titre, auteur, annee, g, duree, mleg);
                break;
            case 3:
                int nbp = Console.readInt(" Nombre de pages :");
                doc = new Livre(code, loc, titre, auteur, annee, g, nbp);
                break;
            default:
                return;
        }
        media.ajouterDocument(doc);
    }

    /**
     * Retirer un document.
     * 
     * @throws OperationImpossible
     *             exception levee.
     */
    static void retirerDocument() throws OperationImpossible {
        System.out.println("Retirer un document");
        String code = Console.readLine("  Code du document :");
        media.retirerDocument(code);
        System.out.println(" - Operation reussie");
    }

    /**
     * Emprunter un document.
     * 
     * @throws OperationImpossible
     *             exception levee.
     * @throws InvariantBroken 
     */
    static void emprunterDocument() throws OperationImpossible, InvariantBroken {
        System.out.println("Emrunt d'un document");
        String code = Console.readLine("  Code du document :");
        String nom = Console.readLine("  Nom du client :");
        String prenom = Console.readLine("  Prenom du client :");
        media.emprunter(nom, prenom, code);
        System.out.println(" - Operation reussie");
    }

    /**
     * Restituer un document.
     * 
     * @throws OperationImpossible
     *             exception levee.
     * @throws InvariantBroken 
     */
    static void restituerDocument() throws OperationImpossible, InvariantBroken {
        System.out.println("Restitution d'un document");
        String code = Console.readLine("  Code du document :");
        String nom = Console.readLine("  Nom du client :");
        String prenom = Console.readLine("  Prenom du client :");
        media.restituer(nom, prenom, code);
        System.out.println(" - Operation reussie");
    }

    /**
     * Rendre un document empruntable.
     * 
     * @throws OperationImpossible
     *             exception levee.
     * @throws InvariantBroken 
     */
    static void empruntable() throws OperationImpossible, InvariantBroken {
        System.out.println("Rendre un document empruntable");
        String code = Console.readLine("  Code du document :");
        media.metEmpruntable(code);
    }

    /**
     * Rendre un document non empruntable.
     * 
     * @throws OperationImpossible
     *             exception levee.
     * @throws InvariantBroken 
     */
    static void nonEmpruntable() throws OperationImpossible, InvariantBroken {
        System.out.println("Rendre un document non empruntable");
        String code = Console.readLine("  Code du document :");
        media.metConsultable(code);
    }

    /**
     * Menu de gestion des clients.
     */
    static void menuClient() {
        int choix = 0;
        do {
            System.out.println("Que voulez vous faire ?");
            System.out.println(" 1- Inscrire un client");
            System.out.println(" 2- Resilier un client");
            System.out.println(" 3- Changer categorie client");
            System.out.println(" 4- Lister les clients");
            try {
                choix = Console.readInt("\nEntrer le choix :");
            } catch (Exception e) {
                System.err.println("Erreur de saisie");
            }
        } while (!(choix > 0 && choix < 4));
        try {
            switch (choix) {
                case 1:
                    inscrireClient();
                    break;
                case 2:
                    resilierClient();
                    break;
                case 3:
                    media.listerClients();
                    break;
                default:
                    break;
            }
        } catch (OperationImpossible o) {
            System.err.println("Erreur \"" + o.getMessage() + "\"");
        }
    }

    /**
     * Menu de choix du type de client.
     * 
     * @return Le type.
     */
    static int menucli() {
        int choix = 0;
        do {
            System.out.println("Type de client ");
            System.out.println(" 1- Normal");
            System.out.println(" 2- Reduit");
            System.out.println(" 3- Abonne");
            try {
                choix = Console.readInt("\nEntrer le choix :");
            } catch (Exception e) {
                System.err.println("Erreur de saisie");
            }
        } while (!(choix > 0 && choix < 4));
        return choix;
    }

    /**
     * Inscrire un client.
     * 
     * @throws OperationImpossible
     *             exception levee.
     */
    static void inscrireClient() throws OperationImpossible {
        int reduc = 0;
        System.out.println("Ajouter un client");
        String nom = Console.readLine("  Nom du client :");
        String prenom = Console.readLine(" Prenom du client  :");
        String adress = Console.readLine("  Adresse du client :");
        int typcli = menucli();
        switch (typcli) {
            case 1:
                media.inscrire(nom, prenom, adress, "TarifNormal");
                break;
            case 2:
                reduc = Console.readInt(" Code de reduction :");
                 media.inscrire(nom, prenom, adress, "TarifReduit", reduc);
                break;
            case 3:
                media.inscrire(nom, prenom, adress,"Abonne");
                break;
            default:
                return;
        }
    }

    /**
     * Changer la categorie d'un client.
     * 
     * @throws OperationImpossible
     *             exception levee.
     */
    static void changerCategorieClient() throws OperationImpossible {
        int reduc;
        System.out.println("Changement de categorie d'un client");
        String nom = Console.readLine("  Nom du client :");
        String prenom = Console.readLine("  Prenom du client :");
        int typcli = menucli();
        switch (typcli) {
            case 1:
                media.changerCategorie(nom, prenom, "TarifNormal", 0);
                break;
            case 2:
                reduc = Console.readInt(" Code de reduction :");
                media.changerCategorie(nom, prenom, "TarifReduit", reduc);
                break;
            case 3:
                media.changerCategorie(nom, prenom, "Abonne", 0);
                break;
            default:
                return;
        }
    }

    /**
     * Resilier l'abonnement d'un client.
     * 
     * @throws OperationImpossible
     *             exception levee.
     */
    static void resilierClient() throws OperationImpossible {
        System.out.println("Desincription d'un client");
        String nom = Console.readLine("  Nom du client :");
        String prenom = Console.readLine("  Prenom du client :");
        media.resilier(nom, prenom);
    }

    private static boolean saveMedia(Mediatheque media) {
        /* Ajout des genres et localisations */
        return media.saveToFile();
    }
}