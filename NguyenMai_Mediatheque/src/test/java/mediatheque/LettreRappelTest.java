package mediatheque;

import static org.junit.Assert.assertTrue;
import mediatheque.document.Video;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author mainguyen
 * Test class {@link LettreRappel}.
 * Purpose is to see the result on the screen.
 */
public class LettreRappelTest extends FicheEmpruntTest{
	LettreRappel lettre;


	@Before
	public void setUp() throws Exception {
		media = new Mediatheque("mediaTest");
		media.ajouterLocalisation("Economic", "Trend");
		media.ajouterCatClient("Normal", 1, 1, 1, 1, false);
		media.ajouterGenre("Video");
		media.inscrire(nom, pre, "vn", "Normal");
		doc = new Video("00001", new Localisation("Economic", "Trend"),
				"Grobal economy, 4 edition", "Ammel", "2009",
				new Genre("Video"), 60, "nxb");
		media.ajouterDocument(doc);
		doc.metEmpruntable();		
		cli = media.chercherClient(nom, pre);
		fiche = new FicheEmprunt(media, cli,doc);
		lettre = new LettreRappel("mediaTest", fiche);
	}

	@After
	public void tearDown() throws Exception {
		media = null;
		fiche = null;
		cli = null;
		doc = null;
		lettre = null;
	}

	@Test
	public void testConstructor() {
		assertTrue(lettre.getDateRappel().equals(fiche.getDateLimite()));
	}

	/**
	 * Test {@link LettreRappel#relancer()}.
	 * Just to see the format will display in the screen.
	 */
	@Test
	public void testRelancer(){
		lettre.relancer();
	}
}
