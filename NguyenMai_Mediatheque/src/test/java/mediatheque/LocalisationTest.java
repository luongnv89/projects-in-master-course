package mediatheque;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LocalisationTest {
	Localisation information;
	Localisation economic;

	@Before
	public void setUp() throws Exception {
		information = new Localisation("Technology", "Languages programming");
		economic = new Localisation("Economy", "Finance");
	}

	@After
	public void tearDown() throws Exception {
		information = null;
		economic = null;
	}

	/**
	 * Test {@link Localisation#Localisation(String, String)} 
	 */
	@Test
	public void testConstructor() {
		assertEquals("Technology", information.getSalle());
		assertEquals("Languages programming", information.getRayon());		
	}

	/**
	 * Test {@link Localisation#equals(Object)}
	 */
	@Test
	public void testEquals(){
		Localisation novel = null;
		Genre book = null;
		Localisation web = new Localisation("Technology", "Languages programming");
		Localisation maketing = new Localisation("Economy", null);
		Localisation finance = new Localisation(null, "Finance");
		assertTrue(information.equals(information));
		assertFalse(information.equals(economic));
		assertTrue(information.equals(web));
		assertFalse(information.equals(novel));
		assertFalse(information.equals(book));
		assertFalse(maketing.equals(economic));
		assertFalse(finance.equals(economic));
	}
	
	/**
	 * Test {@link Localisation#toString()} with <code>information</code>.
	 * It should be "Salle/Rayon : " + Technology + "/" + Languages programming
	 */
	@Test
	public void testToString(){
		String expected = "Salle/Rayon : " + "Technology" + "/" + "Languages programming";
		assertEquals(expected, information.toString());
	}
	
	/**
	 * Test {@link Localisation#setSalle(String)}
	 */
	@Test
	public void testSetSalle(){
		String salle = "Information";
		information.setSalle(salle);
		assertEquals(salle, information.getSalle());
	}
	
	/**
	 * Test {@link Localisation#setRayon(String)}
	 */
	@Test
	public void testSetRayon(){
		String rayon = "Web";
		information.setRayon(rayon);
		assertEquals(rayon,information.getRayon());
				
	}
}
