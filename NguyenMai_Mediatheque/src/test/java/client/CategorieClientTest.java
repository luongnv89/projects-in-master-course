package client;

import static org.junit.Assert.*;

import mediatheque.client.CategorieClient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author mainguyen Test the validation of methods/functions in
 *         {@link CategorieClient} class.
 * 
 */
public class CategorieClientTest {
	CategorieClient categorieClientNotComplete;
	CategorieClient categorieClientCompleteReduce;
	CategorieClient categorieClientCompleteNotReduce;

	String nameNotReduce = "Normal rate";
	int maxNotReduce = 5;
	double cotNotReduce = 1.0;
	double coefDureeNotReduce = 1.0;
	double coefTarifNotReduce = 1.0;
	boolean codeReducActifNotReduce = false;

	String nameReduce = "Discount rate";
	int maxReduce = 2;
	double cotReduce = 1.0;
	double coefDureeReduce = 1.0;
	double coefTarifReduce = 1.0;
	boolean codeReducActifReduce = true;

	@Before
	public void setUp() throws Exception {
		categorieClientNotComplete = new CategorieClient(nameNotReduce);
		categorieClientCompleteNotReduce = new CategorieClient(nameNotReduce,
				maxNotReduce, cotNotReduce, coefDureeNotReduce,
				coefTarifNotReduce, codeReducActifNotReduce);
		categorieClientCompleteReduce = new CategorieClient(nameReduce,
				maxReduce, cotReduce, coefDureeReduce, coefTarifReduce,
				codeReducActifReduce);
	}

	@After
	public void tearDown() throws Exception {
		categorieClientCompleteNotReduce = null;
		categorieClientNotComplete = null;
		categorieClientCompleteReduce = null;
	}

	/**
	 * test complete constructor
	 * {@link CategorieClient#CategorieClient(String, int, double, double, double, boolean)}
	 */
	@Test
	public void testConstructorComplete() {
		// Constructor not reduce
		assertEquals(nameNotReduce, categorieClientCompleteNotReduce.getNom());
		assertEquals(maxNotReduce,
				categorieClientCompleteNotReduce.getNbEmpruntMax());
		assertEquals(cotNotReduce,
				categorieClientCompleteNotReduce.getCotisation(), 0);
		assertEquals(coefDureeNotReduce,
				categorieClientCompleteNotReduce.getCoefDuree(), 0);
		assertEquals(coefTarifNotReduce,
				categorieClientCompleteNotReduce.getCoefTarif(), 0);
		assertFalse(categorieClientCompleteNotReduce.getCodeReducUtilise());

		// Constructor Reduce
		assertEquals(nameReduce, categorieClientCompleteReduce.getNom());
		assertEquals(maxReduce, categorieClientCompleteReduce.getNbEmpruntMax());
		assertEquals(cotReduce, categorieClientCompleteReduce.getCotisation(),
				0);
		assertEquals(coefDureeReduce,
				categorieClientCompleteReduce.getCoefDuree(), 0);
		assertEquals(coefTarifReduce,
				categorieClientCompleteReduce.getCoefTarif(), 0);
		assertTrue(categorieClientCompleteReduce.getCodeReducUtilise());
	}

	/**
	 * test constructor with the name
	 * {@link CategorieClient#CategorieClient(String)}
	 */
	@Test
	public void testConstructorNotComplete() {
		assertEquals(nameNotReduce, categorieClientNotComplete.getNom());
		assertEquals(0, categorieClientNotComplete.getNbEmpruntMax());
		assertEquals(0, categorieClientNotComplete.getCotisation(), 0);
		assertEquals(0, categorieClientNotComplete.getCoefDuree(), 0);
		assertEquals(0, categorieClientNotComplete.getCoefTarif(), 0);
		assertFalse(categorieClientNotComplete.getCodeReducUtilise());
	}

	/**
	 * Test {@link CategorieClient#modifierNom(String)} with
	 * <code>categorieClientNotComplete</code>
	 */
	@Test
	public void testModifierNom() {
		String newName = "Discounted";
		categorieClientNotComplete.modifierNom(newName);
		assertEquals(newName, categorieClientNotComplete.getNom());

	}

	/**
	 * Test {@link CategorieClient#modifierMax(int)} with
	 * <code>categorieClientNotComplete</code>
	 */
	@Test
	public void testModifierMax() {
		int newMax = 2;
		categorieClientNotComplete.modifierMax(newMax);
		assertEquals(newMax, categorieClientNotComplete.getNbEmpruntMax());
	}

	/**
	 * Test {@link CategorieClient#modifierCotisation(double)} with
	 * <code>categorieClientNotComplete</code>
	 */
	@Test
	public void testModifierCotisation() {
		categorieClientNotComplete.modifierCotisation(cotNotReduce);
		assertEquals(cotNotReduce, categorieClientNotComplete.getCotisation(),
				0);
	}

	/**
	 * Test {@link CategorieClient#modifierCoefDuree(double)} with
	 * <code>categorieClientNotComplete</code>
	 */
	@Test
	public void testModifierCoefDuree() {
		categorieClientNotComplete.modifierCoefDuree(coefDureeNotReduce);
		assertEquals(coefDureeNotReduce,
				categorieClientNotComplete.getCoefDuree(), 0);
	}

	/**
	 * Test {@link CategorieClient#modifierCoefTarif(double)} with
	 * <code>categorieClientNotComplete</code>
	 */
	@Test
	public void testModifierCoefTarif() {
		categorieClientNotComplete.modifierCoefTarif(coefTarifNotReduce);
		assertEquals(coefTarifNotReduce,
				categorieClientNotComplete.getCoefTarif(), 0);
	}

	/**
	 * Test {@link CategorieClient#modifierCodeReducActif(boolean)} with
	 * <code>categorieClientNotComplete</code>
	 */
	@Test
	public void testModifierCodeReducActif() {
		categorieClientNotComplete.modifierCodeReducActif(true);
		assertTrue(categorieClientNotComplete.getCodeReducUtilise());
	}

	/**
	 * Test {@link CategorieClient#getNbEmpruntMax()}
	 */
	@Test
	public void testGetNbEmpruntMax() {
		assertEquals(maxNotReduce,
				categorieClientCompleteNotReduce.getNbEmpruntMax());
		assertEquals(maxReduce, categorieClientCompleteReduce.getNbEmpruntMax());
		assertEquals(0, categorieClientNotComplete.getNbEmpruntMax());

	}

	/**
	 * Test {@link CategorieClient#getCotisation()}
	 */
	@Test
	public void testGetCotisation() {
		assertEquals(cotNotReduce,
				categorieClientCompleteNotReduce.getCotisation(), 0);
		assertEquals(cotReduce, categorieClientCompleteReduce.getCotisation(),
				0);
		assertEquals(0, categorieClientNotComplete.getCotisation(), 0);

	}

	/**
	 * Test {@link CategorieClient#getCoefDuree()}
	 */
	@Test
	public void testGetCoefDuree() {
		assertEquals(coefDureeNotReduce,
				categorieClientCompleteNotReduce.getCoefDuree(), 0);
		assertEquals(coefDureeReduce,
				categorieClientCompleteReduce.getCoefDuree(), 0);
		assertEquals(0, categorieClientNotComplete.getCoefDuree(), 0);

	}

	/**
	 * Test {@link CategorieClient#getCoefTarif()}
	 */
	@Test
	public void testGetCoefTarif() {
		assertEquals(coefTarifNotReduce,
				categorieClientCompleteNotReduce.getCoefTarif(), 0);
		assertEquals(coefTarifReduce,
				categorieClientCompleteReduce.getCoefTarif(), 0);
		assertEquals(0, categorieClientNotComplete.getCoefTarif(), 0);

	}

	/**
	 * Test {@link CategorieClient#getNom()}
	 */
	@Test
	public void testGetNom() {
		assertEquals(nameNotReduce, categorieClientCompleteNotReduce.getNom());
		assertEquals(nameReduce, categorieClientCompleteReduce.getNom());
		assertEquals(nameNotReduce, categorieClientNotComplete.getNom());

	}

	/**
	 * Test {@link CategorieClient#getCodeReducUtilise()}
	 */
	@Test
	public void testGetCodeReducUtilise() {
		assertFalse((categorieClientCompleteNotReduce.getCodeReducUtilise()));
		assertTrue((categorieClientCompleteReduce.getCodeReducUtilise()));
		assertFalse(categorieClientNotComplete.getCodeReducUtilise());

	}

	/**
	 * Test {@link CategorieClient#toString()}
	 */
	@Test
	public void testToString() {
		assertEquals(nameNotReduce, categorieClientCompleteNotReduce.toString());
		assertEquals(nameReduce, categorieClientCompleteReduce.toString());
		assertEquals(nameNotReduce, categorieClientNotComplete.toString());
	}

	/**
	 * Test {@link CategorieClient#equals(Object)}.
	 * <code>categorieClientComplete</code> equals itself and after changed the
	 * name of <code>categorieClientNotComplete</code>,
	 * <code>categorieClientComplete</code> doesn't equal to it
	 */
	@Test
	public void testEqual() {
		assertTrue(categorieClientCompleteNotReduce
				.equals(categorieClientCompleteNotReduce));

		categorieClientNotComplete.modifierNom("discounted");
		assertFalse(categorieClientCompleteNotReduce
				.equals(categorieClientNotComplete));
	}
}
