package client;

import static org.junit.Assert.*;

import mediatheque.client.HashClient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HastClientTest {

	HashClient client1;
	
	String nom1 = "Nguyen";
	String pre1 = "Mai";
	
	HashClient client2;
	
	String nom2 = "Nguyen";
	String pre2 = "Luong";
	
	@Before
	public void setUp() throws Exception {
		client1 = new HashClient(nom1, pre1);
		client2 = new HashClient(nom2, pre2);
	}

	@After
	public void tearDown() throws Exception {
		client1 = null;
		client2 = null;
	}

	/**
	 * Test {@link HashClient#HashClient(String, String)}
	 */
	@Test
	public void testConstructor() {
		assertEquals(nom1, client1.getNom());
		assertEquals(pre1, client1.getPrenom());
		assertEquals(nom2, client2.getNom());
		assertEquals(pre2, client2.getPrenom());
	}
	
	/**
	 * Test {@link HashClient#equals(Object)}
	 */
	@Test
	public void testEquals(){
		HashClient client = null;
		
		assertTrue(client1.equals(client1));
		assertFalse(client1.equals(client2));
		assertFalse(client1.equals(client));
	}
	
	/**
	 * Test {@link HashClient#hashCode()}
	 */
	@Test
	public void testHasCode()
	{
		assertNotSame(client2.hashCode(), client2.hashCode());
	}
}
