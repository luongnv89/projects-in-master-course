package client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import mediatheque.FicheEmprunt;
import mediatheque.Mediatheque;
import mediatheque.OperationImpossible;
import mediatheque.client.CategorieClient;
import mediatheque.client.Client;
import mediatheque.document.Document;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Datutil;
import util.InvariantBroken;

public class ClientTest extends CategorieClientTest {

	Client clientNoCode;
	Client clientHasCode;
	Client clientWithOnlyName;

	// attributes of a client whose category is Normal
	String surnameOfNormal = "Nguyen";
	String firstnameOfNormal = "Mai";
	String addressOfNormal = "Evry";

	// attributes of a client whose category is Discount (codeReducActif = true)
	String surnameOfDiscount = "Nguyen";
	String firstnameOfDiscount = "Luong";
	String addressOfDiscount = "Hanoi";
	int codeOfDiscount = 111;

	@Before
	public void setUp() throws Exception {
		// initializes category of Clients
		categorieClientNotComplete = new CategorieClient(nameNotReduce);
		categorieClientCompleteNotReduce = new CategorieClient(nameNotReduce,
				maxNotReduce, cotNotReduce, coefDureeNotReduce,
				coefTarifNotReduce, codeReducActifNotReduce);
		categorieClientCompleteReduce = new CategorieClient(nameReduce,
				maxReduce, cotReduce, coefDureeReduce, coefTarifReduce,
				codeReducActifReduce);

		// initializes Clients
		clientNoCode = new Client(surnameOfNormal, firstnameOfNormal,
				addressOfNormal, categorieClientCompleteNotReduce);
		clientHasCode = new Client(surnameOfDiscount, firstnameOfDiscount,
				addressOfDiscount, categorieClientCompleteReduce,
				codeOfDiscount);
		clientWithOnlyName = new Client(surnameOfNormal, firstnameOfNormal);
	}

	@After
	public void tearDown() throws Exception {
		categorieClientCompleteNotReduce = null;
		categorieClientNotComplete = null;
		clientNoCode = null;
		clientHasCode = null;
		clientWithOnlyName = null;
	}

	/**
	 * Test constructor
	 * {@link Client#Client(String, String, String, CategorieClient)}
	 */
	@Test
	public void testConstructorDefault() {
		assertEquals(surnameOfNormal, clientNoCode.getNom());
		assertEquals(firstnameOfNormal, clientNoCode.getPrenom());
		assertEquals(addressOfNormal, clientNoCode.getAdresse());
		assertTrue(categorieClientCompleteNotReduce.equals(clientNoCode
				.getCategorie()));
	}

	/**
	 * Test constructor with the code of reduce
	 * {@link Client#Client(String, String, String, CategorieClient, int)}
	 */
	@Test
	public void testConstructorWithCode() {
		assertEquals(surnameOfDiscount, clientHasCode.getNom());
		assertEquals(firstnameOfDiscount, clientHasCode.getPrenom());
		assertEquals(addressOfDiscount, clientHasCode.getAdresse());
		assertTrue(categorieClientCompleteReduce.equals(clientHasCode
				.getCategorie()));
		assertEquals(codeOfDiscount, clientHasCode.getReduc());
	}

	/**
	 * Test constructor {@link Client#Client(String, String)}
	 */
	@Test
	public void testConstructorWithOnlyName() {
		assertEquals(surnameOfNormal, clientWithOnlyName.getNom());
		assertEquals(firstnameOfNormal, clientWithOnlyName.getPrenom());
	}

	/**
	 * Test {@link Client#getNom()}
	 */
	@Test
	public void testGetNomClient() {
		assertEquals(surnameOfNormal, clientNoCode.getNom());
		assertEquals(surnameOfDiscount, clientHasCode.getNom());
		assertEquals(surnameOfNormal, clientWithOnlyName.getNom());
	}

	/**
	 * Test {@link Client#getPrenom()}
	 */
	@Test
	public void testGetPrenomClient() {
		assertEquals(firstnameOfNormal, clientNoCode.getPrenom());
		assertEquals(firstnameOfDiscount, clientHasCode.getPrenom());
		assertEquals(firstnameOfNormal, clientWithOnlyName.getPrenom());
	}

	/**
	 * Test {@link Client#getAdresse()}
	 */
	@Test
	public void testGetAddress() {
		assertEquals(addressOfNormal, clientNoCode.getAdresse());
		assertEquals(addressOfDiscount, clientHasCode.getAdresse());
	}

	/**
	 * Test {@link Client#getNbEmpruntsEnCours()}. At first all clients have no
	 * documents. After <code>clientWithOnlyName</code> borrowed, number of
	 * documents of him increases 1
	 */
	@Test
	public void testGetNbEmpruntsEnCours() {
		assertEquals(0, clientHasCode.getNbEmpruntsEnCours());
		assertEquals(0, clientNoCode.getNbEmpruntsEnCours());
		assertEquals(0, clientWithOnlyName.getNbEmpruntsEnCours());

		// after clientNoCode borrows
		clientWithOnlyName.emprunter();
		assertEquals(1, clientWithOnlyName.getNbEmpruntsEnCours());
	}

	/**
	 * Test {@link Client#getNbEmpruntsEffectues()}. At first all clients have
	 * no documents. After <code>clientWithOnlyName</code> borrowed 1 document,
	 * and then he returned it. The number of borrowing still 1.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testGetNbEmpruntsEffectues() throws OperationImpossible {
		assertEquals(0, clientHasCode.getNbEmpruntsEffectues());
		assertEquals(0, clientNoCode.getNbEmpruntsEffectues());
		assertEquals(0, clientWithOnlyName.getNbEmpruntsEffectues());

		// clientNoCode borrows 1 document
		clientWithOnlyName.emprunter();
		// he returned it
		clientWithOnlyName.restituer(false);
		assertEquals(1, clientWithOnlyName.getNbEmpruntsEffectues());
	}

	/**
	 * Test {@link Client#getNbEmpruntsEnRetard()}. At first all clients have no
	 * documents. After <code>clientWithOnlyName</code> borrowed 2 documents,
	 * and there is one document over the deadline.
	 * 
	 * @throws OperationImpossible
	 */
	@Test
	public void testGetNbEmpruntsEnRetard() throws OperationImpossible {
		assertEquals(0, clientHasCode.getNbEmpruntsEnRetard());
		assertEquals(0, clientNoCode.getNbEmpruntsEnRetard());
		assertEquals(0, clientWithOnlyName.getNbEmpruntsEnRetard());

		// clientNoCode borrows 1 document
		clientWithOnlyName.emprunter();
		clientWithOnlyName.emprunter();
		// he returned it
		clientWithOnlyName.marquer();
		assertEquals(1, clientWithOnlyName.getNbEmpruntsEnRetard());
	}

	/**
	 * Test {@link Client#getCoefTarif()} of <code>clientHasCode</code>,
	 * <code>clientNoCode</code>
	 */
	@Test
	public void testGetCoefTarif() {
		assertEquals(categorieClientCompleteNotReduce.getCoefTarif(),
				clientNoCode.getCoefTarif(), 0);
		assertEquals(categorieClientCompleteReduce.getCoefTarif(),
				clientHasCode.getCoefTarif(), 0);
	}
	
	/**
	 * Test {@link Client#getCoefDuree()} of <code>clientHasCode</code>,
	 * <code>clientNoCode</code>
	 */
	@Test
	public void testGetCoefDuree() {
		assertEquals(categorieClientCompleteNotReduce.getCoefDuree(),
				clientNoCode.getCoefDuree(), 0);
		assertEquals(categorieClientCompleteReduce.getCoefDuree(),
				clientHasCode.getCoefDuree(), 0);
	}

	
	/**
	 * Test {@link Client#equals(Object)}. <code>clientHasCode</code> equals to itself. 
	 * <code>clientHasCode</code> does not equal to <code>clientNoCode</code>.
	 */
	@Test
	public void testEqualClients(){
		assertTrue(clientHasCode.equals(clientHasCode));
		assertFalse(clientHasCode.equals(clientNoCode));
	}
	
	/**
	 * Test {@link Client#aDesEmpruntsEnCours()}. 
	 * At the beginning, all three clients borrow no document. Then this function must be false.
	 * After that, let <code>clientNoCode</code> borrows a document, then it must be true
	 */
	@Test
	public void testaDesEmpruntsEnCours(){
		assertFalse(clientHasCode.aDesEmpruntsEnCours());
		assertFalse(clientNoCode.aDesEmpruntsEnCours());
		assertFalse(clientWithOnlyName.aDesEmpruntsEnCours());
		
		//clientNoCode borrows a document
		clientNoCode.emprunter();
		assertTrue(clientNoCode.aDesEmpruntsEnCours());
		
	}
	
	/**
	 * Test {@link Client#peutEmprunter()} with  <code>clientHasCode</code> and <code>clientNoCode</code>
	 * <ul>
	 * <li>At first, both can borrow documents.
	 * <li> Then <code>clientNoCode</code> borrowed and does not return before the deadline.
	 * <li> And <code>clientHasCode</code> already borrowed the number of documents reaches to the max
	 * <ul>
	 * @throws OperationImpossible 
	 */
	@Test
	public void testPeutEmprunter() throws OperationImpossible{
		//At first, both can borrow
		assertTrue(clientHasCode.peutEmprunter());
		assertTrue(clientNoCode.peutEmprunter());
		
		//clientNoCode exceeds the deadline to return the document
		clientNoCode.emprunter();
		clientNoCode.marquer();
		assertFalse(clientNoCode.peutEmprunter());
		
		//clientHasCode exceeds the number of documents can be borrowed
		for( int i = 0; i < categorieClientCompleteReduce.getNbEmpruntMax(); i++){
			clientHasCode.emprunter();
		}
		assertFalse(clientHasCode.peutEmprunter());
	}
	
	/**
	 * Test {@link Client#emprunter()} with <code>clientNoCode</code> and <code>clientHasCode</code>
	 * <ul>
	 * <li><code>clientNoCode</code> does not return before the deadline. Thus, he can not borrow
	 * <li> And <code>clientHasCode</code> haven't borrowed yet. Therefore, he can borrow the specific number of documents.
	 * <ul>
	 */
	@Test
	public void testEmprunter() throws OperationImpossible{
		//clientNoCode exceeds the deadline to return the document				
		clientNoCode.emprunter();				
		clientNoCode.marquer();
		assertFalse(clientNoCode.peutEmprunter());
		
		//clientHasCode borrows
		int nbEmpruntsEffectuesCurrent = clientHasCode.getNbEmpruntsEffectues();
		int nbEmpruntsEnCoursCurrent = clientHasCode.getNbEmpruntsEnCours();
		clientHasCode.emprunter();
		assertEquals(clientHasCode.getNbEmpruntsEffectues(), nbEmpruntsEffectuesCurrent + 1);
		assertEquals(clientHasCode.getNbEmpruntsEnCours(), nbEmpruntsEnCoursCurrent + 1);
	}
	
	/**
	 * Test {@link Client#emprunter(FicheEmprunt)}.
	 * @throws OperationImpossible
	 * @throws InvariantBroken
	 */
	@Test
	public void testemprunterFi() throws OperationImpossible, InvariantBroken{
		Mediatheque	media = new Mediatheque("TestMediatheque");
		Document doc = media.chercherDocument("00001");
		doc.metEmpruntable();
		FicheEmprunt fiche = new FicheEmprunt(media, clientNoCode,doc);
		
		int nbEmpruntsEffectuesCurrent = clientNoCode.getNbEmpruntsEffectues();
		int nbEmpruntsEnCoursCurrent = clientNoCode.getNbEmpruntsEnCours();
		
		clientNoCode.emprunter(fiche);
		
		assertEquals(clientNoCode.getNbEmpruntsEffectues(), nbEmpruntsEffectuesCurrent + 1);
		assertEquals(clientNoCode.getNbEmpruntsEnCours(), nbEmpruntsEnCoursCurrent + 1);
		
	}
	/**
	 * Test {@link Client#marquer()} with <code>clientNoCode</code> 
	 * <code>clientNoCode</code> borrowed 2 documents. And there is one exceeds the deadline.
	 */
	@Test
	public void testMarquer() throws OperationImpossible{
		//clientNoCode exceeds the deadline to return the document				
		clientNoCode.emprunter();	
		clientNoCode.emprunter();
		clientNoCode.marquer();
		assertEquals(1, clientNoCode.getNbEmpruntsEnRetard());
	}
	
	/**
	 * Test {@link Client#restituer(boolean)}.
	 * <ul>
	 * <li><code>clientWithOnlyName</code> have no borrowing document
	 * <li><code>clientHasCode</code> have 1 borrowing document and not exceed the deadline.
	 * <li> code>clientNoCode</code> have 2 borrowing document and one exceed the deadline.
	 * <ul>
	 * @throws OperationImpossible 
	 */
	@Test
	public void testRestituer() throws OperationImpossible{
		//clientWithOnlyName.restituer(false);
		
		clientHasCode.emprunter();
		clientHasCode.restituer(false);
		assertEquals(0, clientHasCode.getNbEmpruntsEnCours());
		
		clientNoCode.emprunter();
		clientNoCode.emprunter();
		clientNoCode.marquer();
		clientNoCode.restituer(true);
		assertEquals(1, clientNoCode.getNbEmpruntsEnCours());
		assertEquals(0, clientNoCode.getNbEmpruntsEnRetard());
	}
	
	/**
	 * Test {@link Client#restituer(FicheEmprunt)}.
	 * @throws OperationImpossible
	 * @throws InvariantBroken
	 */
	@Test
	public void testRestituerFi() throws OperationImpossible, InvariantBroken{
		Mediatheque	media = new Mediatheque("TestMediatheque");
		Document doc = media.chercherDocument("11111");
//		doc.metEmpruntable();
		FicheEmprunt fiche = new FicheEmprunt(media, clientNoCode,doc);
		int nbEmpruntsEnCoursCurrent = clientNoCode.getNbEmpruntsEnCours();
		clientNoCode.restituer(fiche);
		
		assertEquals(clientNoCode.getNbEmpruntsEnCours(), nbEmpruntsEnCoursCurrent - 1);
		
	}
	
	/**
	 * Test {@link Client#dateRetour(Date, int)} with <code>clientWithOnlyName</code>.
	 */
	@Test
	public void testDateRetour(){
		Date borrowDate = Datutil.dateDuJour();
		int duration = (int) clientHasCode.getCoefDuree();
		assertEquals(Datutil.addDate(borrowDate, duration), clientHasCode.dateRetour(borrowDate, duration));
	}
	
	/**
	 * Test {@link Client#sommeDue(double)}
	 */
	@Test
	public void testSommeDue(){
		double tarif = 0.5;
		assertEquals(tarif*(categorieClientCompleteReduce.getCoefTarif()), clientHasCode.sommeDue(tarif),0);
	}
	
	/**
	 * Test {@link Client#nbMaxEmprunt()}.
	 * The maximum number of documents can borrow depends on the category of this client
	 */
	public void testnbMaxEmprunt(){
		assertEquals(categorieClientCompleteNotReduce.getNbEmpruntMax(), clientNoCode.nbMaxEmprunt());
		assertEquals(categorieClientCompleteReduce.getNbEmpruntMax(), clientHasCode.nbMaxEmprunt());
	}
	
	/**
	 * Test {@link Client#getDateCotisation()} with <code>clientHasCode</code> 
	 */
	@Test
	public void testGetDateCotisation(){
		Date date = Datutil.dateDuJour();
		Date retourne = clientHasCode.dateRetour(date, 365);
		assertEquals(retourne, clientHasCode.getDateCotisation());
	}
	
	/**
	 * Test {@link Client#getDateInscription()}.
	 * It is the date of testing (today).
	 */
	@Test
	public void testGetDateInscription(){
		assertEquals(Datutil.dateDuJour(), clientHasCode.getDateInscription());
	}
	
	/**
	 * Test {@link Client#getCategorie()}.
	 * <ul>
	 * <li> clientHasCode category is discount (categorieClientCompleteReduce)
	 * <li> clientNoCode category is normal (categorieClientCompleteNotReduce)
	 * <ul>
	 */
	@Test
	public void testgetCategorie(){
		assertEquals(categorieClientCompleteNotReduce, clientNoCode.getCategorie());
		assertEquals(categorieClientCompleteReduce, clientHasCode.getCategorie());
	}
	
	/**
	 * Test {@link Client#setCategorie(CategorieClient)} uses for clients have not reduce prices.
	 * <code>clientHasCode</code> is categorieClientCompleteReduce (discount). Then
	 * <code>setCategorie</code> for him becomes categorieClientCompleteNotReduce (normal)
	 * @throws OperationImpossible 
	 */
	@Test
	public void testSetCategorieOK1() throws OperationImpossible{
		clientHasCode.setCategorie(categorieClientCompleteNotReduce);
		assertEquals(categorieClientCompleteNotReduce, clientHasCode.getCategorie());
	}
	
	/**
	 * Test {@link Client#setCategorie(CategorieClient)} with a cat has reduce code.
	 * @throws OperationImpossible
	 */
	@Test(expected=OperationImpossible.class)
	public void testSetCategorieKo1() throws OperationImpossible{
		clientNoCode.setCategorie(categorieClientCompleteReduce);
	}
	
	/**
	 * Test {@link Client#setCategorie(CategorieClient, int)} used for clients has reduce code
	 * <code>clientNoCode</code> is categorieClientCompleteNotReduce (discount). Then
	 * <code>setCategorie</code> for him becomes categorieClientCompleteReduce (normal) and provides the code for him.
	 * @throws OperationImpossible 
	 */
	@Test
	public void testSetCategorieOK2() throws OperationImpossible{
		clientNoCode.setCategorie(categorieClientCompleteReduce,codeOfDiscount);
		assertEquals(categorieClientCompleteReduce, clientNoCode.getCategorie());
		assertEquals(codeOfDiscount, clientNoCode.getReduc());
	}
	
	/**
	 * Test {@link Client#setCategorie(CategorieClient)} with a cat has reduce code.
	 * @throws OperationImpossible
	 */
	@Test(expected=OperationImpossible.class)
	public void testSetCategorieKo2() throws OperationImpossible{
		clientHasCode.setCategorie(categorieClientCompleteNotReduce,codeOfDiscount);
	}
	/**
	 * Test {@link Client#setReduc(int)}. 
	 */
	@Test
	public void testSetReduc(){
		int code = 12345;
		clientHasCode.setReduc(code);
		assertEquals(code, clientHasCode.getReduc());
	}
	
	/**
	 * Test {@link Client#setNom(String)}
	 */
	@Test
	public void testSetNom(){
		String nom = "Ngo";
		clientWithOnlyName.setNom(nom);
		assertEquals(nom, clientWithOnlyName.getNom());
	}
	
	/**
	 * Test {@link Client#setPrenom(String)}
	 */
	@Test
	public void testSetPreNom(){
		String prenom = "Quyen";
		clientWithOnlyName.setPrenom(prenom);
		assertEquals(prenom, clientWithOnlyName.getPrenom());
	}
	
	/**
	 * Test {@link Client#setAddresse(String)}
	 */
	@Test
	public void testSetAddress(){
		String addr = "Bach Dang, Vietnam";
		clientWithOnlyName.setAddresse(addr);
		assertEquals(addr, clientWithOnlyName.getAdresse());
	}
	
	/**
	 * Test {@link Client#getReduc()}
	 */
	@Test
	public void testGetReduc(){
		assertEquals(0, clientNoCode.getReduc());
		assertEquals(codeOfDiscount, clientHasCode.getReduc());
	}
	
	@Test
	public void testToString(){
		System.out.println(clientNoCode.toString());
		System.out.println(clientHasCode.toString());
	}
	
	/**
	 * {@link Client#afficherStatistiques()} Display the result on the screen.
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testAfficherStatistiques(){
		clientHasCode.afficherStatistiques();
	}
	
	/**
	 * {@link Client#afficherStatistiques()}, Display the result on the screen.
	 */
	@SuppressWarnings("static-access")
	@Test
	public void testAfficherStatCli(){
		clientHasCode.afficherStatCli();
	}
	}
