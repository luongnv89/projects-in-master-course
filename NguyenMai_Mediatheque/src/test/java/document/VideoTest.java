package document;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import mediatheque.Genre;
import mediatheque.Localisation;
import mediatheque.document.Document;
import mediatheque.document.Video;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VideoTest extends DocumentsAbstractionTest {

	@Before
	public void setUp() throws Exception {
		position = new Localisation("Video", "Languages");
		type = new Genre("Video");
		canBorrow = new Video("001", position, "French fast video",
				"Nguyen", "2000", type, 120, "Audio");
		canBorrow.metEmpruntable();

		cannotBorrow = new Video("002", position,
				"orgirinal French fast video", "Nguyen", "2010", type, 45, "Audio");
	}

	@After
	public void tearDown() throws Exception {
		canBorrow = null;
		cannotBorrow = null;
		position = null;
		type = null;
	}

	/**
	 * Test {@link Video#invariantLivre()}
	 */
	@Test
	public void testLivreInvariant() {
		assertTrue(((Video) canBorrow).invariantVideo());
		assertTrue(((Video) canBorrow).invariantVideo());
	}

	/**
	 * Test {@link Document#getTitre()}
	 */
	@Test
	public void testGetTitle() {
		assertEquals("French fast video", canBorrow.getTitre());
		assertEquals("orgirinal French fast video", cannotBorrow.getTitre());
	}

	/**
	 * Test {@link Document#getCode()}
	 */
	@Test
	public void testgetCode() {
		assertEquals("001", canBorrow.getCode());
		assertEquals("002", cannotBorrow.getCode());
	}

	/**
	 * Test {@link Video#dureeEmprunt()}
	 * For each Video, we can borrow 14 days.
	 */
	@Test
	public void testdureeEmprunt() {
		assertEquals(14, canBorrow.dureeEmprunt());
		assertEquals(14, cannotBorrow.dureeEmprunt());
	}

	/**
	 * Test {@link Video#tarifEmprunt()}
	 */
	@Test
	public void testtarifEmprunt() {
		assertEquals(1.5, canBorrow.tarifEmprunt(), 0);
		assertEquals(1.5, cannotBorrow.tarifEmprunt(), 0);
	}

}
