package document;

import static org.junit.Assert.*;

import mediatheque.Genre;
import mediatheque.Localisation;
import mediatheque.document.Document;
import mediatheque.document.Livre;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LivreTest extends DocumentsAbstractionTest {

	@Before
	public void setUp() throws Exception {
		type = new Genre("Livre");
		position = new Localisation("Livre", "Languages");
		
		canBorrow = new Livre("100", position, "French fast book",
				"Nguyen", "2000", type, 200);
		canBorrow.metEmpruntable();

		cannotBorrow = new Livre("200", position, "French dictionary", "Nguyen",
				"2010", type, 120);
	}

	@After
	public void tearDown() throws Exception {
		canBorrow = null;
		cannotBorrow = null;
		position = null;
		type = null;
	}

	/**
	 * Test {@link Livre#invariantLivre()}
	 */
	@Test
	public void testLivreInvariant() {
		assertTrue(((Livre) canBorrow).invariantLivre());
		assertTrue(((Livre) canBorrow).invariantLivre());
	}

	/**
	 * Test {@link Document#getTitre()}
	 */
	@Test
	public void testGetTitle() {
		assertEquals("French fast book", canBorrow.getTitre());
		assertEquals("French dictionary", cannotBorrow.getTitre());
	}

	/**
	 * Test {@link Document#getCode()}
	 */
	@Test
	public void testgetCode() {
		assertEquals("100", canBorrow.getCode());
		assertEquals("200", cannotBorrow.getCode());
	}

	/**
	 * Test {@link Livre#dureeEmprunt()}
	 * For each Livre, we can borrow 42 days.
	 */
	@Test
	public void testdureeEmprunt() {
		assertEquals(42, canBorrow.dureeEmprunt());
		assertEquals(42, cannotBorrow.dureeEmprunt());
	}

	/**
	 * Test {@link Livre#tarifEmprunt()}
	 */
	@Test
	public void testtarifEmprunt() {
		assertEquals(0.5, canBorrow.tarifEmprunt(), 0);
		assertEquals(0.5, cannotBorrow.tarifEmprunt(), 0);
	}

}
