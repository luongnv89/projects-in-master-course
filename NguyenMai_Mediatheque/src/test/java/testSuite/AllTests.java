package testSuite;


import mediatheque.FicheEmpruntTest;
import mediatheque.GenreTest;
import mediatheque.LettreRappelTest;
import mediatheque.LocalisationTest;
import mediatheque.MediathequeTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import client.CategorieClientTest;
import client.ClientTest;
import client.HastClientTest;

import document.AudioTest;
import document.LivreTest;
import document.VideoTest;

@RunWith(Suite.class)
@SuiteClasses({ AudioTest.class, LivreTest.class,
		VideoTest.class, ClientTest.class,
		CategorieClientTest.class, HastClientTest.class,
		GenreTest.class, LocalisationTest.class,
		MediathequeTest.class, FicheEmpruntTest.class,
		LettreRappelTest.class })
public class AllTests {

}

