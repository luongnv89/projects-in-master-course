/**
 * 
 */
package louis.tests;

import java.util.List;

import louis.objects.Item;
import louis.parser.ItemParser;

import org.junit.Test;

/**
 * @author luongnv89
 *
 */
public class ItemParserTest {

	@Test
	public void test() {
		ItemParser myParser = new ItemParser();
		List<Item> items = myParser.readConfig("list_items.xml");
		for(Item i:items){
			System.out.println(i.toString());
		}
	}

}
